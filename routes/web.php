<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" m
iddleware group. Now create something great!
|
*/


Route::group(['middleware' => ['web']], function () {
    //

Route::get('/',['as'=>'login','uses'=>'LoginController@index']);
Route::get('login',['as'=>'login','uses'=>'LoginController@index']);
Route::post('login',['as'=>'login','uses'=>'LoginController@postLogin']);


Route::post('logout',['as'=>'logout','uses'=>'LoginController@postlogout']);
//dashboard
Route::post('chart',['as'=>'chart','uses'=> 'DashboardController@chart']);
Route::post('piechart',['as'=>'piechart','uses'=> 'DashboardController@piechart']);

//sms
Route::post('smssingle',['as'=>'smssingle','uses'=>'SmsController@singlesms']);
Route::post('sendbulksms',['as'=>'sendbulksms','uses'=>'SmsController@sendbulksms']);
Route::post('smsbulk',['as'=>'smsbulk','uses'=>'SmsController@loadbulksms']);

//////user//////
Route::post('adduser',['as'=>'adduser','uses'=>'UserController@adduser']);
Route::post('edituser',['as'=>'edituser','uses'=>'UserController@edituser']);
Route::post('deleteuser',['as'=>'deleteuser','uses'=>'UserController@deleteuser']);
//////Event///////////////////////
Route::post('addevent',['as'=>'addevent','uses'=>'EventController@save']);
Route::post('editevent',['as'=>'editevent','uses'=>'EventController@update']);
Route::post('deleteevent',['as'=>'deleteevent','uses'=>'EventController@delete']);
///////profile////
Route::post('updateprofile',['as'=>'updateprofile','uses'=>'ProfileController@updateprofile']);
/////member_group///////
Route::post('addmg',['as'=>'addmg','uses'=>'MembergroupController@save']);
Route::post('editmg',['as'=>'editmg','uses'=>'MembergroupController@update']);
Route::post('deletemg',['as'=>'deletemg','uses'=>'MembergroupController@delete']);
////service////
Route::post('addservice',['as'=>'addservice','uses'=>'ServiceController@save']);
Route::post('editservice',['as'=>'editservice','uses'=>'ServiceController@update']);
Route::post('deleteservice',['as'=>'deleteservice','uses'=>'ServiceController@delete']);
    ////service////
    Route::post('addconvenantfamily',['as'=>'addconvenantfamily','uses'=>'Convenant_familyController@save']);
    Route::post('editconvenantfamily',['as'=>'editconvenantfamily','uses'=>'Convenant_familyController@update']);
    Route::post('deleteconvenantfamily',['as'=>'deleteconvenantfamily','uses'=>'Convenant_familyController@delete']);


////Offering Excel///////
    Route::post('excelImportDayBornOffering',['as'=>'excelImportDayBornOffering','uses'=>'OfferingController@excelImportDayBornOffering']);
    Route::get('downloadExceldayborn/{type}',['as'=>'downloadExceldayborn','uses'=> 'OfferingController@downloadExceldayborn']);

    Route::post('excelImportmainOffering',['as'=>'excelImportmainOffering','uses'=>'OfferingController@excelImportmainOffering']);
    Route::get('downloadExcelmain/{type}',['as'=>'downloadExcelmain','uses'=> 'OfferingController@downloadExcelmain']);

    Route::post('excelImportthanksOffering',['as'=>'excelImportthanksOffering','uses'=>'OfferingController@excelImportthanksOffering']);
    Route::get('downloadExcelthanks/{type}',['as'=>'downloadExcelthanks','uses'=> 'OfferingController@downloadExcelthanks']);


    Route::post('excelImportprojectOffering',['as'=>'excelImportprojectOffering','uses'=>'OfferingController@excelImportprojectOffering']);
    Route::get('downloadExcelproject/{type}',['as'=>'downloadExcelproject','uses'=> 'OfferingController@downloadExcelproject']);

    Route::post('excelImporttitheOffering',['as'=>'excelImporttitheOffering','uses'=>'OfferingController@excelImporttitheOffering']);
    Route::get('downloadExceltithe/{type}',['as'=>'downloadExceltithe','uses'=> 'OfferingController@downloadExceltithe']);

    Route::post('excelImportpledgeOffering',['as'=>'excelImportpledgeOffering','uses'=>'OfferingController@excelImportpledgeOffering']);
    Route::get('downloadExcelpledge/{type}',['as'=>'downloadExcelpledge','uses'=> 'OfferingController@downloadExcelpledge']);

    Route::post('excelImportwelfareOffering',['as'=>'excelImportwelfareOffering','uses'=>'OfferingController@excelImportwelfareOffering']);
    Route::get('downloadExcelwelfare/{type}',['as'=>'downloadExcelwelfare','uses'=> 'OfferingController@downloadExcelwelfare']);

    Route::post('excelImportotherOffering',['as'=>'excelImportotherOffering','uses'=>'OfferingController@excelImportotherOffering']);
    Route::get('downloadExcelother/{type}',['as'=>'downloadExcelother','uses'=> 'OfferingController@downloadExcelother']);

    /****************************************************************************
     ****************************************************************************
     ************************Excel Report under Reports tab**********************
     ****************************************************************************
     ****************************************************************************/

    Route::get('financeExcel/{offeringType}/{serviceTyp}/{fromDate}/{toDate}/{title}',['as'=>'financeExcel','uses'=> 'ExcelController@downloadExcelOffering']);
    Route::get('attendanceExcel/{title}/{servicetype}/{date_from}/{date_to}',['as'=>'attendanceExcel','uses'=> 'ExcelController@downloadExcelattendance']);
    Route::get('classExcel/{title}/{level}/{status}/{award}/{date_from}/{date_to}',['as'=>'classExcel','uses'=> 'ExcelController@downloadExcelclass']);
    Route::get('specialExcel/{title}/{date_from}/{date_to}',['as'=>'specialExcel','uses'=> 'ExcelController@downloadExcelspecial']);
    Route::get('memberExcel/{title}/{membergroup}/{gender}/{marital_status}/{member_status}/{locations}/{date_from}/{date_to}',['as'=>'memberExcel','uses'=> 'ExcelController@downloadExcelmember']);
   

    /****************************************************************************
      ****************************************************************************/

////Offering///

Route::post('saveoffering',['as'=>'saveoffering','uses'=>'OfferingController@save']);
Route::post('savedaybornoffering',['as'=>'savedaybornoffering','uses'=>'OfferingController@savedaybornoffering']);

Route::post('editoffering',['as'=>'editoffering','uses'=>'OfferingController@update']);
Route::post('deleteoffering',['as'=>'deleteoffering','uses'=>'OfferingController@delete']);
 Route::post('editdaybornoffering',['as'=>'editdaybornoffering','uses'=>'OfferingController@updatedayborn']);
 Route::post('deletedaybornoffering',['as'=>'deletedaybornoffering','uses'=>'OfferingController@deletedayborn']);
Route::post('offeringchart',['as'=>'offeringchart','uses'=> 'OfferingController@offeringchart']);
//Report//
Route::post('allmembersRpt',['as'=>'allmembersRpt','uses'=> 'PdfController@allmembers']);
    Route::post('quarterlyRpt',['as'=>'quarterlyRpt','uses'=> 'PdfController@quarterly']);

Route::post('weeklyRpt',['as'=>'weeklyRpt','uses'=> 'PdfController@weekly']);
Route::post('financeRpt',['as'=>'financeRpt','uses'=> 'PdfController@finance']);
Route::post('attendanceRpt',['as'=>'attendanceRpt','uses'=> 'PdfController@attendance']);
Route::post('classesRpt',['as'=>'classesRpt','uses'=> 'PdfController@classes']);
Route::post('specialRpt',['as'=>'specialRpt','uses'=> 'PdfController@special']);
//Attendance//
Route::post('saveattendance',['as'=>'saveattendance','uses'=>'AttendanceController@save']);
Route::post('editattendance',['as'=>'editattendance','uses'=>'AttendanceController@update']);
Route::post('deleteattendance',['as'=>'deleteattendance','uses'=>'AttendanceController@delete']);
//Classes
Route::post('addclasses',['as'=>'addclasses','uses'=>'ClassesController@save']);
Route::post('editclasses',['as'=>'editclasses','uses'=>'ClassesController@update']);
Route::post('deleteclasses',['as'=>'deleteclasses','uses'=>'ClassesController@delete']);

//Classes
Route::post('addexpenses',['as'=>'addexpenses','uses'=>'ExpenseController@save']);
Route::post('editexpenses',['as'=>'editexpenses','uses'=>'ExpenseController@update']);
Route::post('deleteexpenses',['as'=>'deleteexpenses','uses'=>'ExpenseController@delete']);

Route::group(['middleware' => ['admin']], function () {
    Route::get('dashboard',['as'=>'dashboard','uses'=>'DashboardController@index']);

    Route::get('newevent',['as'=>'newevent','uses'=>'EventController@index']);
    Route::get('datatable/event',array('as'=>'datatable.event','uses'=>'EventController@get_all_events'));
    
    Route::get('convenantfamily',['as'=>'convenantfamily','uses'=>'Convenant_familyController@index']);
    Route::get('datatable/convenantfamily',array('as'=>'datatable.convenantfamily','uses'=>'Convenant_familyController@get_all_convenantfamily'));
    
    Route::get('newmember',['as'=>'newmember','uses'=>'MemberController@index']);
    Route::get('allmember',['as'=>'allmember','uses'=>'MemberController@memberlist']);
    Route::get('datatable/allmember',array('as'=>'datatable.allmember','uses'=>'MemberController@get_all_members'));

    Route::post('addmember',['as'=>'addmember','uses'=>'MemberController@addmember']);
    Route::post('deletemember',['as'=>'deletemember','uses'=>'MemberController@deletemember']);
    Route::post('editmember',['as'=>'editmember','uses'=>'MemberController@editmember']);

    Route::get('singlesms',['as'=>'singlesms','uses'=>'SmsController@index']);
    Route::get('bulksms',['as'=>'bulksms','uses'=>'SmsController@bulkindex']);

    Route::get('datatable/membercontact',['as'=>'datatable.membercontact','uses'=>'SmsController@membercontact']);

    Route::get('user',['as'=>'user','uses'=>'UserController@index']);
    Route::get('datatable/users',array('as'=>'datatable.users','uses'=>'UserController@get_all_users'));

    Route::get('profile',['as'=>'profile','uses'=>'ProfileController@saveprofile']);

    Route::get('member_group',['as'=>'member_group','uses'=>'MembergroupController@index']);
    Route::get('datatable/memmbergroup',array('as'=>'datatable.memmbergroup','uses'=>'MembergroupController@get_all_group'));

    Route::get('service',['as'=>'service','uses'=>'ServiceController@index']);
    Route::get('datatable/service',array('as'=>'datatable.service','uses'=>'ServiceController@get_all_services'));

    Route::get('classes',['as'=>'classes','uses'=>'ClassesController@index']);
    Route::get('datatable/classes',array('as'=>'datatable.classes','uses'=>'ClassesController@get_all_classes'));

    Route::get('expenses',['as'=>'expenses','uses'=>'ExpenseController@index']);
    Route::get('datatable/expenses',array('as'=>'datatable.expenses','uses'=>'ExpenseController@get_all_expenses'));

    Route::get('offering',['as'=>'offering','uses'=>'OfferingController@index']);
    Route::get('datatable/alloffering',array('as'=>'datatable.alloffering','uses'=>'OfferingController@get_alloffering'));
    Route::get('datatable/alldayborn',array('as'=>'datatable.alldayborn','uses'=>'OfferingController@get_alldayborn'));
    Route::get('alloffering',['as'=>'alloffering','uses'=>'OfferingController@indexall']);
    Route::get('alldaybornoffering',['as'=>'alldaybornoffering','uses'=>'OfferingController@daybornall']);

    Route::get('report',['as'=>'report','uses'=>'ReportController@index']);
    Route::get('quarterlyrpt',['as'=>'quarterlyrpt','uses'=>'ReportController@quarterlyrpt']);
    Route::get('weeklyrpt',['as'=>'weeklyrpt','uses'=>'ReportController@weeklyrpt']);



    Route::get('individualRpt/{memberid}',['as'=>'individualRpt','uses'=> 'PdfController@individual']);
    Route::get('changestatus/{memberid}/{membershipstatus}',['as'=>'changestatus','uses'=> 'MemberController@changestatus']);

    Route::get('newattendance',['as'=>'newattendance','uses'=>'AttendanceController@index']);
    Route::get('datatable/allattendance',array('as'=>'datatable.allattendance','uses'=>'AttendanceController@get_allattendance'));
    Route::get('allattendance',['as'=>'allattendance','uses'=>'AttendanceController@allindex']);

});


    Route::group(['middleware' => ['finance']], function () {
        Route::get('fioffering', ['as' => 'fioffering', 'uses' => 'FiofferingController@index']);
        Route::get('viewallmembers', ['as' => 'viewallmembers', 'uses' => 'FiofferingController@viewallmembers']);
        Route::get('datatable/fialloffering', array('as' => 'datatable.fialloffering', 'uses' => 'FiofferingController@get_alloffering'));
        Route::get('fialloffering', ['as' => 'fialloffering', 'uses' => 'FiofferingController@indexall']);
    });

    Route::group(['middleware' => ['other']], function () {
        Route::get('osinglesms',['as'=>'osinglesms','uses'=>'oSmsController@index']);
        Route::get('obulksms',['as'=>'obulksms','uses'=>'oSmsController@bulkindex']);
        Route::get('oreport',['as'=>'oreport','uses'=>'oReportController@index']);
        Route::get('datatable/omembercontact',['as'=>'datatable.omembercontact','uses'=>'oSmsController@membercontact']);
        Route::get('oviewallmembers', ['as' => 'oviewallmembers', 'uses' => 'oSmsController@viewallmembers']);

    });

});