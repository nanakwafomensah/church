

@extends('layout.localmaster')

<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/personal/datatable.css">

<link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
@section('content')
        <!-- Begin page -->
<div id="wrapper">


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu" style="background-color: #006D5B">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">


                <ul>
                    <li class="has_sub" >
                        <a href="oviewallmembers" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Members</span></a>

                    </li>
                       <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">

                                <li><a href="oreport" style="color: white"> Reports</a></li>

                            </ul>
                        </li>
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="osinglesms" style="color: white">Single</a></li>
                                <li><a href="obulksms" style="color: white">Bulk</a></li>

                            </ul>
                        </li>


                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>


        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">All Contact</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="tables-datatable.html#">ICGC</a>
                                </li>
                                <li>
                                    <a href="tables-datatable.html#">Bulk SMS </a>
                                </li>
                                <li class="active">
                                    All members
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->



                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">



                            <div class="row">
                                <div class="col-md-6">
                                    <div class="demo-box m-t-20">

                                            <form action="" method="post"  id="send_sms" name="single_sms_form">
                                                <div class="p-20">
                                                    <div class="form-group">
                                                        <label for="userName">Number(s)<span class="text-danger">*</span></label>
                                                        <input type="text" name="tel_number" parsley-trigger="change"
                                                               class="form-control" id="tel_number">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Name of receipient<span class="text-danger">*</span></label>
                                                        <input type="text" name="name_of_receipient" parsley-trigger="change"
                                                               class="form-control" id="name_of_receipient">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="pass1">Sender 1D(11 characters)<span class="text-danger">*</span></label>
                                                        <input  type="text" name="sender" id="sender"
                                                                class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="passWord2">Message: <span class="text-danger">*</span></label>
                                        <textarea type="text" id="messagecontent" name="messagecontent" row="5"
                                                  class="form-control" id="passWord2"></textarea>
                                                    </div>
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                    <div class="form-group text-right m-b-0">
                                                        <button style="background-color:#006D5B;color: white;" class="btn  waves-effect waves-light" id="sendsingle"  >
                                                            Send
                                                        </button>

                                                    </div>



                                                </div>
                                            </form>

                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="demo-box m-t-20">
                                        <div class="table-responsive">
                                            <table class="table table table-hover m-0"  id="membercontact">
                                                <thead>
                                                <tr>

                                                    <th>Fullname</th>
                                                    <th>Phone</th>


                                                </tr>
                                                </thead>

                                                <tbody>



                                                </tbody>


                                            </table>

                                        </div> <!-- table-responsive -->
                                    </div>

                                </div>
                            </div>
                            <!--- end row -->





                        </div> <!-- end card-box -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->



            </div> <!-- container -->

        </div>


        <footer class="footer text-right">
            {{date('Y')}} © 1CGC
        </footer>
    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->









    <!-- Right Sidebar -->
    <div class="side-bar right-bar">
        <a href="javascript:void(0);" class="right-bar-toggle">
            <i class="mdi mdi-close-circle-outline"></i>
        </a>
        <h4 class="">Settings</h4>
        <div class="setting-list nicescroll">
            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">Notifications</h5>
                    <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">API Access</h5>
                    <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">Auto Updates</h5>
                    <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">Online Status</h5>
                    <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>
        </div>
    </div>
    <!-- /Right-bar -->
    <!-- /Right-bar -->
</div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
    {{--<script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>--}}

    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Bootstrap JavaScript -->
    {{--<script src="assets/js/bootstrap.min.js"></script>--}}

    <script>
        $('#membercontact').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!!route('datatable.omembercontact')!!}',
            columns: [
                {data: 'fullname', name: 'fullname'},
                {data: 'telephone', name: 'telephone'}

            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column.search(val ? val : '', true, false).draw();
                            });
                });
            }
        });
    </script>

    <script>
        $(document).on('click','#member_click',function(e) {
            e.preventDefault();

            $('#tel_number').val($(this).data('telephone'));
            $('#name_of_receipient').val($(this).data('fullname'));

        });
        $(document).on('click','#telephone_click',function(e) {
            e.preventDefault();
         });
        $(document).on('click','#sendsingle',function(e) {
            e.preventDefault();

            if(validatesinglesms()){

               var token=$('input[name=_token]').val();

                var number = $('#tel_number').val();

                var name_of_receipient = $('#name_of_receipient').val();
                var sender = $('#sender').val();
                var messagecontent = $('#messagecontent').val();
                $.ajax({
                    type:'post' ,
                    url: '{{URL::to('smssingle')}}',
                    data:{
                        '_token':token,
                        'number':number,
                        'name_of_receipient':name_of_receipient,
                        'sender':sender,
                        'messagecontent':messagecontent

                    },
                    success:function(data){
                       // alert(data);
                        //$('.success_message').html(data);
                        toastr.success('Message Sent Out','Success',{timeOut: 2000});
                        location.reload();
                    }
                });
            }
        });
        function validatesinglesms(){

            var validatenumber = document.forms["single_sms_form"]["tel_number"].value;
            var validatereceipient = document.forms["single_sms_form"]["name_of_receipient"].value;
            var sender = document.forms["single_sms_form"]["sender"].value;
            var message = document.forms["single_sms_form"]["messagecontent"].value;

            if (validatenumber == "") {
                $("#tel_number").css({"background-color": "pink"});
                return false;
            }
            else if(validatereceipient== ""){
                $("#name_of_receipient").css({"background-color": "pink"});
                return false;
            }
            else if(sender== ""){
                $("#sender").css({"background-color": "pink"});
                return false;
            }
            else if(message==""){
                $("#messagecontent").css({"background-color": "pink"});
                return false;
            }else{
                $("#number").css({"background-color": "white"});
                $("#name_of_receipient").css({"background-color": "white"});
                $("#sender").css({"background-color": "white"});
                $("#messagecontent").css({"background-color": "white"});
                return true;
            }

        }
    </script>

@endsection

