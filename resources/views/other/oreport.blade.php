

@extends('layout.localmaster')



@section('content')

    <div id="wrapper">
        <!-- ========== Left Sidebar Start ========== -->
        <div class="left side-menu" style="background-color: #006D5B">
            <div class="sidebar-inner slimscrollleft">

                <!--- Sidemenu -->
                <div id="sidebar-menu">


                    <ul>

                        <li class="has_sub" >
                            <a href="oviewallmembers" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Members</span></a>

                        </li>

                          <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">

                                <li><a href="oreport" style="color: white"> Reports</a></li>

                            </ul>
                        </li>
                         <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="osinglesms" style="color: white">Single</a></li>
                                <li><a href="obulksms" style="color: white">Bulk</a></li>

                            </ul>
                        </li>


                    </ul>
                </div>
                <!-- Sidebar -->
                <div class="clearfix"></div>



            </div>
            <!-- Sidebar -left -->

        </div>
        <!-- Left Sidebar End -->
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">


                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Report</h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="form-validation.html#">ICGC</a>
                                    </li>
                                    <li>
                                        <a href="form-validation.html#">Report </a>
                                    </li>
                                    <li class="active">
                                        Report
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->


                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="card-box tilebox-two tilebox-success">
                                <form action="allmembersRpt" method="post" data-parsley-validate="">
                                    <i class="mdi  pull-right text-dark"></i>
                                    <h6 class="text-success text-uppercase m-b-15 m-t-10">All Members</h6>

                                    <div class="form-group">
                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="member_type">

                                            @foreach($membergroups as $s)
                                                <option>{{$s->name}}</option>

                                            @endforeach
                                            <option>all</option>
                                        </select>&nbsp;
                                        <div class="row ">
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label> From</label>
                                                    <input type="date" name="date_from" required/>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label>  To </label>
                                                    <input type="date" name="date_to" required/>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    {{csrf_field()}}
                                    <button type="submit" id="gen_member" style="background-color:#006D5B;color: white;"  class="btn">Generate</button>
                                </form>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="card-box tilebox-two tilebox-primary">
                                <form action="weeklyRpt" method="post" data-parsley-validate="">
                                    <i class="mdi  pull-right text-dark"></i>
                                    <h6 class="text-primary text-uppercase m-b-15 m-t-10">Weekly Report</h6>

                                    <div class="form-group">
                                        <select class="selectpicker" data-style="btn-default" name="member_type" disabled>


                                            <option selected>All</option>


                                        </select>&nbsp;
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label> From</label>
                                                    <input type="date" name="date_from" required/>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label>  To </label>
                                                    <input type="date" name="date_to" required/>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    {{csrf_field()}}
                                    <button type="submit" id="gen_member"  style="background-color:#006D5B;color: white;"  class="btn">Generate</button>
                                </form>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="card-box tilebox-two tilebox-pink">
                                <form action="financeRpt" method="post" data-parsley-validate="">
                                    <i class="mdi  pull-right text-dark"></i>
                                    <h6 class="text-pink text-uppercase m-b-15 m-t-10">Finance</h6>

                                    <div class="form-group">
                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="offering_type">


                                            <option value="mainoffering">main offering</option>
                                            <option value="titheoffering">Tithe offering</option>
                                            <option value="projectoffering">Project offering</option>
                                            <option value="thanksgivingoffering">Thanksgiving offering</option>
                                            <option value="daybornoffering">day-born offering</option>
                                            <option value="pledgeoffering">Pledge offering</option>
                                            <option value="welfareoffering">Welfare offering</option>
                                            <option value="othersoffering">Other offering</option>
                                            <option value="buildingoffering">Building offering</option>
                                            <option value="all">All</option>



                                        </select>&nbsp;
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label> From</label>
                                                    <input type="date" name="date_from" required/>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label>  To </label>
                                                    <input type="date" name="date_to" required />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{csrf_field()}}
                                    <button type="submit" id="gen_member" style="background-color:#006D5B;color: white;"  class="btn ">Generate</button>
                                </form>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="card-box tilebox-two tilebox-info">
                                <form action="attendanceRpt" method="post" data-parsley-validate="">
                                    <i class="mdi  pull-right text-dark"></i>
                                    <h6 class="text-info text-uppercase m-b-15 m-t-10">Attendance</h6>

                                    <div class="form-group">
                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="service_type">
                                            @foreach($servicetype as $s)

                                                <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                            @endforeach

                                        </select>&nbsp;
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label> From</label>
                                                    <input type="date" name="date_from" required />
                                                </div>

                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label>  To </label>
                                                    <input type="date" name="date_to" required />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{csrf_field()}}
                                    <button type="submit" id="gen_member" style="background-color:#006D5B;color: white;"  class="btn ">Generate</button>
                                </form>
                            </div>
                        </div>

                    </div>

                </div> <!-- container -->

            </div> <!-- content -->

            <footer class="footer text-right">
                {{date('Y')}} © 1CGC
            </footer>

        </div>
        <script src="assets/js/jquery.min.js"></script>

        <script>
            function readURL(input){
                if(input.files && input.files[0]){
                    var reader=new FileReader();

                    reader.onload=function(e){
                        $('#preview_image').attr('src',e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $(document).on('change','input[type="file"]',function(){
                readURL(this);

            })
        </script>
        <!-- END wrapper -->
    </div>
    @endsection
            <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->