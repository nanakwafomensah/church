

@extends('layout.localmaster')

<style>

    .form-group input[type=text]:focus {
        border-bottom: 1px solid #006D5B;
        box-shadow: 0 1px 0 0 #006D5B;
    }
    .focus {
        background-color: #006D5B;
        color: #fff;
        cursor: pointer;
        font-weight: bold;
    }

    .pageNumber {
        padding: 2px;
    }

</style>
@section('content')
        <!-- Begin page -->
<!-- Begin page -->
<div id="wrapper" >


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu" style="background-color: #006D5B">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">


                <ul>
                    <li class="has_sub" >
                        <a href="oviewallmembers" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Members</span></a>

                    </li>
                     <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">

                            <li><a href="oreport" style="color: white"> Reports</a></li>

                        </ul>
                    </li>
                      <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="osinglesms" style="color: white">Single</a></li>
                            <li><a href="obulksms" style="color: white">Bulk</a></li>
                            {{--<li><a href="form-validation.html">Form Validation</a></li>--}}
                            {{--<li><a href="form-pickers.html">Form Pickers</a></li>--}}
                            {{--<li><a href="form-wizard.html">Form Wizard</a></li>--}}
                            {{--<li><a href="form-mask.html">Form Masks</a></li>--}}
                            {{--<li><a href="form-summernote.html">Summernote</a></li>--}}
                            {{--<li><a href="form-wysiwig.html">Wysiwig Editors</a></li>--}}
                            {{--<li><a href="form-uploads.html">Multiple File Upload</a></li>--}}
                        </ul>
                    </li>


                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>


        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">All Contact</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="tables-datatable.html#">ICGC</a>
                                </li>
                                <li>
                                    <a href="tables-datatable.html#">Bulk SMS </a>
                                </li>
                                <li class="active">
                                    All members
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->



                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select Group</label>
                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" id="membergroup" name="membergroup" >
                                            @foreach($membergroups as $s)
                                                <option value="{{$s->name}}">{{$s->name}}</option>

                                            @endforeach
                                        </select>

                                    </div>
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <button type="button" style="background-color:#006D5B;color: white;" class="btn " id="loadcontact"><i class="fa fa-download" aria-hidden="true"></i> Load Contacts for SMS</button>

                                </div>


                            </div>
                            <!--- end row -->


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="demo-box m-t-20">
                                        {{--<h4 class="m-t-0 header-title"><b>Info Table</b></h4>--}}
                                        {{--<p class="text-muted font-13 m-b-20">--}}
                                            {{--Use Class <code>.table-colored .table-info</code>--}}
                                        {{--</p>--}}

                                        <div class="table-responsive">
                                            <table class="table"  id="groupcontacttable">
                                                <thead>
                                                <tr>
                                                    <th><input type="checkbox" id="checkAll" name="checkAll" />Select All</th>
                                                    <th>Name</th>
                                                    <th>Phone Number</th>
                                                </tr>
                                                </thead>
                                                <tbody id="bulkcontact"></tbody>


                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="demo-box m-t-20">
                                        <form>

                                            <div class="form-group">
                                                <label for="exampleInputPassword1"><strong>Sender:</strong></label>
                                                <input type="text" id="sender" class="form-control" name="sender" placeholder="Sender" required>
                                            </div>
                                            <div class="form-group">

                                                <label for="exampleInputPassword1"><strong>Message:</strong></label>

                                                <textarea  id="messagecontent" name="messagecontent" class="form-control"placeholder="Message" required></textarea>
                                            </div>
                                        </form></br>

                                        <button style="background-color:#006D5B;color: white;" type="button" class="btn btn-lg" id="send_bulk_sms">Send</button>

                                    </div>

                                </div>
                            </div>
                            <!--- end row -->





                        </div> <!-- end card-box -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->



            </div> <!-- container -->

        </div>


        <footer class="footer text-right">
            {{date('Y')}} © 1CGC
        </footer>
     </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->









    <!-- Right Sidebar -->
    <div class="side-bar right-bar">
        <a href="javascript:void(0);" class="right-bar-toggle">
            <i class="mdi mdi-close-circle-outline"></i>
        </a>
        <h4 class="">Settings</h4>
        <div class="setting-list nicescroll">
            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">Notifications</h5>
                    <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">API Access</h5>
                    <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">Auto Updates</h5>
                    <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">Online Status</h5>
                    <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>
        </div>
    </div>
    <!-- /Right-bar -->


</div>
    <script src="assets/js/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('textarea').focus(function () {
                $(this).css("border-bottom", "1px solid #006D5B");
                $(this).css("box-shadow", "0 1px 0 0 #006D5B");
            });
        });
        $(document).on('click','#loadcontact',function(e) {

            $('table tbody tr').hide();
            $('#pages').hide();

            $.ajax({
                type:'post' ,
                url: '{{URL::to('smsbulk')}}',
                data:{
                    '_token':$('input[name=_token]').val(),
                    'value':$('#membergroup').val()


                },
                success:function(data){
                   // alert(data);
                     $('#bulkcontact').html(data);

                    //perform pagination on the data
                    setTimeout(function(){
                        var totalRows = $('#groupcontacttable tr').length;
                        var recordPerPage = 5;
                        var totalPages = Math.ceil(totalRows / recordPerPage);
                        var $pages = $('<div id="pages"></div>');
                        for (i = 0; i < totalPages; i++) {
                            $('<span class="pageNumber">&nbsp;' + (i + 1) + '</span>').appendTo($pages);
                        }
                        $pages.appendTo('#groupcontacttable');

                        $('.pageNumber').hover(
                                function() {
                                    $(this).addClass('focus');
                                },
                                function() {
                                    $(this).removeClass('focus');
                                }
                        );
                        $('table tbody tr').hide();

                        var tr = $('table tbody tr:has(td)');
                        for (var i = 0; i <= recordPerPage - 1; i++) {
                            $(tr[i]).show();
                        }
                        $('span').click(function(event) {
                            $('#groupcontacttable').find('tbody tr:has(td)').hide();
                            var nBegin = ($(this).text() - 1) * recordPerPage;
                            var nEnd = $(this).text() * recordPerPage - 1;
                            for (var i = nBegin; i <= nEnd; i++) {
                                $(tr[i]).show();
                            }
                        });
                    }, 100);

                    //var recordPerPage = 5;
                    //var totalPages = Math.ceil(totalRows / recordPerPage);

                }
            });

        });



        $('input[type="checkbox"][name="checkAll"]').change(function() {
            if(this.checked) {
                var table = document.getElementById('groupcontacttable');
                var val = table.rows[0].cells[0].children[0].checked;
                for (var i = 1; i < table.rows.length; i++)
                {
                    table.rows[i].cells[0].children[0].checked = val;
                }
            }else{
                var table = document.getElementById('groupcontacttable');
                var val = table.rows[0].cells[0].children[0].unchecked;
                for (var i = 1; i < table.rows.length; i++)
                {
                    table.rows[i].cells[0].children[0].checked = val;
                }
            }
        });

        $(document).on('click','#send_bulk_sms',function(e) {

            $('#sender').parsley().validate();
            $('#messagecontent').parsley().validate();
            if($('#sender').parsley().isValid()&&$('#messagecontent').parsley().isValid()){

                var totalchecked=$('#bulkcontact').find('input[type="checkbox"]:checked').length;
                var count=0;

                $('#bulkcontact').find('input[type="checkbox"]:checked').each(function () {

                    var name=$(this).data('name');
                    var telephone=$(this).data('telephone');
                    var sender=$('#sender').val();
                    var messagecontent=$('#messagecontent').val();
                    $.ajax({
                        type:'post' ,
                        url: '{{URL::to('sendbulksms')}}',
                        data:{
                            '_token':$('input[name=_token]').val(),
                            'name':name,
                            'telephone':telephone,
                            'sender':sender,
                            'messagecontent':messagecontent



                        }, beforeSend: function() {
                            // setting a timeout
                            $('#send_bulk_sms').html("Processing....");
                        },
                        success:function(data){
                            count=count+1;
                            if(count===totalchecked){
                                toastr.success('Message Sent Out','Success',{timeOut: 2000});

                                location.reload();
                            }


                        },complete: function() {
                            $('#send_bulk_sms').html("Send");
                        }
                    });

                });
            }

        });

    </script>

@endsection
