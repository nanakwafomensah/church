
<div class="topbar">


        <div class="topbar-left" style="background-color: #006D5B">

            <a href="dashboard" class="logo"><span>IC<span style="color:lightseagreen">GC</span></span><i class="mdi mdi-cube"></i></a>
            {{--<!-- Image logo -->--}}
            {{--<!--<a href="index.html" class="logo">-->--}}
            {{--<!--<span>-->--}}
            {{--<!--<img src="assets/images/logo.png" alt="" height="30">-->--}}
            {{--<!--</span>-->--}}
            {{--<!--<i>-->--}}
            {{--<!--<img src="assets/images/logo_sm.png" alt="" height="28">-->--}}
            {{--<!--</i>-->--}}
            {{--<!--</a>-->--}}
        </div>


    <div class="navbar navbar-default" role="navigation" style="background-color: #006D5B">
        <div class="container">
            <?php $profiles=App\Profile::all()->first()?>
            <!-- Navbar-left -->
            <ul class="nav navbar-nav navbar-left">
                <li>
                    <button class="button-menu-mobile open-left waves-effect waves-light">
                        <i class="mdi mdi-menu"></i>
                    </button>
                </li>
                {{--<li class="hidden-xs">--}}
                    {{--<form role="search" class="app-search">--}}
                        {{--<input type="text" placeholder="Search..."--}}
                               {{--class="form-control">--}}
                        {{--<a href="chart-morris.html"><i class="fa fa-search"></i></a>--}}
                    {{--</form>--}}
                {{--</li>--}}
                <li class="hidden-xs">
                    <a href="dashboard" class="menu-item waves-effect waves-light">{{$profiles->churchname}}</a>
                </li>
                {{--<li class="dropdown hidden-xs">--}}
                    {{--<a data-toggle="dropdown" class="dropdown-toggle menu-item waves-effect waves-light" href="chart-morris.html#" aria-expanded="false">English--}}
                        {{--<span class="caret"></span></a>--}}
                    {{--<ul role="menu" class="dropdown-menu">--}}
                        {{--<li><a href="chart-morris.html#">German</a></li>--}}
                        {{--<li><a href="chart-morris.html#">French</a></li>--}}
                        {{--<li><a href="chart-morris.html#">Italian</a></li>--}}
                        {{--<li><a href="chart-morris.html#">Spanish</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            </ul>

            <!-- Right(Notification) -->
            <ul class="nav navbar-nav navbar-right">
                {{--<li>--}}
                    {{--<a href="chart-morris.html#" class="right-menu-item dropdown-toggle" data-toggle="dropdown">--}}
                        {{--<i class="mdi mdi-bell"></i>--}}
                        {{--<span class="badge up bg-primary">4</span>--}}
                    {{--</a>--}}

                    {{--<ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right dropdown-lg user-list notify-list">--}}
                        {{--<li>--}}
                            {{--<h5>Notifications</h5>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="chart-morris.html#" class="user-list-item">--}}
                                {{--<div class="icon bg-info">--}}
                                    {{--<i class="mdi mdi-account"></i>--}}
                                {{--</div>--}}
                                {{--<div class="user-desc">--}}
                                    {{--<span class="name">New Signup</span>--}}
                                    {{--<span class="time">5 hours ago</span>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="chart-morris.html#" class="user-list-item">--}}
                                {{--<div class="icon bg-danger">--}}
                                    {{--<i class="mdi mdi-comment"></i>--}}
                                {{--</div>--}}
                                {{--<div class="user-desc">--}}
                                    {{--<span class="name">New Message received</span>--}}
                                    {{--<span class="time">1 day ago</span>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="chart-morris.html#" class="user-list-item">--}}
                                {{--<div class="icon bg-warning">--}}
                                    {{--<i class="mdi mdi-settings"></i>--}}
                                {{--</div>--}}
                                {{--<div class="user-desc">--}}
                                    {{--<span class="name">Settings</span>--}}
                                    {{--<span class="time">1 day ago</span>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="all-msgs text-center">--}}
                            {{--<p class="m-0"><a href="chart-morris.html#">See all Notification</a></p>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}

                {{--<li>--}}
                    {{--<a href="chart-morris.html#" class="right-menu-item dropdown-toggle" data-toggle="dropdown">--}}
                        {{--<i class="mdi mdi-email"></i>--}}
                        {{--<span class="badge up bg-danger">8</span>--}}
                    {{--</a>--}}

                    {{--<ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right dropdown-lg user-list notify-list">--}}
                        {{--<li>--}}
                            {{--<h5>Messages</h5>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="chart-morris.html#" class="user-list-item">--}}
                                {{--<div class="avatar">--}}
                                    {{--<img src="assets/images/users/avatar-2.jpg" alt="">--}}
                                {{--</div>--}}
                                {{--<div class="user-desc">--}}
                                    {{--<span class="name">Patricia Beach</span>--}}
                                    {{--<span class="desc">There are new settings available</span>--}}
                                    {{--<span class="time">2 hours ago</span>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="chart-morris.html#" class="user-list-item">--}}
                                {{--<div class="avatar">--}}
                                    {{--<img src="assets/images/users/avatar-3.jpg" alt="">--}}
                                {{--</div>--}}
                                {{--<div class="user-desc">--}}
                                    {{--<span class="name">Connie Lucas</span>--}}
                                    {{--<span class="desc">There are new settings available</span>--}}
                                    {{--<span class="time">2 hours ago</span>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="chart-morris.html#" class="user-list-item">--}}
                                {{--<div class="avatar">--}}
                                    {{--<img src="assets/images/users/avatar-4.jpg" alt="">--}}
                                {{--</div>--}}
                                {{--<div class="user-desc">--}}
                                    {{--<span class="name">Margaret Becker</span>--}}
                                    {{--<span class="desc">There are new settings available</span>--}}
                                    {{--<span class="time">2 hours ago</span>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="all-msgs text-center">--}}
                            {{--<p class="m-0"><a href="chart-morris.html#">See all Messages</a></p>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}

                {{--<li>--}}
                    {{--<a href="javascript:void(0);" class="right-bar-toggle right-menu-item">--}}
                        {{--<i class="mdi mdi-settings"></i>--}}
                    {{--</a>--}}
                {{--</li>--}}

                <li class="dropdown user-box">
                    <a href="chart-morris.html" class="dropdown-toggle waves-effect waves-light user-link" data-toggle="dropdown" aria-expanded="true">

                        @if(!empty($profiles->avatar))
                            <img src="uploads/avatars/{{$profiles->avatar}}" alt="user-img" class="img-circle user-img">
                        @else
                            <img src="uploads/avatars/default.jpg" alt="user-img" class="img-circle user-img">
                        @endif   </a>

                    <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                        @if(Sentinel::check())
                            <li>
                                <h5> Hello,{{Sentinel::getUser()->first_name}}</h5>
                            </li>
                            {{--<li><a href=""><i class="ti-user m-r-5"></i> Profile</a></li>--}}
                            {{--<li><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> Settings</a></li>--}}
                            <li> <form action="{{route('logout')}}" method="post" id="logout-form">
                                    {{csrf_field()}}
                                    &nbsp;&nbsp;&nbsp;&nbsp; <a href="#" onclick="document.getElementById('logout-form').submit()" style="color: black"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>

                                </form>
                            </li>

                        @endif

                    </ul>
                </li>

            </ul> <!-- end navbar-right -->

        </div><!-- end container -->
    </div><!-- end navbar -->
    </div>




