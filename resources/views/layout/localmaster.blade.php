<!DOCTYPE html>
<html style="zoom: 110%;">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/icgcicon.ico">
    <!-- App title -->
    <title>ICGC</title>

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="assets/plugins/morris/morris.css">

    <!-- App css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="assets/plugins/switchery/switchery.min.css">
    <link rel="stylesheet" href="fontawesome/css/font-awesome.min.css">
    <link href="MaterialDesign/MaterialDesign/css/materialdesignicons.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="parsley/css/parsley.css" media="all" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
    <link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="assets/css/toastr.min.css" rel="stylesheet" />
    <style>

        .form-group input[type=text]:focus {
            border-bottom: 1px solid #006D5B;
            box-shadow: 0 1px 0 0 #006D5B;
        }
    </style>

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>-->
    <!--<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>-->
    <![endif]-->

    <script src="assets/js/modernizr.min.js"></script>
    {{--<script>--}}
        {{--(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){--}}
                    {{--(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),--}}
                {{--m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)--}}
        {{--})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');--}}

        {{--ga('create', 'UA-83057131-1', 'auto');--}}
        {{--ga('send', 'pageview');--}}

    {{--</script>--}}

</head>
<body class="fixed-left">

<!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner ">
            <div class="spinner-wrapper ">
                <div class="rotator"  >
                    <div class="inner-spin " ></div>
                    <div class="inner-spin "></div>

                </div>
            </div>
        </div>
    </div>
</div>
@include('layout.topbar')
@yield('content')



<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/plugins/switchery/switchery.min.js"></script>

<script src="assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

{{--<script type="text/javascript" src="assets/plugins/autocomplete/jquery.mockjax.js"></script>--}}
{{--<script type="text/javascript" src="assets/plugins/autocomplete/jquery.autocomplete.min.js"></script>--}}
{{--<script type="text/javascript" src="assets/plugins/autocomplete/countries.js"></script>--}}
{{--<script type="text/javascript" src="assets/pages/jquery.autocomplete.init.js"></script>--}}

<script type="text/javascript" src="assets/pages/jquery.form-advanced.init.js"></script>

<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- Counter js  -->
<script src="assets/plugins/waypoints/jquery.waypoints.min.js"></script>
<script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

<!--Morris Chart-->
<script src="assets/plugins/morris/morris.min.js"></script>
<script src="assets/plugins/raphael/raphael-min.js"></script>

<!-- Dashboard init -->
<script src="assets/pages/jquery.dashboard.js"></script>

<!-- App js -->
<script src="assets/js/jquery.core.js"></script>
<script src="assets/js/jquery.app.js"></script>
<script src="parsley/js/parsley.min.js"></script>
<script src="assets/js/toastr.min.js"></script>
<script>
    function readURL(input){
        if(input.files && input.files[0]){
            var reader=new FileReader();

            reader.onload=function(e){
                $('#preview_image').attr('src',e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).on('change','input[type="file"]',function(){
        readURL(this);

    })
</script>
<script>
            @if(Session::has('message'))
    var type="{{Session::get('alert-type','info')}}"
    switch(type){
        case 'success':
            toastr.success('{{Session::get('message')}}','Success',{timeOut: 2000});

            break;
        case 'error':
            toastr.error('{{Session::get('message')}}','Success',{timeOut: 2000});

            break;
        case 'info':
            toastr.info('{{Session::get('message')}}','Success',{timeOut: 2000});

            break;
    }
    @endif

</script>
</body>
</html>