

@extends('layout.localmaster')

<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/personal/datatable.css">

@section('content')
        <!-- Begin page -->
<div id="wrapper">


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu" style="background-color: #006D5B">
        <div class="sidebar-inner slimscrollleft">
            <!--- Sidemenu -->
            <div id="sidebar-menu">

                <ul>


                        <li class="has_sub" >
                            <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>

                        </li>



                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newmember" style="color: white">New Member</a></li>
                                <li><a href="allmember"style="color: white">All Members</a></li>
                                {{--<li><a href="admin-nestable.html">Nestable List</a></li>--}}
                                {{--<li><a href="admin-rangeslider.html">Range Slider</a></li>--}}
                                {{--<li><a href="admin-ratings.html">Ratings</a></li>--}}
                                {{--<li><a href="admin-animation.html">Animation</a></li>--}}
                            </ul>
                        </li>
                    <li class="has_sub">
                        <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>
                        {{--<ul class="list-unstyled">--}}
                        {{--<li><a href="newmember" style="color: white">New Member</a></li>--}}
                        {{--<li><a href="allmember"style="color: white">All Members</a></li>--}}
                        {{--<li><a href="admin-nestable.html">Nestable List</a></li>--}}
                        {{--<li><a href="admin-rangeslider.html">Range Slider</a></li>--}}
                        {{--<li><a href="admin-ratings.html">Ratings</a></li>--}}
                        {{--<li><a href="admin-animation.html">Animation</a></li>--}}
                        {{--</ul>--}}
                    </li>
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-calendar"style="color: white" aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newevent" style="color: white">New Event</a></li>

                            </ul>
                        </li>
                    @endif
                       <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="offering" style="color: white">New</a></li>
                                <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                                <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                                <li><a href="expenses" style="color: white"> Expenses</a></li>
                            </ul>
                        </li>


                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">

                                <li><a href="report" style="color: white"> Reports</a></li>
                                <li><a href="quarterlyrpt" style="color: white">Quaterly Reports</a></li>
                                <li><a href="weeklyrpt" style="color: white">Weeky Reports</a></li>

                            </ul>
                        </li>




                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newattendance" style="color: white">New Attendance</a></li>
                                <li><a href="allattendance" style="color: white">All Attendance</a></li>

                            </ul>
                        </li>



                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="singlesms" style="color: white">Single</a></li>
                                <li><a href="bulksms" style="color: white">Bulk</a></li>

                            </ul>
                        </li>

                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="profile" style="color: white">Church Profile</a></li>
                                <li><a href="user" style="color: white">Users</a></li>
                                <li><a href="service " style="color: white">Service</a></li>
                                <li><a href="member_group" style="color: white">Member Group</a></li>
                                <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>


                            </ul>
                        </li>


                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>


        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">All Users</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="dashboard">ICGC</a>
                                </li>
                                <li>
                                    <a href="user">User</a>
                                </li>
                                <li class="active">
                                   Users
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="row">
                    <div class="col-sm-4">
                        <!-- Responsive modal -->
                        <a style="background-color:#006D5B;color: white;" class="btn  waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal">New User</a>
                    </div>
                </div>
                </br>
                <!-- end row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">

                            <h4 class="m-t-0 header-title"><b>All Record</b></h4>


                            <table id="service-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>location</th>
                                    <th>Role</th>

                                    <th>Action</th>

                                </tr>
                                </thead>

                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <form action="{{route('adduser')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Add New User</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Firstname</label>
                                            <input type="text" class="form-control" id="field-1" placeholder="John" name="firstname">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">lastname</label>
                                            <input type="text" class="form-control" id="field-1" placeholder="Mensah" name="lastname">
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Username</label>
                                            <input type="text" class="form-control" id="field-1" placeholder="laadi" name="username">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Email</label>
                                            <input type="text" class="form-control" id="field-1" placeholder="me12@yahoo.com" name="email">
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Location</label>
                                            <input type="text" class="form-control" id="field-1" placeholder="Accra" name="location">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Role</label>
                                            <select name="role" class="form-control">
                                                <option value="Admin">Admin</option>
                                                <option value="Finance">Finance</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Password</label>
                                            <input type="password" class="form-control" id="field-1" placeholder="********" name="password" required>
                                        </div>
                                    </div>


                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button style="background-color:#006D5B;color: white;"type="submit" class="btn  waves-effect waves-light">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal -->
            <div id="edit-user" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <form action="{{route('edituser')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Edit</h4>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" id="id_edit" name="id_edit" />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Firstname</label>
                                            <input type="text" class="form-control"  id="firstname_edit" name="firstname_edit">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">lastname</label>
                                            <input type="text" class="form-control"  id="lastname_edit" name="lastname_edit">
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Username</label>
                                            <input type="text" class="form-control" id="username_edit"  name="username_edit">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Email</label>
                                            <input type="text" class="form-control" id="email_edit"  name="email_edit">
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Location</label>
                                            <input type="text" class="form-control" id="location_edit"  name="location_edit">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Role</label>
                                            <select name="role_edit" class="form-control" id="role_edit" >
                                                <option value="Admin">Admin</option>
                                                <option value="Finance">Finance</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Password</label>
                                        <input type="password" class="form-control"  id="password_edit"  name="password_edit" required>
                                    </div>
                                </div>


                                   </div>



                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button style="background-color:#006D5B;color: white;" type="submit" class="btn  waves-effect waves-light">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal -->
            <div id="delete-user" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <form action="{{route('deleteuser')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Delete</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Are you sure you want to delete <span id="name_delete"></span>?</label>
                                            <input type="hidden" class="form-control" id="id_delete" placeholder="John" name="id_delete" >
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button style="background-color:#006D5B;color: white;" type="submit" class="btn  waves-effect waves-light">Delete</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal -->
            <div id="forgotpasswordModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->

                    <div class="modal-content">


                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Forgot Password</h4>
                        </div>
                        <div class="modal-body">

                            <input type="hidden" id="useridedit"  name="userid" class="form-control" >


                            <div class="form-group">
                                <label for="exampleInputPassword1">Email:</label>
                                <input type="email" id="useremail" name="email"class="form-control"  >
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                            <a type="submit" id="add-row" class="sendcode btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Send Code</a>
                        </div>




                    </div>
                </div>
            </div>






        </div>



    </div>
</div>

<footer class="footer text-right">
    {{date('Y')}} © 1CGC
</footer>

</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- Bootstrap JavaScript -->
{{--<script src="assets/js/bootstrap.min.js"></script>--}}
<script>
    $('#service-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!!route('datatable.users')!!}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'first_name', name: 'first_name'},
            {data: 'last_name', name: 'last_name'},
            {data: 'email', name: 'email'},
            {data: 'location', name: 'location'},
            {data: 'priviledge', name: 'priviledge'},


            {data: 'action', name: 'action'}
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());

                            column.search(val ? val : '', true, false).draw();
                        });
            });
        }
    });
</script>
<script>
    $(document).on('click','.edit-modal',function() {
      // alert($(this).data('id'));
        $('#id_edit').val($(this).data('id'));
        $('#firstname_edit').val($(this).data('firstname'));
        $('#lastname_edit').val($(this).data('lastname'));
        $('#username_edit').val($(this).data('username'));
        $('#location_edit').val($(this).data('location'));
        $('#email_edit').val($(this).data('email'));
        $('#role_edit').val($(this).data('priviledge')).change();



    });
</script>
<script>
    $(document).on('click','.deletebtn',function() {
//        alert("Hey");

        $('#id_delete').val($(this).data('id'));


        $("#name_delete").html($(this).data('firstname')+""+$(this).data('lastname'));

    });
</script>
@endsection

