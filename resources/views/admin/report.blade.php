

@extends('layout.localmaster')



@section('content')

<div id="wrapper">
        <!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu" style="background-color: #006D5B">
    <div class="sidebar-inner slimscrollleft">

        <!--- Sidemenu -->
        <div id="sidebar-menu">


            <ul>


                    <li class="has_sub" >
                        <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>

                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="newmember" style="color: white">New Member</a></li>
                            <li><a href="allmember"style="color: white">All Members</a></li>

                        </ul>
                    </li>
                <li class="has_sub">
                    <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>

                </li>
                @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-calendar" style="color: white" aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="newevent" style="color: white">New Event</a></li>

                        </ul>
                    </li>
                @endif
                      <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="offering" style="color: white">New</a></li>
                            <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                            <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                            <li><a href="expenses" style="color: white"> Expenses</a></li>

                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">

                            <li><a href="report" style="color: white"> Reports</a></li>
                            <li><a href="quarterlyrpt" style="color: white">Quaterly Reports</a></li>
                            <li><a href="weeklyrpt" style="color: white">Weekly Reports</a></li>

                        </ul>
                    </li>


                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="newattendance" style="color: white">New Attendance</a></li>
                            <li><a href="allattendance" style="color: white">All Attendance</a></li>

                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="singlesms" style="color: white">Single</a></li>
                            <li><a href="bulksms" style="color: white">Bulk</a></li>

                        </ul>
                    </li>
                  <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="profile" style="color: white">Church Profile</a></li>
                            <li><a href="user" style="color: white">Users</a></li>
                            <li><a href="service " style="color: white">Service</a></li>
                            <li><a href="member_group" style="color: white">Member Group</a></li>
                            <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>


                        </ul>
                    </li>


            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>



    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
        <!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">


            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Report</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="dashboard">ICGC</a>
                            </li>
                            <li>
                                <a href="report">Report </a>
                            </li>
                            <li class="active">
                                Report
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="card-box tilebox-two tilebox-success">
                        <form action="allmembersRpt" method="post" data-parsley-validate="" id="memberform">
                        <i class="mdi  pull-right text-dark"></i>
                        <h6 class="text-success text-uppercase m-b-15 m-t-10">MEMBER</h6>
                            <div class="row">
                                <div class="col-md-12">

                                       <strong>REPORT TITLE:</strong> <input class="form-control" name="member_sec_title" id="member_sec_title"  required/>

                                </div>
                            </div>
                            <br/>
                           <div class="row">
                               <div class="col-md-3">
                                   <div class="form-group ">
                                       <label>Member Groups</label>
                                       <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="member_type" id="member_type">
                                           @foreach($membergroups as $s)
                                               <option>{{$s->name}}</option>

                                           @endforeach

                                       </select>&nbsp;
                                   </div>
                               </div>
                               <div class="col-md-2">
                                   <div class="form-group ">
                                       <label>Gender</label>
                                       <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="gender_type" id="gender_type">

                                               <option value="male">Male</option>
                                               <option value="female">Female</option>



                                       </select>&nbsp;
                                   </div>
                               </div>
                               <div class="col-md-2">
                                   <div class="form-group ">
                                       <label>Marital status</label>
                                       <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="marital_status_type" id="marital_status_type">

                                           <option value="Single">Single</option>
                                           <option value="Married">Married</option>
                                           <option value="Divorced">Divorced</option>
                                           <option value="Widows">Widows</option>



                                       </select>&nbsp;
                                   </div>
                               </div>
                               <div class="col-md-2">
                                   <div class="form-group ">
                                       <label>Mem Status</label>
                                       <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="membership_status_type" id="membership_status_type">
                                           <option value="member">member</option>
                                           <option value="visitor">visitor</option>
                                           <option value="regular_visitor">Regular visitor</option>


                                       </select>&nbsp;
                                   </div>
                               </div>
                               <div class="col-md-2">
                                   <div class="form-group ">
                                       <label>Location</label>
                                       <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="location_type" id="location_type">
                                          @foreach($memberlocations as $x)
                                               <option value="{{$x->hometown}}">{{$x->hometown}}</option>
                                              @endforeach




                                       </select>&nbsp;
                                   </div>
                               </div>
                           </div>
                           <div class="col-sm-6">
                                        <div class=" form-group">
                                            <label> From</label>
                                            <input type="date" name="date_from" class="form-control"  id="date_from_m" required/>
                                        </div>

                                    </div>
                            <div class="col-sm-6">
                            <div class=" form-group">
                                <label>  To </label>
                                <input type="date" name="date_to" class="form-control" id="date_to_m" required/>
                            </div>


                                </div>

                            {{csrf_field()}}
                            <button type="submit" id="gen_member" style="background-color:#006D5B;color: white;"  class="btn">Generate</button>
                            <a href="#" id="member_excel" class="btn " style="background-color:#006D5B;color: white;">Download Excel</a>

                        </form>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="card-box tilebox-two tilebox-pink">
                        <form action="financeRpt" method="post" data-parsley-validate="" id="financeform">
                        <i class="mdi  pull-right text-dark"></i>
                        <h6 class="text-pink text-uppercase m-b-15 m-t-10">Finance</h6>
                            <div class="row">
                                <div class="col-md-12">

                                    <strong>REPORT TITLE:</strong> <input class="form-control" name="member_sec_title" id="finance_heading" required/>

                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Offering Type</label>
                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="offering_type" id="financeType">


                                            <option value="mainoffering">main offering</option>
                                            <option value="titheoffering">Tithe offering</option>
                                            <option value="projectoffering">Project offering</option>
                                            <option value="thanksgivingoffering">Thanksgiving offering</option>
                                            <option value="daybornoffering">day-born offering</option>
                                            <option value="pledgeoffering">Pledge offering</option>
                                            <option value="welfareoffering">Welfare offering</option>
                                            <option value="othersoffering">Other offering</option>
                                            <option value="buildingoffering">Building offering</option>
                                            <option value="all">All</option>



                                        </select>&nbsp;
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Service Type</label>
                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="service_type" id="serviceType">

                                            @foreach($servicetype as $s)

                                                <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                            @endforeach



                                        </select>&nbsp;
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class=" form-group">
                                        <label> From</label>
                                        <input type="date" name="date_from" id="date_from_finance" class="form-control" required/>
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class=" form-group">
                                        <label>  To </label>
                                        <input type="date" name="date_to" id="date_to_finance" class="form-control" required />
                                    </div>
                                </div>

                            </div>


                        {{csrf_field()}}
                         <button type="submit" id="gen_member" style="background-color:#006D5B;color: white;"  class="btn ">Download PDF</button>
                         {{--<a href="financeExcel/offeringType/serviceType/fromDate/toDate/title" class="btn " style="background-color:#006D5B;color: white;">Download Excel</a>--}}
                         <a href="#" id="finance_excel" class="btn " style="background-color:#006D5B;color: white;">Download Excel</a>

                        </form>
                    </div>
                </div>
            </div>

            <br>

           <div class="row">
               <div class="col-lg-6 col-md-6">
                   <div class="card-box tilebox-two tilebox-info">
                       <form action="attendanceRpt" method="post" data-parsley-validate="" id="attendanceform">
                           <i class="mdi  pull-right text-dark"></i>
                           <h6 class="text-info text-uppercase m-b-15 m-t-10">Attendance</h6>
                           <div class="row">
                               <div class="col-md-12">

                                   <strong>REPORT TITLE:</strong> <input class="form-control" name="member_sec_title" id="member_sec_title_attendance" required/>

                               </div>
                           </div>
                           <br/>
                           <div class="row">
                               <div class="col-md-6">
                           <div class="form-group">
                               <label>Service Type</label>
                               <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="service_type" id="service_type_attendance">
                                   @foreach($servicetype as $s)

                                       <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                   @endforeach

                               </select>
                           </div>
                                   </div>
                           </div>

                           &nbsp;

                               <div class="row">
                                   <div class="col-sm-6">
                                       <div class=" form-group">
                                           <label> From</label>
                                           <input type="date" name="date_from" class="form-control" id="date_from_attendance" required />
                                       </div>

                                   </div>
                                   <div class="col-sm-6">
                                       <div class=" form-group">
                                           <label>  To </label>
                                           <input type="date" name="date_to" class="form-control" id="date_to_attendance" required />
                                       </div>
                                   </div>

                               </div>



                           {{csrf_field()}}
                           <button type="submit" id="gen_member" style="background-color:#006D5B;color: white;"  class="btn ">Generate</button>
                           <a id="attendance_excel" class="btn" style="background-color:#006D5B;color: white;" > Download Excel</a>

                       </form>
                   </div>
               </div>
               <div class="col-lg-6 col-md-6">
                   <div class="card-box tilebox-two tilebox-pink">
                       <form action="classesRpt" method="post" data-parsley-validate="" id="classform">
                           <i class="mdi  pull-right text-dark"></i>
                           <h6 class="text-pink text-uppercase m-b-15 m-t-10">Classes</h6>
                           <div class="row">
                               <div class="col-md-12">

                                   <strong>REPORT TITLE:</strong> <input class="form-control" name="member_sec_title" id="member_sec_title_class" required/>

                               </div>
                           </div>
                           <br/>
                           <div class="row">
                               <div class="col-md-4">
                                   <div class="form-group">
                                       <label>Level</label>
                                       <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="level_type" id="level_class">

                                           <option value="ABC">ABC</option>
                                           <option value="Membership">Membership</option>
                                           <option value="Maturity">Maturity</option>
                                           <option value="Ministry">Ministry</option>
                                           <option value="all">All</option>
                                        </select>
                                   </div>&nbsp;
                               </div>
                               <div class="col-md-4">
                                   <div class="form-group">
                                       <label>Status</label>
                                       <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="completion_status" id="status_class">

                                           <option value="Incomplete">Incomplete</option>
                                           <option value="complete">complete</option>
                                       </select>
                                   </div>&nbsp;
                               </div>
                               <div class="col-md-4">
                                   <div class="form-group">
                                       <label>Award</label>
                                       <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="graduation_status" id="award_class">

                                           <option value="Graduated">Graduated</option>
                                           <option value="Not Graduated">Not Graduated</option>
                                       </select>
                                   </div>&nbsp;
                               </div>
                           </div>

                               <div class="row">
                                   <div class="col-sm-6">
                                       <div class=" form-group">
                                           <label> From</label>
                                           <input type="date" name="date_from" class="form-control" id="date_from_class" required/>
                                       </div>

                                   </div>
                                   <div class="col-sm-6">
                                       <div class=" form-group">
                                           <label>  To </label>
                                           <input type="date" name="date_to" class="form-control" id="date_to_class" required />
                                       </div>
                                   </div>
                               </div>



                           {{csrf_field()}}
                           <button type="submit" id="gen_member" style="background-color:#006D5B;color: white;"  class="btn ">Generate </button>
                           <a id="class_excel" class="btn" style="background-color:#006D5B;color: white;" > Download Excel</a>

                       </form>
                   </div>
               </div>
           </div>
            <br>
            <div>
                <div class="col-lg-12 col-md-12">
                    <div class="card-box tilebox-two tilebox-info">
                        <form action="specialRpt" method="post" data-parsley-validate="" id="specialform">
                            <i class="mdi  pull-right text-dark"></i>
                            <h6 class="text-info text-uppercase m-b-15 m-t-10">Special Report</h6>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select Report Type</label>
                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="specialreporttype" id="specialreporttype">


                                                <option value="1">Employed and Unemployed Members</option>


                                        </select>
                                    </div>
                                </div>

                            </div>

                            &nbsp;

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class=" form-group">
                                        <label> Date of Registration From</label>
                                        <input type="date" name="date_from" id="date_from_spe" class="form-control" required />
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class=" form-group">
                                        <label> Date of Registration To </label>
                                        <input type="date" name="date_to" id="date_to_spe" class="form-control" required />
                                    </div>
                                </div>

                            </div>



                            {{csrf_field()}}
                            <button type="submit" id="gen_member" style="background-color:#006D5B;color: white;"  class="btn ">Generate</button>
                            <a id="special_rpt_excel" class="btn" style="background-color:#006D5B;color: white;" > Download Excel</a>

                        </form>
                    </div>
                </div>
            </div>

            {{--</div>--}}

        </div> <!-- container -->

    </div> <!-- content -->

    <footer class="footer text-right">
        {{date('Y')}} © 1CGC
    </footer>

</div></div>
<script src="assets/js/jquery.min.js"></script>

<script>
    /*************Finance section**************************/
    $(document).on('click','#finance_excel',function(e) {
        e.preventDefault();
        $('#financeform').parsley().validate();

        if($('#financeform').parsley().isValid()){
            var offeringType=$("#financeType").val();
            var serviceTyp=$("#serviceType").val();
            var title=$("#finance_heading").val();
            var fromDate = $("#date_from_finance").val();
            var toDate = $("#date_to_finance").val();
            location.href = "{{url('financeExcel/')}}" + '/'+ offeringType + '/'+ serviceTyp + '/'+ fromDate + '/'+ toDate+ '/'+ title;

        }


    });
     /*************Member section**************************/
    $(document).on('click','#member_excel',function(e) {

        e.preventDefault();
        $('#memberform').parsley().validate();

        if($('#memberform').parsley().isValid()){
            var title=$("#member_sec_title").val();
            var membergroup=$("#member_type").val();
            var gender=$("#gender_type").val();
            var marital_status = $("#marital_status_type").val();
            var member_status = $("#membership_status_type").val();
            var locations = $("#location_type").val();
            var date_from = $("#date_from_m").val();
            var date_to = $("#date_to_m").val();
            location.href = "{{url('memberExcel/')}}" + '/'+ title + '/'+ membergroup + '/'+ gender + '/'+ marital_status + '/'+ member_status + '/'+ locations + '/'+ date_from + '/'+ date_to;

        }


    });
    /*************Attendance section**************************/
    $(document).on('click','#attendance_excel',function(e) {
        e.preventDefault();
        $('#attendanceform').parsley().validate();

        if($('#attendanceform').parsley().isValid()){

            var title=$("#member_sec_title_attendance").val();
            var servicetype=$("#service_type_attendance").val();
            var date_from = $("#date_from_attendance").val();
            var date_to = $("#date_to_attendance").val();
            location.href = "{{url('attendanceExcel/')}}" + '/'+ title + '/'+ servicetype + '/'+ date_from + '/'+ date_to;

        }

    });
    /*************Classes section**************************/
    $(document).on('click','#class_excel',function(e) {
        e.preventDefault();

        $('#classform').parsley().validate();

        if($('#classform').parsley().isValid()){
            var title=$("#member_sec_title_class").val();
            var level=$("#level_class").val();
            var status=$("#status_class").val();
            var award = $("#award_class").val();
            var date_from = $("#date_from_class").val();
            var date_to = $("#date_to_class").val();
            //alert("{{url('classExcel/')}}" + '/'+ title + '/'+ level + '/'+ status +'/'+ award + '/'+ date_from + '/'+ date_to);
            location.href = "{{url('classExcel/')}}" + '/'+ title + '/'+ level + '/'+ status +'/'+ award + '/'+ date_from + '/'+ date_to;

        }

    });
    /*************special_report section**************************/
    $(document).on('click','#special_rpt_excel',function(e) {

        e.preventDefault();

        $('#specialform').parsley().validate();

        if($('#specialform').parsley().isValid()){
            var title=$("#specialreporttype").val();
            var date_from = $("#date_from_spe").val();
            var date_to = $("#date_to_spe").val();

            location.href = "{{url('specialExcel/')}}" + '/'+ title + '/'+ date_from + '/'+ date_to;

        }

    });
</script>
@endsection
        <!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->