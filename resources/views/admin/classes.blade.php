

@extends('layout.localmaster')
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/personal/datatable.css">
@section('content')
        <!-- Begin page -->
<div id="wrapper">


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu" style="background-color: #006D5B">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">


                <ul>

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub" >
                            <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>

                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newmember" style="color: white">New Member</a></li>
                                <li><a href="allmember"style="color: white">All Members</a></li>

                            </ul>
                        </li>
                    @endif
                    <li class="has_sub">
                        <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>

                    </li>
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-calendar"style="color: white" aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newevent" style="color: white">New Event</a></li>

                            </ul>
                        </li>
                    @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin'||'Finance')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="offering" style="color: white">New</a></li>
                                <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                                <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                                <li><a href="expenses" style="color: white"> Expenses</a></li>

                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">

                                <li><a href="report" style="color: white"> Reports</a></li>
                                <li><a href="quarterlyrpt" style="color: white">Quaterly Reports</a></li>
                                <li><a href="weeklyrpt" style="color: white">Weeky Reports</a></li>

                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newattendance" style="color: white">New Attendance</a></li>
                                <li><a href="allattendance" style="color: white">All Attendance</a></li>

                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="singlesms" style="color: white">Single</a></li>
                                <li><a href="bulksms" style="color: white">Bulk</a></li>

                            </ul>
                        </li>
                    @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="profile" style="color: white">Church Profile</a></li>
                                <li><a href="user" style="color: white">Users</a></li>
                                <li><a href="service " style="color: white">Service</a></li>
                                <li><a href="member_group" style="color: white">Member Group</a></li>
                                <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>


                            </ul>
                        </li>
                    @endif


                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>



        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Classes</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="dashboard">ICGC</a>
                                </li>
                                <li>
                                    <a href="classes">Classes</a>
                                </li>
                                <li class="active">
                                   Classes
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="row">
                    <div class="col-sm-4">
                        <!-- Responsive modal -->
                        <a  class="btn  waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal"style="background-color:#006D5B;color: white;">New Registration</a>
                    </div>
                </div>
                </br>
                <!-- end row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">

                            <h4 class="m-t-0 header-title"><b>All Record</b></h4>


                            <table id="classes-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Registration Date</th>
                                    <th>level</th>

                                    <th>Action</th>
                                    <th>Status</th>


                                </tr>
                                </thead>

                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <form action="{{route('addclasses')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" >Add New Class Registration</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Name</label>
                                            <select  class="form-control" id="id" name="id" required>
                                                <option value="">--</option>
                                                @foreach($allmembers as $s)

                                                    <option value="{{$s->member_id}}">{{$s->firstname." ".$s->lastname}}</option>


                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Stage</label>
                                            <select  class="form-control" id="stage" name="stage" required>
                                                <option value="">------</option>
                                                <option value="ABC">ABC</option>
                                                <option value="Membership">Membership</option>
                                                <option value="Maturity">Maturity</option>
                                                <option value="Ministry">Ministry</option>


                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Registration date</label>
                                            <input type="date" class="form-control" id="reg_date" name="reg_date" required>




                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button style="background-color:#006D5B;color: white;" type="submit" class="btn  waves-effect waves-light">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal -->
            <div id="edit-Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <form action="{{route('editclasses')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Edit</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <input type="hidden" name="id_edit" id="id_edit"/>
                                    <div class="col-md-12">
                                        <label for="field-1" class="control-label">Name</label>
                                        <select disabled class="form-control" id="participantid_edit" name="participantid_edit">
                                            @foreach($allmembers as $s)

                                                <option value="{{$s->member_id}}">{{$s->firstname." ".$s->lastname}}</option>


                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Registration Date</label>
                                            <input required type="date"  class="form-control" id="registrationdate_edit" name="registrationdate_edit"/>


                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Stage</label>
                                            <select  class="form-control" id="stage_edit" name="stage_edit" required>
                                                <option value="">------</option>
                                                <option value="ABC">ABC</option>
                                                <option value="Membership">Membership</option>
                                                <option value="Maturity">Maturity</option>
                                                <option value="Ministry">Ministry</option>


                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Status</label>
                                            <select  class="form-control" id="status_edit" name="status_edit" required>
                                                <option value="">------</option>
                                                <option value="Incomplete">Incomplete</option>
                                                <option value="complete">complete</option>



                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Graduation Status</label>
                                            <select  class="form-control" id="graduation_status_edit" name="graduation_status_edit" required>
                                                <option value="">------</option>
                                                <option value="Graduated">Graduated</option>
                                                <option value="Not Graduated">Not Graduated</option>



                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button style="background-color:#006D5B;color: white;" type="submit" class="btn   waves-effect waves-light">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal -->
            <div id="delete-Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <form action="{{route('deleteclasses')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Delete</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Are you sure you want to delete <span id="participantname_delete"></span>?</label>
                                            <input type="hidden" class="form-control" id="id_delete" placeholder="John" name="id_delete" >
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button style="background-color:#006D5B;color: white;" type="submit" class="btn  waves-effect waves-light">Delete</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal -->







        </div>



    </div>
</div>

<footer class="footer text-right">
    {{date('Y')}} © 1CGC
</footer>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- Bootstrap JavaScript -->
{{--<script src="assets/js/bootstrap.min.js"></script>--}}
<script>
    $('#classes-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!!route('datatable.classes')!!}',
        columns: [
            {data: 'participantid', name: 'participantid'},
            {data: 'participantname', name: 'participantname'},
            {data: 'registrationdate', name: 'registrationdate'},
            {data: 'stage', name: 'stage'},
            {data: 'action', name: 'action'},
            {data: 'status', name: 'status'}
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());

                            column.search(val ? val : '', true, false).draw();
                        });
            });
        }
    });
</script>
<script>
    $(document).on('click','.editbtn',function() {
        //alert($(this).data('id'));
        $('#id_edit').val($(this).data('id'));
        $('#participantid_edit').val($(this).data('participantid')).change();
        $('#registrationdate_edit').val($(this).data('registrationdate'));
        $('#stage_edit').val($(this).data('stage')).change();
        $('#status_edit').val($(this).data('status')).change();
        $('#graduation_status_edit').val($(this).data('graduation_status')).change();


    });
</script>
<script>
    $(document).on('click','.deletebtn',function() {


        $('#id_delete').val($(this).data('id'));
        $("#participantname_delete").html($(this).data('participantname'));

    });
</script>

@endsection

