

@extends('layout.localmaster')


@section('content')
        <!-- Begin page -->
<div id="wrapper">


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu" style="background-color: #006D5B">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                {{--<div class="user-details">--}}
                {{--<div class="overlay"></div>--}}
                {{--<div class="text-center">--}}
                {{--<img src="assets/images/users/church.jpg" alt="" class="thumb-md img-circle">--}}
                {{--</div>--}}
                {{--<div class="user-info">--}}
                {{--<div>--}}
                {{--<a href="index.html#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">mamprobi branch <span class="mdi mdi-menu-down"></span></a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="dropdown" id="setting-dropdown">--}}
                {{--<ul class="dropdown-menu">--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-face-profile m-r-5"></i> Profile</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-account-settings-variant m-r-5"></i> Settings</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-lock m-r-5"></i> Lock screen</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-logout m-r-5"></i> Logout</a></li>--}}
                {{--</ul>--}}
                {{--</div>--}}

                <ul>

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub" >
                            <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>
                            {{--<ul class="list-unstyled">--}}
                            {{--<li><a href="index.html">Dashboard 1</a></li>--}}
                            {{--<li><a href="dashboard_2.html">Dashboard 2</a></li>--}}
                            {{--</ul>--}}
                        </li>
                    @endif
                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-invert-colors"></i> <span> Information Mgt </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="ui-buttons.html">Add New Member</a></li>--}}
                    {{--<li><a href="ui-typography.html">Typography</a></li>--}}
                    {{--<li><a href="ui-panels.html">Panel</a></li>--}}
                    {{--<li><a href="ui-portlets.html">Portlets</a></li>--}}
                    {{--<li><a href="ui-modals.html">Modals</a></li>--}}
                    {{--<li><a href="ui-checkbox-radio.html">Checkboxs-Radios</a></li>--}}
                    {{--<li><a href="ui-tabs.html">Tabs</a></li>--}}
                    {{--<li><a href="ui-progressbars.html">Progress Bars</a></li>--}}
                    {{--<li><a href="ui-notifications.html">Notification</a></li>--}}
                    {{--<li><a href="ui-alerts.html">Alerts</a>--}}
                    {{--<li><a href="ui-carousel.html">Carousel</a>--}}
                    {{--<li><a href="ui-video.html">Video</a>--}}
                    {{--<li><a href="ui-tooltips-popovers.html">Tooltips & Popovers</a></li>--}}
                    {{--<li><a href="ui-images.html">Images</a></li>--}}
                    {{--<li><a href="ui-bootstrap.html">Bootstrap UI</a></li>--}}
                    {{--<li><a href="ui-grid.html">Grid</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newmember" style="color: white">New Member</a></li>
                                <li><a href="allmember"style="color: white">All Members</a></li>
                                {{--<li><a href="admin-nestable.html">Nestable List</a></li>--}}
                                {{--<li><a href="admin-rangeslider.html">Range Slider</a></li>--}}
                                {{--<li><a href="admin-ratings.html">Ratings</a></li>--}}
                                {{--<li><a href="admin-animation.html">Animation</a></li>--}}
                            </ul>
                        </li>
                    @endif
                        <li class="has_sub">
                            <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>
                            {{--<ul class="list-unstyled">--}}
                            {{--<li><a href="newmember" style="color: white">New Member</a></li>--}}
                            {{--<li><a href="allmember"style="color: white">All Members</a></li>--}}
                            {{--<li><a href="admin-nestable.html">Nestable List</a></li>--}}
                            {{--<li><a href="admin-rangeslider.html">Range Slider</a></li>--}}
                            {{--<li><a href="admin-ratings.html">Ratings</a></li>--}}
                            {{--<li><a href="admin-animation.html">Animation</a></li>--}}
                            {{--</ul>--}}
                        </li>
                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-calendar"style="color: white" aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="newevent" style="color: white">New Event</a></li>

                                </ul>
                            </li>
                        @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin'||'Finance')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="offering" style="color: white">New</a></li>
                                <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                                <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                                <li><a href="expenses" style="color: white"> Expenses</a></li>
                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">

                                <li><a href="report" style="color: white"> Reports</a></li>
                                <li><a href="quarterlyrpt" style="color: white">Quaterly Reports</a></li>
                                <li><a href="weeklyrpt" style="color: white">Weeky Reports</a></li>

                            </ul>
                        </li>
                    @endif

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text-o" aria-hidden="true"></i><span> Report </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="icons-glyphicons.html">Glyphicons</a></li>--}}
                    {{--<li><a href="icons-colored.html">Colored Icons</a></li>--}}
                    {{--<li><a href="icons-materialdesign.html">Material Design</a></li>--}}
                    {{--<li><a href="icons-ionicons.html">Ion Icons</a></li>--}}
                    {{--<li><a href="icons-fontawesome.html">Font awesome</a></li>--}}
                    {{--<li><a href="icons-themifyicon.html">Themify Icons</a></li>--}}
                    {{--<li><a href="icons-typicons.html">Typicons</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newattendance" style="color: white">New Attendance</a></li>
                                <li><a href="allattendance" style="color: white">All Attendance</a></li>
                                {{--<li><a href="form-validation.html">Form Validation</a></li>--}}
                                {{--<li><a href="form-pickers.html">Form Pickers</a></li>--}}
                                {{--<li><a href="form-wizard.html">Form Wizard</a></li>--}}
                                {{--<li><a href="form-mask.html">Form Masks</a></li>--}}
                                {{--<li><a href="form-summernote.html">Summernote</a></li>--}}
                                {{--<li><a href="form-wysiwig.html">Wysiwig Editors</a></li>--}}
                                {{--<li><a href="form-uploads.html">Multiple File Upload</a></li>--}}
                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="singlesms" style="color: white">Single</a></li>
                                <li><a href="bulksms" style="color: white">Bulk</a></li>
                                {{--<li><a href="form-validation.html">Form Validation</a></li>--}}
                                {{--<li><a href="form-pickers.html">Form Pickers</a></li>--}}
                                {{--<li><a href="form-wizard.html">Form Wizard</a></li>--}}
                                {{--<li><a href="form-mask.html">Form Masks</a></li>--}}
                                {{--<li><a href="form-summernote.html">Summernote</a></li>--}}
                                {{--<li><a href="form-wysiwig.html">Wysiwig Editors</a></li>--}}
                                {{--<li><a href="form-uploads.html">Multiple File Upload</a></li>--}}
                            </ul>
                        </li>
                    @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="profile" style="color: white">Church Profile</a></li>
                                <li><a href="user" style="color: white">Users</a></li>
                                <li><a href="service " style="color: white">Service</a></li>
                                <li><a href="member_group" style="color: white">Member Group</a></li>
                                <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>


                            </ul>
                        </li>
                    @endif
                    {{--<li class="menu-title">Extra</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-chart-arc"></i><span> Charts </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="chart-flot.html">Flot Chart</a></li>--}}
                    {{--<li><a href="chart-morris.html">Morris Chart</a></li>--}}
                    {{--<li><a href="chart-google.html">Google Chart</a></li>--}}
                    {{--<li><a href="chart-chartist.html">Chartist Charts</a></li>--}}
                    {{--<li><a href="chart-chartjs.html">Chartjs Chart</a></li>--}}
                    {{--<li><a href="chart-c3.html">C3 Chart</a></li>--}}
                    {{--<li><a href="chart-sparkline.html">Sparkline Chart</a></li>--}}
                    {{--<li><a href="chart-knob.html">Jquery Knob</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-map"></i> <span> Maps </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="maps-google.html">Google Maps</a></li>--}}
                    {{--<li><a href="maps-vector.html">Vector Maps</a></li>--}}
                    {{--<li><a href="maps-mapael.html">Mapael Maps</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-google-pages"></i><span> Pages </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="page-starter.html">Starter Page</a></li>--}}
                    {{--<li><a href="page-login.html">Login</a></li>--}}
                    {{--<li><a href="page-register.html">Register</a></li>--}}
                    {{--<li><a href="page-logout.html">Logout</a></li>--}}
                    {{--<li><a href="page-recoverpw.html">Recover Password</a></li>--}}
                    {{--<li><a href="page-lock-screen.html">Lock Screen</a></li>--}}
                    {{--<li><a href="page-confirm-mail.html">Confirm Mail</a></li>--}}
                    {{--<li><a href="page-404.html">Error 404</a></li>--}}
                    {{--<li><a href="page-404-alt.html">Error 404-alt</a></li>--}}
                    {{--<li><a href="page-500.html">Error 500</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-gift"></i><span> Extras </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="extras-profile.html">Profile</a></li>--}}
                    {{--<li><a href="extras-sitemap.html">Sitemap</a></li>--}}
                    {{--<li><a href="extras-about.html">About Us</a></li>--}}
                    {{--<li><a href="extras-contact.html">Contact</a></li>--}}
                    {{--<li><a href="extras-members.html">Members</a></li>--}}
                    {{--<li><a href="extras-timeline.html">Timeline</a></li>--}}
                    {{--<li><a href="extras-invoice.html">Invoice</a></li>--}}
                    {{--<li><a href="extras-search-result.html">Search Result</a></li>--}}
                    {{--<li><a href="extras-emailtemplate.html">Email Template</a></li>--}}
                    {{--<li><a href="extras-maintenance.html">Maintenance</a></li>--}}
                    {{--<li><a href="extras-coming-soon.html">Coming Soon</a></li>--}}
                    {{--<li><a href="extras-faq.html">FAQ</a></li>--}}
                    {{--<li><a href="extras-gallery.html">Gallery</a></li>--}}
                    {{--<li><a href="extras-pricing.html">Pricing</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="menu-title">More</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-comment-text-outline"></i><span> Blog </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="blogs-dashboard.html">Dashboard</a></li>--}}
                    {{--<li><a href="blogs-blog-list.html">Blog List</a></li>--}}
                    {{--<li><a href="blogs-blog-column.html">Blog Column</a></li>--}}
                    {{--<li><a href="blogs-blog-post.html">Blog Post</a></li>--}}
                    {{--<li><a href="blogs-blog-add.html">Add Blog</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-home-map-marker"></i><span> Real Estate </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="real-estate-dashboard.html">Dashboard</a></li>--}}
                    {{--<li><a href="real-estate-list.html">Property List</a></li>--}}
                    {{--<li><a href="real-estate-column.html">Property Column</a></li>--}}
                    {{--<li><a href="real-estate-detail.html">Property Detail</a></li>--}}
                    {{--<li><a href="real-estate-agents.html">Agents</a></li>--}}
                    {{--<li><a href="real-estate-profile.html">Agent Details</a></li>--}}
                    {{--<li><a href="real-estate-add.html">Add Property</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

            {{--<div class="help-box">--}}
            {{--<h5 class="text-muted m-t-0">For Help ?</h5>--}}
            {{--<p class=""><span class="text-dark"><b>Email:</b></span> <br/> nanamensah1140@gmail.com</p>--}}
            {{--<p class=""><span class="text-dark"><b>Email:</b></span> <br/> emagbin@yahoo.com</p>--}}
            {{--<p class="m-b-0"><span class="text-dark"><b>Call:</b></span> <br/> 0262489168,0249273086</p>--}}
            {{--</div>--}}

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Attendance</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="tables-datatable.html#">ICGC</a>
                                </li>
                                <li>
                                    <a href="allattendance">All Attendance</a>
                                </li>
                                <li class="active">
                                    New Attendance
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="col-xs-12">
                    <h4 class="header-title m-t-0"></h4>
                    <div class="card-box">

                        <div class="row">
                            <form action="saveattendance" method="post"   data-parsley-validate=""  >
                            <div class="col-sm-6 col-xs-6 col-md-6">

                                    <div class="p-20">
                                        <div class="form-group">
                                            <label for="userName">Service Type<span class="text-danger">*</span></label>
                                            <select class="selectpicker" data-style="btn-default" name="servicetype">
                                                @foreach($servicetype as $s)

                                                    <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="emailAddress">Submitted Date</label>
                                            <input type="date" class="form-control" name="registration_date" required/>
                                        </div>
                                        <div class="form-group">
                                            <label for="day">Day<span class="text-danger">*</span></label>
                                            <select class="selectpicker" data-style="btn-default" name="day" >


                                                    <option value="sunday">Sunday</option>
                                                    <option value="monday">Monday</option>
                                                    <option value="tuesday">Tuesday</option>
                                                    <option value="wednesday">Wednesday</option>
                                                    <option value="thursday">Thursday</option>
                                                    <option value="friday">Friday</option>
                                                    <option value="saturday">Saturday</option>

                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="emailAddress">No. of Male<span class="text-danger">*</span></label>
                                            <input type="text" name="male"
                                                   class="form-control" id="name_of_receipient" required>
                                        </div>
                                      <div class="form-group">
                                            <label for="pass1">No. of Female<span class="text-danger">*</span></label>
                                            <input  type="text" name="female" id="sender"
                                                    class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="pass1">No. of Visitor Male<span class="text-danger">*</span></label>
                                            <input  type="text" name="visitor_male" id="sender"
                                                    class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="pass1">No. of Visitor Female<span class="text-danger">*</span></label>
                                            <input  type="text" name="visitor_female" id="sender"
                                                    class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="pass1">No. of Cars<span class="text-danger">*</span></label>
                                            <input  type="text" name="car" id="sender"
                                                    class="form-control">
                                        </div>
                                        {{csrf_field()}}
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <div class="form-group text-right m-b-0">
                                            <button style="background-color:#006D5B;color: white;" class="btn  waves-effect waves-light" id="sendsingle"  >
                                                Send
                                            </button>

                                        </div>



                                    </div>

                            </div>
                            <div class="col-sm-6 col-xs-6 col-md-6">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Stage(FOR ABC CLASSES ONLY)</label>
                                    <select  class="form-control" id="stage" name="stage" >
                                        <option value="">------</option>
                                        <option value="ABC">ABC</option>
                                        <option value="Membership">Membership</option>
                                        <option value="Maturity">Maturity</option>
                                        <option value="Ministry">Ministry</option>


                                    </select>
                                </div>
                            </div>
                            </form>
                        </div>
                        <>
                        <!-- end row -->


                        <!-- end row -->


                    </div> <!-- end ard-box -->
                </div><!-- end col-->




            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            {{date('Y')}} © 1CGC
        </footer>

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


</div>

@endsection

