

@extends('layout.localmaster')

<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/personal/datatable.css">

@section('content')
        <!-- Begin page -->
<div id="wrapper">


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu" style="background-color: #006D5B">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                {{--<div class="user-details">--}}
                {{--<div class="overlay"></div>--}}
                {{--<div class="text-center">--}}
                {{--<img src="assets/images/users/church.jpg" alt="" class="thumb-md img-circle">--}}
                {{--</div>--}}
                {{--<div class="user-info">--}}
                {{--<div>--}}
                {{--<a href="index.html#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">mamprobi branch <span class="mdi mdi-menu-down"></span></a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="dropdown" id="setting-dropdown">--}}
                {{--<ul class="dropdown-menu">--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-face-profile m-r-5"></i> Profile</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-account-settings-variant m-r-5"></i> Settings</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-lock m-r-5"></i> Lock screen</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-logout m-r-5"></i> Logout</a></li>--}}
                {{--</ul>--}}
                {{--</div>--}}

                <ul>

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub" >
                            <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>
                            {{--<ul class="list-unstyled">--}}
                            {{--<li><a href="index.html">Dashboard 1</a></li>--}}
                            {{--<li><a href="dashboard_2.html">Dashboard 2</a></li>--}}
                            {{--</ul>--}}
                        </li>
                    @endif
                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-invert-colors"></i> <span> Information Mgt </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="ui-buttons.html">Add New Member</a></li>--}}
                    {{--<li><a href="ui-typography.html">Typography</a></li>--}}
                    {{--<li><a href="ui-panels.html">Panel</a></li>--}}
                    {{--<li><a href="ui-portlets.html">Portlets</a></li>--}}
                    {{--<li><a href="ui-modals.html">Modals</a></li>--}}
                    {{--<li><a href="ui-checkbox-radio.html">Checkboxs-Radios</a></li>--}}
                    {{--<li><a href="ui-tabs.html">Tabs</a></li>--}}
                    {{--<li><a href="ui-progressbars.html">Progress Bars</a></li>--}}
                    {{--<li><a href="ui-notifications.html">Notification</a></li>--}}
                    {{--<li><a href="ui-alerts.html">Alerts</a>--}}
                    {{--<li><a href="ui-carousel.html">Carousel</a>--}}
                    {{--<li><a href="ui-video.html">Video</a>--}}
                    {{--<li><a href="ui-tooltips-popovers.html">Tooltips & Popovers</a></li>--}}
                    {{--<li><a href="ui-images.html">Images</a></li>--}}
                    {{--<li><a href="ui-bootstrap.html">Bootstrap UI</a></li>--}}
                    {{--<li><a href="ui-grid.html">Grid</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newmember" style="color: white">New Member</a></li>
                                <li><a href="allmember"style="color: white">All Members</a></li>
                                {{--<li><a href="admin-nestable.html">Nestable List</a></li>--}}
                                {{--<li><a href="admin-rangeslider.html">Range Slider</a></li>--}}
                                {{--<li><a href="admin-ratings.html">Ratings</a></li>--}}
                                {{--<li><a href="admin-animation.html">Animation</a></li>--}}
                            </ul>
                        </li>
                    @endif
                        <li class="has_sub">
                            <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>

                        </li>
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-calendar"style="color: white" aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newevent" style="color: white">New Event</a></li>


                            </ul>
                        </li>
                    @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin'||'Finance')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="offering" style="color: white">New</a></li>
                                <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                                <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                                <li><a href="expenses" style="color: white"> Expenses</a></li>
                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">

                                <li><a href="report" style="color: white"> Reports</a></li>
                                <li><a href="quarterlyrpt" style="color: white">Quaterly Reports</a></li>
                                <li><a href="weeklyrpt" style="color: white">Weeky Reports</a></li>

                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newattendance" style="color: white">New Attendance</a></li>
                                <li><a href="allattendance" style="color: white">All Attendance</a></li>

                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="singlesms" style="color: white">Single</a></li>
                                <li><a href="bulksms" style="color: white">Bulk</a></li>

                            </ul>
                        </li>
                    @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="profile" style="color: white">Church Profile</a></li>
                                <li><a href="user" style="color: white">Users</a></li>
                                <li><a href="service " style="color: white">Service</a></li>
                                <li><a href="member_group" style="color: white">Member Group</a></li>
                                <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>


                            </ul>
                        </li>
                    @endif

                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>


        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">All Events</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="dashboard">ICGC</a>
                                </li>
                                <li>
                                    <a href="newevent">Event</a>
                                </li>
                                <li class="active">
                                   Events
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="row">
                    <div class="col-sm-4">
                        <!-- Responsive modal -->
                        <a style="background-color:#006D5B;color: white;" class="btn  waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal">New Event</a>
                    </div>
                </div>
                </br>
                <!-- end row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">

                            <h4 class="m-t-0 header-title"><b>All Record</b></h4>


                            <table id="event-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Event type</th>
                                    <th>Description</th>
                                    <th>Record date</th>
                                    <th>Action</th>

                                </tr>
                                </thead>

                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <form action="{{route('addevent')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Add New Event</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12 col-md-12">

                                            <div class="p-20">
                                                <div class="form-group">
                                                    <label for="userName">Event type</label>
                                                    <select  class="form-control" id="eventtype" name="eventtype" required>
                                                        <option value="">select an option</option>
                                                        <option value="marriage">Marriage</option>
                                                        <option value="namingceremony">Naming Ceremony</option>
                                                        <option value="birth">Birth</option>
                                                        <option value="babydedication">Baby Dedication</option>
                                                        <option value="burial">Burial</option>


                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Event Name">Eventy Name</label>
                                                    <input  name="eventname" required
                                                            class="form-control" id="eventname" />
                                                </div>
                                                <div class="form-group">
                                                    <label for="emailAddress">Description</label>
                                                        <textarea row="3" name="description" required
                                                                  class="form-control" id="description" ></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="date">Date</label>
                                                    <input type="date" name="recordered_date" required
                                                           class="form-control" id="recordered_date" />
                                                </div>



                                            </div>

                                        </div>


                                    </div>

                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button class="btn waves-effect waves-light" style="background-color: #006D5B; color:white" type="submit">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal -->
            <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <form action="{{route('editevent')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Edit</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12 col-md-12">
                                            <input type="hidden" id="id_edit" name="id_edit"/>
                                            <div class="p-20">
                                                <div class="form-group">
                                                    <label for="userName">Event type</label>
                                                    <select  class="form-control" id="eventtype_edit" name="eventtype_edit" required>
                                                        <option value="">select an option</option>
                                                        <option value="marriage">Marriage</option>
                                                        <option value="namingceremony">Naming Ceremony</option>
                                                        <option value="birth">Birth</option>
                                                        <option value="babydedication">Baby Dedication</option>
                                                        <option value="burial">Burial</option>


                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Event Name">Eventy Name</label>
                                                    <input  name="eventname_edit" required
                                                            class="form-control" id="eventname_edit" />
                                                </div>
                                                <div class="form-group">
                                                    <label for="emailAddress">Description</label>
                                                        <textarea row="3" name="description_edit" required
                                                                  class="form-control" id="description_edit" ></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="date">Date</label>
                                                    <input type="date" name="recordered_date_edit" required
                                                           class="form-control" id="recordered_date_edit" />
                                                </div>



                                            </div>

                                        </div>


                                    </div>

                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button style="background-color:#006D5B;color: white;" type="submit" class="btn  waves-effect waves-light">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal -->
            <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <form action="{{route('deleteevent')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Delete</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Are you sure you want to delete <span id="eventname_delete"></span>?</label>
                                            <input type="hidden" class="form-control" id="id_delete" placeholder="John" name="id_delete" >
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button style="background-color:#006D5B;color: white;" type="submit" class="btn  waves-effect waves-light">Delete</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal -->







        </div>



    </div>
</div>

<footer class="footer text-right">
    {{date('Y')}} © 1CGC
</footer>

</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- Bootstrap JavaScript -->
{{--<script src="assets/js/bootstrap.min.js"></script>--}}
<script>
    $('#event-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!!route('datatable.event')!!}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'eventtype', name: 'eventtype'},
            {data: 'name', name: 'name'},
            {data: 'recordered_date', name: 'recordered_date'},
            {data: 'action', name: 'action'}
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());

                            column.search(val ? val : '', true, false).draw();
                        });
            });
        }
    });
</script>
<script>
    $(document).on('click','.editbtn',function() {

        $('#eventtype_edit').val($(this).data('eventtype')).change();
        $('#eventname_edit').val($(this).data('eventname'));
        $('#description_edit').val($(this).data('description'));
        $('#recordered_date_edit').val($(this).data('recordered_date'));
        $('#id_edit').val($(this).data('id'));


    });
</script>
<script>
    $(document).on('click','.deletebtn',function() {

        $('#id_delete').val($(this).data('id'));


        $("#eventname_delete").html($(this).data('eventname'));

    });
</script>
@endsection

