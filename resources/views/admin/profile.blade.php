

@extends('layout.localmaster')


@section('content')
        <!-- Begin page -->
<div id="wrapper">


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu" style="background-color: #006D5B">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                {{--<div class="user-details">--}}
                {{--<div class="overlay"></div>--}}
                {{--<div class="text-center">--}}
                {{--<img src="assets/images/users/church.jpg" alt="" class="thumb-md img-circle">--}}
                {{--</div>--}}
                {{--<div class="user-info">--}}
                {{--<div>--}}
                {{--<a href="index.html#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">mamprobi branch <span class="mdi mdi-menu-down"></span></a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="dropdown" id="setting-dropdown">--}}
                {{--<ul class="dropdown-menu">--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-face-profile m-r-5"></i> Profile</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-account-settings-variant m-r-5"></i> Settings</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-lock m-r-5"></i> Lock screen</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-logout m-r-5"></i> Logout</a></li>--}}
                {{--</ul>--}}
                {{--</div>--}}

                <ul>

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub" >
                            <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>
                            {{--<ul class="list-unstyled">--}}
                            {{--<li><a href="index.html">Dashboard 1</a></li>--}}
                            {{--<li><a href="dashboard_2.html">Dashboard 2</a></li>--}}
                            {{--</ul>--}}
                        </li>
                    @endif
                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-invert-colors"></i> <span> Information Mgt </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="ui-buttons.html">Add New Member</a></li>--}}
                    {{--<li><a href="ui-typography.html">Typography</a></li>--}}
                    {{--<li><a href="ui-panels.html">Panel</a></li>--}}
                    {{--<li><a href="ui-portlets.html">Portlets</a></li>--}}
                    {{--<li><a href="ui-modals.html">Modals</a></li>--}}
                    {{--<li><a href="ui-checkbox-radio.html">Checkboxs-Radios</a></li>--}}
                    {{--<li><a href="ui-tabs.html">Tabs</a></li>--}}
                    {{--<li><a href="ui-progressbars.html">Progress Bars</a></li>--}}
                    {{--<li><a href="ui-notifications.html">Notification</a></li>--}}
                    {{--<li><a href="ui-alerts.html">Alerts</a>--}}
                    {{--<li><a href="ui-carousel.html">Carousel</a>--}}
                    {{--<li><a href="ui-video.html">Video</a>--}}
                    {{--<li><a href="ui-tooltips-popovers.html">Tooltips & Popovers</a></li>--}}
                    {{--<li><a href="ui-images.html">Images</a></li>--}}
                    {{--<li><a href="ui-bootstrap.html">Bootstrap UI</a></li>--}}
                    {{--<li><a href="ui-grid.html">Grid</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newmember" style="color: white">New Member</a></li>
                                <li><a href="allmember"style="color: white">All Members</a></li>
                                {{--<li><a href="admin-nestable.html">Nestable List</a></li>--}}
                                {{--<li><a href="admin-rangeslider.html">Range Slider</a></li>--}}
                                {{--<li><a href="admin-ratings.html">Ratings</a></li>--}}
                                {{--<li><a href="admin-animation.html">Animation</a></li>--}}
                            </ul>
                        </li>
                    @endif
                        <li class="has_sub">
                            <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>
                            {{--<ul class="list-unstyled">--}}
                            {{--<li><a href="newmember" style="color: white">New Member</a></li>--}}
                            {{--<li><a href="allmember"style="color: white">All Members</a></li>--}}
                            {{--<li><a href="admin-nestable.html">Nestable List</a></li>--}}
                            {{--<li><a href="admin-rangeslider.html">Range Slider</a></li>--}}
                            {{--<li><a href="admin-ratings.html">Ratings</a></li>--}}
                            {{--<li><a href="admin-animation.html">Animation</a></li>--}}
                            {{--</ul>--}}
                        </li>
                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-calendar" style="color: white" aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="newevent" style="color: white">New Event</a></li>

                                </ul>
                            </li>
                        @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin'||'Finance')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="offering" style="color: white">New</a></li>
                                <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                                <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                                <li><a href="expenses" style="color: white"> Expenses</a></li>

                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">

                                <li><a href="report" style="color: white"> Reports</a></li>
                                <li><a href="quarterlyrpt" style="color: white">Quaterly Reports</a></li>
                                <li><a href="weeklyrpt" style="color: white">Weeky Reports</a></li>

                            </ul>
                        </li>
                    @endif

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text-o" aria-hidden="true"></i><span> Report </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="icons-glyphicons.html">Glyphicons</a></li>--}}
                    {{--<li><a href="icons-colored.html">Colored Icons</a></li>--}}
                    {{--<li><a href="icons-materialdesign.html">Material Design</a></li>--}}
                    {{--<li><a href="icons-ionicons.html">Ion Icons</a></li>--}}
                    {{--<li><a href="icons-fontawesome.html">Font awesome</a></li>--}}
                    {{--<li><a href="icons-themifyicon.html">Themify Icons</a></li>--}}
                    {{--<li><a href="icons-typicons.html">Typicons</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newattendance" style="color: white">New Attendance</a></li>
                                <li><a href="allattendance" style="color: white">All Attendance</a></li>
                                {{--<li><a href="form-validation.html">Form Validation</a></li>--}}
                                {{--<li><a href="form-pickers.html">Form Pickers</a></li>--}}
                                {{--<li><a href="form-wizard.html">Form Wizard</a></li>--}}
                                {{--<li><a href="form-mask.html">Form Masks</a></li>--}}
                                {{--<li><a href="form-summernote.html">Summernote</a></li>--}}
                                {{--<li><a href="form-wysiwig.html">Wysiwig Editors</a></li>--}}
                                {{--<li><a href="form-uploads.html">Multiple File Upload</a></li>--}}
                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="singlesms" style="color: white">Single</a></li>
                                <li><a href="bulksms" style="color: white">Bulk</a></li>
                                {{--<li><a href="form-validation.html">Form Validation</a></li>--}}
                                {{--<li><a href="form-pickers.html">Form Pickers</a></li>--}}
                                {{--<li><a href="form-wizard.html">Form Wizard</a></li>--}}
                                {{--<li><a href="form-mask.html">Form Masks</a></li>--}}
                                {{--<li><a href="form-summernote.html">Summernote</a></li>--}}
                                {{--<li><a href="form-wysiwig.html">Wysiwig Editors</a></li>--}}
                                {{--<li><a href="form-uploads.html">Multiple File Upload</a></li>--}}
                            </ul>
                        </li>
                    @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="profile" style="color: white">Church Profile</a></li>
                                <li><a href="user" style="color: white">Users</a></li>
                                <li><a href="service " style="color: white">Service</a></li>
                                <li><a href="member_group" style="color: white">Member Group</a></li>
                                <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>


                            </ul>
                        </li>
                    @endif


                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>



        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">My church Profile</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="dashboard">ICGC</a>
                                </li>
                                <li>
                                    <a href="profile">Profile</a>
                                </li>
                                <li class="active">
                                    All members
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->


                <!-- end row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <form role="form" action="{{route('updateprofile')}}" method="post" enctype="multipart/form-data">
                                @if(!empty($churchdetails->id))
                                    <input name="profileid" value="{{$churchdetails->id}}" type="hidden"/>
                                @endif
                                    <div class="row">
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-4">
                                            <section class="panel panel-default">
                                                <div class="panel-body">
                                                    @if(!empty($churchdetails->avatar))
                                                        <div><img id="preview_image" src="uploads/avatars/{{$churchdetails->avatar}}" width="270px" height="270px" class="img-responsive text-center" ></div>
                                                    @else
                                                        <div><img src="uploads/avatars/default.jpg" width="270px" height="270px" class="img-responsive text-center" ></div>

                                                    @endif
                                                    <br>
                                                    <br>
                                                    {{--//<form enctype="multipart/form-data"  action=""></form>--}}
                                                    <input type="file" id="imgInp" name="avatar" class="form-control">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                <div class="row">
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label>Parent Church Name </label><label class="text-danger">*</label>
                                            <input type="text" class="form-control"  name="parentchurchname" value="{{$churchdetails->parentchurchname}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Church Name </label><label class="text-danger">*</label>
                                            <input type="text" class="form-control"  name="hotelname" value="{{$churchdetails->churchname}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Pastor Name </label><label class="text-danger">*</label>
                                            <input type="text" class="form-control"  name="pastorname" value="{{$churchdetails->pastorname}}">
                                        </div>
                                        <div class="form-group">
                                            <label>No of Pastors </label><label class="text-danger">*</label>
                                            <input type="text" class="form-control"  name="no_of_pastors" value="{{$churchdetails->no_of_pastors}}">
                                        </div>

                                        <div class="form-group">
                                            <label>Address</label><label class="text-danger">&nbsp;*</label>
                                                <textarea class="form-control" rows="4"  name="address" required="">{{$churchdetails->address}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Website</label>
                                            <input type="text" class="form-control"  name="website" value="{{$churchdetails->website}}">
                                        </div>
                                        <div class="form-group">
                                            <label>City</label>
                                            <input type="text" class="form-control"  name="city" value="{{$churchdetails->city}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Country</label>
                                            <input type="text" class="form-control"  name="country" value="{{$churchdetails->country}}">
                                        </div>
                                        <div class="form-group">
                                            <label>District</label>
                                            <input type="text" class="form-control"  name="district" value="{{$churchdetails->district}}">
                                        </div>


                                    </div>
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control"  name="email" value="{{$churchdetails->email}}">
                                        </div>

                                        <div class="form-group">
                                            <label>Phone</label><label class="text-danger">&nbsp;*</label>
                                            <input type="text" class="form-control"  name="phone" value="{{$churchdetails->phone}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Mobile</label>
                                            <input type="text" class="form-control"  name="telephone" value="{{$churchdetails->telephone}}">
                                        </div>
                                        <div class="form-group">
                                            <label>SMS Sender Id</label>
                                            <input type="text" class="form-control"  name="smssenderid" value="{{$churchdetails->smssenderid}}">
                                        </div>
                                        {{csrf_field()}}
                                        <button style="background-color:#006D5B;color: white;"  type="submit" class="btn btn-sm  pull-right">Save Changes</button>

                                    </div>
                                </div>
                            </form>


                        </div>
                    </div>
                </div>
                <!-- end row -->







            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            {{date('Y')}} © 1CGC
        </footer>

    </div>




</div>
<script>
    $(document).ready(function() {
        $('textarea').focus(function () {
            $(this).css("border-bottom", "1px solid #006D5B");
            $(this).css("box-shadow", "0 1px 0 0 #006D5B");
        });
    });
</script>
@endsection

