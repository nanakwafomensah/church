

@extends('layout.localmaster')


@section('content')

    <div id="wrapper">
        <!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu" style="background-color: #006D5B">
    <div class="sidebar-inner slimscrollleft">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            {{--<div class="user-details">--}}
            {{--<div class="overlay"></div>--}}
            {{--<div class="text-center">--}}
            {{--<img src="assets/images/users/church.jpg" alt="" class="thumb-md img-circle">--}}
            {{--</div>--}}
            {{--<div class="user-info">--}}
            {{--<div>--}}
            {{--<a href="index.html#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">mamprobi branch <span class="mdi mdi-menu-down"></span></a>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}

            {{--<div class="dropdown" id="setting-dropdown">--}}
            {{--<ul class="dropdown-menu">--}}
            {{--<li><a href="javascript:void(0)"><i class="mdi mdi-face-profile m-r-5"></i> Profile</a></li>--}}
            {{--<li><a href="javascript:void(0)"><i class="mdi mdi-account-settings-variant m-r-5"></i> Settings</a></li>--}}
            {{--<li><a href="javascript:void(0)"><i class="mdi mdi-lock m-r-5"></i> Lock screen</a></li>--}}
            {{--<li><a href="javascript:void(0)"><i class="mdi mdi-logout m-r-5"></i> Logout</a></li>--}}
            {{--</ul>--}}
            {{--</div>--}}

            <ul>

                @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub" >
                        <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>
                        {{--<ul class="list-unstyled">--}}
                        {{--<li><a href="index.html">Dashboard 1</a></li>--}}
                        {{--<li><a href="dashboard_2.html">Dashboard 2</a></li>--}}
                        {{--</ul>--}}
                    </li>
                @endif
                {{--<li class="has_sub">--}}
                {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-invert-colors"></i> <span> Information Mgt </span> <span class="menu-arrow"></span></a>--}}
                {{--<ul class="list-unstyled">--}}
                {{--<li><a href="ui-buttons.html">Add New Member</a></li>--}}
                {{--<li><a href="ui-typography.html">Typography</a></li>--}}
                {{--<li><a href="ui-panels.html">Panel</a></li>--}}
                {{--<li><a href="ui-portlets.html">Portlets</a></li>--}}
                {{--<li><a href="ui-modals.html">Modals</a></li>--}}
                {{--<li><a href="ui-checkbox-radio.html">Checkboxs-Radios</a></li>--}}
                {{--<li><a href="ui-tabs.html">Tabs</a></li>--}}
                {{--<li><a href="ui-progressbars.html">Progress Bars</a></li>--}}
                {{--<li><a href="ui-notifications.html">Notification</a></li>--}}
                {{--<li><a href="ui-alerts.html">Alerts</a>--}}
                {{--<li><a href="ui-carousel.html">Carousel</a>--}}
                {{--<li><a href="ui-video.html">Video</a>--}}
                {{--<li><a href="ui-tooltips-popovers.html">Tooltips & Popovers</a></li>--}}
                {{--<li><a href="ui-images.html">Images</a></li>--}}
                {{--<li><a href="ui-bootstrap.html">Bootstrap UI</a></li>--}}
                {{--<li><a href="ui-grid.html">Grid</a></li>--}}
                {{--</ul>--}}
                {{--</li>--}}
                @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="newmember" style="color: white">New Member</a></li>
                            <li><a href="allmember"style="color: white">All Members</a></li>
                            {{--<li><a href="admin-nestable.html">Nestable List</a></li>--}}
                            {{--<li><a href="admin-rangeslider.html">Range Slider</a></li>--}}
                            {{--<li><a href="admin-ratings.html">Ratings</a></li>--}}
                            {{--<li><a href="admin-animation.html">Animation</a></li>--}}
                        </ul>
                    </li>
                @endif
                    <li class="has_sub">
                        <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>

                    </li>
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-calendar" style="color: white"aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newevent" style="color: white">New Event</a></li>

                            </ul>
                        </li>
                    @endif
                @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin'||'Finance')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="offering" style="color: white">New</a></li>
                            <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                            <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                            <li><a href="expenses" style="color: white"> Expenses</a></li>

                        </ul>
                    </li>
                @endif

                @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">

                            <li><a href="report" style="color: white"> Reports</a></li>
                            <li><a href="quarterlyrpt" style="color: white">Quaterly Reports</a></li>
                            <li><a href="weeklyrpt" style="color: white">Weeky Reports</a></li>

                        </ul>
                    </li>
                @endif

                @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="newattendance" style="color: white">New Attendance</a></li>
                            <li><a href="allattendance" style="color: white">All Attendance</a></li>

                        </ul>
                    </li>
                @endif

                @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="singlesms" style="color: white">Single</a></li>
                            <li><a href="bulksms" style="color: white">Bulk</a></li>

                        </ul>
                    </li>
                @endif
                @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="profile" style="color: white">Church Profile</a></li>
                            <li><a href="user" style="color: white">Users</a></li>
                            <li><a href="service " style="color: white">Service</a></li>
                            <li><a href="member_group" style="color: white">Member Group</a></li>
                            <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>


                        </ul>
                    </li>
                @endif


            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>


    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
        <!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">


            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Offerings</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="dashboard">ICGC</a>
                            </li>
                            <li>
                                <a href="alloffering">All Offerings</a>
                            </li>
                            <li class="active">
                                Offerings
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Menu</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="nav nav-pills m-b-30 pull-right">
                                <li class="active">
                                    <a href="ui-tabs.html#navpills-1" data-toggle="tab" aria-expanded="true" >Main</a>
                                </li>
                                <li class="">
                                    <a href="ui-tabs.html#navpills-2" data-toggle="tab" aria-expanded="false">Thanksgiving</a>
                                </li>
                                <li class="">
                                    <a href="ui-tabs.html#navpills-3" data-toggle="tab" aria-expanded="false">Project</a>
                                </li>
                                <li class="">
                                    <a href="ui-tabs.html#navpills-4" data-toggle="tab" aria-expanded="false">Tithe</a>
                                </li>
                                <li class="">
                                    <a href="ui-tabs.html#navpills-5" data-toggle="tab" aria-expanded="false">Pledge</a>
                                </li>
                                <li class="">
                                    <a href="ui-tabs.html#navpills-6" data-toggle="tab" aria-expanded="false">Welfare</a>
                                </li>
                                <li class="">
                                    <a href="ui-tabs.html#navpills-7" data-toggle="tab" aria-expanded="false">Other Income</a>
                                </li>
                                <li class="">
                                    <a href="ui-tabs.html#navpills-8" data-toggle="tab" aria-expanded="false">Day Born</a>
                                </li>
                            </ul>
                            <div class="tab-content br-n pn">
                                <div id="navpills-1" class="tab-pane active">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <form  action="excelImportmainOffering" class="form-horizontal" method="post" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="file" name="excel" />
                                                        <button class="btn " style="background-color:#006D5B;color: white;">Import ExcelFile</button>
                                                    </form>
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelmain/xls"><button class="btn " style="background-color:#006D5B;color: white;">Download xls</button></a>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelmain/xlsx"><button class="btn " style="background-color:#006D5B;color: white;">Download xlsx</button></a>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelmain/csv"><button class="btn" style="background-color:#006D5B;color: white;">Download CSV</button></a>

                                                </div>
                                            </div>
                                        <form action="saveoffering" method="post" data-parsley-validate="">
                                            <div class="p-20">
                                                <div class="form-group">
                                                    <label for="userName">Date Received :<span class="text-danger">*</span></label>
                                                    <input type="date" name="date_submitted"
                                                           class="form-control" id="userName" required>
                                                </div>
                                                <input type="hidden" name="offeringtype" value="mainoffering"/>
                                                <input type="hidden" name="number" value="{{mt_rand(100000, 999999).'M'}}"/>
                                                <input type="hidden" name="member_id" value="ICGC"/>
                                                <div class="form-group">
                                                    <label for="emailAddress">Amount :<span class="text-danger">*</span></label>
                                                    <input type="text" name="amount" required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"
                                                           class="form-control" id="emailAddress">
                                                </div>
                                                <div class="form-group">
                                                    <label for="emailAddress">Description :<span class="text-danger">*</span></label>
                                                    <textarea class="form-control" name="description" rows="4"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="passWord2">Service Type<span class="text-danger">*</span></label>
                                                    <select class="selectpicker" data-style="btn-default" name="servicetype">
                                                    @foreach($servicetype as $s)

                                                            <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                     @endforeach
                                                    </select>
                                                </div>
                                                {{csrf_field()}}
                                                <div class="form-group text-right m-b-0">
                                                    <button  style="background-color:#006D5B;color: white;" class="btn waves-effect waves-light" type="submit">
                                                        Submit
                                                    </button>
                                                    <button type="reset" class="btn btn-default waves-effect m-l-5">
                                                        Cancel
                                                    </button>
                                                </div>
                                              </div>
                                        </form>
                                        </div>
                                        <div class="col-md-4">
                                            {{--<img src="assets/images/small/img-1.jpg" class="img-responsive thumbnail m-r-15">--}}
                                            <div class="row">

                                                <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                        <div class=" pull-left">
                                                            <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                        </div>
                                                        <div class="text-right">
                                                            <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Year</p>
                                                            <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$main_year->total}}</span></h2>
                                                        </div>
                                                    </div>
                                                <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                    <div class=" pull-left">
                                                        <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                    </div>
                                                    <div class="text-right">
                                                        <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Month</p>
                                                        <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$main_month->total}}</span></h2>
                                                    </div>
                                                </div>
                                                <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                    <div class=" pull-left">
                                                        <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                    </div>
                                                    <div class="text-right">
                                                        <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Week</p>
                                                        <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$main_week->total}}</span></h2>
                                                    </div>
                                                </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="navpills-2" class="tab-pane">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <form  action="excelImportthanksOffering" class="form-horizontal" method="post" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="file" name="excel" />
                                                        <button class="btn " style="background-color:#006D5B;color: white;">Import ExcelFile</button>
                                                    </form>
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelthanks/xls"><button class="btn " style="background-color:#006D5B;color: white;">Download xls</button></a>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelthanks/xlsx"><button class="btn " style="background-color:#006D5B;color: white;">Download xlsx</button></a>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelthanks/csv"><button class="btn" style="background-color:#006D5B;color: white;">Download CSV</button></a>

                                                </div>
                                            </div>
                                            <form action="saveoffering" method="post" data-parsley-validate="">
                                            <div class="p-20">
                                                <div class="form-group">
                                                    <label for="userName">Offering Number :<span class="text-danger">*</span></label>
                                                    <input type="text" name="number" value="{{mt_rand(100000, 999999).'G'}}"
                                                           class="form-control" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="userName">Date Received :<span class="text-danger">*</span></label>
                                                    <input type="date" name="date_submitted" required
                                                           class="form-control" id="userName">
                                                </div>
                                                <input type="hidden" name="offeringtype" value="thanksgivingoffering"/>
                                                <div class="form-group">
                                                    <label for="emailAddress">Member Name:<span class="text-danger">*</span></label>
                                                    <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="member_id" >
                                                        @foreach($allmembers as $s)

                                                                <option value="{{$s->firstname." ".$s->lastname}}">{{$s->firstname." ".$s->lastname}}</option>


                                                        @endforeach


                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="emailAddress">Amount :<span class="text-danger">*</span></label>
                                                    <input type="text" name="amount" parsley-trigger="change" required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"
                                                           class="form-control" id="emailAddress">
                                                </div>
                                                <div class="form-group">
                                                    <label for="emailAddress">Description :<span class="text-danger">*</span></label>
                                                    <textarea class="form-control" name="description" rows="4"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="passWord2">Service Type<span class="text-danger">*</span></label>
                                                    <select class="selectpicker" data-style="btn-default" name="servicetype">
                                                        @foreach($servicetype as $s)

                                                            <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                {{csrf_field()}}
                                                <div class="form-group text-right m-b-0">
                                                    <button style="background-color:#006D5B;color: white;" class="btn waves-effect waves-light" type="submit">
                                                        Submit
                                                    </button>
                                                    <button type="reset" class="btn btn-default waves-effect m-l-5">
                                                        Cancel
                                                    </button>
                                                </div>
                                            </div>
                                                </form>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Year</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$thank_year->total}}</span></h2>
                                                </div>
                                            </div>
                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Month</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$thank_month->total}}</span></h2>
                                                </div>
                                            </div>
                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Week</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$thank_week->total}}</span></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="navpills-3" class="tab-pane">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <form  action="excelImportprojectOffering" class="form-horizontal" method="post" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="file" name="excel" />
                                                        <button class="btn " style="background-color:#006D5B;color: white;">Import ExcelFile</button>
                                                    </form>
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelproject/xls"><button class="btn " style="background-color:#006D5B;color: white;">Download xls</button></a>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelproject/xlsx"><button class="btn " style="background-color:#006D5B;color: white;">Download xlsx</button></a>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelproject/csv"><button class="btn" style="background-color:#006D5B;color: white;">Download CSV</button></a>

                                                </div>
                                            </div>
                                            <form action="saveoffering" method="post" data-parsley-validate="">
                                                <div class="p-20">
                                                    <div class="form-group">
                                                        <label for="userName">Offering Number :<span class="text-danger">*</span></label>
                                                        <input type="text" name="number" value="{{mt_rand(100000, 999999).'P'}}"
                                                               class="form-control" readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="userName">Date Received :<span class="text-danger">*</span></label>
                                                        <input type="date" name="date_submitted" required
                                                               class="form-control" id="userName">
                                                    </div>
                                                    <input type="hidden" name="offeringtype" value="projectoffering"/>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Member Name:<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="member_id" >
                                                            @foreach($allmembers as $s)

                                                                <option value="{{$s->firstname." ".$s->lastname}}">{{$s->firstname." ".$s->lastname}}</option>


                                                            @endforeach
                                                                <option value="{{"others"}}">{{"others"}}</option>


                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Amount :<span class="text-danger">*</span></label>
                                                        <input type="text" name="amount" required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"
                                                               class="form-control" id="emailAddress">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Description :<span class="text-danger">*</span></label>
                                                        <textarea class="form-control" name="description" rows="4"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="passWord2">Service Type<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-style="btn-default" name="servicetype">
                                                            @foreach($servicetype as $s)

                                                                <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    {{csrf_field()}}
                                                    <div class="form-group text-right m-b-0">
                                                        <button style="background-color:#006D5B;color: white;" class="btn waves-effect waves-light" type="submit">
                                                            Submit
                                                        </button>
                                                        <button type="reset" class="btn btn-default waves-effect m-l-5">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">cuurent year</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$project_year->total}}</span></h2>
                                                </div>
                                            </div>
                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">current month</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$project_month->total}}</span></h2>
                                                </div>
                                            </div>
                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">current week</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$project_week->total}}</span></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="navpills-4" class="tab-pane">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <form  action="excelImporttitheOffering" class="form-horizontal" method="post" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="file" name="excel" />
                                                        <button class="btn " style="background-color:#006D5B;color: white;">Import ExcelFile</button>
                                                    </form>
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExceltithe/xls"><button class="btn " style="background-color:#006D5B;color: white;">Download xls</button></a>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExceltithe/xlsx"><button class="btn " style="background-color:#006D5B;color: white;">Download xlsx</button></a>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExceltithe/csv"><button class="btn" style="background-color:#006D5B;color: white;">Download CSV</button></a>

                                                </div>
                                            </div>
                                            <form action="saveoffering" method="post" data-parsley-validate="">
                                                <div class="p-20">
                                                    <div class="form-group">
                                                        <label for="userName">Offering Number :<span class="text-danger">*</span></label>
                                                        <input  pattern=".{7,7}" type="text" name="number"
                                                               class="form-control" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="userName">Date Received :<span class="text-danger">*</span></label>
                                                        <input type="date" name="date_submitted"required
                                                               class="form-control" id="userName">
                                                    </div>
                                                    <input type="hidden" name="offeringtype" value="titheoffering"/>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Member Name:<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="member_id" >
                                                            @foreach($allmembers as $s)

                                                                <option value="{{$s->firstname." ".$s->lastname}}">{{$s->firstname." ".$s->lastname}}</option>


                                                            @endforeach


                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Amount :<span class="text-danger">*</span></label>
                                                        <input type="text" name="amount" required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"
                                                               class="form-control" id="emailAddress">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Description :<span class="text-danger">*</span></label>
                                                        <textarea class="form-control" name="description" rows="4"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="passWord2">Service Type<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-style="btn-default" name="servicetype">
                                                            @foreach($servicetype as $s)

                                                                <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    {{csrf_field()}}
                                                    <div class="form-group text-right m-b-0">
                                                        <button style="background-color:#006D5B;color: white;" class="btn  waves-effect waves-light" type="submit">
                                                            Submit
                                                        </button>
                                                        <button type="reset" class="btn  waves-effect m-l-5" >
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Year</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$tithe_year->total}}</span></h2>
                                                </div>
                                            </div>
                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Month</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$tithe_month->total}}</span></h2>
                                                </div>
                                            </div>
                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Week</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$tithe_week->total}}</span></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="navpills-5" class="tab-pane">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <form  action="excelImportpledgeOffering" class="form-horizontal" method="post" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="file" name="excel" />
                                                        <button class="btn " style="background-color:#006D5B;color: white;">Import ExcelFile</button>
                                                    </form>
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelpledge/xls"><button class="btn " style="background-color:#006D5B;color: white;">Download xls</button></a>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelpledge/xlsx"><button class="btn " style="background-color:#006D5B;color: white;">Download xlsx</button></a>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelpledge/csv"><button class="btn" style="background-color:#006D5B;color: white;">Download CSV</button></a>

                                                </div>
                                            </div>
                                            <form action="saveoffering" method="post" data-parsley-validate="">
                                                <div class="p-20">
                                                    <div class="form-group">
                                                        <label for="userName">Offering Number :<span class="text-danger">*</span></label>
                                                        <input type="text" name="number" value="{{mt_rand(100000, 999999).'PD'}}"
                                                               class="form-control" readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="userName">Date Received :<span class="text-danger">*</span></label>
                                                        <input type="date" name="date_submitted" required
                                                               class="form-control" id="userName">
                                                    </div>
                                                    <input type="hidden" name="offeringtype" value="pledgeoffering"/>
                                                    <input type="hidden" name="member_id" value="ICGC"/>

                                                    <div class="form-group">
                                                        <label for="emailAddress">Amount :<span class="text-danger">*</span></label>
                                                        <input type="text" name="amount" parsley-trigger="change" required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"
                                                               class="form-control" id="emailAddress">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Description :<span class="text-danger">*</span></label>
                                                        <textarea class="form-control" name="description" rows="4"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="passWord2">Service Type<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-style="btn-default" name="servicetype">
                                                            @foreach($servicetype as $s)

                                                                <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    {{csrf_field()}}
                                                    <div class="form-group text-right m-b-0">
                                                        <button style="background-color:#006D5B;color: white;" class="btn waves-effect waves-light" type="submit">
                                                            Submit
                                                        </button>
                                                        <button type="reset" class="btn btn-default waves-effect m-l-5">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Year</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$pledge_year->total}}</span></h2>
                                                </div>
                                            </div>
                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Month</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$pledge_month->total}}</span></h2>
                                                </div>
                                            </div>
                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Week</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$pledge_week->total}}</span></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="navpills-6" class="tab-pane">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <form  action="excelImportwelfareOffering" class="form-horizontal" method="post" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="file" name="excel" />
                                                        <button class="btn " style="background-color:#006D5B;color: white;">Import ExcelFile</button>
                                                    </form>
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelwelfare/xls"><button class="btn " style="background-color:#006D5B;color: white;">Download xls</button></a>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelwelfare/xlsx"><button class="btn " style="background-color:#006D5B;color: white;">Download xlsx</button></a>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelwelfare/csv"><button class="btn" style="background-color:#006D5B;color: white;">Download CSV</button></a>

                                                </div>
                                            </div>
                                            <form action="saveoffering" method="post" data-parsley-validate="">
                                                <div class="p-20">
                                                    <div class="form-group">
                                                        <label for="userName">Offering Number :<span class="text-danger">*</span></label>
                                                        <input type="text" name="number" value="{{mt_rand(100000, 999999).'W'}}"
                                                               class="form-control" readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="userName">Date Received :<span class="text-danger">*</span></label>
                                                        <input type="date" name="date_submitted" required
                                                               class="form-control" id="userName">
                                                    </div>
                                                    <input type="hidden" name="offeringtype" value="welfareoffering"/>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Member Name:<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="member_id" >
                                                            @foreach($allmembers as $s)

                                                                <option value="{{$s->firstname." ".$s->lastname}}">{{$s->firstname." ".$s->lastname}}</option>


                                                            @endforeach


                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Amount :<span class="text-danger">*</span></label>
                                                        <input type="text" name="amount" parsley-trigger="change" required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"
                                                               class="form-control" id="emailAddress">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Description :<span class="text-danger">*</span></label>
                                                        <textarea class="form-control" name="description" rows="4"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="passWord2">Service Type<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-style="btn-default" name="servicetype">
                                                            @foreach($servicetype as $s)

                                                                <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    {{csrf_field()}}
                                                    <div class="form-group text-right m-b-0">
                                                        <button style="background-color:#006D5B;color: white;" class="btn waves-effect waves-light" type="submit">
                                                            Submit
                                                        </button>
                                                        <button type="reset" class="btn btn-default waves-effect m-l-5">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Year</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$welfare_year->total}}</span></h2>
                                                </div>
                                            </div>
                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Month</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$welfare_month->total}}</span></h2>
                                                </div>
                                            </div>
                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Week</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$welfare_week->total}}</span></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="navpills-7" class="tab-pane">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <form  action="excelImportotherOffering" class="form-horizontal" method="post" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="file" name="excel" />
                                                        <button class="btn " style="background-color:#006D5B;color: white;">Import ExcelFile</button>
                                                    </form>
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelother/xls"><button class="btn " style="background-color:#006D5B;color: white;">Download xls</button></a>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelother/xlsx"><button class="btn " style="background-color:#006D5B;color: white;">Download xlsx</button></a>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExcelother/csv"><button class="btn" style="background-color:#006D5B;color: white;">Download CSV</button></a>

                                                </div>
                                            </div>
                                            <form action="saveoffering" method="post" data-parsley-validate="">
                                                <div class="p-20">
                                                    <div class="form-group">
                                                        <label for="userName">Offering Number :<span class="text-danger">*</span></label>
                                                        <input type="text" name="number" value="{{mt_rand(100000, 999999).'O'}}"
                                                               class="form-control" readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="userName">Date Received :<span class="text-danger">*</span></label>
                                                        <input type="date" name="date_submitted" required
                                                               class="form-control" id="userName">
                                                    </div>
                                                    <input type="hidden" name="offeringtype" value="othersoffering"/>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Member Name:<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="member_id" >
                                                            @foreach($allmembers as $s)

                                                                <option value="{{$s->firstname." ".$s->lastname}}">{{$s->firstname." ".$s->lastname}}</option>


                                                            @endforeach
                                                                <option value="{{"others"}}">{{"others"}}</option>


                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Amount :<span class="text-danger">*</span></label>
                                                        <input type="text" name="amount" parsley-trigger="change" required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"
                                                               class="form-control" id="emailAddress">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Description :<span class="text-danger">*</span></label>
                                                        <textarea class="form-control" name="description" rows="4"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="passWord2">Service Type<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-style="btn-default" name="servicetype">
                                                            @foreach($servicetype as $s)

                                                                <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    {{csrf_field()}}
                                                    <div class="form-group text-right m-b-0">
                                                        <button style="background-color:#006D5B;color: white;" class="btn waves-effect waves-light" type="submit">
                                                            Submit
                                                        </button>
                                                        <button type="reset" class="btn btn-default waves-effect m-l-5">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Year</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$other_year->total}}</span></h2>
                                                </div>
                                            </div>
                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Month</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$other_month->total}}</span></h2>
                                                </div>
                                            </div>
                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Week</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$other_week->total}}</span></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="navpills-8" class="tab-pane">

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <form  action="excelImportDayBornOffering" class="form-horizontal" method="post" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="file" name="excel" />
                                                        <button class="btn " style="background-color:#006D5B;color: white;">Import ExcelFile</button>
                                                    </form>
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExceldayborn/xls"><button class="btn " style="background-color:#006D5B;color: white;">Download xls</button></a>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExceldayborn/xlsx"><button class="btn " style="background-color:#006D5B;color: white;">Download xlsx</button></a>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="downloadExceldayborn/csv"><button class="btn" style="background-color:#006D5B;color: white;">Download CSV</button></a>

                                                </div>
                                            </div>
                                             <form action="savedaybornoffering" method="post" data-parsley-validate="" enctype="multipart/form-data">

                                                <div class="p-20">
                                                    <div class="form-group">
                                                        <label for="userName">Date Received :<span class="text-danger">*</span></label>
                                                        <input type="date" name="date_received" required
                                                               class="form-control" id="date_received">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="dayborn">Day Born:<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="dayborn" >

                                                            <option value="sunday">Sunday</option>
                                                            <option value="monday">Monday</option>
                                                            <option value="tuesday">Tuesday</option>
                                                            <option value="wednesday">Wednesday</option>
                                                            <option value="thursday">Thursday</option>
                                                            <option value="friday">Friday</option>


                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="userName">Amount :<span class="text-danger">*</span></label>
                                                        <input type="text" name="amount_received" required
                                                               class="form-control" id="amount_received">
                                                    </div>

                                                    {{csrf_field()}}
                                                    <div class="form-group text-right m-b-0">
                                                        <button style="background-color:#006D5B;color: white;" class="btn waves-effect waves-light" type="submit">
                                                            Submit
                                                        </button>
                                                        <button type="reset" class="btn btn-default waves-effect m-l-5">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Year</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$dayborn_year->total}}</span></h2>
                                                </div>
                                            </div>
                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Month</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$dayborn_month->total}}</span></h2>
                                                </div>
                                            </div>
                                            <div class="card-box widget-box-three" style="background-color:#006D5B;color: white;">
                                                <div class=" pull-left">
                                                    <i class="fa fa-check-square fa-5x"  aria-hidden="true"></i>
                                                </div>
                                                <div class="text-right">
                                                    <p class="text-muted m-t-5 text-uppercase font-600 font-secondary" style="color: white;">Current Week</p>
                                                    <h2 class="m-b-10" style="color: white;">GHC <span data-plugin="counterup">{{$dayborn_week->total}}</span></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12" >
                    <div  id="container_chart" style=" margin: 0 auto"></div>
                </div>
            </div>
            <!-- end row -->


        </div> <!-- container -->

    </div> <!-- content -->

    <footer class="footer text-right">
        {{date('Y')}} © 1CGC
    </footer>

</div>
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>

<script>
    $(function() {
        // Handler for .ready() called.

        $.ajax({
            type: 'POST',
            url: '{{URL::to('offeringchart')}}',
            data: {
                '_token': "{{ csrf_token() }}"


            },
            success:function(data){

//                alert(data);
                var res = data.split("|");
                var main_01 =parseInt(res[0]) ;
                var thank_01 =parseInt(res[1]);
                var project_01 =parseInt(res[2]);
                var tithe_01 =parseInt(res[2]);
                var pledge_01 =parseInt(res[4]);
                var welfare_01 =parseInt(res[5]);
                var other_01 =parseInt(res[6]);


                var main_02 =parseInt(res[7]) ;
                var thank_02 =parseInt(res[8]);
                var project_02 =parseInt(res[9]);
                var tithe_02 =parseInt(res[10]);
                var pledge_02 =parseInt(res[11]);
                var welfare_02 =parseInt(res[12]);
                var other_02 =parseInt(res[13]);


                var main_03 =parseInt(res[14]) ;
                var thank_03 =parseInt(res[15]);
                var project_03 =parseInt(res[16]);
                var tithe_03 =parseInt(res[17]);
                var pledge_03 =parseInt(res[18]);
                var welfare_03 =parseInt(res[19]);
                var other_03 =parseInt(res[20]);

                var main_04 =parseInt(res[21]) ;
                var thank_04 =parseInt(res[22]);
                var project_04 =parseInt(res[23]);
                var tithe_04 =parseInt(res[24]);
                var pledge_04 =parseInt(res[25]);
                var welfare_04 =parseInt(res[26]);
                var other_04 =parseInt(res[27]);

                var main_05 =parseInt(res[28]) ;
                var thank_05 =parseInt(res[29]);
                var project_05 =parseInt(res[30]);
                var tithe_05 =parseInt(res[31]);
                var pledge_05 =parseInt(res[32]);
                var welfare_05 =parseInt(res[33]);
                var other_05 =parseInt(res[34]);


                var main_06 =parseInt(res[35]) ;
                var thank_06 =parseInt(res[36]);
                var project_06 =parseInt(res[37]);
                var tithe_06 =parseInt(res[38]);
                var pledge_06 =parseInt(res[39]);
                var welfare_06 =parseInt(res[40]);
                var other_06 =parseInt(res[41]);


                var main_07 =parseInt(res[42]) ;
                var thank_07 =parseInt(res[43]);
                var project_07 =parseInt(res[44]);
                var tithe_07 =parseInt(res[45]);
                var pledge_07 =parseInt(res[46]);
                var welfare_07 =parseInt(res[47]);
                var other_07 =parseInt(res[48]);

                var main_08 =parseInt(res[49]) ;
                var thank_08 =parseInt(res[50]);
                var project_08 =parseInt(res[51]);
                var tithe_08 =parseInt(res[52]);
                var pledge_08 =parseInt(res[53]);
                var welfare_08 =parseInt(res[54]);
                var other_08 =parseInt(res[55]);


                var main_09 =parseInt(res[56]) ;
                var thank_09 =parseInt(res[57]);
                var project_09 =parseInt(res[58]);
                var tithe_09 =parseInt(res[59]);
                var pledge_09 =parseInt(res[60]);
                var welfare_09 =parseInt(res[61]);
                var other_09 =parseInt(res[62]);


                var main_10 =parseInt(res[63]) ;
                var thank_10 =parseInt(res[64]);
                var project_10 =parseInt(res[65]);
                var tithe_10 =parseInt(res[66]);
                var pledge_10 =parseInt(res[67]);
                var welfare_10 =parseInt(res[68]);
                var other_10 =parseInt(res[69]);


                var main_11 =parseInt(res[70]) ;
                var thank_11 =parseInt(res[71]);
                var project_11 =parseInt(res[72]);
                var tithe_11 =parseInt(res[73]);
                var pledge_11 =parseInt(res[74]);
                var welfare_11 =parseInt(res[75]);
                var other_11 =parseInt(res[76]);


                var main_12 =parseInt(res[77]) ;
                var thank_12 =parseInt(res[78]);
                var project_12 =parseInt(res[79]);
                var tithe_12 =parseInt(res[80]);
                var pledge_12 =parseInt(res[81]);
                var welfare_12 =parseInt(res[82]);
                var other_12 =parseInt(res[83]);

                Highcharts.chart('container_chart', {
                    chart: {
                        type: 'line'
                    },
                    title: {
                        text: 'Monthly Total Offering'
                    },
                    subtitle: {
                        text: 'Source: icgcinternational.com'
                    },
                    xAxis: {
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    },
                    yAxis: {
                        title: {
                            text: 'Amount (GHC)'
                        }
                    },
                    plotOptions: {
                        line: {
                            dataLabels: {
                                enabled: true
                            },
                            enableMouseTracking: false
                        }
                    },
                    series: [{
                        name: 'Main',
                        data: [main_01, main_02, main_03, main_04, main_05, main_06,main_07, main_08, main_09, main_10, main_11, main_12]
                    }, {
                        name: 'Pledge',
                        data: [pledge_01, pledge_02, pledge_03, pledge_04, pledge_05, pledge_06, pledge_07, pledge_08, pledge_09, pledge_10, pledge_11, pledge_12]
                    } ,{
                        name: 'Welfare',
                        data: [welfare_01, welfare_02, welfare_03, welfare_04, welfare_05, welfare_06, welfare_07, welfare_08, welfare_09  ,welfare_10, welfare_11, welfare_12]
                    },{
                        name: 'Thanksgiving',
                        data: [thank_01, thank_02, thank_03, thank_04, thank_05, thank_06, thank_07, thank_08, thank_09, thank_10, thank_11, thank_12]
                    },{
                        name: 'Project ',
                        data: [project_01, project_02, project_03, project_04, project_05, project_06, project_07, project_08, project_09, project_10, project_11, project_12]
                    },{
                        name: 'Other ',
                        data: [other_01, other_02, other_03, other_04, other_05, other_06, other_07, other_08, other_09, other_10,other_11, other_12]
                    },{
                        name: 'Tithe ',
                        data: [tithe_01, tithe_02, tithe_03, tithe_04, tithe_05, tithe_06, tithe_07, tithe_08, tithe_09, tithe_10, tithe_11, tithe_12]
                    }]
                });
            }
        });
    });
</script>
        <script>

        </script>
        </div>

@endsection
        <!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->