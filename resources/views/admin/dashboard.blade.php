


@extends('layout.localmaster')

@section('content')
<!-- Begin page -->
<div id="wrapper">


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu" style="background-color: #006D5B">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                {{--<div class="user-details">--}}
                {{--<div class="overlay"></div>--}}
                {{--<div class="text-center">--}}
                {{--<img src="assets/images/users/church.jpg" alt="" class="thumb-md img-circle">--}}
                {{--</div>--}}
                {{--<div class="user-info">--}}
                {{--<div>--}}
                {{--<a href="index.html#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">mamprobi branch <span class="mdi mdi-menu-down"></span></a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="dropdown" id="setting-dropdown">--}}
                {{--<ul class="dropdown-menu">--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-face-profile m-r-5"></i> Profile</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-account-settings-variant m-r-5"></i> Settings</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-lock m-r-5"></i> Lock screen</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-logout m-r-5"></i> Logout</a></li>--}}
                {{--</ul>--}}
                {{--</div>--}}

                <ul>

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub" >
                        <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>

                    </li>
                   @endif

                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="newmember" style="color: white">New Member</a></li>
                            <li><a href="allmember"style="color: white">All Members</a></li>

                        </ul>
                    </li>
                        @endif
                        <li class="has_sub">
                            <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>

                        </li>
                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-calendar" style="color: white" aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="newevent" style="color: white">New Event</a></li>

                                </ul>
                            </li>
                        @endif
                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin'||'Finance')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="offering" style="color: white">New</a></li>
                            <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                            <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                            <li><a href="expenses" style="color: white"> Expenses</a></li>

                        </ul>
                    </li>
                      @endif

                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">

                            <li><a href="report" style="color: white"> Reports</a></li>
                            <li><a href="quarterlyrpt" style="color: white">Quaterly Reports</a></li>
                            <li><a href="weeklyrpt" style="color: white">Weeky Reports</a></li>

                        </ul>
                    </li>
                        @endif

                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="newattendance" style="color: white">New Attendance</a></li>
                            <li><a href="allattendance" style="color: white">All Attendance</a></li>

                        </ul>
                    </li>
                        @endif

                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="singlesms" style="color: white">Single</a></li>
                            <li><a href="bulksms" style="color: white">Bulk</a></li>

                        </ul>
                    </li>
                        @endif
                   @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="profile" style="color: white">Church Profile</a></li>
                            <li><a href="user" style="color: white">Users</a></li>
                            <li><a href="service " style="color: white">Service</a></li>
                            <li><a href="member_group" style="color: white">Member Group</a></li>
                            <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>


                        </ul>
                    </li>
                    @endif


                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>


        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->




    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Dashboard</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="dashboard">ICGC</a>
                                </li>
                                <li>
                                    <a href="dashboard">Dashboard</a>
                                </li>
                                <li class="active">
                                    Dashboard
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="row text-center">

                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <div class="card-box widget-box-one">
                            <div class="wigdet-one-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Total members</p>
                                <h2 class="text-danger"><span data-plugin="counterup">{{$totalmembers}}</span></h2>
                                <p class="text-muted m-0"><b>Male:</b> {{$totalmembers_male}}</p>
                                <p class="text-muted m-0"><b>Female:</b> {{$totalmembers_female}}</p>

                            </div>
                        </div>
                    </div><!-- end col -->

                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <div class="card-box widget-box-one">
                            <div class="wigdet-one-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Total Groups</p>
                                <h2 class="text-dark"><span data-plugin="counterup">{{$totalgroups}}</span> </h2>
                                <p class="text-muted m-0"><b>Active:</b> {{ $totalgroups_active}}</p>
                                <p class="text-muted m-0"><b>Inactive:</b>{{$totalgroups_inactive}}</p>

                            </div>
                        </div>
                    </div><!-- end col -->

                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <div class="card-box widget-box-one">
                            <div class="wigdet-one-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Total money</p>
                                <h2 class="text-success">GHC <span data-plugin="counterup">{{$totaltithe_total}}</span></h2>
                                <p class="text-muted m-0"><b>Main offering:GHC </b>{{$totaltithe_main}} </p>
                                <p class="text-muted m-0"><b>Tithe offering:GHC </b> {{$totaltithe_tithe}}</p>

                            </div>
                        </div>
                    </div><!-- end col -->

                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <div class="card-box widget-box-one">
                            <div class="wigdet-one-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Week Attendance</p>
                                <h2 class="text-warning"><span data-plugin="counterup">{{$previousattendance}}</span> </h2>
                                <p class="text-muted m-0"><b>Male:</b>{{$previousattend_male}}</p>
                                <p class="text-muted m-0"><b>Female:</b>{{$previousattend_female}}</p>

                            </div>
                        </div>
                    </div><!-- end col -->

                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <div class="card-box widget-box-one">
                            <div class="wigdet-one-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Visitors</p>
                                <h2 class="text-primary"><span data-plugin="counterup">{{$totalmembers_visitors}}</span> </h2>
                                <p class="text-muted m-0"><b>Male:</b> {{$totalmembers_male_visitors}}</p>
                                <p class="text-muted m-0"><b>Female:</b>{{$totalmembers_female_visitors}}</p>

                            </div>
                        </div>
                    </div><!-- end col -->

                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <div class="card-box widget-box-one">
                            <div class="wigdet-one-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Total Sent SMS</p>
                                <h2 class="text-danger"><span data-plugin="counterup">0</span> </h2>
                                <p class="text-muted m-0"><b>Single:</b> 0k</p>
                                <p class="text-muted m-0"><b>Bulk:</b> 0k</p>

                            </div>
                        </div>
                    </div><!-- end col -->
                    <hr style="background-color:#006D5B">
                </div>
                <!-- end row -->



                <div class="row ">
                    <div class="col-lg-8 m-t-20">

                        <div class="card-box demo-box">


    <div id="container" style="min-width: 310px;height: 400px; margin: 0 auto"> </div>


                        </div> <!-- demo-box -->

                    </div> <!-- end row -->
                    <div class="col-lg-4 m-t-20">

                        <div class="card-box demo-box">
                            <div id="container2" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                        </div> <!-- demo-box -->

                    </div> <!-- end row -->
                </div>
                <!-- end row -->





                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-color panel-green">
                            <div class="panel-heading" style="background-color:#006D5B;color: white;">
                                <h3 class="panel-title">Upcoming birthdays for {{date('M Y')}} <span class="badge up bg-danger" style="font-size: larger">{{$birthdaymemberscount}}</span></h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table table-hover m-0">
                                        <thead>
                                        <tr>
                                            <th>Photo</th>
                                            <th>Member Name</th>
                                            <th>Phone</th>
                                            <th>Nationality</th>
                                            <th>Date</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($birthdaymembers as $b)
                                            <tr>
                                                <th>
                                                    <img src="uploads/avatars/{{$b->photo}}" alt="user" class="thumb-sm img-circle" />
                                                </th>
                                                <td>
                                                    <h5 class="m-0">{{$b->fullname}}</h5>
                                                    <p class="m-0 text-muted font-13"><small>{{$b->occupation}}</small></p>
                                                </td>
                                                <td>{{$b->telephone}}</td>
                                                <td>{{$b->nationality}}</td>
                                                <td>{{$b->created_at}}</td>

                                            </tr>

                                        @endforeach

                                        <span style="color:#17202A">{{$birthdaymembers->render()}}</span>

                                        </tbody>
                                    </table>
                                    {{ $newlymembers->links() }}
                                </div> <!-- table-responsive -->
                            </div>
                        </div>

                    </div>
                    <!-- end col -->

                    <div class="col-lg-6">
                        <div class="panel" >
                            <div class="panel-heading" style="background-color:#006D5B;color: white;">
                                <h3 class="panel-title">Recent Added Members <span class="badge up bg-danger" style="font-size: larger">{{$newlymemberscount}}</span></h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table table-hover m-0">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>User Name</th>
                                            <th>Phone</th>
                                            <th>Location</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($newlymembers as $n)
                                            <tr>
                                                <th>
                                                    <span class="avatar-sm-box bg-success">{{$n->fullname[0]}}</span>
                                                </th>
                                                <td>
                                                    <h5 class="m-0">{{$n->fullname}}</h5>
                                                    <p class="m-0 text-muted font-13"><small>{{$n->occupation}}</small></p>
                                                </td>
                                                <td>{{$n->telephone}}</td>
                                                <td>{{$n->nationality}}</td>
                                                <td>{{$n->created_at}}</td>
                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>
                                    {{ $newlymembers->links() }}
                                </div> <!-- table-responsive -->
                            </div>
                        </div> <!-- end panel -->

                    </div>
                    <!-- end col -->

                </div>
                <!-- end row -->




















            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            {{date('Y')}} © 1CGC
        </footer>

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <!-- Right Sidebar -->
    <div class="side-bar right-bar">
        <a href="javascript:void(0);" class="right-bar-toggle">
            <i class="mdi mdi-close-circle-outline"></i>
        </a>
        <h4 class="">Settings</h4>
        <div class="setting-list nicescroll">
            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">Notifications</h5>
                    <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">API Access</h5>
                    <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">Auto Updates</h5>
                    <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">Online Status</h5>
                    <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>
        </div>
    </div>
    <!-- /Right-bar -->

</div>
<!-- END wrapper -->
<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script>
    $(function() {
        // Handler for .ready() called.
        var d = new Date();
        var current_year = d.getFullYear();

        $.ajax({
            type:'POST' ,
            url: '{{URL::to('chart')}}',
            data:{
                '_token':"{{ csrf_token() }}",
                'current_year':current_year



            },
            success:function(data){
  //alert(data);
                var res = data.split("|");
                var member_01 =parseInt(res[0]) ;
                var visitor_01 =parseInt(res[1]);

                var member_02 =parseInt(res[2]) ;
                var visitor_02 =parseInt(res[3]);

                var member_03 =parseInt(res[4]) ;
                var visitor_03 =parseInt(res[5]);

                var member_04 =parseInt(res[6]) ;
                var visitor_04 =parseInt(res[7]);

                var member_05 =parseInt(res[8]) ;
                var visitor_05 =parseInt(res[9]);

                var member_06 =parseInt(res[10]) ;
                var visitor_06 =parseInt(res[11]);

                var member_07 =parseInt(res[12]) ;
                var visitor_07 =parseInt(res[13]);

                var member_08 =parseInt(res[14]) ;
                var visitor_08 =parseInt(res[15]);

                var member_09 =parseInt(res[16]) ;
                var visitor_09 =parseInt(res[17]);

                var member_10 =parseInt(res[18]) ;
                var visitor_10 =parseInt(res[19]);

                var member_11 =parseInt(res[20]) ;
                var visitor_11 =parseInt(res[21]);

                var member_12 =parseInt(res[22]) ;
                var visitor_12 =parseInt(res[23]);

                var regvisitor_01=parseInt(res[24]);
                var regvisitor_02=parseInt(res[25]);
                var regvisitor_03=parseInt(res[26]);
                var regvisitor_04=parseInt(res[27]);
                var regvisitor_05=parseInt(res[28]);
                var regvisitor_06=parseInt(res[29]);
                var regvisitor_07=parseInt(res[30]);
                var regvisitor_08=parseInt(res[31]);
                var regvisitor_09=parseInt(res[32]);
                var regvisitor_10=parseInt(res[33]);
                var regvisitor_11=parseInt(res[35]);
                var regvisitor_12=parseInt(res[36]);
                Highcharts.chart('container', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Monthly Average Church growth <?php echo date('Y');?>'
                    },
                    subtitle: {
                        text: 'Source: icgcinternational.com'
                    },
                    xAxis: {
                        categories: [
                            'Jan',
                            'Feb',
                            'Mar',
                            'Apr',
                            'May',
                            'Jun',
                            'Jul',
                            'Aug',
                            'Sep',
                            'Oct',
                            'Nov',
                            'Dec'
                        ],
                        crosshair: true
                    },
                    yAxis: {
                        allowDecimals: false,
                        min: null,
                        title: {
                            text: 'Members (Count)'
                        }
                    },
                    tooltip: {

                        allowDecimals: false,
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        name: 'Members',
                        data: [member_01, member_02, member_03, member_04, member_05, member_06, member_07, member_08, member_09, member_10, member_11, member_12]

                    }, {
                        name: 'Visitor',
                        data: [visitor_01, visitor_02,visitor_03, visitor_04, visitor_05,visitor_06,visitor_07,visitor_08, visitor_09, visitor_10, visitor_11, visitor_12]

                    }, {
                        name: 'Regular Visitor',
                        data: [ regvisitor_01, regvisitor_02,regvisitor_03, regvisitor_04, regvisitor_05,regvisitor_06,regvisitor_07,regvisitor_08, regvisitor_09, regvisitor_10, regvisitor_11, regvisitor_12]

                    }]
                });

            }
        });
///////////////////////////////////pie chart/////////////
        $.ajax({
            type: 'POST',
            url: '{{URL::to('piechart')}}',
            data: {
                '_token': "{{ csrf_token() }}",
                'current_year': current_year


            },
            success: function (data) {
//                alert(data);
                var res = data.split("|");
                var total_total =parseFloat(res[0]) ;
                var total_main =parseFloat(res[1]) ;
                var total_thanks =parseFloat(res[2]) ;
                var total_project =parseFloat(res[3]) ;
                var total_tithe =parseFloat(res[4]) ;
                var total_pledge =parseFloat(res[5]) ;
                var total_welfare =parseFloat(res[6]) ;
                var total_others =parseFloat(res[7]) ;
                Highcharts.chart('container2', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'General Finance <?php echo date('Y');?> (GHC '+total_total+')'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                            name: 'main Offering',
                            y: total_main
                        }, {
                            name: 'Tithe',
                            y: total_tithe,
                            sliced: true,
                            selected: true
                        }, {
                            name: 'project',
                            y: total_project
                        }, {
                            name: 'Thanksgiving',
                            y: total_thanks
                        }, {
                            name: 'Pledge',
                            y: total_pledge
                        }, {
                            name: 'Welfare',
                            y: total_welfare
                        },{
                            name: 'Others',
                            y: total_others
                        }]
                    }]
                });
            }
        });

        /////////////////////////
    });

</script>
<script>

</script>
@endsection
