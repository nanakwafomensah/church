

@extends('layout.localmaster')

<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/personal/datatable.css">
<link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />

@section('content')
<!-- Begin page -->
<div id="wrapper">


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu" style="background-color: #006D5B">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">


                <ul>

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub" >
                            <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>

                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newmember" style="color: white">New Member</a></li>
                                <li><a href="allmember"style="color: white">All Members</a></li>

                            </ul>
                        </li>
                    @endif
                        <li class="has_sub">
                            <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>

                        </li>
                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-calendar" style="color: white" aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="newevent" style="color: white">New Event</a></li>

                                </ul>
                            </li>
                        @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin'||'Finance')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="offering" style="color: white">New</a></li>
                                <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                                <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                                <li><a href="expenses" style="color: white"> Expenses</a></li>

                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">

                                <li><a href="report" style="color: white"> Reports</a></li>
                                <li><a href="quarterlyrpt" style="color: white">Quaterly Reports</a></li>
                                <li><a href="weeklyrpt" style="color: white">Weeky Reports</a></li>

                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newattendance" style="color: white">New Attendance</a></li>
                                <li><a href="allattendance" style="color: white">All Attendance</a></li>

                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="singlesms" style="color: white">Single</a></li>
                                <li><a href="bulksms" style="color: white">Bulk</a></li>

                            </ul>
                        </li>
                    @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="profile" style="color: white">Church Profile</a></li>
                                <li><a href="user" style="color: white">Users</a></li>
                                <li><a href="service " style="color: white">Service</a></li>
                                <li><a href="member_group" style="color: white">Member Group</a></li>
                                <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>


                            </ul>
                        </li>
                    @endif


                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>



        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">All Members</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="dashboard">ICGC</a>
                                </li>
                                <li>
                                    <a href="allmember">All Members</a>
                                </li>
                                <li class="active">
                                    All members
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->



                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">

                            <h4 class="m-t-0 header-title"><b>All Record</b></h4>


                            <table id="member-table" class="table table-striped table-bordered">
                                <thead >
                                <tr>
                                    <th>Photo</th>
                                    <th>Member ID</th>
                                    <th>Fullname</th>
                                    <th>Occupation</th>
                                    <th>Telephone</th>
                                    <th>Marital status</th>
                                    <th>Groups</th>
                                    <th>Member status</th>
                                    <th>First Fruit</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                            </table>

                        </div>
                    </div>
                </div>



                <div id="edit-Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <form action="{{'editmember'}}" method="post" enctype="multipart/form-data"  data-parsley-validate="">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title"style="color: #006D5B;">Edit Membership Information</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="col-xs-6">

                                        <h4 class="header-title">Photo</h4>

                                        {{--<div class="col-lg-6 col-md-4">--}}
                                            <div class="text-center card-box">
                                                <div class="member-card">
                                                    <div class="thumb-xl member-thumb m-b-10 center-block" id="show_picture" >
                                                        {{--<img id="preview_image" src="uploads/avatars/default.jpg" class="img-circle img-thumbnail" alt="profile-image">--}}
                                                        {{--<i class="mdi mdi-star-circle member-star text-success" title="verified user"></i>--}}
                                                    </div>


                                                    <input type="file" id="myeditphoto" name="avatar" class="form-control">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                </div></div>
                                            <!-- end row -->
                                        {{--</div>--}}

                                        <!-- end row -->



                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <input type="hidden" id="member_edit_id" name="member_edit_id"/>
                                                <label for="field-1" class="control-label">First name</label>
                                                <input type="text" class="form-control" id="firstname_edit" name="firstname_edit" placeholder="John">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">

                                                <label for="field-1" class="control-label">Last Name</label>
                                                <input type="text" class="form-control" id="lastname_edit" name="lastname_edit" placeholder="John">
                                            </div>
                                        </div>

                                      </div>
                                    <div class="row">
                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label for="field-1" class="control-label">Occupation</label>
                                                <select  data-live-search="true" class="form-control" id="occupation_edit" name="occupation_edit" data-style="btn-default" required>
                                                    <option>Not Employed</option>
                                                    <option>Pastor</option>
                                                    <option>Mason</option>
                                                    <option>Tailor</option>
                                                    <option>Trader</option>
                                                    <option>Engineer</option>

                                                </select>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">

                                                <label for="field-1" class="control-label">Nationality</label>
                                                <select name="nationality_edit" id="nationality_edit" class="form-control" required>
                                                    <option value="">Select an Option</option>
                                                    <option value="ghanaian">Ghanaian</option>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">

                                                <label for="field-1" class="control-label">Telephone</label>
                                                <input type="text" class="form-control" id="telephone_edit" name="telephone_edit" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label for="field-1" class="control-label">Lccation </label>
                                                <input type="text" class="form-control" id="hometown_edit" name="hometown_edit" placeholder="John">
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label for="field-1" class="control-label">Date of Birth</label>
                                                <input type="date" class="form-control" id="dob_edit" name="dob_edit" placeholder="John">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">

                                                <label for="field-1" class="control-label">Marital Status</label>

                                                <select class="form-control" data-style="btn-default" id="maritalstatus_edit" name="maritalstatus_edit">
                                                    <option value="">---</option>
                                                    <option value="Single">Single</option>
                                                    <option value="Married">Married</option>
                                                    <option value="Divorced">Divorced</option>
                                                    <option value="Widows">Widows</option>

                                                </select>
                                            </div>
                                            <div class="form-group" id="couplename">
                                                <label for="passWord2">Couplename</label>
                                                <input type="text" name="couplename_edit"
                                                       class="form-control" id="couplename_edit">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label for="field-1" class="control-label">Gender</label>
                                                <select class="form-control"  name="gender_edit">
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">

                                                <label for="field-1" class="control-label">Convenant Family</label>
                                                <select class="form-control" name="convenant_edit" id="convenant_edit" required>
                                                    @foreach($cf as $s)

                                                        <option value="{{$s->convenantname}}">{{$s->convenantname}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">

                                                <label for="field-1" class="control-label">First Fruit</label>

                                                <input type="text" class="form-control" id="firstfruit_edit" name="firstfruit_edit" placeholder="John" readonly>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="emailAddress">Membership Category</label>
                                                <select class="form-control" name="membership_cat_edit" id="membership_cat_edit" required>
                                                    <option value="">---</option>
                                                    <option value="adult">Adult</option>
                                                    <option value="youth">Youth</option>
                                                    <option value="children">Children</option>
                                                </select>
                                            </div>


                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group" id="level_edu">
                                                <label for="emailAddress">Level of Education</label>
                                                <select class="form-control" name="level_edu_edit" id="level_edu_edit">
                                                    <option value="">--</option>
                                                    <option value="ece">Early Childhood Education </option>
                                                    <option value="pe">Primary Education </option>
                                                    <option value="jhs">JHS</option>
                                                    <option value="shs">SHS</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="emailAddress">Registration Date</label>
                                                <input type="date" id="registration_date_edit" name="registration_date_edit" class="form-control" required/>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="emailAddress">Teachers qualification</label>
                                                <select class="form-control" id="teacher_qualification_edit" name="teacher_qualification_edit" >
                                                    <option value="">--</option>
                                                    <option value="trained">Trained</option>
                                                    <option value="nontrained">Non Trained</option>

                                                </select>
                                            </div>


                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">

                                                <p class="text-muted font-13 m-b-15 m-t-20">Passed to Glory ?</p>
                                                <div class="radio radio-single">
                                                    <input type="radio" id="singleRadio1" value="yes" name="passed_to_glory_edit" aria-label="Single radio One">
                                                    <label>Yes</label>
                                                </div>
                                                <div class="radio radio-success radio-single">
                                                    <input type="radio" id="singleRadio2" value="no" name="passed_to_glory_edit"  aria-label="Single radio Two">
                                                    <label>No</label>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">

                                                <p class="text-muted font-13 m-b-15 m-t-20">Training to do Councelling ?</p>
                                                <div class="radio radio-single">
                                                    <input type="radio" id="singleRadio1" value="yes" name="trC_edit" aria-label="Single radio One">
                                                    <label>Yes</label>
                                                </div>
                                                <div class="radio radio-success radio-single">
                                                    <input type="radio" id="singleRadio2" value="no" name="trC_edit"  aria-label="Single radio Two">
                                                    <label>No</label>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="emailAddress">Membership Status</label>
                                                <select class="form-control" id="membershipstatus_edit" name="membershipstatus_edit">
                                                    <option value="">---</option>
                                                    <option value="member">member</option>
                                                    <option value="visitor">visitor</option>
                                                    <option value="regular_visitor">Regular visitor</option>
                                                </select>
                                            </div>


                                        </div>

                                    </div>

                                <div class="row">
                                    <div class="form-group">
                                        <select multiple="multiple" class="multi-select" id="my_multi_select1" name="my_multi_select1[]" data-plugin="multiselect" required>
                                            @foreach($membergroups as $s)
                                                <option>{{$s->name}}</option>

                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                </div>
                                {{csrf_field()}}
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn waves-effect waves-light" style="background-color: #006D5B; color:white">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal -->


                <div id="delete-Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form action="{{'deletemember'}}" method="post">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" style="color: #006D5B;">Delete Member</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Are you sure you want to delete Member with id <span id="offering_number_delete_data"></span>?</label>
                                                <input type="hidden" class="form-control" id="member_id_delete" name="member_id_delete" >
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                {{csrf_field()}}
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn waves-effect waves-light" style="background-color: #006D5B;color:white">Delete</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal -->






            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            {{date('Y')}} © 1CGC
        </footer>

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Bootstrap JavaScript -->
    {{--<script src="assets/js/bootstrap.min.js"></script>--}}




     <script>
        $('#member-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!!route('datatable.allmember')!!}',
            columns: [
                {data: 'photo', name: 'photo'},
                {data: 'member_id', name: 'member_id'},
                {data: 'fullname', name: 'fullname'},
                {data: 'occupation', name: 'occupation'},
                {data: 'telephone', name: 'telephone'},
                {data: 'marital_status', name: 'marital_status'},
                {data: 'groups', name: 'groups'},
                {data: 'membershipstatus', name: 'membershipstatus'},
                {data: 'firstfruit', name: 'firstfruit'},
                {data: 'action', name: 'action'}
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column.search(val ? val : '', true, false).draw();
                            });
                });
            }
        });
    </script>
    <script>

        $(document).on('click','.editbtn',function() {
//            alert($(this).data('trc'));
            $('#show_picture').html(' <img id="preview_image" src="uploads/avatars/'+$(this).data('photo')+'" class="img-circle img-thumbnail" alt="profile-image"> <i class="mdi mdi-star-circle member-star text-success" title="verified user"></i>');

            $('#telephone_edit').val($(this).data('telephone'));
            $('#member_edit_id').val($(this).data('memberid'));
            $('#firstname_edit').val($(this).data('firstname'));
            $('#lastname_edit').val($(this).data('lastname'));
            $('#occupation_edit').val($(this).data('occupation')).change();
            $('#nationality_edit').val($(this).data('nationality')).change();
            $('#registration_date_edit').val($(this).data('registration_date'));

            if($(this).data('trc')=='no'){
                $('input:radio[name="trC_edit"][value="no"]').attr('checked', true);
            }else if($(this).data('trc')=='yes'){
                $('input:radio[name="trC_edit"][value="yes"]').attr('checked', true);
            }

            if($(this).data('passed_to_glory')=='no'){
                $('input:radio[name="passed_to_glory_edit"][value="no"]').attr('checked', true);
            }else if($(this).data('passed_to_glory')=='yes'){
                $('input:radio[name="passed_to_glory_edit"][value="yes"]').attr('checked', true);
            }

            $('#membershipstatus_edit').val($(this).data('membershipstatus')).change();
            $('#teacher_qualification_edit').val($(this).data('teacher_qualification')).change();

            $('#membership_cat_edit').val($(this).data('membershipcat')).change();
            $('#maritalstatus_edit').val($(this).data('maritalstatus')).change();
            if($(this).data('membershipcat')=='adult'){
                $('#level_edu').hide();
            }else{
                $('#level_edu_edit').val($(this).data('leveledu')).change();
            }
            if($(this).data('maritalstatus')=='Married'){
                $('#couplename').show();
                $('#couplename_edit').val($(this).data('couplename'));
            }
            $('#dob_edit').val($(this).data('dob'));
            $('#gender_edit').val($(this).data('gender')).change();
            $('#convenant_edit').val($(this).data('convenant'));
            $('#firstfruit_edit').val($(this).data('firstfruit'));
            $('#hometown_edit').val($(this).data('hometown'));
            $('#myeditphoto').val($(this).data('photo'));




            if($(this).data('couplename')== ""){
                $('#couplename').hide();
            }else{
                $('#couplename').show();
            }



        });
        $(document).on('click','.deletebtn',function() {

            $('#member_id_delete').val($(this).data('memberid'));
            $('#offering_number_delete_data').html($(this).data('firstname')+" "+$(this).data('lastname'));



        });
    </script>
    <script>
        function readURL(input){
            if(input.files && input.files[0]){
                var reader=new FileReader();

                reader.onload=function(e){
                    $('#preview_image').attr('src',e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).on('change','input[type="file"]',function(){
            readURL(this);

        })
    </script>
    <script>
        $(function(){
            $('#level_edu').hide();
            $('#couplename').hide();
        });
        $('#membership_cat_edit').change(function() {
            if($('#membership_cat_edit :selected').val() == "youth" ){
                $('#level_edu').show();
            }
            if($('#membership_cat_edit :selected').val() == "child" ){
                $('#level_edu').show();
            }
            if($('#membership_cat_edit :selected').val() == "adult" ){
                $('#level_edu').hide();
            }
            if($('#membership_cat_edit :selected').val() == "" ){
                $('#level_edu').hide();
            }
        });
        $('#maritalstatus_edit').change(function() {

            if($('#maritalstatus_edit :selected').val() == "Married" ){
                $('#couplename').show();
            }else{
                $('#couplename').hide();
            }

        });
    </script>
</div>
@endsection

