

@extends('layout.localmaster')

@section('content')
    <div id="wrapper">

        <!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu" style="background-color: #006D5B">
    <div class="sidebar-inner slimscrollleft">

        <!--- Sidemenu -->
        <div id="sidebar-menu">


            <ul>

                @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub" >
                        <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>

                    </li>
                @endif

                @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="newmember" style="color: white">New Member</a></li>
                            <li><a href="allmember"style="color: white">All Members</a></li>

                        </ul>
                    </li>
                @endif
                    <li class="has_sub">
                        <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>

                    </li>
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-calendar"style="color: white" aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newevent" style="color: white">New Event</a></li>

                            </ul>
                        </li>
                    @endif
                @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin'||'Finance')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="offering" style="color: white">New</a></li>
                            <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                            <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                            <li><a href="expenses" style="color: white"> Expenses</a></li>
                        </ul>
                    </li>
                @endif

                @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">

                            <li><a href="report" style="color: white"> Reports</a></li>
                            <li><a href="quarterlyrpt" style="color: white">Quaterly Reports</a></li>
                            <li><a href="weeklyrpt" style="color: white">Weeky Reports</a></li>

                        </ul>
                    </li>
                @endif


                @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="newattendance" style="color: white">New Attendance</a></li>
                            <li><a href="allattendance" style="color: white">All Attendance</a></li>

                        </ul>
                    </li>
                @endif

                @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="singlesms" style="color: white">Single</a></li>
                            <li><a href="bulksms" style="color: white">Bulk</a></li>

                        </ul>
                    </li>
                @endif
                @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="profile" style="color: white">Church Profile</a></li>
                            <li><a href="user" style="color: white">Users</a></li>
                            <li><a href="service " style="color: white">Service</a></li>
                            <li><a href="member_group" style="color: white">Member Group</a></li>
                            <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>


                        </ul>
                    </li>
                @endif

            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>



    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
    <!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">


            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Membership Form</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="dashboard">ICGC</a>
                            </li>
                            <li>
                                <a href="newmember">Membership Form </a>
                            </li>
                            <li class="active">
                                MemberShip Form
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="card-box">
            <div class="row">
                <form action="{{route('addmember')}}" method="post" enctype="multipart/form-data"  data-parsley-validate="">
                <div class="col-xs-6">
                    <h4 class="header-title">Photo</h4>

                        <div class="col-lg-6 col-md-4">
                        <div class="text-center card-box">
                            <div class="member-card">
                                <div class="thumb-xl member-thumb m-b-10 center-block" >
                                    <img id="preview_image" src="uploads/avatars/default.jpg" class="img-circle img-thumbnail" alt="profile-image">
                                    <i class="mdi mdi-star-circle member-star text-success" title="verified user"></i>
                                </div>


                                <input type="file" id="imgInp" name="avatar" class="form-control">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                            </div></div>
                        <!-- end row -->
                        </div>

                        <!-- end row -->



                </div><!-- end col-->
                <div class="col-xs-12">
                    <h4 class="header-title m-t-0">Personal Information</h4>
                    <div class="card-box">

                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-6">

                                <div class="p-20">
                                    <input type="hidden" name="member_id" value="{{'IC'.mt_rand(10000, 99999).'GC'}}"/>
                                        <div class="form-group">
                                            <label for="userName">First Name</label>
                                            <input type="text" name="firstname" required
                                                 class="form-control" id="userName">
                                        </div>
                                        <div class="form-group">
                                            <label for="emailAddress">Last Name</label>
                                            <input type="text" name="lastname" required
                                                   class="form-control" id="emailAddress" >
                                        </div>
                                        <div class="form-group">
                                            <label for="pass1">Nationality</label>
                                            <select name="nationality" class="form-control" required>
                                                <option value="">Select an Option</option>
                                                <option value="ghanaian">Ghanaian</option>
                                            </select>

                                        </div>

                                        <div class="form-group">
                                            <label for="passWord2">Choose your Occupation </label>
                                            <select name="occupation" data-live-search="true" class="selectpicker" data-style="btn-default" required>
                                                <option>Not Employed</option>
                                                <option>Pastor</option>
                                                <option>Mason</option>
                                                <option>Tailor</option>
                                                <option>Trader</option>
                                                <option>Engineer</option>

                                            </select>

                                        </div>





                                </div>

                            </div>

                            <div class="col-sm-12 col-xs-12 col-md-6">


                                <div class="p-20">

                                        <div class="form-group">
                                            <label for="userName">Telephone Number</label>
                                            <input required type="text" name="telephone" parsley-trigger="change"
                                                    class="form-control" id="userName">
                                        </div>
                                        <div class="form-group">
                                            <label for="emailAddress">Gender</label>
                                            <select class="selectpicker" data-style="btn-default" name="gender" required>
                                                <option value="">Select an Option</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>

                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="pass1">Date of Birth</label>
                                            <input id="pass1" type="date" name="dob" required
                                                   class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="passWord2">Marital Status</label>
                                            <select class="selectpicker" data-style="btn-default" name="marital_status" id="marital_status" required>
                                                <option value="">---</option>
                                                <option value="Single">Single</option>
                                                <option value="Married">Married</option>
                                                <option value="Divorced">Divorced</option>
                                                <option value="Widows">Widows</option>

                                            </select>
                                        </div>
                                    <div class="form-group" id="couplename">
                                        <label for="passWord2">Couplename</label>
                                        <input type="text" name="couplename"
                                               class="form-control" id="couplename">
                                    </div>
                                        <div class="form-group">
                                            <label for="passWord2">Location</label>
                                            <input required type="text" name="hometown"
                                                    class="form-control" id="passWord2">
                                        </div>




                                </div>


                            </div>
                        </div>
                        <!-- end row -->


                        <!-- end row -->


                    </div> <!-- end ard-box -->
                </div><!-- end col-->
                <div class="col-xs-12">
                    <h4 class="header-title m-t-0">Church Details</h4>
                    <div class="card-box">

                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-6">



                                <div class="p-20">

                                        <div class="form-group">
                                            <label for="userName">First Fruit Number</label>
                                            <input type="text" name="firstfruit" value="{{mt_rand(100000, 999999).'T'}}" required
                                                   class="form-control" id="userName">
                                        </div>
                                        <div class="form-group">
                                            <label for="emailAddress">Convenant Family</label>
                                            <select class="selectpicker" data-live-search="true" data-style="btn-default" name="convenant_family" required>
                                                @foreach($cf as $s)

                                                    <option value="{{$s->convenantname}}">{{$s->convenantname}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    <div class="form-group">
                                        <label for="emailAddress">Membership Status</label>
                                        <select class="selectpicker" data-style="btn-default" name="membership_status" required>
                                            <option value="">---</option>
                                            <option value="member">member</option>
                                            <option value="visitor">visitor</option>
                                            <option value="regular_visitor">Regular visitor</option>


                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="emailAddress">Membership Category</label>
                                        <select class="selectpicker" data-style="btn-default" id="membership_cat" name="membership_cat" required>
                                            <option value="">--</option>
                                            <option value="adult">Adult</option>
                                            <option value="youth">Youth</option>
                                            <option value="child">Children</option>
                                        </select>
                                    </div>
                                    <div class="form-group" id="level_edu">
                                        <label for="emailAddress">Level of Education</label>
                                        <select class="selectpicker" data-style="btn-default" name="level_edu">
                                            <option value="">--</option>
                                            <option value="ece">Early Childhood Education </option>
                                            <option value="pe">Primary Education </option>
                                            <option value="jhs">JHS</option>
                                            <option value="shs">SHS</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="emailAddress">Registration Date</label>
                                        <input type="date" class="form-control" name="registration_date" required/>
                                    </div>


                                </div>

                            </div>
                            <div class="col-sm-12 col-xs-12 col-md-6">
                                <div class="p-20">
                                    <label>Select group</label>
                                <div class="form-group">
                                    <select required multiple="multiple" class="multi-select" id="my_multi_select1" name="my_multi_select1[]" data-plugin="multiselect">
                                        @foreach($membergroups as $s)
                                        <option>{{$s->name}}</option>

                                        @endforeach
                                    </select>
                                </div>
                                    <div class="form-group">

                                        <p class="text-muted font-13 m-b-15 m-t-20">Training to do Councelling ?</p>
                                        <div class="radio radio-single">
                                            <input type="radio" id="singleRadio1" value="yes" name="trC" aria-label="Single radio One">
                                            <label>Yes</label>
                                        </div>
                                        <div class="radio radio-success radio-single">
                                            <input type="radio" id="singleRadio2" value="no" name="trC" checked aria-label="Single radio Two">
                                            <label>No</label>
                                        </div>

                                    </div>
                                    <div class="form-group">

                                        <p class="text-muted font-13 m-b-15 m-t-20">Passed to Glory ?</p>
                                        <div class="radio radio-single">
                                            <input type="radio" id="singleRadio1" value="yes" name="passed_to_glory" aria-label="Single radio One">
                                            <label>Yes</label>
                                        </div>
                                        <div class="radio radio-success radio-single">
                                            <input type="radio" id="singleRadio2" value="no" name="passed_to_glory" checked aria-label="Single radio Two">
                                            <label>No</label>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="emailAddress">Teachers qualification</label>
                                        <select class="selectpicker" data-style="btn-default" id="teacher_qualification" name="teacher_qualification" >
                                            <option value="">--</option>
                                            <option value="trained">Trained</option>
                                            <option value="nontrained">Non Trained</option>

                                        </select>
                                    </div>
                                    {{csrf_field()}}
                                <div class="form-group text-right m-b-0">
                                    <button class="btn waves-effect waves-light" style="background-color: #006D5B; color:white" type="submit">
                                        Submit
                                    </button>
                                    <button type="reset" class="btn btn-default waves-effect m-l-5">
                                        Cancel
                                    </button>
                                </div>
                                    </div>
                            </div>

                        </div>
                        <!-- end row -->


                        <!-- end row -->


                    </div> <!-- end ard-box -->
                </div><!-- end col-->
                    </form>
            </div>
            <!-- end row -->


        </div> <!-- container -->

    </div> <!-- content -->

    <footer class="footer text-right">
        {{date('Y')}} © 1CGC
    </footer>

</div>

</div>
        <script src="assets/js/jquery.min.js"></script>
        <script>
            $(function(){
                $('#level_edu').hide();
                $('#couplename').hide();
            });
            $('#membership_cat').change(function() {
                if($('#membership_cat :selected').val() == "youth" ){
                     $('#level_edu').show();
                }
                if($('#membership_cat :selected').val() == "child" ){
                     $('#level_edu').show();
                }
                if($('#membership_cat :selected').val() == "adult" ){
                    $('#level_edu').hide();
                }
                if($('#membership_cat :selected').val() == "" ){
                    $('#level_edu').hide();
                }
            });

            $('#marital_status').change(function() {
                if($('#marital_status :selected').val() == "Married" ){
                    $('#couplename').show();
                }else{
                    $('#couplename').hide();
                }

            });
        </script>
@endsection

<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->