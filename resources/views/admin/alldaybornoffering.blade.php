

@extends('layout.localmaster')


<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/personal/datatable.css">


@section('content')
<!-- Begin page -->
<div id="wrapper">


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu" style="background-color: #006D5B">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                {{--<div class="user-details">--}}
                    {{--<div class="overlay"></div>--}}
                    {{--<div class="text-center">--}}
                        {{--<img src="assets/images/users/church.jpg" alt="" class="thumb-md img-circle">--}}
                        {{--</div>--}}
                    {{--<div class="user-info">--}}
                        {{--<div>--}}
                            {{--<a href="index.html#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">mamprobi branch <span class="mdi mdi-menu-down"></span></a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                {{--<div class="dropdown" id="setting-dropdown">--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li><a href="javascript:void(0)"><i class="mdi mdi-face-profile m-r-5"></i> Profile</a></li>--}}
                        {{--<li><a href="javascript:void(0)"><i class="mdi mdi-account-settings-variant m-r-5"></i> Settings</a></li>--}}
                        {{--<li><a href="javascript:void(0)"><i class="mdi mdi-lock m-r-5"></i> Lock screen</a></li>--}}
                        {{--<li><a href="javascript:void(0)"><i class="mdi mdi-logout m-r-5"></i> Logout</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}

                <ul>

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub" >
                        <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>
                        {{--<ul class="list-unstyled">--}}
                            {{--<li><a href="index.html">Dashboard 1</a></li>--}}
                            {{--<li><a href="dashboard_2.html">Dashboard 2</a></li>--}}
                            {{--</ul>--}}
                    </li>
                    @endif
                    {{--<li class="has_sub">--}}
                        {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-invert-colors"></i> <span> Information Mgt </span> <span class="menu-arrow"></span></a>--}}
                        {{--<ul class="list-unstyled">--}}
                            {{--<li><a href="ui-buttons.html">Add New Member</a></li>--}}
                            {{--<li><a href="ui-typography.html">Typography</a></li>--}}
                            {{--<li><a href="ui-panels.html">Panel</a></li>--}}
                            {{--<li><a href="ui-portlets.html">Portlets</a></li>--}}
                            {{--<li><a href="ui-modals.html">Modals</a></li>--}}
                            {{--<li><a href="ui-checkbox-radio.html">Checkboxs-Radios</a></li>--}}
                            {{--<li><a href="ui-tabs.html">Tabs</a></li>--}}
                            {{--<li><a href="ui-progressbars.html">Progress Bars</a></li>--}}
                            {{--<li><a href="ui-notifications.html">Notification</a></li>--}}
                            {{--<li><a href="ui-alerts.html">Alerts</a>--}}
                                {{--<li><a href="ui-carousel.html">Carousel</a>--}}
                                {{--<li><a href="ui-video.html">Video</a>--}}
                                {{--<li><a href="ui-tooltips-popovers.html">Tooltips & Popovers</a></li>--}}
                            {{--<li><a href="ui-images.html">Images</a></li>--}}
                            {{--<li><a href="ui-bootstrap.html">Bootstrap UI</a></li>--}}
                            {{--<li><a href="ui-grid.html">Grid</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="newmember" style="color: white">New Member</a></li>
                            <li><a href="allmember"style="color: white">All Members</a></li>
                            {{--<li><a href="admin-nestable.html">Nestable List</a></li>--}}
                            {{--<li><a href="admin-rangeslider.html">Range Slider</a></li>--}}
                            {{--<li><a href="admin-ratings.html">Ratings</a></li>--}}
                            {{--<li><a href="admin-animation.html">Animation</a></li>--}}
                        </ul>
                    </li>
                    @endif
                    <li class="has_sub">
                        <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>

                    </li>
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i style="color: white" class="fa fa-calendar" aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="newevent" style="color: white">New Event</a></li>

                        </ul>
                    </li>
                    @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin'||'Finance')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="offering" style="color: white">New</a></li>
                            <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                            <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                            <li><a href="expenses" style="color: white"> Expenses</a></li>


                        </ul>
                    </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">

                            <li><a href="report" style="color: white"> Reports</a></li>
                            <li><a href="quarterlyrpt" style="color: white">Quaterly Reports</a></li>
                            <li><a href="weeklyrpt" style="color: white">Weeky Reports</a></li>

                        </ul>
                    </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="newattendance" style="color: white">New Attendance</a></li>
                            <li><a href="allattendance" style="color: white">All Attendance</a></li>

                        </ul>
                    </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="singlesms" style="color: white">Single</a></li>
                            <li><a href="bulksms" style="color: white">Bulk</a></li>

                        </ul>
                    </li>
                    @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="profile" style="color: white">Church Profile</a></li>
                            <li><a href="user" style="color: white">Users</a></li>
                            <li><a href="service " style="color: white">Service</a></li>
                            <li><a href="member_group" style="color: white">Member Group</a></li>
                            <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>


                        </ul>
                    </li>
                    @endif

                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>



        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">All Members</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="dashboard">ICGC</a>
                                </li>
                                <li>
                                    <a href="offering">New Offering </a>
                                </li>
                                <li class="active">
                                    All Offerings
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->



                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">

                            <h4 class="m-t-0 header-title"><b>All Record</b></h4>


                            <table id="member-table" class="table table-striped table-bordered">
                                <thead>

                                <tr>
                                    <th>Number</th>
                                    <th>Date Submitted</th>
                                    <th>Day</th>
                                    <th>(GH)Amount</th>
                                    <th>created At</th>
                                    <th>updated At</th>

                                    <th>Action</th>
                                </tr>
                                </thead>

                            </table>

                        </div>
                    </div>
                </div>

                <div id="edit-Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form action="{{'editdaybornoffering'}}" method="post">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <input type="hidden" class="form-control" id="daybornid" name="daybornid">

                                            <div class="form-group">

                                                <label for="field-1" class="control-label">Date Submitted</label>
                                                <input type="date" class="form-control" id="date_submitted_edit" name="date_submitted_edit" placeholder="John">
                                            </div>
                                            <div class="form-group" id="servicetype_input">
                                                <label for="passWord2">Day<span class="text-danger">*</span></label>
                                                <select class="form-control" data-style="btn-info" id="dayborn_edit" name="dayborn_edit">


                                                        <option value="sunday">Sunday</option>
                                                        <option value="monday">Monday</option>
                                                        <option value="tuesday">Tuesday</option>
                                                        <option value="wednesday">Wednesday</option>
                                                        <option value="thursday">Thursday</option>
                                                        <option value="friday">Friday</option>
                                                        <option value="saturday">Saturday</option>

                                                </select>
                                            </div>
                                            <div class="form-group">

                                                <label for="field-1" class="control-label">Amount</label>
                                                <input type="text" class="form-control" id="amount_edit" name="amount_edit" placeholder="John">
                                            </div>




                                        </div>

                                    </div>

                                </div>
                                {{csrf_field()}}
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                    <button style="background-color:#006D5B;color: white;" type="submit" class="btn  waves-effect waves-light">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal -->



                <div id="delete-Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form action="{{'deletedaybornoffering'}}" method="post">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">Delete</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Are you sure you want to delete offering number <span id="offering_number_delete_data"></span>?</label>
                                                <input type="hidden" class="form-control" id="offering_number_delete" name="offering_number_delete" >
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                {{csrf_field()}}
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                    <button style="background-color:#006D5B;color: white;"type="submit" class="btn  waves-effect waves-light">Delete</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal -->











            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            {{date('Y')}} © 1CGC
        </footer>

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Bootstrap JavaScript -->
    {{--<script src="assets/js/bootstrap.min.js"></script>--}}
    <script>
        $('#member-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!!route('datatable.alldayborn')!!}',
            columns: [
            {data: 'id', name: 'id'},
            {data: 'date', name: 'date'},
            {data: 'dayborn', name: 'dayborn'},
            {data: 'amount', name: 'amount'},
            {data: 'created_at', name: 'created_at'},
            {data: 'updated_at', name: 'updated_at'},
            {data: 'action', name: 'action'}
        ],
            initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());

                        column.search(val ? val : '', true, false).draw();
                    });
            });
        }
        });
    </script>
    <script>
        $(document).on('click','.editbtn',function() {
            // alert("you");
            $('#daybornid').val($(this).data('id'));
            $('#date_submitted_edit').val($(this).data('date'));

            $('#dayborn_edit').val($(this).data('dayborn')).change();
            $('#amount_edit').val($(this).data('amount'));


        });
        $(document).on('click','.deletebtn',function() {

            $('#offering_number_delete').val($(this).data('id'));
            $('#offering_number_delete_data').html($(this).data('id'));



        });
    </script>
</div>
@endsection

