

@extends('layout.localmaster')

<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/personal/datatable.css">

<link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
@section('content')
        <!-- Begin page -->
<div id="wrapper">


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu" style="background-color: #006D5B">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                {{--<div class="user-details">--}}
                {{--<div class="overlay"></div>--}}
                {{--<div class="text-center">--}}
                {{--<img src="assets/images/users/church.jpg" alt="" class="thumb-md img-circle">--}}
                {{--</div>--}}
                {{--<div class="user-info">--}}
                {{--<div>--}}
                {{--<a href="index.html#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">mamprobi branch <span class="mdi mdi-menu-down"></span></a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="dropdown" id="setting-dropdown">--}}
                {{--<ul class="dropdown-menu">--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-face-profile m-r-5"></i> Profile</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-account-settings-variant m-r-5"></i> Settings</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-lock m-r-5"></i> Lock screen</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-logout m-r-5"></i> Logout</a></li>--}}
                {{--</ul>--}}
                {{--</div>--}}

                <ul>

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub" >
                            <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>
                            {{--<ul class="list-unstyled">--}}
                            {{--<li><a href="index.html">Dashboard 1</a></li>--}}
                            {{--<li><a href="dashboard_2.html">Dashboard 2</a></li>--}}
                            {{--</ul>--}}
                        </li>
                    @endif
                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-invert-colors"></i> <span> Information Mgt </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="ui-buttons.html">Add New Member</a></li>--}}
                    {{--<li><a href="ui-typography.html">Typography</a></li>--}}
                    {{--<li><a href="ui-panels.html">Panel</a></li>--}}
                    {{--<li><a href="ui-portlets.html">Portlets</a></li>--}}
                    {{--<li><a href="ui-modals.html">Modals</a></li>--}}
                    {{--<li><a href="ui-checkbox-radio.html">Checkboxs-Radios</a></li>--}}
                    {{--<li><a href="ui-tabs.html">Tabs</a></li>--}}
                    {{--<li><a href="ui-progressbars.html">Progress Bars</a></li>--}}
                    {{--<li><a href="ui-notifications.html">Notification</a></li>--}}
                    {{--<li><a href="ui-alerts.html">Alerts</a>--}}
                    {{--<li><a href="ui-carousel.html">Carousel</a>--}}
                    {{--<li><a href="ui-video.html">Video</a>--}}
                    {{--<li><a href="ui-tooltips-popovers.html">Tooltips & Popovers</a></li>--}}
                    {{--<li><a href="ui-images.html">Images</a></li>--}}
                    {{--<li><a href="ui-bootstrap.html">Bootstrap UI</a></li>--}}
                    {{--<li><a href="ui-grid.html">Grid</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newmember" style="color: white">New Member</a></li>
                                <li><a href="allmember"style="color: white">All Members</a></li>
                                {{--<li><a href="admin-nestable.html">Nestable List</a></li>--}}
                                {{--<li><a href="admin-rangeslider.html">Range Slider</a></li>--}}
                                {{--<li><a href="admin-ratings.html">Ratings</a></li>--}}
                                {{--<li><a href="admin-animation.html">Animation</a></li>--}}
                            </ul>
                        </li>
                    @endif
                        <li class="has_sub">
                            <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>
                            {{--<ul class="list-unstyled">--}}
                            {{--<li><a href="newmember" style="color: white">New Member</a></li>--}}
                            {{--<li><a href="allmember"style="color: white">All Members</a></li>--}}
                            {{--<li><a href="admin-nestable.html">Nestable List</a></li>--}}
                            {{--<li><a href="admin-rangeslider.html">Range Slider</a></li>--}}
                            {{--<li><a href="admin-ratings.html">Ratings</a></li>--}}
                            {{--<li><a href="admin-animation.html">Animation</a></li>--}}
                            {{--</ul>--}}
                        </li>
                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-calendar" style="color: white" aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="newevent" style="color: white">New Event</a></li>

                                </ul>
                            </li>
                        @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin'||'Finance')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="offering" style="color: white">New Offering</a></li>
                                <li><a href="alloffering" style="color: white"> All Offering</a></li>
                                <li><a href="expenses" style="color: white"> Expenses</a></li>

                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="offering" style="color: white">New</a></li>
                                <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                                <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                                <li><a href="expenses" style="color: white"> Expenses</a></li>
                            </ul>
                        </li>
                    @endif

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text-o" aria-hidden="true"></i><span> Report </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="icons-glyphicons.html">Glyphicons</a></li>--}}
                    {{--<li><a href="icons-colored.html">Colored Icons</a></li>--}}
                    {{--<li><a href="icons-materialdesign.html">Material Design</a></li>--}}
                    {{--<li><a href="icons-ionicons.html">Ion Icons</a></li>--}}
                    {{--<li><a href="icons-fontawesome.html">Font awesome</a></li>--}}
                    {{--<li><a href="icons-themifyicon.html">Themify Icons</a></li>--}}
                    {{--<li><a href="icons-typicons.html">Typicons</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newattendance" style="color: white">New Attendance</a></li>
                                <li><a href="allattendance" style="color: white">All Attendance</a></li>
                                {{--<li><a href="form-validation.html">Form Validation</a></li>--}}
                                {{--<li><a href="form-pickers.html">Form Pickers</a></li>--}}
                                {{--<li><a href="form-wizard.html">Form Wizard</a></li>--}}
                                {{--<li><a href="form-mask.html">Form Masks</a></li>--}}
                                {{--<li><a href="form-summernote.html">Summernote</a></li>--}}
                                {{--<li><a href="form-wysiwig.html">Wysiwig Editors</a></li>--}}
                                {{--<li><a href="form-uploads.html">Multiple File Upload</a></li>--}}
                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="singlesms" style="color: white">Single</a></li>
                                <li><a href="bulksms" style="color: white">Bulk</a></li>
                                {{--<li><a href="form-validation.html">Form Validation</a></li>--}}
                                {{--<li><a href="form-pickers.html">Form Pickers</a></li>--}}
                                {{--<li><a href="form-wizard.html">Form Wizard</a></li>--}}
                                {{--<li><a href="form-mask.html">Form Masks</a></li>--}}
                                {{--<li><a href="form-summernote.html">Summernote</a></li>--}}
                                {{--<li><a href="form-wysiwig.html">Wysiwig Editors</a></li>--}}
                                {{--<li><a href="form-uploads.html">Multiple File Upload</a></li>--}}
                            </ul>
                        </li>
                    @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="profile" style="color: white">Church Profile</a></li>
                                <li><a href="user" style="color: white">Users</a></li>
                                <li><a href="service " style="color: white">Service</a></li>
                                <li><a href="member_group" style="color: white">Member Group</a></li>
                                <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>


                            </ul>
                        </li>
                    @endif
                    {{--<li class="menu-title">Extra</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-chart-arc"></i><span> Charts </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="chart-flot.html">Flot Chart</a></li>--}}
                    {{--<li><a href="chart-morris.html">Morris Chart</a></li>--}}
                    {{--<li><a href="chart-google.html">Google Chart</a></li>--}}
                    {{--<li><a href="chart-chartist.html">Chartist Charts</a></li>--}}
                    {{--<li><a href="chart-chartjs.html">Chartjs Chart</a></li>--}}
                    {{--<li><a href="chart-c3.html">C3 Chart</a></li>--}}
                    {{--<li><a href="chart-sparkline.html">Sparkline Chart</a></li>--}}
                    {{--<li><a href="chart-knob.html">Jquery Knob</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-map"></i> <span> Maps </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="maps-google.html">Google Maps</a></li>--}}
                    {{--<li><a href="maps-vector.html">Vector Maps</a></li>--}}
                    {{--<li><a href="maps-mapael.html">Mapael Maps</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-google-pages"></i><span> Pages </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="page-starter.html">Starter Page</a></li>--}}
                    {{--<li><a href="page-login.html">Login</a></li>--}}
                    {{--<li><a href="page-register.html">Register</a></li>--}}
                    {{--<li><a href="page-logout.html">Logout</a></li>--}}
                    {{--<li><a href="page-recoverpw.html">Recover Password</a></li>--}}
                    {{--<li><a href="page-lock-screen.html">Lock Screen</a></li>--}}
                    {{--<li><a href="page-confirm-mail.html">Confirm Mail</a></li>--}}
                    {{--<li><a href="page-404.html">Error 404</a></li>--}}
                    {{--<li><a href="page-404-alt.html">Error 404-alt</a></li>--}}
                    {{--<li><a href="page-500.html">Error 500</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-gift"></i><span> Extras </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="extras-profile.html">Profile</a></li>--}}
                    {{--<li><a href="extras-sitemap.html">Sitemap</a></li>--}}
                    {{--<li><a href="extras-about.html">About Us</a></li>--}}
                    {{--<li><a href="extras-contact.html">Contact</a></li>--}}
                    {{--<li><a href="extras-members.html">Members</a></li>--}}
                    {{--<li><a href="extras-timeline.html">Timeline</a></li>--}}
                    {{--<li><a href="extras-invoice.html">Invoice</a></li>--}}
                    {{--<li><a href="extras-search-result.html">Search Result</a></li>--}}
                    {{--<li><a href="extras-emailtemplate.html">Email Template</a></li>--}}
                    {{--<li><a href="extras-maintenance.html">Maintenance</a></li>--}}
                    {{--<li><a href="extras-coming-soon.html">Coming Soon</a></li>--}}
                    {{--<li><a href="extras-faq.html">FAQ</a></li>--}}
                    {{--<li><a href="extras-gallery.html">Gallery</a></li>--}}
                    {{--<li><a href="extras-pricing.html">Pricing</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="menu-title">More</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-comment-text-outline"></i><span> Blog </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="blogs-dashboard.html">Dashboard</a></li>--}}
                    {{--<li><a href="blogs-blog-list.html">Blog List</a></li>--}}
                    {{--<li><a href="blogs-blog-column.html">Blog Column</a></li>--}}
                    {{--<li><a href="blogs-blog-post.html">Blog Post</a></li>--}}
                    {{--<li><a href="blogs-blog-add.html">Add Blog</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-home-map-marker"></i><span> Real Estate </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="real-estate-dashboard.html">Dashboard</a></li>--}}
                    {{--<li><a href="real-estate-list.html">Property List</a></li>--}}
                    {{--<li><a href="real-estate-column.html">Property Column</a></li>--}}
                    {{--<li><a href="real-estate-detail.html">Property Detail</a></li>--}}
                    {{--<li><a href="real-estate-agents.html">Agents</a></li>--}}
                    {{--<li><a href="real-estate-profile.html">Agent Details</a></li>--}}
                    {{--<li><a href="real-estate-add.html">Add Property</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

            {{--<div class="help-box">--}}
            {{--<h5 class="text-muted m-t-0">For Help ?</h5>--}}
            {{--<p class=""><span class="text-dark"><b>Email:</b></span> <br/> nanamensah1140@gmail.com</p>--}}
            {{--<p class=""><span class="text-dark"><b>Email:</b></span> <br/> emagbin@yahoo.com</p>--}}
            {{--<p class="m-b-0"><span class="text-dark"><b>Call:</b></span> <br/> 0262489168,0249273086</p>--}}
            {{--</div>--}}

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">All Contact</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="tables-datatable.html#">ICGC</a>
                                </li>
                                <li>
                                    <a href="tables-datatable.html#">Bulk SMS </a>
                                </li>
                                <li class="active">
                                    All members
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->



                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">



                            <div class="row">
                                <div class="col-md-6">
                                    <div class="demo-box m-t-20">

                                            <form action="smssingle" method="post"  id="send_sms" name="single_sms_form">
                                                <div class="p-20">
                                                    <div class="form-group">
                                                        <label for="userName">Number(s)<span class="text-danger">*</span></label>
                                                        <input type="text" name="tel_number" parsley-trigger="change"
                                                               class="form-control" id="tel_number">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Name of receipient<span class="text-danger">*</span></label>
                                                        <input type="text" name="name_of_receipient" parsley-trigger="change"
                                                               class="form-control" id="name_of_receipient">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="pass1">Sender 1D(11 characters)<span class="text-danger">*</span></label>
                                                        <input  type="text" name="sender" id="sender"
                                                                class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="passWord2">Message: <span class="text-danger">*</span></label>
                                        <textarea type="text" id="messagecontent" name="messagecontent" row="5"
                                                  class="form-control" id="passWord2"></textarea>
                                                    </div>
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                    <div class="form-group text-right m-b-0">
                                                        <button style="background-color:#006D5B;color: white;" class="btn  waves-effect waves-light" id="sendsingle"  >
                                                            Send
                                                        </button>

                                                    </div>



                                                </div>
                                            </form>

                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="demo-box m-t-20">
                                        <div class="table-responsive">
                                            <table class="table table table-hover m-0"  id="membercontact">
                                                <thead>
                                                <tr>

                                                    <th>Fullname</th>
                                                    <th>Phone</th>


                                                </tr>
                                                </thead>

                                                <tbody>



                                                </tbody>


                                            </table>

                                        </div> <!-- table-responsive -->
                                    </div>

                                </div>
                            </div>
                            <!--- end row -->





                        </div> <!-- end card-box -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->



            </div> <!-- container -->

        </div>


        <footer class="footer text-right">
            {{date('Y')}} © 1CGC
        </footer>
    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->









    <!-- Right Sidebar -->
    <div class="side-bar right-bar">
        <a href="javascript:void(0);" class="right-bar-toggle">
            <i class="mdi mdi-close-circle-outline"></i>
        </a>
        <h4 class="">Settings</h4>
        <div class="setting-list nicescroll">
            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">Notifications</h5>
                    <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">API Access</h5>
                    <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">Auto Updates</h5>
                    <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">Online Status</h5>
                    <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>
        </div>
    </div>
    <!-- /Right-bar -->
    <!-- /Right-bar -->
</div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
    {{--<script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>--}}

    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Bootstrap JavaScript -->
    {{--<script src="assets/js/bootstrap.min.js"></script>--}}

    <script>
        $('#membercontact').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!!route('datatable.membercontact')!!}',
            columns: [
                {data: 'fullname', name: 'fullname'},

                {data: 'telephone', name: 'telephone'}

            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column.search(val ? val : '', true, false).draw();
                            });
                });
            }
        });
    </script>

    <script>
        $(document).on('click','#member_click',function(e) {
            e.preventDefault();

            $('#tel_number').val($(this).data('telephone'));
            $('#name_of_receipient').val($(this).data('fullname'));

        });
        $(document).on('click','#telephone_click',function(e) {
            e.preventDefault();
         });
        {{--$(document).on('click','#sendsingle',function(e) {--}}
            {{--e.preventDefault();--}}

            {{--if(validatesinglesms()){--}}

               {{--var token=$('input[name=_token]').val();--}}

                {{--var number = $('#tel_number').val();--}}

                {{--var name_of_receipient = $('#name_of_receipient').val();--}}
                {{--var sender = $('#sender').val();--}}
                {{--var messagecontent = $('#messagecontent').val();--}}
                {{--$.ajax({--}}
                    {{--type:'post' ,--}}
                    {{--url: '{{URL::to('smssingle')}}',--}}
                    {{--data:{--}}
                        {{--'_token':token,--}}
                        {{--'number':number,--}}
                        {{--'name_of_receipient':name_of_receipient,--}}
                        {{--'sender':sender,--}}
                        {{--'messagecontent':messagecontent--}}

                    {{--},--}}
                    {{--success:function(data){--}}
                       {{--// alert(data);--}}
                        {{--//$('.success_message').html(data);--}}
                        {{--toastr.success('Message Sent Out','Success',{timeOut: 2000});--}}
                        {{--location.reload();--}}
                    {{--}--}}
                {{--});--}}
            {{--}--}}
        {{--});--}}
        function validatesinglesms(){

            var validatenumber = document.forms["single_sms_form"]["tel_number"].value;
            var validatereceipient = document.forms["single_sms_form"]["name_of_receipient"].value;
            var sender = document.forms["single_sms_form"]["sender"].value;
            var message = document.forms["single_sms_form"]["messagecontent"].value;

            if (validatenumber == "") {
                $("#tel_number").css({"background-color": "pink"});
                return false;
            }
            else if(validatereceipient== ""){
                $("#name_of_receipient").css({"background-color": "pink"});
                return false;
            }
            else if(sender== ""){
                $("#sender").css({"background-color": "pink"});
                return false;
            }
            else if(message==""){
                $("#messagecontent").css({"background-color": "pink"});
                return false;
            }else{
                $("#number").css({"background-color": "white"});
                $("#name_of_receipient").css({"background-color": "white"});
                $("#sender").css({"background-color": "white"});
                $("#messagecontent").css({"background-color": "white"});
                return true;
            }

        }
    </script>

@endsection

