

@extends('layout.localmaster')

<style>

    .form-group input[type=text]:focus {
        border-bottom: 1px solid #006D5B;
        box-shadow: 0 1px 0 0 #006D5B;
    }
    .focus {
        background-color: #006D5B;
        color: #fff;
        cursor: pointer;
        font-weight: bold;
    }

    .pageNumber {
        padding: 2px;
    }

</style>
@section('content')
        <!-- Begin page -->
<!-- Begin page -->
<div id="wrapper" >


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu" style="background-color: #006D5B">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">


                <ul>

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub" >
                            <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>

                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newmember" style="color: white">New Member</a></li>
                                <li><a href="allmember"style="color: white">All Members</a></li>

                            </ul>
                        </li>
                    @endif
                        <li class="has_sub">
                            <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>

                        </li>
                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i style="color: white" class="fa fa-calendar" aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="newevent" style="color: white">New Event</a></li>

                                </ul>
                            </li>
                        @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin'||'Finance')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="offering" style="color: white">New</a></li>
                                <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                                <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                                <li><a href="expenses" style="color: white"> Expenses</a></li>
                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">

                                <li><a href="report" style="color: white"> Reports</a></li>
                                <li><a href="quarterlyrpt" style="color: white">Quaterly Reports</a></li>
                                <li><a href="weeklyrpt" style="color: white">Weeky Reports</a></li>

                            </ul>
                        </li>
                    @endif


                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newattendance" style="color: white">New Attendance</a></li>
                                <li><a href="allattendance" style="color: white">All Attendance</a></li>
                                 </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="singlesms" style="color: white">Single</a></li>
                                <li><a href="bulksms" style="color: white">Bulk</a></li>

                            </ul>
                        </li>
                    @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="profile" style="color: white">Church Profile</a></li>
                                <li><a href="user" style="color: white">Users</a></li>
                                <li><a href="service " style="color: white">Service</a></li>
                                <li><a href="member_group" style="color: white">Member Group</a></li>
                                <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>


                            </ul>
                        </li>
                    @endif


                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>



        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">All Contact</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="tables-datatable.html#">ICGC</a>
                                </li>
                                <li>
                                    <a href="tables-datatable.html#">Bulk SMS </a>
                                </li>
                                <li class="active">
                                    All members
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->



                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select Group</label>
                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" id="membergroup" name="membergroup" >
                                            @foreach($membergroups as $s)
                                                <option value="{{$s->name}}">{{$s->name}}</option>

                                            @endforeach
                                        </select>

                                    </div>
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <button type="button" style="background-color:#006D5B;color: white;" class="btn " id="loadcontact"><i class="fa fa-download" aria-hidden="true"></i> Load Contacts for SMS</button>

                                </div>


                            </div>
                            <!--- end row -->


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="demo-box m-t-20">
                                        {{--<h4 class="m-t-0 header-title"><b>Info Table</b></h4>--}}
                                        {{--<p class="text-muted font-13 m-b-20">--}}
                                            {{--Use Class <code>.table-colored .table-info</code>--}}
                                        {{--</p>--}}

                                        <div class="table-responsive">
                                            <table class="table"  id="groupcontacttable">
                                                <thead>
                                                <tr>
                                                    <th><input type="checkbox" id="checkAll" name="checkAll" />Select All</th>
                                                    <th>Name</th>
                                                    <th>Phone Number</th>
                                                </tr>
                                                </thead>
                                                <tbody id="bulkcontact"></tbody>


                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="demo-box m-t-20">
                                        <form>

                                            <div class="form-group">
                                                <label for="exampleInputPassword1"><strong>Sender:</strong></label>
                                                <input type="text" id="sender" class="form-control" name="sender" placeholder="Sender" required>
                                            </div>
                                            <div class="form-group">

                                                <label for="exampleInputPassword1"><strong>Message:</strong></label>

                                                <textarea  id="messagecontent" name="messagecontent" class="form-control"placeholder="Message" required></textarea>
                                            </div>
                                        </form></br>

                                        <button style="background-color:#006D5B;color: white;" type="button" class="btn btn-lg" id="send_bulk_sms">Send</button>

                                    </div>

                                </div>
                            </div>
                            <!--- end row -->





                        </div> <!-- end card-box -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->



            </div> <!-- container -->

        </div>


        <footer class="footer text-right">
            {{date('Y')}} © 1CGC
        </footer>
     </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->









    <!-- Right Sidebar -->
    <div class="side-bar right-bar">
        <a href="javascript:void(0);" class="right-bar-toggle">
            <i class="mdi mdi-close-circle-outline"></i>
        </a>
        <h4 class="">Settings</h4>
        <div class="setting-list nicescroll">
            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">Notifications</h5>
                    <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">API Access</h5>
                    <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">Auto Updates</h5>
                    <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-xs-8">
                    <h5 class="m-0">Online Status</h5>
                    <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                </div>
                <div class="col-xs-4 text-right">
                    <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                </div>
            </div>
        </div>
    </div>
    <!-- /Right-bar -->


</div>
    <script src="assets/js/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('textarea').focus(function () {
                $(this).css("border-bottom", "1px solid #006D5B");
                $(this).css("box-shadow", "0 1px 0 0 #006D5B");
            });
        });
        $(document).on('click','#loadcontact',function(e) {

            $('table tbody tr').hide();
            $('#pages').hide();

            $.ajax({
                type:'post' ,
                url: '{{URL::to('smsbulk')}}',
                data:{
                    '_token':$('input[name=_token]').val(),
                    'value':$('#membergroup').val()


                },
                success:function(data){
                   // alert(data);
                     $('#bulkcontact').html(data);

                    //perform pagination on the data
                    setTimeout(function(){
                        var totalRows = $('#groupcontacttable tr').length;
                        var recordPerPage = 5;
                        var totalPages = Math.ceil(totalRows / recordPerPage);
                        var $pages = $('<div id="pages"></div>');
                        for (i = 0; i < totalPages; i++) {
                            $('<span class="pageNumber">&nbsp;' + (i + 1) + '</span>').appendTo($pages);
                        }
                        $pages.appendTo('#groupcontacttable');

                        $('.pageNumber').hover(
                                function() {
                                    $(this).addClass('focus');
                                },
                                function() {
                                    $(this).removeClass('focus');
                                }
                        );
                        $('table tbody tr').hide();

                        var tr = $('table tbody tr:has(td)');
                        for (var i = 0; i <= recordPerPage - 1; i++) {
                            $(tr[i]).show();
                        }
                        $('span').click(function(event) {
                            $('#groupcontacttable').find('tbody tr:has(td)').hide();
                            var nBegin = ($(this).text() - 1) * recordPerPage;
                            var nEnd = $(this).text() * recordPerPage - 1;
                            for (var i = nBegin; i <= nEnd; i++) {
                                $(tr[i]).show();
                            }
                        });
                    }, 100);

                    //var recordPerPage = 5;
                    //var totalPages = Math.ceil(totalRows / recordPerPage);

                }
            });

        });

        $('input[type="checkbox"][name="checkAll"]').change(function() {
            if(this.checked) {
                var table = document.getElementById('groupcontacttable');
                var val = table.rows[0].cells[0].children[0].checked;
                for (var i = 1; i < table.rows.length; i++)
                {
                    table.rows[i].cells[0].children[0].checked = val;
                }
            }else{
                var table = document.getElementById('groupcontacttable');
                var val = table.rows[0].cells[0].children[0].unchecked;
                for (var i = 1; i < table.rows.length; i++)
                {
                    table.rows[i].cells[0].children[0].checked = val;
                }
            }
        });

        $(document).on('click','#send_bulk_sms',function(e) {

            $('#sender').parsley().validate();
            $('#messagecontent').parsley().validate();
            if($('#sender').parsley().isValid()&&$('#messagecontent').parsley().isValid()){

                var totalchecked=$('#bulkcontact').find('input[type="checkbox"]:checked').length;
                var count=0;

                $('#bulkcontact').find('input[type="checkbox"]:checked').each(function () {

                    var name=$(this).data('name');
                    var telephone=$(this).data('telephone');
                    var sender=$('#sender').val();
                    var messagecontent=$('#messagecontent').val();
                    $.ajax({
                        type:'post' ,
                        url: '{{URL::to('sendbulksms')}}',
                        data:{
                            '_token':$('input[name=_token]').val(),
                            'name':name,
                            'telephone':telephone,
                            'sender':sender,
                            'messagecontent':messagecontent



                        }, beforeSend: function() {
                            // setting a timeout
                            $('#send_bulk_sms').html("Processing....");
                        },
                        success:function(data){
                            count=count+1;
                            if(count===totalchecked){
                                toastr.success('Message Sent Out','Success',{timeOut: 2000});

                                location.reload();
                            }


                        },complete: function() {
                            $('#send_bulk_sms').html("Send");
                        }
                    });

                });
            }

        });

    </script>

@endsection
