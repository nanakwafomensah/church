

@extends('layout.localmaster')

<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/personal/datatable.css">

@section('content')
        <!-- Begin page -->
<div id="wrapper">


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu" style="background-color: #006D5B">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                {{--<div class="user-details">--}}
                {{--<div class="overlay"></div>--}}
                {{--<div class="text-center">--}}
                {{--<img src="assets/images/users/church.jpg" alt="" class="thumb-md img-circle">--}}
                {{--</div>--}}
                {{--<div class="user-info">--}}
                {{--<div>--}}
                {{--<a href="index.html#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">mamprobi branch <span class="mdi mdi-menu-down"></span></a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="dropdown" id="setting-dropdown">--}}
                {{--<ul class="dropdown-menu">--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-face-profile m-r-5"></i> Profile</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-account-settings-variant m-r-5"></i> Settings</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-lock m-r-5"></i> Lock screen</a></li>--}}
                {{--<li><a href="javascript:void(0)"><i class="mdi mdi-logout m-r-5"></i> Logout</a></li>--}}
                {{--</ul>--}}
                {{--</div>--}}

                <ul>

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub" >
                            <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>
                            {{--<ul class="list-unstyled">--}}
                            {{--<li><a href="index.html">Dashboard 1</a></li>--}}
                            {{--<li><a href="dashboard_2.html">Dashboard 2</a></li>--}}
                            {{--</ul>--}}
                        </li>
                    @endif
                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-invert-colors"></i> <span> Information Mgt </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="ui-buttons.html">Add New Member</a></li>--}}
                    {{--<li><a href="ui-typography.html">Typography</a></li>--}}
                    {{--<li><a href="ui-panels.html">Panel</a></li>--}}
                    {{--<li><a href="ui-portlets.html">Portlets</a></li>--}}
                    {{--<li><a href="ui-modals.html">Modals</a></li>--}}
                    {{--<li><a href="ui-checkbox-radio.html">Checkboxs-Radios</a></li>--}}
                    {{--<li><a href="ui-tabs.html">Tabs</a></li>--}}
                    {{--<li><a href="ui-progressbars.html">Progress Bars</a></li>--}}
                    {{--<li><a href="ui-notifications.html">Notification</a></li>--}}
                    {{--<li><a href="ui-alerts.html">Alerts</a>--}}
                    {{--<li><a href="ui-carousel.html">Carousel</a>--}}
                    {{--<li><a href="ui-video.html">Video</a>--}}
                    {{--<li><a href="ui-tooltips-popovers.html">Tooltips & Popovers</a></li>--}}
                    {{--<li><a href="ui-images.html">Images</a></li>--}}
                    {{--<li><a href="ui-bootstrap.html">Bootstrap UI</a></li>--}}
                    {{--<li><a href="ui-grid.html">Grid</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newmember" style="color: white">New Member</a></li>
                                <li><a href="allmember"style="color: white">All Members</a></li>
                                {{--<li><a href="admin-nestable.html">Nestable List</a></li>--}}
                                {{--<li><a href="admin-rangeslider.html">Range Slider</a></li>--}}
                                {{--<li><a href="admin-ratings.html">Ratings</a></li>--}}
                                {{--<li><a href="admin-animation.html">Animation</a></li>--}}
                            </ul>
                        </li>
                    @endif
                        <li class="has_sub">
                            <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>
                            {{--<ul class="list-unstyled">--}}
                            {{--<li><a href="newmember" style="color: white">New Member</a></li>--}}
                            {{--<li><a href="allmember"style="color: white">All Members</a></li>--}}
                            {{--<li><a href="admin-nestable.html">Nestable List</a></li>--}}
                            {{--<li><a href="admin-rangeslider.html">Range Slider</a></li>--}}
                            {{--<li><a href="admin-ratings.html">Ratings</a></li>--}}
                            {{--<li><a href="admin-animation.html">Animation</a></li>--}}
                            {{--</ul>--}}
                        </li>
                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-calendar" style="color: white" aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="newevent" style="color: white">New Event</a></li>

                                </ul>
                            </li>
                        @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin'||'Finance')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="offering" style="color: white">New</a></li>
                                <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                                <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                                <li><a href="expenses" style="color: white"> Expenses</a></li>

                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">

                                <li><a href="report" style="color: white"> Reports</a></li>
                                <li><a href="quarterlyrpt" style="color: white">Quaterly Reports</a></li>
                                <li><a href="weeklyrpt" style="color: white">Weeky Reports</a></li>

                            </ul>
                        </li>
                    @endif

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text-o" aria-hidden="true"></i><span> Report </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="icons-glyphicons.html">Glyphicons</a></li>--}}
                    {{--<li><a href="icons-colored.html">Colored Icons</a></li>--}}
                    {{--<li><a href="icons-materialdesign.html">Material Design</a></li>--}}
                    {{--<li><a href="icons-ionicons.html">Ion Icons</a></li>--}}
                    {{--<li><a href="icons-fontawesome.html">Font awesome</a></li>--}}
                    {{--<li><a href="icons-themifyicon.html">Themify Icons</a></li>--}}
                    {{--<li><a href="icons-typicons.html">Typicons</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newattendance" style="color: white">New Attendance</a></li>
                                <li><a href="allattendance" style="color: white">All Attendance</a></li>
                                {{--<li><a href="form-validation.html">Form Validation</a></li>--}}
                                {{--<li><a href="form-pickers.html">Form Pickers</a></li>--}}
                                {{--<li><a href="form-wizard.html">Form Wizard</a></li>--}}
                                {{--<li><a href="form-mask.html">Form Masks</a></li>--}}
                                {{--<li><a href="form-summernote.html">Summernote</a></li>--}}
                                {{--<li><a href="form-wysiwig.html">Wysiwig Editors</a></li>--}}
                                {{--<li><a href="form-uploads.html">Multiple File Upload</a></li>--}}
                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="singlesms" style="color: white">Single</a></li>
                                <li><a href="bulksms" style="color: white">Bulk</a></li>
                                {{--<li><a href="form-validation.html">Form Validation</a></li>--}}
                                {{--<li><a href="form-pickers.html">Form Pickers</a></li>--}}
                                {{--<li><a href="form-wizard.html">Form Wizard</a></li>--}}
                                {{--<li><a href="form-mask.html">Form Masks</a></li>--}}
                                {{--<li><a href="form-summernote.html">Summernote</a></li>--}}
                                {{--<li><a href="form-wysiwig.html">Wysiwig Editors</a></li>--}}
                                {{--<li><a href="form-uploads.html">Multiple File Upload</a></li>--}}
                            </ul>
                        </li>
                    @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="profile" style="color: white">Church Profile</a></li>
                                <li><a href="user" style="color: white">Users</a></li>
                                <li><a href="service " style="color: white">Service</a></li>
                                <li><a href="member_group" style="color: white">Member Group</a></li>
                                <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>


                            </ul>
                        </li>
                    @endif
                    {{--<li class="menu-title">Extra</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-chart-arc"></i><span> Charts </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="chart-flot.html">Flot Chart</a></li>--}}
                    {{--<li><a href="chart-morris.html">Morris Chart</a></li>--}}
                    {{--<li><a href="chart-google.html">Google Chart</a></li>--}}
                    {{--<li><a href="chart-chartist.html">Chartist Charts</a></li>--}}
                    {{--<li><a href="chart-chartjs.html">Chartjs Chart</a></li>--}}
                    {{--<li><a href="chart-c3.html">C3 Chart</a></li>--}}
                    {{--<li><a href="chart-sparkline.html">Sparkline Chart</a></li>--}}
                    {{--<li><a href="chart-knob.html">Jquery Knob</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-map"></i> <span> Maps </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="maps-google.html">Google Maps</a></li>--}}
                    {{--<li><a href="maps-vector.html">Vector Maps</a></li>--}}
                    {{--<li><a href="maps-mapael.html">Mapael Maps</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-google-pages"></i><span> Pages </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="page-starter.html">Starter Page</a></li>--}}
                    {{--<li><a href="page-login.html">Login</a></li>--}}
                    {{--<li><a href="page-register.html">Register</a></li>--}}
                    {{--<li><a href="page-logout.html">Logout</a></li>--}}
                    {{--<li><a href="page-recoverpw.html">Recover Password</a></li>--}}
                    {{--<li><a href="page-lock-screen.html">Lock Screen</a></li>--}}
                    {{--<li><a href="page-confirm-mail.html">Confirm Mail</a></li>--}}
                    {{--<li><a href="page-404.html">Error 404</a></li>--}}
                    {{--<li><a href="page-404-alt.html">Error 404-alt</a></li>--}}
                    {{--<li><a href="page-500.html">Error 500</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-gift"></i><span> Extras </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="extras-profile.html">Profile</a></li>--}}
                    {{--<li><a href="extras-sitemap.html">Sitemap</a></li>--}}
                    {{--<li><a href="extras-about.html">About Us</a></li>--}}
                    {{--<li><a href="extras-contact.html">Contact</a></li>--}}
                    {{--<li><a href="extras-members.html">Members</a></li>--}}
                    {{--<li><a href="extras-timeline.html">Timeline</a></li>--}}
                    {{--<li><a href="extras-invoice.html">Invoice</a></li>--}}
                    {{--<li><a href="extras-search-result.html">Search Result</a></li>--}}
                    {{--<li><a href="extras-emailtemplate.html">Email Template</a></li>--}}
                    {{--<li><a href="extras-maintenance.html">Maintenance</a></li>--}}
                    {{--<li><a href="extras-coming-soon.html">Coming Soon</a></li>--}}
                    {{--<li><a href="extras-faq.html">FAQ</a></li>--}}
                    {{--<li><a href="extras-gallery.html">Gallery</a></li>--}}
                    {{--<li><a href="extras-pricing.html">Pricing</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="menu-title">More</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-comment-text-outline"></i><span> Blog </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="blogs-dashboard.html">Dashboard</a></li>--}}
                    {{--<li><a href="blogs-blog-list.html">Blog List</a></li>--}}
                    {{--<li><a href="blogs-blog-column.html">Blog Column</a></li>--}}
                    {{--<li><a href="blogs-blog-post.html">Blog Post</a></li>--}}
                    {{--<li><a href="blogs-blog-add.html">Add Blog</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="has_sub">--}}
                    {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-home-map-marker"></i><span> Real Estate </span> <span class="menu-arrow"></span></a>--}}
                    {{--<ul class="list-unstyled">--}}
                    {{--<li><a href="real-estate-dashboard.html">Dashboard</a></li>--}}
                    {{--<li><a href="real-estate-list.html">Property List</a></li>--}}
                    {{--<li><a href="real-estate-column.html">Property Column</a></li>--}}
                    {{--<li><a href="real-estate-detail.html">Property Detail</a></li>--}}
                    {{--<li><a href="real-estate-agents.html">Agents</a></li>--}}
                    {{--<li><a href="real-estate-profile.html">Agent Details</a></li>--}}
                    {{--<li><a href="real-estate-add.html">Add Property</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

            {{--<div class="help-box">--}}
            {{--<h5 class="text-muted m-t-0">For Help ?</h5>--}}
            {{--<p class=""><span class="text-dark"><b>Email:</b></span> <br/> nanamensah1140@gmail.com</p>--}}
            {{--<p class=""><span class="text-dark"><b>Email:</b></span> <br/> emagbin@yahoo.com</p>--}}
            {{--<p class="m-b-0"><span class="text-dark"><b>Call:</b></span> <br/> 0262489168,0249273086</p>--}}
            {{--</div>--}}

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">All Services</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="tables-datatable.html#">ICGC</a>
                                </li>
                                <li>
                                    <a href="tables-datatable.html#">Service</a>
                                </li>
                                <li class="active">
                                    Services
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="row">
                    <div class="col-sm-4">
                        <!-- Responsive modal -->
                        <a style="background-color:#006D5B;color: white;" class="btn  waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal">New Service</a>
                        </div>
                        </div>
</br>
                <!-- end row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">

                            <h4 class="m-t-0 header-title"><b>All Record</b></h4>


                            <table id="service-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Service type</th>
                                    <th>Updated_at</th>
                                    <th>Created At</th>
                                    <th>Action</th>

                                </tr>
                                </thead>

                            </table>

                        </div>
                    </div>
                </div>
                </div>
                <!-- end row -->


            <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <form action="{{route('addservice')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Add New service Type</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Service Type</label>
                                            <input type="text" class="form-control" id="field-1" placeholder="John" name="name">
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button style="background-color:#006D5B;color: white;"type="submit" class="btn  waves-effect waves-light">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal -->
            <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <form action="{{route('editservice')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Edit</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" class="form-control" name="id_edit" id="id_edit" placeholder="John">
                                            <label for="field-1" class="control-label">Name</label>
                                            <input type="text" class="form-control" id="name_edit" name="name_edit" placeholder="John">
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button style="background-color:#006D5B;color: white;" type="submit" class="btn  waves-effect waves-light">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal -->
            <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <form action="{{route('deleteservice')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Delete</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Are you sure you want to delete <span id="name_delete"></span>?</label>
                                            <input type="hidden" class="form-control" id="id_delete" placeholder="John" name="id_delete" >
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button style="background-color:#006D5B;color: white;" type="submit" class="btn  waves-effect waves-light">Delete</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal -->







        </div>



</div>
</div>

<footer class="footer text-right">
    {{date('Y')}} © 1CGC
</footer>

</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- Bootstrap JavaScript -->
{{--<script src="assets/js/bootstrap.min.js"></script>--}}
<script>
    $('#service-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!!route('datatable.service')!!}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'service_name', name: 'service_name'},
            {data: 'created_at', name: 'created_at'},
            {data: 'updated_at', name: 'updated_at'},
            {data: 'action', name: 'action'}
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());

                            column.search(val ? val : '', true, false).draw();
                        });
            });
        }
    });
</script>
<script>
    $(document).on('click','.editbtn',function() {
//        alert("Hey");
        $('#name_edit').val($(this).data('name'));
        $('#id_edit').val($(this).data('id'));


    });
</script>
<script>
    $(document).on('click','.deletebtn',function() {
//        alert("Hey");

        $('#id_delete').val($(this).data('id'));


        $("#name_delete").html($(this).data('name'));

    });
</script>
@endsection

