

@extends('layout.localmaster')


<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/personal/datatable.css">


@section('content')
        <!-- Begin page -->
<div id="wrapper">






    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu" style="background-color: #006D5B">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">


                <ul>

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub" >
                            <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>

                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newmember" style="color: white">New Member</a></li>
                                <li><a href="allmember"style="color: white">All Members</a></li>

                            </ul>
                        </li>
                    @endif
                        <li class="has_sub">
                            <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>

                        </li>
                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-calendar" style="color: white" aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="newevent" style="color: white">New Event</a></li>


                                </ul>
                            </li>
                        @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin'||'Finance')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="offering" style="color: white">New</a></li>
                                <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                                <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                                <li><a href="expenses" style="color: white"> Expenses</a></li>

                            </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">

                                <li><a href="report" style="color: white"> Reports</a></li>
                                <li><a href="quarterlyrpt" style="color: white">Quaterly Reports</a></li>
                                <li><a href="weeklyrpt" style="color: white">Weeky Reports</a></li>

                            </ul>
                        </li>
                    @endif


                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="newattendance" style="color: white">New Attendance</a></li>
                                <li><a href="allattendance" style="color: white">All Attendance</a></li>
                              </ul>
                        </li>
                    @endif

                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="singlesms" style="color: white">Single</a></li>
                                <li><a href="bulksms" style="color: white">Bulk</a></li>
                            </ul>
                        </li>
                    @endif
                    @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="profile" style="color: white">Church Profile</a></li>
                                <li><a href="user" style="color: white">Users</a></li>
                                <li><a href="service " style="color: white">Service</a></li>
                                <li><a href="member_group" style="color: white">Member Group</a></li>
                                <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>

                            </ul>
                        </li>
                    @endif


                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">All Attendance</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="tables-datatable.html#">ICGC</a>
                                </li>
                                <li>
                                    <a href="tables-datatable.html#">Tables </a>
                                </li>
                                <li class="active">
                                    All members
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->



                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">

                            <h4 class="m-t-0 header-title"><b>All Record</b></h4>


                            <table id="attendance-table" class="table table-striped table-bordered">
                                <thead>

                                <tr>
                                    <th>Service Type</th>
                                    <th>No. of Male</th>
                                    <th>No. of Female</th>
                                    <th>No. of Visitors Male</th>
                                    <th>No. of Visitors Female</th>
                                    <th>No. of Cars</th>
                                    <th>Day</th>

                                    <th>Action</th>
                                </tr>
                                </thead>

                            </table>

                        </div>
                    </div>
                </div>

                <div id="edit-Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form action="{{'editattendance'}}" method="post">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3><span id="offeringtypeheader"></span></h3>
                                            <input type="hidden" class="form-control" id="attendanceedit_id" name="attendanceedit_id">
                                            <div class="form-group" >
                                                <label for="passWord2">Service Type<span class="text-danger">*</span></label>
                                                <select class="form-control" data-style="btn-info" name="servicetypeedit" id="servicetypeedit">
                                                    @foreach($servicetype as $s)

                                                        <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                 <label for="field-1" class="control-label">Registration Date</label>
                                                 <input type="date" class="form-control" name="registration_dateedit" id="registration_dateedit"/>
                                            </div>
                                            <div class="form-group">

                                                <label for="field-1" class="control-label">Day</label>
                                                <select class="selectpicker" data-style="btn-default" name="dayedit" id="dayedit" >
                                                    <option value="sunday">Sunday</option>
                                                    <option value="monday">Monday</option>
                                                    <option value="tuesday">Tuesday</option>
                                                    <option value="wednesday">Wednesday</option>
                                                    <option value="thursday">Thursday</option>
                                                    <option value="friday">Friday</option>
                                                    <option value="saturday">Saturday</option>

                                                </select>
                                            </div>
                                            <div class="form-group">

                                                <label for="field-1" class="control-label">No. Male</label>
                                                <input type="text" class="form-control" id="maleedit" name="maleedit" placeholder="John">
                                            </div>
                                            <div class="form-group">

                                                <label for="field-1" class="control-label">No. Female</label>
                                                <input type="text" class="form-control" id="femaleedit" name="femaleedit" placeholder="John">
                                            </div>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="form-group" id="member_input">
                                                <label for="emailAddress">No. Visitors Male:</label>
                                                <input type="text" class="form-control" id="visitormaleedit" name="visitormaleedit" >

                                            </div>
                                            <div class="form-group" id="member_input">
                                                <label for="emailAddress">No. Visitors Female:</label>
                                                <input type="text" class="form-control" id="visitorfemaleedit" name="visitorfemaleedit" >

                                            </div>
                                            <div class="form-group">

                                                <label for="field-1" class="control-label">No. Cars</label>
                                                <input type="text" class="form-control" id="caredit" name="caredit" >
                                            </div>
                                            <div class="form-group" id="stage_classes_form">
                                                <label for="field-1" class="control-label">Stage(FOR ABC CLASSES ONLY)</label>
                                                <select  class="form-control" id="stage_classes_edit" name="stage_classes_edit" >
                                                    <option value="">------</option>
                                                    <option value="ABC">ABC</option>
                                                    <option value="Membership">Membership</option>
                                                    <option value="Maturity">Maturity</option>
                                                    <option value="Ministry">Ministry</option>


                                                </select>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                    <button style="background-color:#006D5B;color: white;" type="submit" class="btn  waves-effect waves-light">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal -->
                <div id="delete-Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form action="{{'deleteattendance'}}" method="post">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">Delete</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Are you sure you want to delete offering number <span id="offering_number_delete_data"></span>?</label>
                                                <input type="hidden" class="form-control" id="offering_number_delete" name="offering_number_delete" >
                                            </div>
                                        </div>

                                    </div>
                                    {{csrf_field()}}
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                    <button style="background-color:#006D5B;color: white;" type="submit" class="btn  waves-effect waves-light">Delete</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal -->











            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            {{date('Y')}} © 1CGC
        </footer>

    </div>


    <script src="assets/js/jquery.min.js"></script>
    {{--<script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>--}}
    {{--<script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>--}}

    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Bootstrap JavaScript -->
    {{--<script src="assets/js/bootstrap.min.js"></script>--}}
    <script>
        $('#attendance-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!!route('datatable.allattendance')!!}',
            columns: [

                {data: 'servicetype', name: 'servicetype'},
                {data: 'male', name: 'male'},
                {data: 'female', name: 'female'},
                {data: 'visitor_male', name: 'visitor_male'},
                {data: 'visitor_female', name: 'visitor_female'},
                {data: 'car', name: 'car'},
                {data: 'day', name: 'day'},
                {data: 'action', name: 'action'}
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column.search(val ? val : '', true, false).draw();
                            });
                });
            }
        });
    </script>
    <script>
        $(document).on('click','.editbtn',function() {

            $('#attendanceedit_id').val($(this).data('id'));
            $('#servicetypeedit').val($(this).data('servicetype')).change();
            $('#maleedit').val($(this).data('male'));
            $('#femaleedit').val($(this).data('female'));
            $('#visitormaleedit').val($(this).data('visitormale'));
            $('#visitorfemaleedit').val($(this).data('visitorfemale'));
            $('#caredit').val($(this).data('car'));
            $('#dayedit').val($(this).data('day')).change();
            if($(this).data('stage_classes')==""){
                $('stage_classes_form').hide();
            }else{
                $('stage_classes_form').show();
                $('#stage_classes_edit').val($(this).data('stage_classes')).change();
            }

            $('#registration_dateedit').val($(this).data('created_at'));

        });
        $(document).on('click','.deletebtn',function() {
//            alert($(this).data('id'));
            $('#offering_number_delete').val($(this).data('id'));
//            $('#offering_number_delete_data').html($(this).data('number'));



        });
    </script>
</div>
@endsection

