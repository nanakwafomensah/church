

@extends('layout.localmaster')

@section('content')
    <div id="wrapper">

        <!-- ========== Left Sidebar Start ========== -->
        <div class="left side-menu" style="background-color: #006D5B">
            <div class="sidebar-inner slimscrollleft">

                <!--- Sidemenu -->
                <div id="sidebar-menu">
                    {{--<div class="user-details">--}}
                    {{--<div class="overlay"></div>--}}
                    {{--<div class="text-center">--}}
                    {{--<img src="assets/images/users/church.jpg" alt="" class="thumb-md img-circle">--}}
                    {{--</div>--}}
                    {{--<div class="user-info">--}}
                    {{--<div>--}}
                    {{--<a href="index.html#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">mamprobi branch <span class="mdi mdi-menu-down"></span></a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="dropdown" id="setting-dropdown">--}}
                    {{--<ul class="dropdown-menu">--}}
                    {{--<li><a href="javascript:void(0)"><i class="mdi mdi-face-profile m-r-5"></i> Profile</a></li>--}}
                    {{--<li><a href="javascript:void(0)"><i class="mdi mdi-account-settings-variant m-r-5"></i> Settings</a></li>--}}
                    {{--<li><a href="javascript:void(0)"><i class="mdi mdi-lock m-r-5"></i> Lock screen</a></li>--}}
                    {{--<li><a href="javascript:void(0)"><i class="mdi mdi-logout m-r-5"></i> Logout</a></li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}

                    <ul>

                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub" >
                                <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>
                                {{--<ul class="list-unstyled">--}}
                                {{--<li><a href="index.html">Dashboard 1</a></li>--}}
                                {{--<li><a href="dashboard_2.html">Dashboard 2</a></li>--}}
                                {{--</ul>--}}
                            </li>
                        @endif
                        {{--<li class="has_sub">--}}
                        {{--<a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-invert-colors"></i> <span> Information Mgt </span> <span class="menu-arrow"></span></a>--}}
                        {{--<ul class="list-unstyled">--}}
                        {{--<li><a href="ui-buttons.html">Add New Member</a></li>--}}
                        {{--<li><a href="ui-typography.html">Typography</a></li>--}}
                        {{--<li><a href="ui-panels.html">Panel</a></li>--}}
                        {{--<li><a href="ui-portlets.html">Portlets</a></li>--}}
                        {{--<li><a href="ui-modals.html">Modals</a></li>--}}
                        {{--<li><a href="ui-checkbox-radio.html">Checkboxs-Radios</a></li>--}}
                        {{--<li><a href="ui-tabs.html">Tabs</a></li>--}}
                        {{--<li><a href="ui-progressbars.html">Progress Bars</a></li>--}}
                        {{--<li><a href="ui-notifications.html">Notification</a></li>--}}
                        {{--<li><a href="ui-alerts.html">Alerts</a>--}}
                        {{--<li><a href="ui-carousel.html">Carousel</a>--}}
                        {{--<li><a href="ui-video.html">Video</a>--}}
                        {{--<li><a href="ui-tooltips-popovers.html">Tooltips & Popovers</a></li>--}}
                        {{--<li><a href="ui-images.html">Images</a></li>--}}
                        {{--<li><a href="ui-bootstrap.html">Bootstrap UI</a></li>--}}
                        {{--<li><a href="ui-grid.html">Grid</a></li>--}}
                        {{--</ul>--}}
                        {{--</li>--}}
                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="newmember" style="color: white">New Member</a></li>
                                    <li><a href="allmember"style="color: white">All Members</a></li>
                                    {{--<li><a href="admin-nestable.html">Nestable List</a></li>--}}
                                    {{--<li><a href="admin-rangeslider.html">Range Slider</a></li>--}}
                                    {{--<li><a href="admin-ratings.html">Ratings</a></li>--}}
                                    {{--<li><a href="admin-animation.html">Animation</a></li>--}}
                                </ul>
                            </li>
                        @endif
                            <li class="has_sub">
                                <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>

                            </li>
                            @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                                <li class="has_sub">
                                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-calendar" style="color: white"aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                                    <ul class="list-unstyled">
                                        <li><a href="newevent" style="color: white">New Event</a></li>

                                    </ul>
                                </li>
                            @endif
                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin'||'Finance')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="offering" style="color: white">New Offering</a></li>
                                    <li><a href="alloffering" style="color: white"> All Offering</a></li>
                                    <li><a href="expenses" style="color: white"> Expenses</a></li>


                                </ul>
                            </li>
                        @endif

                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="offering" style="color: white">New</a></li>
                                    <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                                    <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                                    <li><a href="expenses" style="color: white"> Expenses</a></li>
                                </ul>
                            </li>
                        @endif

                        {{--<li class="has_sub">--}}
                        {{--<a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text-o" aria-hidden="true"></i><span> Report </span> <span class="menu-arrow"></span></a>--}}
                        {{--<ul class="list-unstyled">--}}
                        {{--<li><a href="icons-glyphicons.html">Glyphicons</a></li>--}}
                        {{--<li><a href="icons-colored.html">Colored Icons</a></li>--}}
                        {{--<li><a href="icons-materialdesign.html">Material Design</a></li>--}}
                        {{--<li><a href="icons-ionicons.html">Ion Icons</a></li>--}}
                        {{--<li><a href="icons-fontawesome.html">Font awesome</a></li>--}}
                        {{--<li><a href="icons-themifyicon.html">Themify Icons</a></li>--}}
                        {{--<li><a href="icons-typicons.html">Typicons</a></li>--}}
                        {{--</ul>--}}
                        {{--</li>--}}
                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="newattendance" style="color: white">New Attendance</a></li>
                                    <li><a href="allattendance" style="color: white">All Attendance</a></li>

                                </ul>
                            </li>
                        @endif

                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="singlesms" style="color: white">Single</a></li>
                                    <li><a href="bulksms" style="color: white">Bulk</a></li>

                                </ul>
                            </li>
                        @endif
                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="profile" style="color: white">Church Profile</a></li>
                                    <li><a href="user" style="color: white">Users</a></li>
                                    <li><a href="service " style="color: white">Service</a></li>
                                    <li><a href="member_group" style="color: white">Member Group</a></li>
                                    <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>


                                </ul>
                            </li>
                        @endif


                    </ul>
                </div>
                <!-- Sidebar -->
                <div class="clearfix"></div>


            </div>
            <!-- Sidebar -left -->

        </div>
        <!-- Left Sidebar End -->
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">


                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Quarterly Form</h4>
                                <ol class="breadcrumb p-0 m-0">

                                    <li class="active">
                                        Quarterly Form  (Please fill up these basic details)
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
         <form method="post" action="{{route('quarterlyRpt')}}">
                    <div class="card-box">
                        <div class="row">


                                <div class="col-xs-12">



                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12 col-md-12">
                                                     <div class="form-group">
                                                        <label for="userName">FROM:</label>
                                                        <input type="date"  name="from_date" required
                                                               class="form-control" id="from_date"><br/>
                                                        <label for="userName">TO:</label>
                                                        <input type="date" name="to_date" required
                                                               class="form-control" id="to_date" >
                                                    </div>

                                            </div>
                                        </div>

                                </div>


                        </div>
                        <!-- end row -->


                    </div> <!-- container -->
                    <h4>CHURCH GROWTH AND EXPANSION</h4>
                    <div class="card-box">
                        <div class="row">


                            <div class="col-xs-12">



                                <div class="row">
                                    <div class="col-sm-12 col-xs-12 col-md-12">
                                        <div class="form-group">

                                            <table>
                                                <tr>
                                                    <td>1.	Was your assembly involved in the mission’s activities in the district?  </td>
                                                    <td>
                                                        {{--<form >--}}
                                                            <input type="radio" name="rad1" value="Yes" required /> Yes
                                                            <input type="radio" name="rad1" value="No" required /> No

                                                        {{--</form>--}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> 2.	Did your assembly in particular plant any church?  </td>
                                                    <td>
                                                        {{--<form >--}}
                                                            <input type="radio" name="rad2" value="Yes" required/> Yes
                                                            <input type="radio" name="rad2" value="No" required/> No

                                                        {{--</form>--}}
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                    </div>
                                </div>

                            </div>


                        </div>
                        <!-- end row -->


                    </div> <!-- container -->
                    <h4>MEMBER  ASSIMILATION</h4>
                    <div class="card-box">
                        <div class="row">


                            <div class="col-xs-12">



                                <div class="row">
                                    <div class="col-sm-12 col-xs-12 col-md-12">
                                        <div class="form-group">

                                            <table>
                                                <tr>
                                                    <td>
                                                        Do you hold a special ceremony to intentionally transition individuals who completed ABC into the service departments of the church?  </td>
                                                    <td>
                                                        {{--<form>--}}
                                                            <input type="radio" name="rad3" value="Yes" required/> Yes
                                                            <input type="radio" name="rad3" value="No" required/> No

                                                        {{--</form>--}}
                                                    </td>
                                                </tr>

                                            </table>

                                        </div>

                                    </div>
                                </div>

                            </div>


                        </div>
                        <!-- end row -->


                    </div> <!-- container -->
                    <h4>COUNSELLING WITH FAMILIES</h4>
                    <div class="card-box">
                        <div class="row">
                               <div class="col-xs-12">
                                  <div class="row">
                                    <div class="col-sm-12 col-xs-12 col-md-12">
                                        <div class="form-group">

                                            <table>
                                                <tr>
                                                    <td>
                                                        Does your church have a pre-marital Counseling program?
                                                    </td>
                                                    <td>
                                                        {{--<form >--}}
                                                            <input type="radio" name="rad4" value="Yes" required /> Yes
                                                            <input type="radio" name="rad4" value="No" required /> No

                                                        {{--</form>--}}
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        Does your church have a post-marital Counseling program?
                                                    </td>
                                                    <td>
                                                        {{--<form >--}}
                                                            <input type="radio" name="rad5" value="Yes" required /> Yes
                                                            <input type="radio" name="rad5" value="No" required /> No

                                                        {{--</form>--}}
                                                    </td>
                                                </tr>

                                            </table>

                                        </div>

                                    </div>
                                </div>

                            </div>


                        </div>
                        <!-- end row -->


                    </div> <!-- container -->
                    <h4>ADMINISTRATIVE SUPPORT SYSTEMS</h4>
                    <div class="card-box">
                        <div class="row">
                               <div class="col-xs-12">
                                  <div class="row">
                                    <div class="col-sm-12 col-xs-12 col-md-12">
                                        <div class="form-group">

                                            <table>
                                                <tr>
                                                    <td>
                                                        Does your church have an administrative support office?
                                                    </td>
                                                    <td>
                                                        {{--<form >--}}
                                                            <input type="radio" name="rad6" value="Yes" required /> Yes
                                                            <input type="radio" name="rad6" value="No" required /> No

                                                        {{--</form>--}}
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        Does your assembly use the church’s official letterhead
                                                    </td>
                                                    <td>
                                                        {{--<form >--}}
                                                            <input type="radio" name="rad7" value="Yes"  required/> Yes
                                                            <input type="radio" name="rad7" value="No" required /> No

                                                        {{--</form>--}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Is your assembly using the church’s approved signage?
                                                    </td>
                                                    <td>
                                                        {{--<form >--}}
                                                            <input type="radio" name="rad8" value="Yes" required /> Yes
                                                            <input type="radio" name="rad8" value="No"  required/> No

                                                        {{--</form>--}}
                                                    </td>
                                                </tr>

                                            </table>

                                        </div>

                                    </div>
                                </div>

                            </div>


                        </div>
                        <!-- end row -->
                    </div> <!-- container -->
                    <h4>RECORD KEEPING</h4>
                    <div class="card-box">
                         <div class="row">
                             <div class="col-xs-12">
                                 <div class="row">
                                     <div class="col-sm-12 col-xs-12 col-md-12">
                                         <div class="form-group">

                                             <table>
                                                 <tr>
                                                     <td>
                                                         Is your assembly operating the account management system introduced by the Head Office?
                                                     </td>
                                                     <td>
                                                         {{--<form >--}}
                                                         <input type="radio" name="rad9" value="Yes" required/> Yes
                                                         <input type="radio" name="rad9" value="No" required /> No

                                                         {{--</form>--}}
                                                     </td>
                                                 </tr>

                                                 <tr>
                                                     <td>
                                                         Did your assembly prepare an annual budget?
                                                     </td>
                                                     <td>
                                                         {{--<form >--}}
                                                         <input type="radio" name="rad10" value="Yes" required /> Yes
                                                         <input type="radio" name="rad10" value="No" required /> No

                                                         {{--</form>--}}
                                                     </td>
                                                 </tr>
                                                 <tr>
                                                     <td>
                                                         Did your financial operations follow the approved budget?
                                                     </td>
                                                     <td>
                                                         {{--<form >--}}
                                                         <input type="radio" name="rad11" value="Yes" required /> Yes
                                                         <input type="radio" name="rad11" value="No" required/> No

                                                         {{--</form>--}}
                                                     </td>
                                                 </tr>
                                                 <tr>
                                                     <td>
                                                         Are your accounts ready for auditors?
                                                     </td>
                                                     <td>
                                                         {{--<form >--}}
                                                         <input type="radio" name="rad12" value="Yes" required/> Yes
                                                         <input type="radio" name="rad12" value="No" required /> No

                                                         {{--</form>--}}
                                                     </td>
                                                 </tr>
                                                 <tr>
                                                     <td>
                                                         Do you as a pastor regularly use data kept by the administrative office to make decisions regarding church operations?
                                                     </td>
                                                     <td>
                                                         {{--<form >--}}
                                                         <input type="radio" name="rad13" value="Yes" required /> Yes
                                                         <input type="radio" name="rad13" value="No" required/> No

                                                         {{--</form>--}}
                                                     </td>
                                                 </tr> <tr>
                                                     <td>
                                                         Dis you send quarterly reports to the district office
                                                     </td>
                                                     <td>
                                                         {{--<form >--}}
                                                         <input type="radio" name="rad14" value="Yes" required /> Yes
                                                         <input type="radio" name="rad14" value="No" required /> No

                                                         {{--</form>--}}
                                                     </td>
                                                 </tr>

                                             </table>

                                         </div>

                                     </div>
                                 </div>

                             </div>


                         </div>
                         <!-- end row -->
                     </div> <!-- container -->
                    <h4>STATUTORY PAYMENTS</h4>
                    <div class="card-box">
                 <div class="row">
                     <div class="col-xs-12">
                         <div class="row">
                             <div class="col-sm-12 col-xs-12 col-md-12">
                                 <div class="form-group">

                                     <table>
                                         <tr>
                                             <td>
                                                 How many months payment were made to the Missions Common Fund (MCF) during the quarter under review?
                                             </td>
                                             <td>
                                                 {{--<form >--}}
                                                 <input type="radio" name="rad15" value="Yes" required /> Yes
                                                 <input type="radio" name="rad15" value="No" required /> No

                                                 {{--</form>--}}
                                             </td>
                                         </tr>

                                         <tr>
                                             <td>
                                                 Did you pay Provident fund for employees of the church?
                                             </td>
                                             <td>
                                                 {{--<form >--}}
                                                 <input type="radio" name="rad16" value="Yes" required/> Yes
                                                 <input type="radio" name="rad16" value="No" required/> No

                                                 {{--</form>--}}
                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 Do you pay social security for employed staff?
                                             </td>
                                             <td>
                                                 {{--<form >--}}
                                                 <input type="radio" name="rad17" value="Yes" required/> Yes
                                                 <input type="radio" name="rad17" value="No" required/> No

                                                 {{--</form>--}}
                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 Do you pay IRS [PAYE for employed staff?
                                             </td>
                                             <td>
                                                 {{--<form >--}}
                                                 <input type="radio" name="rad18" value="Yes"  required/> Yes
                                                 <input type="radio" name="rad18" value="No" required/> No

                                                 {{--</form>--}}
                                             </td>
                                         </tr>

                                     </table>

                                 </div>

                             </div>
                         </div>

                     </div>


                 </div>
                 <!-- end row -->
             </div> <!-- container -->
                     <h4>Administrative Support for Pastor </h4>
                    <div class="card-box">
                 <div class="row">
                     <div class="col-xs-12">
                         <div class="row">
                             <div class="col-sm-12 col-xs-12 col-md-12">
                                 <div class="form-group">

                                     <table>
                                         <tr>
                                             <td>
                                                 Which of the following do you have? (Please check which is applicable for your assembly)
                                             </td>
                                             <td>
                                                 {{--<form >--}}
                                                 <select name="do_you_have" class="form-control" required>
                                                     <option value="">--</option>
                                                     <option value="churchcouncil">Church Council</option>
                                                     <option value="managementcommitee">Management Committee</option>
                                                 </select>


                                                 {{--</form>--}}
                                             </td>
                                         </tr>

                                         <tr>
                                             <td>
                                                 How many members do you have serving on this Body?
                                             </td>
                                             <td>
                                                 {{--<form >--}}
                                                 <input type="text" name="members_do_you_have_serving_on_this_Body" class="form-control" required/>
                                                 {{--<input type="radio" name="rad16" value="No" /> No--}}

                                                 {{--</form>--}}
                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 How many times did they meet in the quarter?
                                             </td>
                                             <td>
                                                 {{--<form >--}}
                                                 <input type="text" name="times_did_they_meet_in_the_quarter" class="form-control" required />

                                                 {{--</form>--}}
                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 Indicate in the column below one major decision that you took together with the council of the management committee
                                             </td>
                                             <td>
                                                 {{--<form >--}}
                                                 <textarea row="3" name="decision" class="form-control" required></textarea>


                                                 {{--</form>--}}
                                             </td>
                                         </tr>

                                     </table>

                                 </div>

                             </div>
                         </div>

                     </div>


                 </div>
                 <!-- end row -->
             </div> <!-- container -->
                    <h4>LEADERSHIP DEVELOPMENT </h4>
                    <div class="card-box">
                 <div class="row">
                     <div class="col-xs-12">
                         <div class="row">
                             <div class="col-sm-12 col-xs-12 col-md-12">
                                 <div class="form-group">

                                     <table>
                                         <tr>
                                             <td>
                                                 What is the total number of your lay leaders?
                                             </td>
                                             <td>
                                                <input type="text" name="no_of_layleaders" class="form-control"  required />

                                             </td>
                                         </tr>

                                         <tr>
                                             <td>
                                                 Did you train individuals in basic leadership principles before appointing them to leadership positions in the church?
                                             </td>
                                             <td>
                                                 <input type="radio" name="rad19" value="Yes" required /> Yes
                                                 <input type="radio" name="rad19" value="No" required /> No

                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 Did your leaders receive orientation regarding the specific positions you appointed them to? E.g. youth facilitators’ orientation?
                                             </td>
                                             <td>
                                                 <input type="radio" name="rad20" value="Yes" required /> Yes
                                                 <input type="radio" name="rad20" value="No" required /> No
                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 How many programs did you organize to improve the capacity and skills of your leaders after being appointed?
                                             </td>
                                             <td>
                                                 <input type="text" name="no_of_programs" class="form-control" required  />

                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 How many lay leaders participated in the training program(s) you organized?
                                             </td>
                                             <td>
                                                 <input type="text" name="leaders_in_training" class="form-control"  required />

                                             </td>
                                         </tr>

                                     </table>

                                 </div>

                             </div>
                         </div>

                     </div>


                 </div>
                 <!-- end row -->
             </div> <!-- container -->
                    <br/>
                    <h4>LEGACY & SUCCESSION </h4>
                    <div class="card-box">
                 <div class="row">
                     <div class="col-xs-12">
                         <div class="row">
                             <div class="col-sm-12 col-xs-12 col-md-12">
                                 <div class="form-group">
                                     <h4>Children’s Ministries </h4>
                                     <table>
                                         <tr>
                                             <td>
                                                 Do you have a suitable meeting place for your children’s ministry?
                                             </td>
                                             <td>
                                                 <input type="radio" name="rad21" value="Yes" required/> Yes
                                                 <input type="radio" name="rad21" value="No" required /> No
                                             </td>
                                         </tr>

                                         <tr>
                                             <td>
                                                 How many Children’s ministry facilitators do you have in the children’s church?

                                               </td>
                                             <td>
                                                 <input type="text" class="form-control" name="children_facilitators" required  />

                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 How many times did the pastor meet with the children’s ministry facilitators?
                                             </td>
                                             <td>
                                                 <input type="text" class="form-control" name="pastor_meet_children"  required />
                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 How often did the pastor interact with the children’s ministry this quarter?
                                             </td>
                                             <td>
                                                 <input type="text" class="form-control"  name="pastor_interact_children" required />

                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 Do you involve the parents in the children’s church?
                                             </td>
                                             <td>
                                                 <input type="radio" name="rad22" value="Yes" required /> Yes
                                                 <input type="radio" name="rad22" value="No"  required/> No
                                             </td>
                                         </tr>

                                     </table>

                                 </div>

                             </div>
                         </div>

                     </div>


                 </div>
                 <!-- end row -->
             </div> <!-- container -->
                <h4>Youth Ministries</h4>
                    <div class="card-box">
                 <div class="row">
                     <div class="col-xs-12">
                         <div class="row">
                             <div class="col-sm-12 col-xs-12 col-md-12">
                                 <div class="form-group">

                                     <table>
                                         <tr>
                                             <td>
                                                 Do you have a suitable meeting place for your youth ministry?
                                             </td>
                                             <td>
                                                 <input type="radio" name="rad23" value="Yes"  required/> Yes
                                                 <input type="radio" name="rad23" value="No" required /> No
                                             </td>
                                         </tr>

                                         <tr>
                                             <td>
                                                 How many Children’s ministry facilitators do you have in the youth church?
                                             </td>
                                             <td>
                                                 <input required type="text" class="form-control" name="youth_children_facilatators"  />

                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 How many times did the pastor meet with the youth ministry facilitators?
                                             </td>
                                             <td>
                                                 <input required type="text" class="form-control" name="youth_pastor_meet_facilatators"  />
                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 How often did the pastor interact with the youth ministry this quarter?
                                             </td>
                                             <td>
                                                 <input required type="text" class="form-control"  name="pastor_interact_youth" />

                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 Do you involve the parents in the youth church?
                                             </td>
                                             <td>
                                                 <input type="radio" name="rad24" value="Yes" required /> Yes
                                                 <input type="radio" name="rad24" value="No"  required/> No
                                             </td>
                                         </tr>

                                     </table>

                                 </div>

                             </div>
                         </div>

                     </div>


                 </div>
                 <!-- end row -->
             </div> <!-- container -->
                    <br/>
                    <h3>SOCIAL INTERVENTION</h3>
                    <div class="card-box">
                 <div class="row">
                     <div class="col-xs-12">
                         <div class="row">
                             <div class="col-sm-12 col-xs-12 col-md-12">
                                 <div class="form-group">
                                     <h5>Fund-Raising for corporate Social Action </h5>
                                     <table>
                                         <tr>
                                             <td>
                                                 Did your assembly organize Gold Frankincense and Myrrh?
                                             </td>
                                             <td>
                                                 <input type="radio" name="rad25" value="Yes" required /> Yes
                                                 <input type="radio" name="rad25" value="No" required/> No
                                             </td>
                                         </tr>

                                         <tr>
                                             <td>
                                                 Did your assembly send the funds raised during the GFM to the Head Office?

                                             </td>
                                             <td>
                                                 <input type="radio" name="rad26" value="Yes" required /> Yes
                                                 <input type="radio" name="rad26" value="No" required /> No
                                             </td>
                                         </tr>

                                         <tr>
                                             <td>
                                                 Did your church observe the Central Aid Day?
                                             </td>
                                             <td>
                                                 <input type="radio" name="rad27" value="Yes" required /> Yes
                                                 <input type="radio" name="rad27" value="No" required /> No

                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 Were the funds realized from the CA Day sent to the Head office?
                                             </td>
                                             <td>
                                                 <input type="radio" name="rad28" value="Yes" required /> Yes
                                                 <input type="radio" name="rad28" value="No" required /> No
                                             </td>
                                         </tr>

                                     </table>

                                 </div>

                             </div>
                         </div>

                     </div>


                 </div>
                 <!-- end row -->
             </div> <!-- container -->
                    <h4>Local Involvement </h4>
                    <div class="card-box">
                 <div class="row">
                     <div class="col-xs-12">
                         <div class="row">
                             <div class="col-sm-12 col-xs-12 col-md-12">
                                 <div class="form-group">

                                     <p>What did your assembly do as social intervention?</p>
                                     <table>
                                         <tr>
                                             <td>
                                                 Donation to orphanages
                                             </td>
                                             <td>
                                                 <input type="radio" name="rad29" value="Yes" required /> Yes
                                                 <input type="radio" name="rad29" value="No" required /> No
                                             </td>
                                         </tr>

                                         <tr>
                                             <td>
                                                 Donation to hospitals

                                             </td>
                                             <td>
                                                 <input type="radio" name="rad30" value="Yes" required /> Yes
                                                 <input type="radio" name="rad30" value="No" required /> No
                                             </td>
                                         </tr>

                                         <tr>
                                             <td>
                                                 Community development project [briefly describe]
                                             </td>
                                             <td>
                                                 <textarea required class="form-control" row="3" name="community_project"  ></textarea>

                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 Other(s)
                                               </td>
                                             <td>
                                                 <textarea required class="form-control" row="3" name="others"  ></textarea>

                                             </td>
                                         </tr>


                                     </table>

                                 </div>

                             </div>
                         </div>

                     </div>


                 </div>
                 <!-- end row -->
             </div> <!-- container -->
                <h4>Special Financial Obligations </h4>
             <div class="card-box">
                 <div class="row">
                     <div class="col-xs-12">
                         <div class="row">
                             <div class="col-sm-12 col-xs-12 col-md-12">
                                 <div class="form-group">


                                     <table>
                                         <tr>
                                             <td>
                                                 Total Common Fund Payments
                                             </td>
                                             <td>
                                                 <input required type="text" name="Total_Common_Fund_Payments" class="form-control" />

                                             </td>
                                         </tr>

                                         <tr>
                                             <td>
                                                 Central Aid Day Contribution

                                             </td>
                                             <td>
                                                 <input required type="text" name="Central_Aid_Day_Contribution" class="form-control"/>

                                             </td>
                                         </tr>



                                     </table>

                                 </div>

                             </div>
                         </div>

                     </div>


                 </div>
                 <!-- end row -->
             </div> <!-- container -->

                    {{csrf_field()}}
                    <input type="submit"   class="btn btn-block btn-sm btn-success " value="PROCEED TO DOWNLOAD"/>
         </form>
                </div> <!-- content -->

                <footer class="footer text-right">
                    {{date('Y')}} © 1CGC
                </footer>

            </div>

        </div>
        <script src="assets/js/jquery.min.js"></script>
        <script>
            $('#proceed_download').on('click', function(e) {
                e.preventDefault();
                var from_date=$("#from_date").val() ;
                var to_date=$("#to_date").val() ;

                 var rad1=$('input[name=rad1]:checked').val();
                 var rad2=$('input[name=rad2]:checked').val();
                 var rad3=$('input[name=rad3]:checked').val();

                $.ajax({
                    type:'post' ,
                    url: '{{URL::to('quarterlyRpt')}}',
                    data:{
                        '_token':$('input[name=_token]').val(),
                        'from_date':from_date,
                        'to_date':to_date,
                        'rad1':rad1,
                        'rad2':rad2,
                        'rad3':rad3



                    },
                    success:function(data){
       }
                });
            });
        </script>
        @endsection
                <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->