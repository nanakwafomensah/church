

@extends('layout.localmaster')

@section('content')
    <div id="wrapper">

        <!-- ========== Left Sidebar Start ========== -->
        <div class="left side-menu" style="background-color: #006D5B">
            <div class="sidebar-inner slimscrollleft">

                <!--- Sidemenu -->
                <div id="sidebar-menu">

                    <ul>

                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub" >
                                <a href="dashboard" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Dashboard </span></a>

                            </li>
                        @endif

                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-info-circle" style="color: white"aria-hidden="true"></i><span style="color: white"> Information Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="newmember" style="color: white">New Member</a></li>
                                    <li><a href="allmember"style="color: white">All Members</a></li>

                                </ul>
                            </li>
                        @endif
                            <li class="has_sub">
                                <a href="classes" class="waves-effect"><i class="fa fa-graduation-cap" style="color: white"aria-hidden="true"></i><span style="color: white">Classes</span> <span class="menu-arrow" style="color: white"></span></a>

                            </li>
                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-calendar" style="color: white" aria-hidden="true"></i><span style="color: white"> Event Mgt </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="newevent" style="color: white">New Event</a></li>

                                </ul>
                            </li>
                        @endif
                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin'||'Finance')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="offering" style="color: white">New</a></li>
                                    <li><a href="alloffering" style="color: white">All Major Offerings</a></li>
                                    <li><a href="alldaybornoffering" style="color: white">DayBorn Offerings</a></li>
                                    <li><a href="expenses" style="color: white"> Expenses</a></li>

                                </ul>
                            </li>
                        @endif

                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">

                                    <li><a href="report" style="color: white"> Reports</a></li>
                                    <li><a href="quarterlyrpt" style="color: white">Quaterly Reports</a></li>
                                    <li><a href="weeklyrpt" style="color: white">Weeky Reports</a></li>

                                </ul>
                            </li>
                        @endif

                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o" aria-hidden="true" style="color: white" ></i><span style="color: white"> Attendance</span><span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="newattendance" style="color: white">New Attendance</a></li>
                                    <li><a href="allattendance" style="color: white">All Attendance</a></li>

                                </ul>
                            </li>
                        @endif

                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn" style="color: white"aria-hidden="true"></i> <span style="color: white"> Broadcast SMS</span><span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="singlesms" style="color: white">Single</a></li>
                                    <li><a href="bulksms" style="color: white">Bulk</a></li>

                                </ul>
                            </li>
                        @endif
                        @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='Admin')
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog" style="color: white" aria-hidden="true"></i> <span style="color: white"> Settings </span> <span class="menu-arrow" style="color: white"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="profile" style="color: white">Church Profile</a></li>
                                    <li><a href="user" style="color: white">Users</a></li>
                                    <li><a href="service " style="color: white">Service</a></li>
                                    <li><a href="member_group" style="color: white">Member Group</a></li>
                                    <li><a href="convenantfamily" style="color: white">Convenant Family</a></li>


                                </ul>
                            </li>
                        @endif


                    </ul>
                </div>
                <!-- Sidebar -->
                <div class="clearfix"></div>


            </div>
            <!-- Sidebar -left -->

        </div>
        <!-- Left Sidebar End -->
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">


                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Weekly Form</h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="dashboard">ICGC</a>
                                    </li>
                                    <li>
                                        <a href="weeklyrpt">Weekly Form </a>
                                    </li>
                                    <li class="active">
                                        Weekly Form
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <form action="weeklyRpt" method="post">

                    <div class="card-box">

                        <div class="row">
                             <div class="col-sm-12 col-xs-12 col-md-12">
                                        <div class="form-group">
                                            <label for="userName">FROM:</label>
                                            <input type="date"  name="from_date" required
                                                   class="form-control" id="from_date"><br/>
                                            <label for="userName">TO:</label>
                                            <input type="date" name="to_date" required
                                                   class="form-control" id="to_date" >
                                        </div>

                                    </div>
                            </div>
                        <!-- end row -->


                    </div> <!-- container -->
                        <div class="card-box">

                            <div class="row">
                                <div class="col-sm-4 col-xs-4 col-md-4">
                                    <div class="form-group">
                                        <label for="userName">Convenant family baseline value:</label>
                                        <input type="text"  name="baseline" required class="form-control" ><br/>

                                    </div>

                                </div>
                                <div class="col-sm-4 col-xs-4 col-md-4">
                                    <div class="form-group">
                                        <label for="userName">Convenant family that met:</label>
                                        <input type="text" name="convenant_did_met" required class="form-control" ><br/>

                                    </div>

                                </div>
                                <div class="col-sm-4 col-xs-4 col-md-4">
                                    <div class="form-group">
                                        <label for="userName">Convenant family that did not meet:</label>
                                        <input type="text"  name="convenant_did_not_met" required class="form-control" ><br/>

                                    </div>

                                </div>
                            </div>
                            <!-- end row -->


                        </div>
                        <h4>Portion for Missions Common Fund (MCF)</h4>
                        <div class="card-box">

                            <div class="row">
                                <div class="col-sm-4 col-xs-4 col-md-4">
                                    <div class="form-group">
                                        <label for="userName">Provident fund for employees :</label>
                                        <input type="text"  name="Provident_fund_for_employees" required class="form-control" ><br/>

                                    </div>

                                </div>
                                <div class="col-sm-4 col-xs-4 col-md-4">
                                    <div class="form-group">
                                        <label for="userName">Social Security for employed staff:</label>
                                        <input type="text" name="Social_Security_for_employed_staff" required class="form-control" ><br/>

                                    </div>

                                </div>
                                <div class="col-sm-4 col-xs-4 col-md-4">
                                    <div class="form-group">
                                        <label for="userName">IRS - PAYE for employed staff:</label>
                                        <input type="text"  name="PAYE_for_employed_staff" required class="form-control" ><br/>

                                    </div>

                                </div>
                            </div>
                            <!-- end row -->


                        </div>
                        <h4>LEADERSHIP DEVELOPMENT</h4>
                        <div class="card-box">
                          <p>Preparing people for serving in departments</p>
                            <div class="row">
                                <table style="width:100%">
                                    <thead>
                                    <tr>
                                        <td style="width:40%"><strong>Department</strong></td>
                                        <td style="width:40%"><strong>Nature of program</strong></td>
                                        <td style="width:20%"><strong>Attendance</strong></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Children Department</td>&nbsp;&nbsp;
                                        <td><input name="child_dpt_nature" required type="" class="form-control"/></td>&nbsp;&nbsp;
                                        <td><input name="child_dpt_atten" required type="" class="form-control"/></td>&nbsp;&nbsp;
                                    <tr>
                                    <tr>
                                        <td>Youth Department</td>
                                        <td><input name="youth_dpt_nature" required type="" class="form-control"/></td>
                                        <td><input name="youth_dpt_atten"  required type="" class="form-control"/></td>
                                    <tr>
                                    <tr>
                                        <td>Covenant Family Leaders</td>
                                        <td><input name="convenant_fami_nature" required type="" class="form-control"/></td>
                                        <td><input name="convenant_fami_atten"  required type="" class="form-control"/></td>
                                    <tr>
                                    <tr>
                                        <td>Counselors</td>
                                        <td><input name="Counselors_nature" required type="" class="form-control"/></td>
                                        <td><input name="Counselors_atten" required type="" class="form-control"/></td>
                                    <tr>
                                    <tr>
                                        <td>Church Council/Management</td>
                                        <td><input name="Church_Council_nature" required type="" class="form-control"/></td>
                                        <td><input name="Church_Council_atten" required type="" class="form-control"/></td>
                                    <tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- end row -->


                        </div>
                        <h4>LEGACY & SUCCESSION</h4>
                        <div class="card-box">
                            <p>Preparing people for serving in departments</p>
                            <div class="row">
                                <table style="width:100%">
                                    <thead>
                                    <tr>
                                        <td  style="width:50%"><strong>Ministry/Department</strong></td>
                                        <td  style="width:50%"><strong>Nature/Purpose of Meeting</strong></td>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><input name="ministry_department1" required type="" class="form-control"/></td>
                                        <td><input name="Purpose_of_Meeting1" required type="" class="form-control"/></td>
                                     </tr>
                                    <tr>
                                        <td><input name="ministry_department2" required type="" class="form-control"/></td>
                                        <td><input name="Purpose_of_Meeting2" required type="" class="form-control"/></td>
                                         <tr>
                                     </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- end row -->


                        </div>
                        {{csrf_field()}}

                    <input type="submit"  class="btn btn-block btn-sm btn-success " value="PROCEED TO DOWNLOAD"/>
</form>
                </div> <!-- content -->

                <footer class="footer text-right">
                    {{date('Y')}} © 1CGC
                </footer>

            </div>

        </div>
        <script src="assets/js/jquery.min.js"></script>
        <script>
            $('#proceed_download').on('click', function(e) {
                e.preventDefault();
                var from_date=$("#from_date").val() ;
                var to_date=$("#to_date").val() ;



                $.ajax({
                    type:'post' ,
                    url: '{{URL::to('weeklyRpt')}}',
                    data:{
                        '_token':$('input[name=_token]').val(),
                        'from_date':from_date,
                        'to_date':to_date
                      },
                    success:function(data){
                    }
                });
            });
        </script>
        @endsection
                <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->