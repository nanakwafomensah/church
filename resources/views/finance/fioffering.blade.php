

@extends('layout.localmaster')


@section('content')

    <div id="wrapper">
        <!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu" style="background-color: #006D5B">
    <div class="sidebar-inner slimscrollleft">

        <!--- Sidemenu -->
        <div id="sidebar-menu">


            <ul>


                <li class="has_sub" >
                    <a href="viewallmembers" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Members</span></a>

                </li>



                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="fioffering" style="color: white">New Offering</a></li>
                        <li><a href="fialloffering" style="color: white"> All Offering</a></li>


                    </ul>
                </li>
                 <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                    <ul class="list-unstyled">

                        <li><a href="oreport" style="color: white"> Reports</a></li>

                    </ul>
                </li>

            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>


    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
        <!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">


            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Offerings</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="dashboard">ICGC</a>
                            </li>
                            <li>
                                <a href="alloffering">All Offerings</a>
                            </li>
                            <li class="active">
                                Offerings
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Menu</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="nav nav-pills m-b-30 pull-right">
                                <li class="active">
                                    <a href="ui-tabs.html#navpills-1" data-toggle="tab" aria-expanded="true" >Main</a>
                                </li>
                                <li class="">
                                    <a href="ui-tabs.html#navpills-2" data-toggle="tab" aria-expanded="false">Thanksgiving</a>
                                </li>
                                <li class="">
                                    <a href="ui-tabs.html#navpills-3" data-toggle="tab" aria-expanded="false">Project</a>
                                </li>
                                <li class="">
                                    <a href="ui-tabs.html#navpills-4" data-toggle="tab" aria-expanded="false">Tithe</a>
                                </li>
                                <li class="">
                                    <a href="ui-tabs.html#navpills-5" data-toggle="tab" aria-expanded="false">Pledge</a>
                                </li>
                                <li class="">
                                    <a href="ui-tabs.html#navpills-6" data-toggle="tab" aria-expanded="false">Welfare</a>
                                </li>
                                <li class="">
                                    <a href="ui-tabs.html#navpills-7" data-toggle="tab" aria-expanded="false">Other Income</a>
                                </li>
                            </ul>
                            <div class="tab-content br-n pn">
                                <div id="navpills-1" class="tab-pane active">
                                    <div class="row">
                                        <div class="col-md-8">
                                        <form action="saveoffering" method="post" data-parsley-validate="">
                                            <div class="p-20">
                                                <div class="form-group">
                                                    <label for="userName">Date Received :<span class="text-danger">*</span></label>
                                                    <input type="date" name="date_submitted"
                                                           class="form-control" id="userName" required>
                                                </div>
                                                <input type="hidden" name="offeringtype" value="mainoffering"/>
                                                <input type="hidden" name="number" value="{{mt_rand(100000, 999999).'M'}}"/>
                                                <input type="hidden" name="member_id" value="ICGC"/>
                                                <div class="form-group">
                                                    <label for="emailAddress">Amount :<span class="text-danger">*</span></label>
                                                    <input type="text" name="amount" required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"
                                                           class="form-control" id="emailAddress">
                                                </div>
                                                <div class="form-group">
                                                    <label for="emailAddress">Description :<span class="text-danger">*</span></label>
                                                    <textarea class="form-control" name="description" rows="4"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="passWord2">Service Type<span class="text-danger">*</span></label>
                                                    <select class="selectpicker" data-style="btn-default" name="servicetype">
                                                    @foreach($servicetype as $s)

                                                            <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                     @endforeach
                                                    </select>
                                                </div>
                                                {{csrf_field()}}
                                                <div class="form-group text-right m-b-0">
                                                    <button  style="background-color:#006D5B;color: white;" class="btn waves-effect waves-light" type="submit">
                                                        Submit
                                                    </button>
                                                    <button type="reset" class="btn btn-default waves-effect m-l-5">
                                                        Cancel
                                                    </button>
                                                </div>
                                              </div>
                                        </form>
                                        </div>
                                        <div class="col-md-4">
                                            <img src="assets/images/small/img-1.jpg" class="img-responsive thumbnail m-r-15">

                                        </div>
                                    </div>
                                </div>
                                <div id="navpills-2" class="tab-pane">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <form action="saveoffering" method="post" data-parsley-validate="">
                                            <div class="p-20">
                                                <div class="form-group">
                                                    <label for="userName">Offering Number :<span class="text-danger">*</span></label>
                                                    <input type="text" name="number" value="{{mt_rand(100000, 999999).'G'}}"
                                                           class="form-control" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="userName">Date Received :<span class="text-danger">*</span></label>
                                                    <input type="date" name="date_submitted" required
                                                           class="form-control" id="userName">
                                                </div>
                                                <input type="hidden" name="offeringtype" value="thanksgivingoffering"/>
                                                <div class="form-group">
                                                    <label for="emailAddress">Member Name:<span class="text-danger">*</span></label>
                                                    <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="member_id" >
                                                        @foreach($allmembers as $s)

                                                                <option value="{{$s->firstname." ".$s->lastname}}">{{$s->firstname." ".$s->lastname}}</option>


                                                        @endforeach


                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="emailAddress">Amount :<span class="text-danger">*</span></label>
                                                    <input type="text" name="amount" parsley-trigger="change" required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"
                                                           class="form-control" id="emailAddress">
                                                </div>
                                                <div class="form-group">
                                                    <label for="emailAddress">Description :<span class="text-danger">*</span></label>
                                                    <textarea class="form-control" name="description" rows="4"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="passWord2">Service Type<span class="text-danger">*</span></label>
                                                    <select class="selectpicker" data-style="btn-default" name="servicetype">
                                                        @foreach($servicetype as $s)

                                                            <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                {{csrf_field()}}
                                                <div class="form-group text-right m-b-0">
                                                    <button style="background-color:#006D5B;color: white;" class="btn waves-effect waves-light" type="submit">
                                                        Submit
                                                    </button>
                                                    <button type="reset" class="btn btn-default waves-effect m-l-5">
                                                        Cancel
                                                    </button>
                                                </div>
                                            </div>
                                                </form>
                                        </div>
                                        <div class="col-md-4">
                                            <img src="assets/images/small/img-1.jpg" class="img-responsive thumbnail m-r-15">

                                        </div>
                                    </div>
                                </div>
                                <div id="navpills-3" class="tab-pane">
                                    <div class="row">
                                        <div class="col-md-8">

                                            <form action="saveoffering" method="post" data-parsley-validate="">
                                                <div class="p-20">
                                                    <div class="form-group">
                                                        <label for="userName">Offering Number :<span class="text-danger">*</span></label>
                                                        <input type="text" name="number" value="{{mt_rand(100000, 999999).'P'}}"
                                                               class="form-control" readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="userName">Date Received :<span class="text-danger">*</span></label>
                                                        <input type="date" name="date_submitted" required
                                                               class="form-control" id="userName">
                                                    </div>
                                                    <input type="hidden" name="offeringtype" value="projectoffering"/>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Member Name:<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="member_id" >
                                                            @foreach($allmembers as $s)

                                                                <option value="{{$s->firstname." ".$s->lastname}}">{{$s->firstname." ".$s->lastname}}</option>


                                                            @endforeach
                                                                <option value="{{"others"}}">{{"others"}}</option>


                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Amount :<span class="text-danger">*</span></label>
                                                        <input type="text" name="amount" required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"
                                                               class="form-control" id="emailAddress">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Description :<span class="text-danger">*</span></label>
                                                        <textarea class="form-control" name="description" rows="4"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="passWord2">Service Type<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-style="btn-default" name="servicetype">
                                                            @foreach($servicetype as $s)

                                                                <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    {{csrf_field()}}
                                                    <div class="form-group text-right m-b-0">
                                                        <button style="background-color:#006D5B;color: white;" class="btn waves-effect waves-light" type="submit">
                                                            Submit
                                                        </button>
                                                        <button type="reset" class="btn btn-default waves-effect m-l-5">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-4">
                                            <img src="assets/images/small/img-1.jpg" class="img-responsive thumbnail m-r-15">

                                        </div>
                                    </div>
                                </div>
                                <div id="navpills-4" class="tab-pane">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <form action="saveoffering" method="post" data-parsley-validate="">
                                                <div class="p-20">
                                                    <div class="form-group">
                                                        <label for="userName">Offering Number :<span class="text-danger">*</span></label>
                                                        <input  pattern=".{7,7}" type="text" name="number"
                                                               class="form-control" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="userName">Date Received :<span class="text-danger">*</span></label>
                                                        <input type="date" name="date_submitted"required
                                                               class="form-control" id="userName">
                                                    </div>
                                                    <input type="hidden" name="offeringtype" value="titheoffering"/>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Member Name:<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="member_id" >
                                                            @foreach($allmembers as $s)

                                                                <option value="{{$s->firstname." ".$s->lastname}}">{{$s->firstname." ".$s->lastname}}</option>


                                                            @endforeach


                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Amount :<span class="text-danger">*</span></label>
                                                        <input type="text" name="amount" required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"
                                                               class="form-control" id="emailAddress">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Description :<span class="text-danger">*</span></label>
                                                        <textarea class="form-control" name="description" rows="4"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="passWord2">Service Type<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-style="btn-default" name="servicetype">
                                                            @foreach($servicetype as $s)

                                                                <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    {{csrf_field()}}
                                                    <div class="form-group text-right m-b-0">
                                                        <button style="background-color:#006D5B;color: white;" class="btn  waves-effect waves-light" type="submit">
                                                            Submit
                                                        </button>
                                                        <button type="reset" class="btn  waves-effect m-l-5" >
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-4">
                                            <img src="assets/images/small/img-1.jpg" class="img-responsive thumbnail m-r-15">

                                        </div>
                                    </div>
                                </div>
                                <div id="navpills-5" class="tab-pane">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <form action="saveoffering" method="post" data-parsley-validate="">
                                                <div class="p-20">
                                                    <div class="form-group">
                                                        <label for="userName">Offering Number :<span class="text-danger">*</span></label>
                                                        <input type="text" name="number" value="{{mt_rand(100000, 999999).'PD'}}"
                                                               class="form-control" readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="userName">Date Received :<span class="text-danger">*</span></label>
                                                        <input type="date" name="date_submitted" required
                                                               class="form-control" id="userName">
                                                    </div>
                                                    <input type="hidden" name="offeringtype" value="pledgeoffering"/>
                                                    <input type="hidden" name="member_id" value="ICGC"/>
                                                    {{--<div class="form-group">--}}
                                                        {{--<label for="emailAddress">Member Name:<span class="text-danger">*</span></label>--}}
                                                        {{--<select class="selectpicker" data-live-search="true"  data-style="btn-default" name="member_id" >--}}
                                                            {{--@foreach($allmembers as $s)--}}

                                                                {{--<option value="{{$s->firstname." ".$s->lastname}}">{{$s->firstname." ".$s->lastname}}</option>--}}


                                                            {{--@endforeach--}}


                                                        {{--</select>--}}
                                                    {{--</div>--}}
                                                    <div class="form-group">
                                                        <label for="emailAddress">Amount :<span class="text-danger">*</span></label>
                                                        <input type="text" name="amount" parsley-trigger="change" required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"
                                                               class="form-control" id="emailAddress">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Description :<span class="text-danger">*</span></label>
                                                        <textarea class="form-control" name="description" rows="4"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="passWord2">Service Type<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-style="btn-default" name="servicetype">
                                                            @foreach($servicetype as $s)

                                                                <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    {{csrf_field()}}
                                                    <div class="form-group text-right m-b-0">
                                                        <button style="background-color:#006D5B;color: white;" class="btn waves-effect waves-light" type="submit">
                                                            Submit
                                                        </button>
                                                        <button type="reset" class="btn btn-default waves-effect m-l-5">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-4">
                                            <img src="assets/images/small/img-1.jpg" class="img-responsive thumbnail m-r-15">

                                        </div>
                                    </div>
                                </div>
                                <div id="navpills-6" class="tab-pane">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <form action="saveoffering" method="post" data-parsley-validate="">
                                                <div class="p-20">
                                                    <div class="form-group">
                                                        <label for="userName">Offering Number :<span class="text-danger">*</span></label>
                                                        <input type="text" name="number" value="{{mt_rand(100000, 999999).'W'}}"
                                                               class="form-control" readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="userName">Date Received :<span class="text-danger">*</span></label>
                                                        <input type="date" name="date_submitted" required
                                                               class="form-control" id="userName">
                                                    </div>
                                                    <input type="hidden" name="offeringtype" value="welfareoffering"/>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Member Name:<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="member_id" >
                                                            @foreach($allmembers as $s)

                                                                <option value="{{$s->firstname." ".$s->lastname}}">{{$s->firstname." ".$s->lastname}}</option>


                                                            @endforeach


                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Amount :<span class="text-danger">*</span></label>
                                                        <input type="text" name="amount" parsley-trigger="change" required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"
                                                               class="form-control" id="emailAddress">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Description :<span class="text-danger">*</span></label>
                                                        <textarea class="form-control" name="description" rows="4"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="passWord2">Service Type<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-style="btn-default" name="servicetype">
                                                            @foreach($servicetype as $s)

                                                                <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    {{csrf_field()}}
                                                    <div class="form-group text-right m-b-0">
                                                        <button style="background-color:#006D5B;color: white;" class="btn waves-effect waves-light" type="submit">
                                                            Submit
                                                        </button>
                                                        <button type="reset" class="btn btn-default waves-effect m-l-5">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-4">
                                            <img src="assets/images/small/img-1.jpg" class="img-responsive thumbnail m-r-15">

                                        </div>
                                    </div>
                                </div>
                                <div id="navpills-7" class="tab-pane">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <form action="saveoffering" method="post" data-parsley-validate="">
                                                <div class="p-20">
                                                    <div class="form-group">
                                                        <label for="userName">Offering Number :<span class="text-danger">*</span></label>
                                                        <input type="text" name="number" value="{{mt_rand(100000, 999999).'O'}}"
                                                               class="form-control" readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="userName">Date Received :<span class="text-danger">*</span></label>
                                                        <input type="date" name="date_submitted" required
                                                               class="form-control" id="userName">
                                                    </div>
                                                    <input type="hidden" name="offeringtype" value="othersoffering"/>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Member Name:<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-live-search="true"  data-style="btn-default" name="member_id" >
                                                            @foreach($allmembers as $s)

                                                                <option value="{{$s->firstname." ".$s->lastname}}">{{$s->firstname." ".$s->lastname}}</option>


                                                            @endforeach
                                                                <option value="{{"others"}}">{{"others"}}</option>


                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Amount :<span class="text-danger">*</span></label>
                                                        <input type="text" name="amount" parsley-trigger="change" required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"
                                                               class="form-control" id="emailAddress">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="emailAddress">Description :<span class="text-danger">*</span></label>
                                                        <textarea class="form-control" name="description" rows="4"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="passWord2">Service Type<span class="text-danger">*</span></label>
                                                        <select class="selectpicker" data-style="btn-default" name="servicetype">
                                                            @foreach($servicetype as $s)

                                                                <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    {{csrf_field()}}
                                                    <div class="form-group text-right m-b-0">
                                                        <button style="background-color:#006D5B;color: white;" class="btn waves-effect waves-light" type="submit">
                                                            Submit
                                                        </button>
                                                        <button type="reset" class="btn btn-default waves-effect m-l-5">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-4">
                                            <img src="assets/images/small/img-1.jpg" class="img-responsive thumbnail m-r-15">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- end row -->


        </div> <!-- container -->

    </div> <!-- content -->

    <footer class="footer text-right">
        {{date('Y')}} © 1CGC
    </footer>

</div>
{{--<script src="assets/js/jquery.min.js"></script>--}}
{{--<script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>--}}
{{--<script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>--}}
<script>
    function readURL(input){
        if(input.files && input.files[0]){
            var reader=new FileReader();

            reader.onload=function(e){
                $('#preview_image').attr('src',e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).on('change','input[type="file"]',function(){
        readURL(this);

    })
</script>
<!-- END wrapper -->
        </div>
@endsection
        <!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->