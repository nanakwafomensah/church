@extends('layout.localmaster')

 @section('content')
        <!-- Begin page -->
<div id="wrapper">
<!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu" style="background-color: #006D5B">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">


                <ul>


                    <li class="has_sub" >
                        <a href="viewallmembers" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Members</span></a>

                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="fioffering" style="color: white">New Offering</a></li>
                            <li><a href="fialloffering" style="color: white"> All Offering</a></li>


                        </ul>
                    </li>



                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                        <ul class="list-unstyled">

                            <li><a href="oreport" style="color: white"> Reports</a></li>

                        </ul>
                    </li>



                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>


        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->




    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title"> Member Details </h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="#">ICGC</a>
                                </li>
                                <li>
                                    <a href="#">All Members </a>
                                </li>
                                <li class="active">
                                    Member Details
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                {{--<div class="row text-center">--}}
                    {{--<div class="col-sm-12">--}}
                        {{--<h3 class="m-t-20">Search Properties</h3>--}}
                        {{--<div class="border center-block m-b-20"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <!-- end row -->

                {{--<div class="row">--}}
                    {{--<div class="col-sm-12">--}}
                        {{--<form role="form" class="row">--}}
                            {{--<div class="col-sm-6 col-md-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--<select class="selectpicker show-tick" data-style="btn-default">--}}
                                        {{--<option value="0" disabled selected>Status</option>--}}
                                        {{--<option value="1">Rent</option>--}}
                                        {{--<option value="2">Sale</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-sm-6 col-md-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--<select class="selectpicker show-tick" data-style="btn-default">--}}
                                        {{--<option value="">Country</option>--}}
                                        {{--<option value="1">France</option>--}}
                                        {{--<option value="2">Great Britain</option>--}}
                                        {{--<option value="3">Spain</option>--}}
                                        {{--<option value="4">Russia</option>--}}
                                        {{--<option value="5">United States</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-sm-6 col-md-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--<select class="selectpicker show-tick" data-style="btn-default">--}}
                                        {{--<option value="">City</option>--}}
                                        {{--<option value="1">New York</option>--}}
                                        {{--<option value="2">Los Angeles</option>--}}
                                        {{--<option value="3">Chicago</option>--}}
                                        {{--<option value="4">Houston</option>--}}
                                        {{--<option value="5">Philadelphia</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-sm-6 col-md-3">--}}
                                {{--<div class="form-group">--}}
                                    {{--<select class="selectpicker show-tick" data-style="btn-default">--}}
                                        {{--<option value="">Property Type</option>--}}
                                        {{--<option value="1">Apartment</option>--}}
                                        {{--<option value="2">Condominium</option>--}}
                                        {{--<option value="3">Cottage</option>--}}
                                        {{--<option value="4">Flat</option>--}}
                                        {{--<option value="5">House</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-xs-12 m-b-30 text-center m-t-10">--}}
                                {{--<button type="submit" class="btn btn-purple waves-effect waves-light"><i class="mdi mdi-magnify m-r-5"></i>Search</button>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- end row -->--}}


                <div class="row">
                    @foreach($allmembers as $s)
                        <div class="col-md-3 col-sm-6">
                            <div class="property-card">
                                <div class="property-image" style="background: url('uploads/avatars/{{$s->photo}}') center center / cover no-repeat;">
                                    <span class="property-label label label-warning">{{$s->membershipstatus}}</span>
                                </div>

                                <div class="property-content">
                                    <div class="listingInfo">
                                        <div class="">
                                            <h5 class="text-success m-t-0">{{$s->member_id}}</h5>
                                        </div>
                                        <div class="">
                                            <h3 class="text-overflow"><a href="real-estate-column.html#" class="text-dark">{{ strtoupper($s->firstname.' '.$s->lastname)}}</a></h3>
                                            <p class="text-muted text-overflow"><i class="mdi mdi-map-marker-radius m-r-5"></i>{{$s->hometown}}</p>
                                            <p class="text-muted text-overflow"><i class="mdi mdi-map-marker-number m-r-5"></i>First Fruit: {{$s->firstfruit}}</p>



                                        </div>
                                    </div>
                                    <!-- end. Card actions -->
                                </div>
                                <!-- /inner row -->
                            </div>
                            <!-- End property item -->
                        </div>
                        @endforeach

                    <!-- end col -->


                </div>
                <!-- end row -->


                <div class="text-right">
                    <ul class="pagination">
                        {{ $allmembers->links() }}
                    </ul>
                </div> <!-- end .text-right -->


            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            {{date('Y')}} © ICGC.
        </footer>

    </div>







</div>
<!-- END wrapper -->
<script src="assets/js/jquery.min.js"></script>



@endsection
