

@extends('layout.localmaster')


<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/personal/datatable.css">


@section('content')
        <!-- Begin page -->
<div id="wrapper">


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu" style="background-color: #006D5B">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">


                <ul>


                        <li class="has_sub" >
                            <a href="viewallmembers" style="background-color: #006D5B; " class="waves-effect"><i class="fa fa-tachometer"style="color: white" aria-hidden="true"></i> <span style="color: white"> Members</span></a>

                        </li>


                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"style="color: white" aria-hidden="true"></i><span style="color: white"> Finance </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="fioffering" style="color: white">New Offering</a></li>
                                <li><a href="fialloffering" style="color: white"> All Offering</a></li>


                            </ul>
                        </li>



                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"style="color: white" aria-hidden="true"></i><span style="color: white"> Report </span> <span class="menu-arrow" style="color: white"></span></a>
                            <ul class="list-unstyled">

                                <li><a href="oreport" style="color: white"> Reports</a></li>

                            </ul>
                        </li>



                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>


        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">All Members</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="dashboard">ICGC</a>
                                </li>
                                <li>
                                    <a href="offering">New Offering </a>
                                </li>
                                <li class="active">
                                    All Offerings
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->



                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">

                            <h4 class="m-t-0 header-title"><b>All Record</b></h4>


                            <table id="member-table" class="table table-striped table-bordered">
                                <thead>

                                <tr>
                                    <th>Number</th>
                                    <th>Offering Type</th>
                                    <th>Service Type</th>
                                    <th>(GH)Amount</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Date Recorded</th>

                                    <th>Action</th>
                                </tr>
                                </thead>

                            </table>

                        </div>
                    </div>
                </div>

                 <div id="edit-Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form action="{{'editoffering'}}" method="post">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                          <h3><span id="offeringtypeheader"></span></h3>
                                            <input type="hidden" class="form-control" id="offering_number_edit_id" name="offering_number_edit_id">

                                            <div class="form-group">

                                                <label for="field-1" class="control-label">Date Submitted</label>
                                                <input type="date" class="form-control" id="date_submitted_edit" name="date_submitted_edit" placeholder="John">
                                            </div>
                                            <div class="form-group">

                                                <label for="field-1" class="control-label">Amount</label>
                                                <input type="text" class="form-control" id="amount_edit" name="amount_edit" placeholder="John">
                                            </div>
                                            <div class="form-group" id="servicetype_input">
                                                <label for="passWord2">Service Type<span class="text-danger">*</span></label>
                                                <select class="form-control" data-style="btn-info" id="servicetype_edit" name="servicetype_edit">
                                                    @foreach($servicetype as $s)

                                                        <option value="{{$s->service_name}}">{{$s->service_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group" id="member_input">
                                                <label for="emailAddress">Member Name:<span class="text-danger">*</span></label>
                                                <select class="form-control"   data-style="btn-info" id="member_edit" name="member_edit" >
                                                    @foreach($allmembers as $s)

                                                        <option value="{{$s->firstname." ".$s->lastname}}">{{$s->firstname." ".$s->lastname}}</option>


                                                    @endforeach


                                                </select>
                                            </div>
                                            <div class="form-group">

                                                <label for="field-1" class="control-label">Offering Number</label>
                                                <input type="text" class="form-control" id="offering_number_edit" name="offering_number_edit" disabled>
                                            </div>
                                            <div class="form-group">

                                                <label for="field-1" class="control-label">Description</label>
                                               <textarea class="form-control" rows="3" id="description_edit" name="description_edit"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                {{csrf_field()}}
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                    <button style="background-color:#006D5B;color: white;" type="submit" class="btn  waves-effect waves-light">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal -->



                <div id="delete-Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form action="{{'deleteoffering'}}" method="post">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">Delete</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Are you sure you want to delete offering number <span id="offering_number_delete_data"></span>?</label>
                                                <input type="hidden" class="form-control" id="offering_number_delete" name="offering_number_delete" >
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                {{csrf_field()}}
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                    <button style="background-color:#006D5B;color: white;"type="submit" class="btn  waves-effect waves-light">Delete</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal -->











            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            {{date('Y')}} © 1CGC
        </footer>

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Bootstrap JavaScript -->
    {{--<script src="assets/js/bootstrap.min.js"></script>--}}
    <script>
        $('#member-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!!route('datatable.fialloffering')!!}',
            columns: [
                {data: 'number', name: 'number'},
                {data: 'offeringtype', name: 'offeringtype'},
                {data: 'servicetype', name: 'servicetype'},
                {data: 'amount', name: 'amount'},
                {data: 'memberid', name: 'memberid'},
                {data: 'description', name: 'description'},
                {data: 'date_submitted', name: 'date_submitted'},

                {data: 'action', name: 'action'}
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column.search(val ? val : '', true, false).draw();
                            });
                });
            }
        });
    </script>
    <script>
        $(document).on('click','.editbtn',function() {
           // alert("you");
            $('#offering_number_edit').val($(this).data('number'));
            $('#offering_number_edit_id').val($(this).data('number'));

            if($(this).data('offeringtype')=='thanksgivingoffering'){
                $('#offeringtypeheader').html("Thanks Giving Offering");
            }else if($(this).data('offeringtype')=='projectoffering'){
                $('#offeringtypeheader').html("Project Offering");
            }else if($(this).data('offeringtype')=='titheoffering'){
                $('#offeringtypeheader').html("Tithe Offering");
            }else if($(this).data('offeringtype')=='mainoffering'){
                $('#offeringtypeheader').html("Main Offering");
                $('#servicetype_input').hide();
                $('#member_input').hide();
            }
            else if($(this).data('offeringtype')=='pledgeoffering'){
                $('#offeringtypeheader').html("Pledge Offering");
            }else if($(this).data('offeringtype')=='othersoffering'){
                $('#offeringtypeheader').html("other Offering");
            }else if($(this).data('offeringtype')=='welfareoffering'){
                $('#offeringtypeheader').html("Welfare Offering");
            }
            //alert($(this).data('name'));
            $('#servicetype_edit').val($(this).data('servicetype')).change();
            $('#amount_edit').val($(this).data('amount'));
            $('#member_edit').val($(this).data('name')).change();
            $('#date_submitted_edit').val($(this).data('date_submitted'));
            $('#description_edit').val($(this).data('description'));

        });
        $(document).on('click','.deletebtn',function() {
           // alert("Hey");
            $('#offering_number_delete').val($(this).data('number'));
            $('#offering_number_delete_data').html($(this).data('number'));



        });
    </script>
</div>
@endsection

