<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>ALl Members Report</title>
    {{--<link rel="stylesheet" href="pdfstyle/style.css" media="all" />--}}
    {{--<link rel="stylesheet" href="pdfstyle/style.css" media="all" />--}}
   <style>
         .iconDetails {
            margin-left:2%;
            float:left;
            height:40px;
            width:40px;
        }

        .container2 {
            width:100%;
            height:auto;
            padding:1%;
        }

        h4 {
            margin:0px;
        }
        #linebreak,h4{
            color: darkgreen;
        }


        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 10px;
        }

        table th,
        table td {
            padding: 5px;
            background: #EEEEEE;
            text-align: center;
            border-bottom: 1px solid darkgreen;
        }

        table th {
            white-space: nowrap;
            font-weight: normal;
        }

        table td {
            text-align: center;
        }

        table td h3{
            color: #57B223;
            font-size: 1.0em;
            font-weight: normal;
            margin: 0 0 0.2em 0;
        }

    </style>
</head>
<body>
<div class='container2'>
    <div>

        <center><img src="uploads/avatars/ICGC2.png" alt="user-img" class="iconDetails"></center>

    </div>
    <div >
        <center> <h4>{{$churchdetails->churchname}}</h4></center>
        <div style="font-size:.6em"><center>{{$churchdetails->address}}, {{$churchdetails->phone}}, {{$churchdetails->city}}</center></div>
        <div style="float:right;font-size:.6em"><center>{{$churchdetails->email}}</center></div>

    </div>
    <hr id="linebreak">

    <center> <h4> {{$title}}</h4></center>
    <center> <h4> {{$date_from}}    to    {{$date_to}} </h4></center>
    <br/>
    <div>


        <table>
            <thead>
            <tr>
                <th>PHOTO </th>
                <th>MEMBER ID</th>
                <th>MEMBER NAME</th>
                <th>PHONE NO.</th>
                <th>DATE OF BIRTH</th>
            </tr>
            </thead>
            <tbody>
            @foreach($allmembers as $s)
            <tr>
                <td><img src="uploads/avatars/{{$s->photo}}" alt="user-img" width="50px" height="50px" id="individual_image"></td>
                <td>{{$s->member_id}}</td>
                <td>{{$s->fullname}}</td>

                <td>{{$s->telephone}}</td>
                <td>{{$s->dob}}</td>
            </tr>
            @endforeach
            <tbody>
        </table>
        <hr id="linebreak">
    </div>

</div>


</body>
</html>