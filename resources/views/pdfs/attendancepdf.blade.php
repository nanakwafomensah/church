<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Attendance Report</title>
    {{--<link rel="stylesheet" href="pdfstyle/style.css" media="all" />--}}
    {{--<link rel="stylesheet" href="pdfstyle/style.css" media="all" />--}}
    <style>
        .iconDetails {
            margin-left:2%;
            float:left;
            height:40px;
            width:40px;
        }

        .container2 {
            width:100%;
            height:auto;
            padding:1%;
        }

        h4 {
            margin:0px;
        }
        #linebreak,h4{
            color: darkgreen;
        }


        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 10px;
        }

        table th,
        table td {
            padding: 5px;
            background: #EEEEEE;
            text-align: center;
            border-bottom: 1px solid darkgreen;
        }

        table th {
            white-space: nowrap;
            font-weight: normal;
        }

        table td {
            text-align: center;
        }

        table td h3{
            color: #57B223;
            font-size: 1.0em;
            font-weight: normal;
            margin: 0 0 0.2em 0;
        }

    </style>
</head>
<body>
<div class='container2'>
    <div>

        <center><img src="uploads/avatars/ICGC2.png" alt="user-img" class="iconDetails"></center>

    </div>
    <div >
        <center> <h4>{{$churchdetails->churchname}}</h4></center>
        <div style="font-size:.6em"><center>{{$churchdetails->address}}, {{$churchdetails->phone}}, {{$churchdetails->city}}</center></div>
        <div style="float:right;font-size:.6em"><center>{{$churchdetails->email}}</center></div>
    </div>
    <hr id="linebreak">

    <div>
        <center> <h4>{{$member_sec_title}}</h4></center>
        <center> <h4> {{$date_from}}  to   {{$date_to}} </h4></center><br/>
        <h3>GENERAL ATTENDANCE</h3>
        <table>
            <thead>

            <tr>
                <th >Service Type </th>
                <th style="background-color: #00aced">Male </th>
                <th style="background-color: #00aced">Female </th>
                <th style="background-color: coral">Cumulated Total </th>

                <th style="background-color:#00aced">visitors </th>
                <th style="background-color:coral">Cumulated Total </th>
                <th>Number of Cars </th>
                <th>Date Captured </th>


            </tr>
            </thead>
            <tbody>
            @foreach($attendancedetails as $s)
            <tr>
                <td>{{$s->servicetype}}</td>
                <td style="background-color: #00aced">{{$s->male}}</td>
                <td style="background-color: #00aced">{{$s->female}}</td>
                <td style="background-color: coral">{{number_format($s->male)+ number_format($s->female)}}</td>

                <td style="background-color:#00aced">{{$s->visitor}}</td>
                <td style="background-color:coral">{{number_format($s->male)+ number_format($s->female)+number_format($s->visitor)}}</td>
                <td>{{$s->car}}</td>
                <td>{{$s->created_at}}</td>
            </tr>
         @endforeach

            <tbody>
        </table>


    </div>

</div>


</body>
</html>