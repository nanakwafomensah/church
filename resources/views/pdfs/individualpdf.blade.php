<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Weekly Report</title>
    {{--<link rel="stylesheet" href="pdfstyle/style.css" media="all" />--}}
<style>
    @font-face {
        font-family: SourceSansPro;

    }
    body {
        position: relative;
        /*width: 21cm;  */
        /*height: 29.7cm; */
        margin: 0 auto;
        color: #555555;
        background: #FFFFFF;
        font-family: Arial, sans-serif;
        font-size: 14px;
        font-family: SourceSansPro;
    }
    .iconDetails {
    margin-left:2%;
    float:left;
    height:40px;
    width:40px;
    }

    .container2 {
    width:100%;
    height:auto;
    padding:1%;
    }

    h4 {
    margin:0px;
    }
    #linebreak,h4{
        color: darkgreen;
    }
    #individual_image{
       float: right;
        height: 100px;
        margin-left: 80%
    }
    #background{
        position:absolute;
        z-index:0;
        background:white;
        display:block;
        min-height:50%;
        min-width:50%;
        color:yellow;
    }

    #content{
        position:absolute;
        z-index:1;
    }

    #bg-text
    {
        color:lightgrey;
        font-size:90px;
        transform:rotate(200deg);
        -webkit-transform:rotate(400deg);
    }
</style>
</head>
<body>

<div id="content">
<div class='container2'>
    <div>

        <center><img src="uploads/avatars/ICGC2.png" alt="user-img" class="iconDetails"></center>

    </div>
    <div >
        <center> <h4>{{$churchdetails->churchname}}</h4></center>
        <div style="font-size:.6em"><center>{{$churchdetails->address}}, {{$churchdetails->phone}}, {{$churchdetails->city}}</center></div>
        <div style="float:right;font-size:.6em"><center>{{$churchdetails->email}}</center></div>
    </div>
    <hr id="linebreak">
    <br><br>
    <div>

                <img src="uploads/avatars/{{$members->photo}}" alt="user-img" id="individual_image">
            <table>
                <thead>
                <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Memberid:</td>
                    <td>{{$members->member_id}}</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

                    <td>First Fruit:</td>
                    <td>{{$members->firstfruit}}</td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td>{{$members->fullname}}</td>
                    <td></td>
                    <td>Member Groups:</td>
                    <td>{{$members->groups}}</td>
                </tr>
                <tr>
                    <td>Gender:</td>
                    <td>{{$members->gender}}</td>
                </tr>
                <tr>
                    <td>Nationality:</td>
                    <td>{{$members->nationality}}</td>
                </tr>
                <tr>
                    <td>Occupation:</td>
                    <td>{{$members->occupation}}</td>
                </tr>
                <tr>
                    <td>Telephone:</td>
                    <td>{{$members->telephone}}</td>
                </tr>
                <tr>
                    <td>DOB:</td>
                    <td>{{$members->dob}}</td>
                </tr>
                <tr>
                    <td>Marital status:</td>
                    <td>{{$members->marital_status}}</td>
                </tr>
                <tr>
                    <td>Location:</td>
                    <td>{{$members->hometown}}</td>
                </tr>
                <tbody>
            </table>
        <hr id="linebreak">
    </div>
</div>

<div id="background">
    <p id="bg-text">Membership Form</p>
</div>

</div>


</body>
</html>