<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>All Members Report</title>

    <style>
        .page-break {
            page-break-after: always;
        }
        b.dotted{
            border-bottom: 1px dotted #333;
            text-decoration: none;
        }
        .profile{
            text-align: center;
        }
        #firstpage-header{
            font-size: 60px;
            font-family: "Times New Roman";
            text-align: center;
        }
        table {
            border-collapse: collapse;
        }

        table, td, th {
            border: 1px solid black;
        }


        .iconDetails {
            margin-left:2%;
            float:left;
            height:40px;
            width:40px;
        }

        .container {
            width:100%;
            height:auto;
            padding:1%;

        }

        h4 {
            margin:0px;
        }
        #linebreak{
            color: darkgreen;
        }


    </style>
</head>
<body>
<div class='container'>
    <div>

        <center><img src="uploads/avatars/ICGC2.png" alt="user-img" class="iconDetails"></center>

    </div>
    <div class="profile" >
         <h4>{{$churchdetails->parentchurchname}}</h4>
         <h3>{{$churchdetails->churchname}}</h3>
        <div style="font-size:.6em">{{$churchdetails->address}}, {{$churchdetails->phone}}, {{$churchdetails->city}}</div>
        <div style="float:right;font-size:.6em">{{$churchdetails->email}}</div>
    </div>
    <hr id="linebreak">
    <br><br>
    <br><br>
    <br><br>
    <br><br>
    <br><br>
    <br><br>
    <br><br>
    <br><br>
    <br><br>
    <div class="page1">
       <h1 id="firstpage-header">ASSEMBLY</br> QUARTERTY</br> REPORTING</br> SYSTEM</h1>
        <hr id="linebreak">
    </div>

</div>
<!--end off first page -->




<div class="page-break"></div>




<!--begin second page -->
<div class='container'>
        <div style="text-align: center;">
            <h5>{{strtoupper($churchdetails->parentchurchname)}}</h5>
            <h4 style="font-weight: bold">ASSEMBLIES QUATERLY REPORTING FORM - GHANA</h4>
            =================================================================================
        </div>
    <div>
        REPORTING PERIOD:  .................................<b class="dotted"> {{$from_date}}</b>-<b class="dotted"> {{$to_date}}</b>......................................<br/>
        Name Of Assembly:  .................................<b class="dotted">{{strtoupper($churchdetails->churchname)}}</b>.......................................<br/>
        Head Pastor:  ..................<b class="dotted">{{strtoupper($churchdetails->pastorname)}}</b>........................Number of pastors: ..... <b class="dotted">{{strtoupper($churchdetails->no_of_pastors)}}</b> ........................<br/>
        District:  .................................<b class="dotted">{{strtoupper($churchdetails->district)}}</b>...................................
    <br/><br/>
     <h4 style="background-color: forestgreen;font-weight: bold" >CHURCH GROWTH AND EXPANSION</h4>
     <div>
       1. Was  your assembly in the missions activity? <b class="dotted">{{$rad1}}</b><br/>
       2. Did your assembly in particular plant any church? <b class="dotted">{{$rad2}}</b>
     </div>
        <br/><br/>
        <h4 style="background-color: forestgreen;font-weight: bold" >ATTENDANCE/MEMBERSHIP DATA</h4>
        <br/>
        <div>
            <p> Attendance[Individual attending church(Including visitors)] </p>
            <table style="width:100%">
                <tr>
                    <th style="width:70%"></th>
                    <th style="width:15%">Male</th>
                    <th style="width:15%">Female</th>
                </tr>
                <tr>
                    <td>Average Sunday Church Attendance at Adult Service</td>
                    <td><b class="dotted">{{$average_Sunday_Church_Attendance_at_Aldult_Servicemale}}</b></td>
                    <td><b class="dotted">{{$average_Sunday_Church_Attendance_at_Aldult_Servicefemale}}</b></td>
                </tr>
                <tr>
                    <td>Average Sunday Church Attendance at Youth  Service</td>
                    <td><b class="dotted">{{$average_Sunday_Church_Attendance_at_youth_Servicemale}}</b></td>
                    <td><b class="dotted">{{$average_Sunday_Church_Attendance_at_youth_Servicefemale}}</b></td>
                </tr>
                <tr>
                    <td>Average Sunday Church Attendance at Children Service</td>
                    <td><b class="dotted">{{$average_Sunday_Church_Attendance_at_Children_Servicemale}}</b></td>
                    <td><b class="dotted">{{$average_Sunday_Church_Attendance_at_Children_Servicfemale}}</b></td>
                </tr>
            </table>
        </div>

        <br/>

        <div>
            <p> Membership Data [individuals you consider bona fide members] </p>
            <table style="width:100%">
                <tr>
                    <th style="width:70%"></th>
                    <th style="width:15%">Male</th>
                    <th style="width:15%">Female</th>
                </tr>
                <tr>
                    <td>Current Adult membership </td>
                    <td>{{$Current_Adult_membership_male}}</td>
                    <td>{{$Current_Adult_membership_female}}</td>
                </tr>
                <tr>
                    <td>Current Youth Membership</td>
                    <td>{{$Current_youth_membership_male}}</td>
                    <td>{{$Current_youth_membership_female}}</td>
                </tr>
                <tr>
                    <td>Current Children Membership</td>
                    <td>{{$Current_child_membership_male}}</td>
                    <td>{{$Current_child_membership_male}}</td>
                </tr>
            </table>
        </div>



        <br/>

        <div>
            <p> Membership Tracking [what factors affect your membership data?] </p>
            <table style="width:100%">
                <tr>
                    <th style="width:5%"></th>
                    <th style="width:65%"></th>
                    <th style="width:15%">Male</th>
                    <th style="width:15%">Female</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>How many new members were added to your assembly? </td>
                    <td>{{$newmembers_were_added_to_your_assembly_male}}</td>
                    <td>{{$newmembers_were_added_to_your_assembly_female}}</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>How many new members went through membership classes?</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>How many of new members were baptized by the church?</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>How many of such members filled membership forms?</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>How many members completed all levels of the ABC?</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>How many members left the church altogether?</td>
                    <td>{{$members_left_the_church_altogether_male}}</td>
                    <td>{{$members_left_the_church_altogether_female}}</td>
                </tr>
            </table>
        </div>
        <br/>

        <div>
          <h4>Member Assimilation</h4>
            Do you hold a special ceremony to intentionally transition individuals who completed ABC into the service departments of the church? <b class="dotted">{{$rad3}}</b>

        </div>
        <br/><br/>
        <h4 style="background-color: forestgreen;font-weight: bold" >PASTORIAL CARE</h4>
        <div>
            <p>Covenant Family</p>
            <table style="width:100%">
                <tr>
                    <th style="width:5%">No.</th>
                    <th style="width:90%">Questionare</th>
                    <th style="width:5%"></th>
                </tr>
                <tr>
                    <td>1.</td>
                    <td>How many covenant families were there at the beginning of the quarter?</td>
                    <td>{{$covenant_families_were_there_at_the_beginning_of_the_quarter}}</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>How many new covenant families were established in your church?</td>
                    <td>{{$new_covenant_families_were_established_in_your_church}}</td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td>The number of covenant families that meet weekly in your assembly</td>
                    <td>{{$number_of_covenant_families_that_meet_weekly_in_your_assembly}}</td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td>The number of covenant families that are not active (hardly meet)</td>
                    <td>{{$covenant_families_that_are_not_active_hardly_meet}}</td>
                </tr>
            </table>
        </div>

        <br/><br/>
        <div>
            <p>Small Groups</p>
            <table style="width:100%">
                <tr>
                    <th style="width:5%"></th>
                    <th style="width:90%"></th>
                    <th style="width:5%"></th>
                </tr>
                <tr>
                    <td>1.</td>
                    <td>How many small groups does the assembly have</td>
                    <td>{{$small_groups_does_the_assembly}}</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>Which of the small groups are being use for pastoral care?</td>
                    <td>{{$small_groups_are_being_use_for_pastoral_care}}</td>
                </tr>

            </table>
        </div>

        <br/><br/>
        <div>
            <p style="font-weight: bold">Family Enrichment </p>
            <p>Basic Marriage Data </p>
            <table style="width:100%">
                <tr>
                    <th style="width:5%"></th>
                    <th style="width:90%"></th>
                    <th style="width:5%"></th>
                </tr>
                <tr>
                    <td>1.</td>
                    <td>How many marriages at the beginning of the quarter [baseline]</td>
                    <td><b class="dotted">{{$marriages_at_the_beginning_of_the_quarter}}</b></td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>How many new marriages were blessed in the church? </td>
                    <td><b class="dotted">{{$new_marriages_were_blessed_in_the_church}}</b></td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td>How many couples joined the assembly already married?</td>
                    <td><b class="dotted">{{$couples_joined_the_assembly_already_married}}</b></td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td>Current Number of Married Couples/families in the Church </td>
                    <td><b class="dotted">{{$Married_Couples_families_in_the_Church }}</b></td>
                </tr>
                <tr>
                    <td>5.</td>
                    <td>Number of Widows/widowers in the church  </td>
                    <td><b class="dotted">{{$Number_of_Widows_widowers_in_the_church}}</b></td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td>How many divorce cases happened in the church?   </td>
                    <td><b class="dotted">{{$divorce_cases_happened_in_the_church}}</b></td>
                </tr>

            </table>
        </div>

        <br/><br/>
        <div>

            <p>Counseling with Families</p>
            <table style="width:100%">
                <tr>
                    <th style="width:5%"></th>
                    <th style="width:85%"></th>
                    <th style="width:10%"></th>
                    {{--<th style="width:5%"></th>--}}
                </tr>
                <tr>
                    <td>1.</td>
                    <td>Does your church have a pre-marital Counseling program?</td>
                    <td><b class="dotted">{{$rad4}}</b></td>
                    {{--<td></td>--}}
                </tr>
                <tr>
                    <td>2.</td>
                    <td>How many individuals have been given some training to do counseling? </td>
                    <td><b class="dotted">{{$training_to_do_councelling}}</b></td>
                    {{--<td></td>--}}
                </tr>
                <tr>
                    <td>3.</td>
                    <td>Does your church have a post-marital Counseling program?</td>
                    <td><b class="dotted">{{$rad4}}</b></td>
                    {{--<td></td>--}}
                </tr>


            </table>
        </div>

        <br/><br/>
        <div>

            <p>Ministry to Families - Children’s Data</p>
            <table style="width:100%">
                <tr>
                    <th style="width:5%"></th>
                    <th style="width:85%"></th>
                    <th style="width:10%"></th>

                </tr>
                <tr>
                    <td>1.</td>
                    <td>How many births were recorded in the Assembly? </td>
                    <td><b class="dotted">{{$births_were_recorded_in_the_Assembly}}</b></td>

                </tr>
                <tr>
                    <td>2.</td>
                    <td>How man baby naming ceremonies were organized? </td>
                    <td><b class="dotted">{{$baby_naming_ceremonies_were_organized}}</b></td>

                </tr>
                <tr>
                    <td>3.</td>
                    <td>How many baby dedications were done in the quarter?</td>
                    <td><b class="dotted">{{$babydedication}}</b></td>

                </tr>


            </table>
        </div>


        <br/><br/>
        <div>

            <p>Ministry to Bereaved Families </p>
            <table style="width:100%">
                <tr>
                    <th style="width:5%"></th>
                    <th style="width:85%"></th>
                    <th style="width:10%"></th>

                </tr>
                <tr>
                    <td>1.</td>
                    <td>How many members passed on to glory in the Assembly?  </td>
                    <td><b class="dotted">{{$members_passed_on_to_glory_in_the_Assembly}}</b></td>

                </tr>
                <tr>
                    <td>2.</td>
                    <td>How many burial services did you organize? </td>
                    <td><b class="dotted">{{$burial_services_did_you_organize}}</b></td>

                </tr>



            </table>
        </div>

        <br/><br/>
        <h4 style="background-color: forestgreen;font-weight: bold" >SPIRITUAL FORMATION</h4>

        <div>
            1.	Does your assembly run the ABC Classes?..........<b class="dotted">Yes</b>.........
            <table style="width:100%">
                <tr>
                    <th style="width:25%"></th>
                    <th style="width:25%">Total enrolments</th>
                    <th style="width:25%">Number that Completed </th>
                    <th style="width:25%">Graduated</th>
                </tr>
                <tr>
                    <td style="font-weight: bold">Beginners</td>
                    <td><b class="dotted">{{$totalenrollment_beginners}}</b></td>
                    <td><b class="dotted">{{$completed_beginners}}</b></td>
                    <td><b class="dotted">{{$gra_beginners}}</b></td>
                </tr>
                <tr>
                    <td style="font-weight: bold">Membership</td>
                    <td><b class="dotted">{{$totalenrollment_Membership}}</b></td>
                    <td><b class="dotted">{{$completed_Membership}}</b></td>
                    <td><b class="dotted">{{$gra_Membership}}</b></td>
                </tr>
                <tr>
                    <td style="font-weight: bold">Maturity</td>
                    <td><b class="dotted">{{$totalenrollment_Maturity}}</b></td>
                    <td><b class="dotted">{{$completed_Maturity}}</b></td>
                    <td><b class="dotted">{{$gra_Maturity}}</b></td>
                </tr>
                <tr>
                    <td style="font-weight: bold">Ministry</td>
                    <td><b class="dotted">{{$totalenrollment_Ministry}}</b></td>
                    <td><b class="dotted">{{$completed_Maturity}}</b></td>
                    <td><b class="dotted">{{$gra_Maturity}}</b></td>
                </tr>
            </table>
        </div>
        <br/><br/>
        <h4 style="background-color: forestgreen;font-weight: bold" >CHURCH ORDER</h4>
        <div>
            <p>Administrative Support Systems</p>
            <table style="width:100%">
                <tr>
                    <th style="width:5%"></th>
                    <th style="width:85%"></th>
                    <th style="width:10%">Yes/No</th>

                </tr>
                <tr>
                    <td>1.</td>
                    <td>Does your church have an administrative support office?</td>
                    <td><b class="dotted">{{$rad6}}</b></td>

                </tr>
                <tr>
                    <td>2.</td>
                    <td>Does your assembly use the church’s official letterhead</td>
                    <td><b class="dotted">{{$rad7}}</b></td>

                </tr>
                <tr>
                    <td>3.</td>
                    <td>Is your assembly using the church’s approved signage?</td>
                    <td><b class="dotted">{{$rad8}}</b></td>

                </tr>

            </table>
        </div>



        <br/><br/>
        <div>
            <p>Office Staff [person helping with church office work:]</p>
            <table style="width:100%">

                <tr>
                    <td>Category/person’s background:  </td>
                    <td>
                        <th>Admin</th>
                        <th>Finance</th>
                        <th>Admin/Finance</th>
                    </td>

                </tr>
                <tr>
                    <td>Qualification Status: </td>
                    <td>
                        <th>not trained</th>
                        <th>Some training</th>
                        <th>Highly trained</th>
                    </td>

                </tr>
                <tr>
                    <td>Availability/compensation:</td>
                    <td>
                        <th>volunteer </th>
                        <th>paid, part-time</th>
                        <th>paid, full-time</th>
                    </td>

                </tr>

            </table>
        </div>

        <br/><br/>
        <div>
            <p>Record-Keeping</p>
            <table style="width:100%">
                <tr>
                    <th style="width:5%"></th>
                    <th style="width:85%"></th>
                    <th style="width:10%">Y</th>

                </tr>
                <tr>
                    <td>1.</td>
                    <td>Is your assembly operating the account management system introduced by the Head Office?</td>
                    <td><b class="dotted">{{$rad9}}</b></td>

                </tr>
                <tr>
                    <td>2.</td>
                    <td>Did your assembly prepare an annual budget?</td>
                    <td><b class="dotted">{{$rad10}}</b></td>

                </tr>
                <tr>
                    <td>3.</td>
                    <td>Did your financial operations follow the approved budget?</td>
                    <td><b class="dotted">{{$rad11}}</b></td>

                </tr>
                <tr>
                    <td>4.</td>
                    <td>Are your accounts ready for auditors?</td>
                    <td><b class="dotted">{{$rad12}}</b></td>

                </tr>
                <tr>
                    <td>5.</td>
                    <td>Do you as a pastor regularly use data kept by the administrative office to make decisions regarding church operations?</td>
                    <td><b class="dotted">{{$rad13}}</b></td>

                </tr>
                <tr>
                    <td>6.</td>
                    <td>Dis you send quarterly reports to the district office</td>
                    <td><b class="dotted">{{$rad14}}</b></td>

                </tr>

            </table>
        </div>

        <br/><br/>
        <div>
            <p>Statutory Payments</p>
            <table style="width:100%">
                <tr>
                    <th style="width:5%"></th>
                    <th style="width:85%"></th>
                    <th style="width:5%">Y</th>

                </tr>
                <tr>
                    <td>1.</td>
                    <td>How many months payment were made to the Missions Common Fund (MCF) during the quarter under review?</td>
                    <td><b class="dotted">{{$rad15}}</b></td>

                </tr>
                <tr>
                    <td>2.</td>
                    <td>Did you pay Provident fund for employees of the church?</td>
                    <td><b class="dotted">{{$rad16}}</b></td>

                </tr>
                <tr>
                    <td>3.</td>
                    <td>Do you pay social security for employed staff?</td>
                    <td><b class="dotted">{{$rad17}}</b></td>

                </tr>
                <tr>
                    <td>4.</td>
                    <td>Do you pay IRS [PAYE for employed staff?</td>
                    <td><b class="dotted">{{$rad18}}</b></td>

                </tr>

            </table>
        </div>

        <br/><br/>
        <div>
            <p>Administrative Support for Pastor</p>
            <p>
                Which of the following do you have? (Please check which is applicable for your assembly)<br/>
                <ul>
                <li>Church Council @if($do_you_have =='churchcouncil') <input type="checkbox" checked/>@else  <input type="checkbox" />@endif </li>
                <li>Management Committee [usually for fairly new assemblies] @if($do_you_have =='managementcommitee') <input type="checkbox" checked/>@else  <input type="checkbox" /> @endif</li>
                </ul>


            </p>
            <table style="width:100%">
                <tr>
                    <th style="width:5%">No.</th>
                    <th style="width:85%">Questionaires</th>
                    <th style="width:10%">Answer</th>

                </tr>
                <tr>
                    <td>1.</td>
                    <td>How many members do you have serving on this Body?</td>
                    <td><b class="dotted">{{$members_do_you_have_serving_on_this_Body}}</b></td>

                </tr>
                <tr>
                    <td>2.</td>
                    <td>Are they actively involved in discussions and in decision-making?</td>
                    <td></td>

                </tr>
                <tr>
                    <td>3.</td>
                    <td>How many times did they meet in the quarter?</td>
                    <td><b class="dotted">{{$times_did_they_meet_in_the_quarter}}</b></td>

                </tr>
                <tr>
                    <td>4.</td>
                    <td>Indicate in the column below one major decision that you took together with the council of the management committee</td>
                    <td><b class="dotted"></b></td>

                </tr>
                <tr>
                    <td></td>
                    <td>..<b class="dotted">{{$decision}}</b></td>


                </tr>

            </table>
        </div>


        <br/><br/>
        <h4 style="background-color: forestgreen;font-weight: bold" >LEADERSHIP DEVELOPMENT</h4>
        <div>
            <p>This section captures data on the people you have put in the various leadership positions in the church – deacons, covenant family leaders, ABC facilitators, children’s ministry facilitators, youth ministry facilitators, etc.</p>
            <table style="width:100%">
                <tr>
                    <th style="width:5%">No.</th>
                    <th style="width:85%">Questionare</th>
                    <th style="width:10%">Answer</th>
                </tr>
                <tr>
                    <td>1.</td>
                    <td>What is the total number of your lay leaders?</td>
                    <td><b class="dotted">{{$no_of_layleaders}}</b></td>

                </tr>
                <tr>
                    <td>2.</td>
                    <td>Did you train individuals in basic leadership principles before appointing them to leadership positions in the church?</td>
                    <td><b class="dotted">{{$rad19}}</b></td>

                </tr>
                <tr>
                    <td>3.</td>
                    <td>Did your leaders receive orientation regarding the specific positions you appointed them to? E.g. youth facilitators’ orientation?</td>
                    <td><b class="dotted">{{$rad20}}</b></td>

                </tr>
                <tr>
                    <td>4.</td>
                    <td>How many programs did you organize to improve the capacity and skills of your leaders after being appointed?</td>
                    <td><b class="dotted">{{$no_of_programs}}</b></td>

                </tr>
                <tr>
                    <td>5.</td>
                    <td>How many lay leaders participated in the training program(s) you organized?</td>
                    <td><b class="dotted">{{$leaders_in_training}}</b></td>

                </tr>

            </table>
        </div>


        <br/><br/>
        <h4 style="background-color: forestgreen;font-weight: bold" >LEGACY & SUCCESSION</h4>
        <div>
            <p>Children’s Ministries</p>
            <table style="width:100%">
                <tr>
                    <th style="width:5%">No.</th>
                    <th style="width:85%">Questionare</th>
                    <th style="width:10%">Answer</th>
                </tr>
                <tr>
                    <td>1.</td>
                    <td>Do you have a suitable meeting place for your children’s ministry?</td>
                    <td><b class="dotted">{{$rad21}}</b></td>

                </tr>
                <tr>
                    <td>2.</td>
                    <td>How many Children’s ministry facilitators do you have in the children’s church?</td>
                    <td><b class="dotted">{{$children_facilitators}}</b></td>

                </tr>
                <tr>
                    <td>3.</td>
                    <td>How many times did the pastor meet with the children’s ministry facilitators?</td>
                    <td><b class="dotted">{{$pastor_meet_children}}</b></td>

                </tr>
                <tr>
                    <td>4.</td>
                    <td>How often did the pastor interact with the children’s ministry this quarter?</td>
                    <td><b class="dotted">{{$pastor_interact_children}}</b></td>

                </tr>
                <tr>
                    <td>5.</td>
                    <td>Do you involve the parents in the children’s church?</td>
                    <td><b class="dotted">{{$rad22}}</b></td>

                </tr>

            </table>
        </div>
        <br/><br/>
        <div>
            <p>Youth Ministries</p>
            <table style="width:100%">
                <tr>
                    <th style="width:5%">No.</th>
                    <th style="width:85%">Questionare</th>
                    <th style="width:10%">Answer</th>
                </tr>
                <tr>
                    <td>1.</td>
                    <td>Do you have a suitable meeting place for your youth ministry?</td>
                    <td><b class="dotted">{{$rad23}}</b></td>

                </tr>
                <tr>
                    <td>2.</td>
                    <td>How many Children’s ministry facilitators do you have in the youth church?</td>
                    <td><b class="dotted">{{$youth_children_facilatators}}</b></td>

                </tr>
                <tr>
                    <td>3.</td>
                    <td>How many times did the pastor meet with the youth ministry facilitators?</td>
                    <td><b class="dotted">{{$youth_pastor_meet_facilatators}}</b></td>

                </tr>
                <tr>
                    <td>4.</td>
                    <td>How often did the pastor interact with the youth ministry this quarter?</td>
                    <td><b class="dotted">{{$pastor_interact_youth}}</b></td>

                </tr>
                <tr>
                    <td>5.</td>
                    <td>Do you involve the parents in the youth church?</td>
                    <td><b class="dotted">{{$rad24}}</b></td>

                </tr>

            </table>
        </div>

        <br/><br/>
        <h4 style="background-color: forestgreen;font-weight: bold" >SOCIAL INTERVENTION</h4>
        <div>
            <p>Fund-Raising for corporate Social Action</p>
            <table style="width:100%">
                <tr>
                    <th style="width:5%">No.</th>
                    <th style="width:85%">Questionare</th>
                    <th style="width:10%">Answer</th>
                </tr>
                <tr>
                    <td>1.</td>
                    <td>Did your assembly organize Gold Frankincense and Myrrh?</td>
                    <td><b class="dotted">{{$rad25}}</b></td>

                </tr>
                <tr>
                    <td>2.</td>
                    <td>Did your assembly send the funds raised during the GFM to the Head Office?</td>
                    <td><b class="dotted">{{$rad26}}</b></td>

                </tr>
                <tr>
                    <td>3.</td>
                    <td>Did your church observe the Central Aid Day?</td>
                    <td><b class="dotted">{{$rad27}}</b></td>

                </tr>
                <tr>
                    <td>4.</td>
                    <td>Were the funds realized from the CA Day sent to the Head office?</td>
                    <td><b class="dotted">{{$rad28}}</b></td>

                </tr>


            </table>
        </div>

        <br/><br/>
        <div>
            <p>Local Involvement</p>
            What did your assembly do as social intervention?
            <table style="width:100%">
                <tr>
                    <th style="width:5%">No.</th>
                    <th style="width:85%">Questionare</th>
                    <th style="width:10%">Answer</th>
                </tr>
                <tr>
                    <td>1.</td>
                    <td>Donation to orphanages</td>
                    <td><b class="dotted">{{$rad29}}</b></td>

                </tr>
                <tr>
                    <td>2.</td>
                    <td>Donation to hospitals</td>
                    <td><b class="dotted">{{$rad30}}</b></td>

                </tr>
                <tr>
                    <td>3.</td>
                    <td>Community development project [briefly describe]</td>
                    <td><b class="dotted">{{$community_project}}</b></td>

                </tr>
                <tr>
                    <td>4.</td>
                    <td>Other(s)</td>
                    <td><b class="dotted">{{$others}}</b></td>

                </tr>


            </table>
        </div>

        <br/><br/>
        <h4 style="background-color: forestgreen;font-weight: bold" >EDUCATION [For assemblies Running a Regular School]</h4>
        <div>
            <p>Student information</p>

            <table style="width:100%">
                <tr>

                    <th style="width:80%">Questionare</th>
                    <th style="width:10%">Male</th>
                    <th style="width:10%">Female</th>
                </tr>
                <tr>
                    <td>Number of Children in Early Childhood Education </td>
                    <td><b class="dotted">{{$student_ece_male}}</b></td>
                    <td><b class="dotted">{{$student_ece_female}}</b></td>


                </tr>
                <tr>
                    <td>Number of Students in Primary Education </td>
                    <td><b class="dotted">{{$student_pe_male}}</b></td>
                    <td><b class="dotted">{{$student_pe_female}}</b></td>


                </tr>
                <tr>
                    <td>Number of Students in Junior High School (JHS) Education </td>
                    <td><b class="dotted">{{$student_jhs_male}}</b></td>
                    <td><b class="dotted">{{$student_jhs_female}}</b></td>


                </tr>
                <tr>
                    <td>Number of Students in senior secondary school (SHS) </td>
                    <td><b class="dotted">{{$student_shs_male}}</b></td>
                    <td><b class="dotted">{{$student_shs_female}}</b></td>
_

                </tr>


            </table>
        </div>
        <div>
            <p>Teacher information</p>

            <table style="width:100%">
                <tr>

                    <th style="width:80%">Questionare</th>
                    <th style="width:10%">Male</th>
                    <th style="width:10%">Female</th>
                </tr>
                <tr>
                    <td>Total number of teachers [trained] with qualification  </td>
                    <td><b class="dotted">{{$teacher_trained_male}}</b></td>
                    <td><b class="dotted">{{$teacher_trained_female}}</b></td>


                </tr>
                <tr>
                    <td>Total number of non-trained teachers in your school </td>
                    <td><b class="dotted">{{$teacher_nontrained_male}}</b></td>
                    <td><b class="dotted">{{$teacher_nontrained_female}}</b></td>


                </tr>
                <tr>
                    <td>Programs  organized to enhance teacher capability and skills</td>
                    <td><b class="dotted"></b></td>
                    <td><b class="dotted"></b></td>


                </tr>



            </table>
        </div>
        <br/><br/>
        <h4 style="background-color: forestgreen;font-weight: bold" >CHURCH FINANCES</h4>

        <div>
            <p>Income</p>

            <table style="width:100%">
                <tr>

                    <th style="width:40%"></th>
                    <th style="width:30%">Amount</th>
                    <th style="width:30%">% increase/decrease over previous quarter</th>
                </tr>
                <tr>
                    <td>First fruits  </td>
                    <td><b class="dotted">{{$firstfruit_amount}}</b></td>
                    <td><b class="dotted">{{$firstfruit_percentage}}% </b></td>

                </tr>
                <tr>
                    <td>All other Offerings  </td>
                    <td><b class="dotted">{{$allotheroffering_amount}}</b></td>
                    <td><b class="dotted">{{$allotheroffering_amount_percentage}}% </b></td>

                </tr>
                <tr>
                    <td>Total Annual Income </td>
                    <td><b class="dotted">{{$Total_Annual_Income}}</b></td>
                    <td><b class="dotted">{{$Total_Annual_Income_percentage}}% </b></td>

                </tr>
             </table>
        </div>

        <br/><br/>
        <div>
            <p>Expenditure</p>

            <table style="width:100%">
                <tr>

                    <th style="width:40%"></th>
                    <th style="width:30%">Amount</th>
                    <th style="width:30%">% increase/decrease over previous quarter</th>
                </tr>
                <tr>
                    <td>Missions/Church Planting Expenses </td>
                    <td><b class="dotted">{{$MissionsChurchPlantingExpenses}}</b></td>
                    <td><b class="dotted">{{$MissionsChurchPlantingExpenses_percentage}}%</b></td>

                </tr>
                <tr>
                    <td>Administrative Expenses</td>
                    <td><b class="dotted">{{$Administrative_Expenses}}</b></td>
                    <td><b class="dotted">{{$Administrative_Expenses_percentage}}%</b></td>

                </tr>
                <tr>
                    <td>Ministry Expenses  </td>
                    <td><b class="dotted">{{$MinistryExpenses}}</b></td>
                    <td><b class="dotted">{{$MinistryExpenses_percentage}}%</b></td>

                </tr>
                <tr>
                    <td>Social action Project  </td>
                    <td><b class="dotted">{{$SocialActionProjects}}</b></td>
                    <td><b class="dotted">{{$SocialActionProjects_percentage}}%</b></td>

                </tr>
                <tr>
                    <td>Capital Expenditure </td>
                    <td><b class="dotted">{{$CapitalExpenditure }}</b></td>
                    <td><b class="dotted">{{$CapitalExpenditure_percentage}}%</b></td>

                </tr>
                <tr>
                    <td style="font-weight: bold">Total Expenditure</td>
                    <td><b class="dotted">{{$TotalExpenditure}}</b></td>
                    <td><b class="dotted">{{$TotalExpenditure_percentage}}%</b></td>

                </tr>
            </table>
        </div>

        <br/><br/>
        <div>
            <p>Special Financial Obligations</p>

            <table style="width:100%">

                <tr>
                    <td style="width:50%">Total Common Fund Payments </td>
                    <td style="width:50%"><b class="dotted">{{$Total_Common_Fund_Payments}}</b></td>


                </tr>
                <tr>
                    <td style="width:50%">Central Aid Day Contribution </td>
                    <td style="width:50%"><b class="dotted">{{$Central_Aid_Day_Contribution}}</b></td>


                </tr>

            </table>
        </div>
    </div>
</div>


</body>
</html>