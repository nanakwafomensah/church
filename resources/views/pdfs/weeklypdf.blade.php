<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>All Members Report</title>

    <style>
        .page-break {
            page-break-after: always;
        }
        b.dotted{
            border-bottom: 1px dotted #333;
            text-decoration: none;
        }
        .profile{
            text-align: center;
        }
        #firstpage-header{
            font-size: 60px;
            font-family: "Times New Roman";
            text-align: center;
        }
        table {
            border-collapse: collapse;
        }

        table, td, th {
            border: 1px solid black;
        }


        .iconDetails {
            margin-left:2%;
            float:left;
            height:40px;
            width:40px;
        }

        .container {
            width:100%;
            height:auto;
            padding:1%;

        }

        h4 {
            margin:0px;
        }
        #linebreak{
            color: darkgreen;
        }


    </style>
</head>
<body>
<div class='container'>
    <div>

        <center><img src="uploads/avatars/ICGC2.png" alt="user-img" class="iconDetails"></center>

    </div>
    <div class="profile" >
        <h4>{{$churchdetails->parentchurchname}}</h4>
        <h3>{{$churchdetails->churchname}}</h3>
        <div style="font-size:.6em">{{$churchdetails->address}}, {{$churchdetails->phone}}, {{$churchdetails->city}}</div>
        <div style="float:right;font-size:.6em">{{$churchdetails->email}}</div>
    </div>
    <hr id="linebreak">
    <br><br>
    <br><br>
    <br><br>
    <br><br>
    <br><br>
    <br><br>
    <br><br>
    <br><br>
    <br><br>
    <div class="page1">
        <h1 id="firstpage-header">ASSEMBLY</br> WEEKLY</br> REPORTING</br> SYSTEM</h1>
        <hr id="linebreak">
    </div>

</div>
<!--end off first page -->




<div class="page-break"></div>
<div class="container">
    <h3>Introduction</h3>
    This form is to be filled every week after the service preferably. It will help collect data at the assembly, which then will be posted to the Assembly Data Booklet.
    These notes guide the filling of this form. Please note that if this form is filled with due diligence on weekly basis, it is easier to post data eventually to the Assembly Reporting Form. [ARF] to be submitted to the District Office.
    <h4>Sunday Service Composite Data</h4>
    1.	Head usher is to mobilize ushers to count males and females in adult service and submit the attendance to administrator.
    2.	The Head of Children’s ministry and Youth Ministry will count male and female attendance and submit to administrator.
    3.	The Cash team is expected to present all financial data on this form and submit to administrator.
    <h4>ABC Attendance Data</h4>
    <p>1.	Facilitators will record participation for each session and give to the administrator to be captured on this form.
     </p>
    <h4> Covenant Family Attendance Data</h4>
   <p> 1.	For the rest of 2017, this form will capture data on covenant families located in the communities, i.e. community-based covenant families.
    2.	Covenant Family attendance is expected to be recorded every week. This form only captures evidence that the covenant family met. The details of member attendance are not required for this report.
    </p>
    <h4>Family Enrichment</h4>
    <p>
    1.	The Marriage and Family Data Section is to capture the current numerical status of families in the church, in other words how many married couples are in the church.
    2.	To get an accurate number of couples in the church, beginning July (third quarter) take the number of married couples in the church as of July 1.  This is your baseline for family data.
    3.	Be careful to capture couples who were married before joining the church else the family data will be incomplete. Work closely with the Hosts and hostesses who capture visitors Sunday after Sunday for this data.
    4.	In recording number of couples in the church, be sure to record pairs. In other words, your figure of 400 couples indicates 400 pairs, not 200 families multiplied by 2.
    5.	Families that are married traditionally but have not had the “church wedding” are also captured as married before joining.
     </p>

    </div>

   <div class="page-break"></div>

<div class="container">
    <div style="text-align: center;">
        <h5>{{strtoupper($churchdetails->parentchurchname)}}</h5>
        <h4 style="font-weight: bold">Weekly Data Form</h4>
        =================================================================================
    </div>
    <div>
        REPORTING PERIOD:  .................................<b class="dotted">{{$from_date}} </b>-<b class="dotted">{{$to_date}} </b>......................................<br/>
        Name Of Assembly:  .................................<b class="dotted">{{strtoupper($churchdetails->churchname)}}</b>.......................................<br/>
        Head Pastor:  ..................<b class="dotted">{{strtoupper($churchdetails->pastorname)}}</b>........................Number of pastors: ..... <b class="dotted">{{strtoupper($churchdetails->no_of_pastors)}}</b> ........................<br/>
        District:  .................................<b class="dotted">{{strtoupper($churchdetails->district)}}</b>...................................
        <br/><br/>
    </div>
    <br/>
    <br/>
    <br/>
    <h4 style="background-color: forestgreen;font-weight: bold" >ATTENDANCE DATA</h4>
    <div>
        <table style="width:100%">
            <tr>
                <th style="width:70%"></th>
                <th style="width:15%">Male</th>
                <th style="width:15%">Female</th>
            </tr>
            <tr>
                <td>Attendance at Adults service</td>
                <td><b class="dotted">{{$Adultsservice_attendancedetailsquery_male_value}}</b></td>
                <td><b class="dotted">{{$Adultsservice_attendancedetailsquery_female_value}}</b></td>
            </tr>
            <tr>
                <td>Attendance at Youth Service</td>
                <td><b class="dotted">{{$YouthService_attendancedetailsquery_male_value}}</b></td>
                <td><b class="dotted">{{$YouthService_attendancedetailsquery_female_value}}</b></td>
            </tr>
            <tr>
                <td>Attendance at Children Service</td>
                <td><b class="dotted">{{$ChildrenService_attendancedetailsquery_male_value}}</b></td>
                <td><b class="dotted">{{$ChildrenService_attendancedetailsquery_female_value}}</b></td>
            </tr>
        </table>
    </div>
    <br/>
    <br/>
    <br/>
    <h4 style="background-color: forestgreen;font-weight: bold" >PASTORAL CARE</h4>
    <div>
        <table style="width:100%">
            <tr>
                <th style="width:5%"></th>
                <th style="width:60%"><h4>Covenant Family</h4></th>
                <th style="width:35%"></th>
            </tr>
            <tr>
                <td></td>
                <td>Baseline</td>
                <td><b class="dotted">{{$baselinevalue}}</b></td>
            </tr>
            <tr>
                <td></td>
                <td style="margin-left: 2cm;">New convenant families established</td>
                <td><b class="dotted">{{$convenantfamilyestablished}}</b></td>
            </tr>
            <tr>
                <td></td>
                <td style="margin-left: 2cm;">Covenant Families that met</td>
                <td><b class="dotted">{{$convenantfamily_that_meet}}</b></td>
            </tr>
            <tr>
                <td></td>
                <td style="margin-left: 2cm;">Did not meet</td>
                <td><b class="dotted">{{$convenantfamily_that_did_not_meet}}</b></td>
            </tr>
            <tr>
                <th style="width:5%"></th>
                <th style="width:60%"><h4>Family Enrichment</h4></th>
                <th style="width:35%"></th>
            </tr>
            <tr>
                <td></td>
                <td style="margin-left: 2cm;">Marriages blessed  by this assembly[ICGC Weds]</td>
                <td><b class="dotted">{{$Marriages_blessed_by_this_assembly}}</b></td>
            </tr>
            <tr>
                <td></td>
                <td style="margin-left: 2cm;">Couples who joined already married  </td>
                <td><b class="dotted">{{$couples_joined_the_assembly_already_married_number}}</b></td>
            </tr>
            <tr>
                <td></td>
                <td style="margin-left: 2cm;">Widows    </td>
                <td><b class="dotted">{{$widows_weekly}}</b></td>
            </tr>
            <tr>
                <td></td>
                <td style="margin-left: 2cm;">Divorced   </td>
                <td><b class="dotted">{{$divorce_weekly}}</b></td>
            </tr>
            <tr>
                <th style="width:5%"></th>
                <th style="width:60%"><h4>Ministry to Families - Children’s Data</h4></th>
                <th style="width:35%"></th>
            </tr>
            <tr>
                <td></td>
                <td style="margin-left: 2cm;">Birth </td>
                <td><b class="dotted">{{$Births}}</b></td>
            </tr>
            <tr>
                <td></td>
                <td style="margin-left: 2cm;">Baby Naming ceremonies</td>
                <td><b class="dotted">{{$Baby_Naming_ceremonies}}</b></td>
            </tr>
            <tr>
                <td></td>
                <td style="margin-left: 2cm;">Baby dedications</td>
                <td><b class="dotted">{{$Baby_dedications}}</b></td>
            </tr>
            <tr>
                <th style="width:5%"></th>
                <th style="width:60%"><h4>Ministry to Bereaved Families</h4></th>
                <th style="width:35%"></th>
            </tr>
            <tr>
                <td></td>
                <td style="margin-left: 2cm;">Deaths</td>
                <td><b class="dotted">{{$Deaths}}</b></td>
            </tr>
            <tr>
                <td></td>
                <td style="margin-left: 2cm;">Burial services</td>
                <td><b class="dotted">{{$Burial_services}}</b></td>
            </tr>
        </table>
    </div>
    <br/>
    <br/>
    <br/>
    <h4 style="background-color: forestgreen;font-weight: bold" >SPIRITUAL FORMATION</h4>
    <h4 style="" >ABC Attendance</h4>
    <div>
        <table style="width:100%">
            <tr>
                <th style="width:70%"></th>
                <th style="width:15%">Male</th>
                <th style="width:15%">Female</th>
            </tr>
            <tr>
                <td>New Beginners</td>
                <td><b class="dotted">{{$abc_male__New_Beginners}}</b></td>
                <td><b class="dotted">{{$abc_female_New_Beginners}}</b></td>
            </tr>
            <tr>
                <td>Membership</td>
                <td><b class="dotted">{{$abc_male_membership}}</b></td>
                <td><b class="dotted">{{$abc_female_membership}}</b></td>
            </tr>
            <tr>
                <td>Maturity</td>
                <td><b class="dotted">{{$abc_male_Maturity}}</b></td>
                <td><b class="dotted">{{$abc_female_Maturity}}</b></td>
            </tr>
            <tr>
                <td>Ministry</td>
                <td><b class="dotted">{{$abc_male_Ministry}}</b></td>
                <td><b class="dotted">{{$abc_female_Ministry}}</b></td>
            </tr>
        </table>
    </div>
    <br/>
    <br/>
    <br/>
    <h4 style="background-color: forestgreen;font-weight: bold" >FINANCIAL DATE/OBLIGATIONS</h4>
    <div>Financial Data/Obligations
        <table  style="width:100%">
            <tr>
                <th style="width:70%">Financial Data/Obligations</th>
                <th style="width:30%"></th>

            </tr>
            <tr>
                <td>Firstfruits</td>
                <td><b class="dotted">{{$firstfruit_amount}}</b></td>

            </tr>
            <tr>
                <td>Main Offerings</td>
                <td><b class="dotted">{{$mainoffering_amount}}</b></td>

            </tr>
            <tr>
                <td>Project Offering</td>
                <td><b class="dotted">{{$projectoffering_amount}}</b></td>

            </tr>
            <tr>
                <td>Tuesday Offering</td>
                <td><b class="dotted">{{$tuesdayoffering_amount}}</b></td>

            </tr>
            <tr>
                <td>Total Income</td>
                <td><b class="dotted">{{$firstfruit_amount+$mainoffering_amount+$projectoffering_amount+$tuesdayoffering_amount}}</b></td>

            </tr>
            <tr>
                <td style="width:50%"><strong>Portion for Missions Common Fund (MCF)</strong></td>
                <td style="width:50%"><b class="dotted"></b></td>

            </tr>
            <tr>
                <td >Provident fund for employees </td>
                <td ><b class="dotted">{{$Provident_fund_for_employees}}</b></td>
            </tr>
            <tr>
                <td >Social Security for employed staff </td>
                <td ><b class="dotted">{{$Social_Security_for_employed_staff}}</b></td>
            </tr>
            <tr>
                <td >IRS - PAYE for employed staff </td>
                <td ><b class="dotted">{{$PAYE_for_employed_staff}}</b></td>
            </tr>

        </table>
    </div>
    <br/>
    <br/>
    <br/>
    <h4 style="background-color: forestgreen;font-weight: bold" >LEADERSHIP DEVELOPMENT</h4>
    <h4 style="" >Preparing people for serving in departments</h4>

    <div>
        <table style="width:100%">
            <tr>
                <th style="width:70%">Department</th>
                <th style="width:15%">Nature of Program</th>
                <th style="width:15%">Attendance</th>
            </tr>
            <tr>
                <td>Children Department</td>
                <td><b class="dotted">{{$child_dpt_nature}}</b></td>
                <td><b class="dotted">{{$child_dpt_atten}}</b></td>
            </tr>
            <tr>
                <td>Youth Department</td>
                <td><b class="dotted">{{$youth_dpt_nature}}</b></td>
                <td><b class="dotted">{{$youth_dpt_atten}}</b></td>
            </tr>
            <tr>
                <td>Covenant Family Leaders</td>
                <td><b class="dotted">{{$convenant_fami_nature}}</b></td>
                <td><b class="dotted">{{$convenant_fami_atten}}</b></td>
            </tr>
            <tr>
                <td>Counselors</td>
                <td><b class="dotted">{{$Counselors_nature}}</b></td>
                <td><b class="dotted">{{$Counselors_atten}}</b></td>
            </tr>
            <tr>
                <td>Church Council/Management</td>
                <td><b class="dotted">{{$Church_Council_nature}}</b></td>
                <td><b class="dotted">{{$Church_Council_atten}}</b></td>
            </tr>
        </table>
    </div>
    <br/>
    <br/>
    <br/>
    <h4>LEGACY & SUCCESSION</h4>
    <h4 style="" >Pastor’s Meeting with Ministries</h4>
    <div>
        <table style="width:100%">
            <thead>
            <tr>
                <td style="width:50%"><strong>Ministry/Department</strong></td>
                <td style="width:50%"><strong>Nature/Purpose of Meeting</strong></td>
            </tr>
            </thead>
            <tbody>
                 <tr>
                     <td>{{$ministry_department1}}</td>
                     <td>{{$Purpose_of_Meeting1}}</td>
                 </tr>
                <tr>
                     <td>{{$ministry_department2}}</td>
                     <td>{{$Purpose_of_Meeting2}}</td>
                 </tr>

            </tbody>
        </table>
    </div>
</div>


</body>
</html>