<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {

            $table->increments('id');
            $table->string('member_id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('nationality');
            $table->string('occupation');
            $table->string('telephone');
            $table->string('gender');
            $table->string('dob');
            $table->string('marital_status');
            $table->string('hometown');
            $table->string('firstfruit');
            $table->string('convenant_family');
            $table->string('groups');
            
            $table->string('photo')->default('default.jpg');
            $table->string('fullname');
            $table->string('membershipstatus');
            $table->string('membership_cat');
            $table->dateTime('registration_date');
            $table->string('couplename');
            $table->string('passed_to_glory');
            $table->string('trC');
            $table->string('level_edu');
            $table->string('teacher_qualification');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
