<?php

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('services')->delete();
        DB::table('services')->insert(array(
            array('service_name'=>'children service','created_at'=>date('Y-m-d h:i:s'),'updated_at'=>date('Y-m-d h:i:s')),
            array('service_name'=>'youth service','created_at'=>date('Y-m-d h:i:s'),'updated_at'=>date('Y-m-d h:i:s')),
            array('service_name'=>'tuesday service','created_at'=>date('Y-m-d h:i:s'),'updated_at'=>date('Y-m-d h:i:s')),
            array('service_name'=>'friday service','created_at'=>date('Y-m-d h:i:s'),'updated_at'=>date('Y-m-d h:i:s')),
            array('service_name'=>'crossover service','created_at'=>date('Y-m-d h:i:s'),'updated_at'=>date('Y-m-d h:i:s')),
            array('service_name'=>'main service','created_at'=>date('Y-m-d h:i:s'),'updated_at'=>date('Y-m-d h:i:s')),
            array('service_name'=>'adult service','created_at'=>date('Y-m-d h:i:s'),'updated_at'=>date('Y-m-d h:i:s')),
            array('service_name'=>'abc classes','created_at'=>date('Y-m-d h:i:s'),'updated_at'=>date('Y-m-d h:i:s')),


        ));
    }
}
