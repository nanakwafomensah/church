<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->delete();
        $credentials=[
            'first_name'=>'Admin',
            'last_name'=>'Mensah',
            'email'=>'admin@gmail.com',
            'password'=>'12345',
            'location'=>"Dansoman",
            'username'=>'admin',

        ];
        $financecredentials=[
            'first_name'=>'nana',
            'last_name'=>'Mensah',
            'email'=>'finance@example.com',
            'password'=>'finance',
            'location'=>"Dansoman",
            'username'=>'finance',

        ];
        $Othercredentials=[
            'first_name'=>'magbin',
            'last_name'=>'Mensah',
            'email'=>'other@example.com',
            'password'=>'other',
            'location'=>"Dansoman",
            'username'=>'other',

        ];
        $user = Sentinel::registerAndActivate($credentials);
        $role=Sentinel::findRoleBySlug('Admin');
        $role->users()->attach($user);

        $userf = Sentinel::registerAndActivate($financecredentials);
        $rolef=Sentinel::findRoleBySlug('Finance');
        $rolef->users()->attach($userf);

        $usero= Sentinel::registerAndActivate($Othercredentials);
        $roleo=Sentinel::findRoleBySlug('Other');
        $roleo->users()->attach($usero);
    }
}
