<?php

use Illuminate\Database\Seeder;
use App\Profile;
class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('profiles')->delete();
        $p = new Profile();

        $p->churchname='DIVINE TEMPLE';
        $p->parentchurchname='International Central Gospel Church';
        $p->website='www.internationalcentralgospel.com';
        $p->address='p.o.box 2136';
        $p->telephone='03028788889';
        $p->phone='0262489168';
        $p->avatar='default.jpg';
        $p->country='Ghana';
        $p->city='Accra';
        $p->email='fiesta@gmail.com';
        $p->smssenderid='ICGC';
        $p->no_of_pastors='3';
        $p->pastorname='Nana Kwafo Mensah';
        $p->district='AKWAPEM SOUTH';
        $p->save();
    }
}
