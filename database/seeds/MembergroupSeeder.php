<?php

use Illuminate\Database\Seeder;

class MembergroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('membergroups')->delete();
        DB::table('membergroups')->insert(array(
            array('name'=>'all','created_at'=>date('Y-m-d h:i:s'),'updated_at'=>date('Y-m-d h:i:s'),'purpose'=>'pastorial','status'=>'active')



        ));
    }
}
