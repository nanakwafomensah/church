<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes;
use Image;
use DB;
use Datatables;
class ClassesController extends Controller
{
    //
    public  function index(){
        $allmembers=DB::table('members')->select(['id','member_id','firstname','lastname'])->get();
        return view('admin.classes')->with(['allmembers'=>$allmembers]);
    }
    public function get_all_classes(){
        $members = DB::table('classes')->select(['id','participantid','participantname',DB::raw('date(registrationdate) as registrationdate'), 'stage','status','graduation_status'])->orderBy('registrationdate','asc');

        return Datatables::of($members)
            ->addColumn('action', function ($members) {

                return'<a href="" data-toggle="modal" data-target="#edit-Modal"  data-id="'.$members->id.'" data-participantid="'.$members->participantid.'"  data-participantname="'.$members->participantname.'" data-registrationdate="'.$members->registrationdate.'" data-stage="'.$members->stage.'"  data-status="'.$members->status.'" data-graduation_status="'.$members->graduation_status.'" class="editbtn btn btn-warning" > <i class="fa fa-pencil-square-o" aria-hidden="true">&nbsp;</i></a>
                       <a href="" data-toggle="modal" data-target="#delete-Modal" data-id="'.$members->id.'" data-participantid="'.$members->participantid.'"  data-participantname="'.$members->participantname.'" data-registrationdate="'.$members->registrationdate.'" data-stage="'.$members->stage.'"   class="deletebtn btn btn-danger" ><i class="fa fa-trash" aria-hidden="true">&nbsp;</i></a>
                                             ';
            })
            ->addColumn('level', function ($members) {
                     if($members->status == 'Incomplete'){
                         return"<button > Incomplete</button>";
                     }else{
                         return'<button> Incomplete</button>';
                     }
               
            })
            ->make(true);
    }
    public function save(Request $request){
        $m = new Classes();
       $registrationdate=$request->reg_date;
       $participantid=$request->id;
       $participantstage=$request->stage;

       $memberdetails =  DB::table('members')->where('member_id',trim($participantid))->first();
       $participantname= $memberdetails->fullname;

        $m->participantid=$participantid;
        $m->participantname=$participantname;
        $m->registrationdate=$registrationdate;
        $m->stage=$participantstage;
        if($m->save()){
            $notification=array(
                'message'=>"New Applicant has been Succesfully Added ",
                'alert-type'=>'success'
            );

            return redirect('classes')->with($notification);
        }
    }
    public function update(Request $request){


        try{

            $m = Classes::where('id', $request->id_edit)->firstOrFail();
//            $m->participantid=$request->participantid_edit;
            $m->registrationdate=$request->registrationdate_edit;
            $m->stage=$request->stage_edit;
            $m->status=$request->status_edit;
            $m->graduation_status=$request->graduation_status_edit;




            if($m->update()){

                $notification=array(
                    'message'=>" details has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('classes')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"details  failed to be updated",
                'alert-type'=>'success'
            );
            return redirect('classes')->with($notification);
        }
    }
    public function delete(Request $request){

        try{
            $m = Classes::where('id', $request->id_delete)->firstOrFail();
            if($m->delete()){
                $notification=array(
                    'message'=>"Record  has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('classes')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error Occured",
                'alert-type'=>'error'
            );
            return redirect('classes')->with($notification);
        }
    }
}
