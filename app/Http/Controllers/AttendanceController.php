<?php

namespace App\Http\Controllers;

use App\Attendance;
use Illuminate\Http\Request;
use Image;
use DB;
use Datatables;
class AttendanceController extends Controller
{
    //
    public function index(){
        $servicetype=DB::table('services')->select(['id','service_name'])->get();
        return view('admin/newattendance')->with(['servicetype'=>$servicetype]);
        
    }
    public function allindex(){
        $servicetype=DB::table('services')->select(['id','service_name'])->get();
        return view('admin/allattendance')->with(['servicetype'=>$servicetype]);
        ;

    }
    public function get_allattendance(){
        $members = DB::table('attendances')->select(['id','servicetype','day', 'male', 'female', 'visitor_male','visitor_female','car',DB::raw('date(registration_date) as registration_date'),'stage_classes']) ->orderBy('registration_date','asc');

        return Datatables::of($members)
            ->addColumn('action', function ($members) {

                return'<a href="" data-toggle="modal" data-target="#edit-Modal"  data-id="'.$members->id.'"  data-servicetype="'.$members->servicetype.'" data-day="'.$members->day.'" data-male="'.$members->male.'" data-female="'.$members->female.'" data-visitormale="'.$members->visitor_male.'" data-visitorfemale="'.$members->visitor_female.'" data-car="'.$members->car.'" data-created_at="'.$members->registration_date.'"  data-stage_classes="'.$members->stage_classes.'" class="editbtn btn btn-warning" > <i class="fa fa-pencil-square-o" aria-hidden="true">&nbsp;</i></a>
                                          <a href="" data-toggle="modal" data-target="#delete-Modal"    data-id="'.$members->id.'" data-day="'.$members->day.'"  data-servicetype="'.$members->servicetype.'" data-male="'.$members->male.'" data-female="'.$members->female.'" data-visitormale="'.$members->visitor_male.'" data-car="'.$members->car.'" data-created_at="'.$members->registration_date.'"   class="deletebtn btn btn-danger" ><i class="fa fa-trash" aria-hidden="true">&nbsp;</i></a>
                                             ';
            })


            ->make(true);

    }
    public function save( Request $request){
        try{
            $m = new Attendance();

            $m->servicetype=$request->servicetype;
            $m->male=$request->male;
            $m->female=$request->female;
            $m->visitor_male=$request->visitor_male;
            $m->visitor_female=$request->visitor_female;
            $m->car=$request->car;
            $m->day=$request->day;
            $m->stage_classes=$request->stage;
            
            $m->registration_date=$request->registration_date;

            if($m->save()){
                $notification=array(
                    'message'=>"New Attendance has been Succesfully Added",
                    'alert-type'=>'success'
                );

                return redirect('allattendance')->with($notification);
            }
        }catch(Exception $e){
            $notification=array(
                'message'=>'A Error Occured',
                'alert-type'=>'error'
            );
            return redirect('newattendance')->with($notification);
        }
    }
    public function update(Request $request){
       
        try{
            $m = Attendance::findorfail($request->attendanceedit_id);
            $m->servicetype=$request->servicetypeedit;
            $m->male=$request->maleedit;
            $m->female=$request->femaleedit;
            $m->visitor_male=$request->visitormaleedit;
            $m->visitor_female=$request->visitorfemaleedit;
            $m->car=$request->caredit;
            $m->day=$request->dayedit;
            $m->stage_classes=$request->stage_classes_edit;
            $m->registration_date=$request->registration_dateedit;

            if($m->update()){

                $notification=array(
                    'message'=>"Details details has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('allattendance')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"Attendance  details  failed to updated",
                'alert-type'=>'success'
            );
            return redirect('allattendance')->with($notification);
        }
    }
    public function delete(Request $request){
        try{
            $m = Attendance::findorfail($request->offering_number_delete);
            if($m->delete()){
                $notification=array(
                    'message'=>"Attendance has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('allattendance')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error occured",
                'alert-type'=>'error'
            );
            return redirect('allattendance')->with($notification);
        }
    }
}
