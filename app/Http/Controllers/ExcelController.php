<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Classes;
use Illuminate\Http\Request;
use App\Offering;
use App\Member;

use Image;
use DB;
use Datatables;
use Excel;
class ExcelController extends Controller
{
    //

    public function downloadExcelOffering($offeringType,$serviceTyp,$fromDate,$toDate,$title){
        $type='csv';
        $title=$title;
        $data = Offering::get()->where('offeringtype',$offeringType)->where('servicetype',$serviceTyp)->where('date_submitted','>=',$fromDate)->where('date_submitted','<=',$toDate)->toArray();
        return Excel::create('excel', function($excel) use ($data,$title) {

            $excel->sheet('mySheet', function($sheet) use ($data,$title)
            {

                $sheet->cell('A1', function($cell) use($title)
                {

                    $cell->setValue($title);
                });

                $sheet->cell('A2', function($cell) {$cell->setValue('OfferingID');   });
                $sheet->cell('B2', function($cell) {$cell->setValue('Offering Type');   });
                $sheet->cell('C2', function($cell) {$cell->setValue('Service Type');   });
                $sheet->cell('D2', function($cell) {$cell->setValue('Amount');   });
                $sheet->cell('E2', function($cell) {$cell->setValue('Description');   });
                $sheet->cell('F2', function($cell) {$cell->setValue('Date Submitted');   });
                if (!empty($data)) {
                    foreach ($data as $key => $value) {
                        $i= $key+3;
                        $sheet->cell('A'.$i, $value['number']);
                        $sheet->cell('B'.$i, $value['offeringtype']);
                        $sheet->cell('C'.$i, $value['servicetype']);
                        $sheet->cell('D'.$i, $value['amount']);
                        $sheet->cell('E'.$i, $value['description']);
                        $sheet->cell('F'.$i, $value['date_submitted']);
                    }
//                    $sheet->fromArray($data);
                }

            });
        })->download($type);
    }
    public function downloadExcelclass($title,$level,$status,$award,$date_from,$date_to){
        $type='csv';
        $title=$title;
        $data = Classes::get()->where('stage',$level)->where('status',$status)->where('graduation_status',$award)->where('registrationdate','>=',$date_from)->where('registrationdate','<=',$date_to)->toArray();
        return Excel::create('excel', function($excel) use ($data,$title) {

            $excel->sheet('mySheet', function($sheet) use ($data,$title)
            {

                $sheet->cell('A1', function($cell) use($title)
                {

                    $cell->setValue($title);
                });

                $sheet->cell('A2', function($cell) {$cell->setValue('ParticipantID');   });
                $sheet->cell('B2', function($cell) {$cell->setValue('Name');   });
                $sheet->cell('C2', function($cell) {$cell->setValue('Registration Date');   });
                $sheet->cell('D2', function($cell) {$cell->setValue('Stage');   });
                $sheet->cell('E2', function($cell) {$cell->setValue('Status');   });
                $sheet->cell('F2', function($cell) {$cell->setValue('Award');   });
                if (!empty($data)) {
                    foreach ($data as $key => $value) {
                        $i= $key+3;
                        $sheet->cell('A'.$i, $value['participantid']);
                        $sheet->cell('B'.$i, $value['participantname']);
                        $sheet->cell('C'.$i, $value['registrationdate']);
                        $sheet->cell('D'.$i, $value['stage']);
                        $sheet->cell('E'.$i, $value['status']);
                        $sheet->cell('F'.$i, $value['graduation_status']);
                    }
//                    $sheet->fromArray($data);
                }

            });
        })->download($type);
    }
    public function downloadExcelattendance($title,$servicetype,$date_from,$date_to){
        $type='csv';
        $title=$title;
        $data = Attendance::get()->where('servicetype',$servicetype)->where('registration_date','>=',$date_from)->where('registration_date','<=',$date_to)->toArray();
        return Excel::create('excel', function($excel) use ($data,$title) {

            $excel->sheet('mySheet', function($sheet) use ($data,$title)
            {

                $sheet->cell('A1', function($cell) use($title)
                {

                    $cell->setValue($title);
                });

                $sheet->cell('A2', function($cell) {$cell->setValue('SERVICE TYPE');   });
                $sheet->cell('B2', function($cell) {$cell->setValue('MALE');   });
                $sheet->cell('C2', function($cell) {$cell->setValue('FEMALE');   });
                $sheet->cell('D2', function($cell) {$cell->setValue('VISITOR MALE');   });
                $sheet->cell('E2', function($cell) {$cell->setValue('VISITOR FEMALE');   });
                $sheet->cell('F2', function($cell) {$cell->setValue('CAR');   });
                $sheet->cell('G2', function($cell) {$cell->setValue('DAY');   });
                $sheet->cell('H2', function($cell) {$cell->setValue('STAGE CLASSES');   });
                $sheet->cell('I2', function($cell) {$cell->setValue('REGISTRATION DATE');   });
                if (!empty($data)) {
                    foreach ($data as $key => $value) {
                        $i= $key+3;
                        $sheet->cell('A'.$i, $value['servicetype']);
                        $sheet->cell('B'.$i, $value['male']);
                        $sheet->cell('C'.$i, $value['female']);
                        $sheet->cell('D'.$i, $value['visitor_male']);
                        $sheet->cell('E'.$i, $value['visitor_female']);
                        $sheet->cell('F'.$i, $value['car']);
                        $sheet->cell('G'.$i, $value['day']);
                        $sheet->cell('H'.$i, $value['stage_classes']);
                        $sheet->cell('I'.$i, $value['registration_date']);

                    }
//                    $sheet->fromArray($data);
                }

            });
        })->download($type);
    }
    public function downloadExcelmember($title,$membergroup,$gender,$marital_status,$member_status,$locations,$date_from,$date_to){
        $type='csv';
        $title=$title;
        $data = Member::get()
            ->where('groups',$membergroup)
            ->where('gender',$gender)
            ->where('marital_status',$marital_status)
            ->where('membershipstatus',$member_status)
            ->where('hometown',$locations)
            ->where('registration_date','>=',$date_from)
            ->where('registration_date','<=',$date_to)->toArray();
        return Excel::create('excel', function($excel) use ($data,$title) {

            $excel->sheet('mySheet', function($sheet) use ($data,$title)
            {

                $sheet->cell('A1', function($cell) use($title)
                {

                    $cell->setValue($title);
                });

                $sheet->cell('A2', function($cell) {$cell->setValue('MEMBER ID');   });
                $sheet->cell('B2', function($cell) {$cell->setValue('FIRSTNAME');   });
                $sheet->cell('C2', function($cell) {$cell->setValue('LASTNAME');   });
                $sheet->cell('D2', function($cell) {$cell->setValue('NATIONALITY');   });
                $sheet->cell('E2', function($cell) {$cell->setValue('OCCUPATION');   });
                $sheet->cell('F2', function($cell) {$cell->setValue('TELEPHONE');   });
                $sheet->cell('G2', function($cell) {$cell->setValue('GENDER');   });
                $sheet->cell('H2', function($cell) {$cell->setValue('DOB');   });
                $sheet->cell('I2', function($cell) {$cell->setValue('MARITAL STATUS');   });
                $sheet->cell('J2', function($cell) {$cell->setValue('HOMETOWN');   });
                $sheet->cell('K2', function($cell) {$cell->setValue('FIRSTFRUIT');   });
                $sheet->cell('L2', function($cell) {$cell->setValue('CONVENANT FAMILY');   });
                $sheet->cell('M2', function($cell) {$cell->setValue('GROUPS');   });
                $sheet->cell('N2', function($cell) {$cell->setValue('MEMBERSHIP STATUS');   });
                $sheet->cell('O2', function($cell) {$cell->setValue('MEMBERSHIP');   });
                if (!empty($data)) {
                    foreach ($data as $key => $value) {
                        $i= $key+3;
                        $sheet->cell('A'.$i, $value['member_id']);
                        $sheet->cell('B'.$i, $value['firstname']);
                        $sheet->cell('C'.$i, $value['lastname']);
                        $sheet->cell('D'.$i, $value['nationality']);
                        $sheet->cell('E'.$i, $value['occupation']);
                        $sheet->cell('F'.$i, $value['telephone']);
                        $sheet->cell('G'.$i, $value['gender']);
                        $sheet->cell('H'.$i, $value['dob']);
                        $sheet->cell('I'.$i, $value['marital_status']);
                        $sheet->cell('J'.$i, $value['hometown']);
                        $sheet->cell('K'.$i, $value['firstfruit']);
                        $sheet->cell('L'.$i, $value['convenant_family']);
                        $sheet->cell('M'.$i, $value['groups']);
                        $sheet->cell('N'.$i, $value['membershipstatus']);
                        $sheet->cell('O'.$i, $value['membership_cat']);

                    }
//                    $sheet->fromArray($data);
                }

            });
        })->download($type);
    }
    public function downloadExcelspecial($title,$date_from,$date_to){
        if($title=='1'){
            $title='EMPLOYED AND UNEMPLOYED MEMBERS';

            $type='csv';
          
            $data =  Member::get()
                ->where('membershipstatus','member')
                ->where('registration_date','>=',$date_from)
                ->where('registration_date','<=',$date_to)->toArray();

            return Excel::create('excel', function($excel) use ($data,$title) {

                $excel->sheet('mySheet', function($sheet) use ($data,$title)
                {

                    $sheet->cell('A1', function($cell) use($title)
                    {

                        $cell->setValue($title);
                    });

                    $sheet->cell('A2', function($cell) {$cell->setValue('MEMBER ID');   });
                    $sheet->cell('B2', function($cell) {$cell->setValue('FIRSTNAME');   });
                    $sheet->cell('C2', function($cell) {$cell->setValue('LASTNAME');   });
                    $sheet->cell('D2', function($cell) {$cell->setValue('NATIONALITY');   });
                    $sheet->cell('E2', function($cell) {$cell->setValue('OCCUPATION');   });
                    $sheet->cell('F2', function($cell) {$cell->setValue('TELEPHONE');   });
                    $sheet->cell('G2', function($cell) {$cell->setValue('GENDER');   });
                    $sheet->cell('H2', function($cell) {$cell->setValue('DOB');   });
                    $sheet->cell('I2', function($cell) {$cell->setValue('MARITAL STATUS');   });
                    $sheet->cell('J2', function($cell) {$cell->setValue('HOMETOWN');   });
                    $sheet->cell('K2', function($cell) {$cell->setValue('FIRSTFRUIT');   });
                    $sheet->cell('L2', function($cell) {$cell->setValue('CONVENANT FAMILY');   });
                    $sheet->cell('M2', function($cell) {$cell->setValue('GROUPS');   });
                    $sheet->cell('N2', function($cell) {$cell->setValue('MEMBERSHIP STATUS');   });
                    $sheet->cell('O2', function($cell) {$cell->setValue('MEMBERSHIP');   });
                    if (!empty($data)) {
                        foreach ($data as $key => $value) {
                            $i= $key+3;
                            $sheet->cell('A'.$i, $value['member_id']);
                            $sheet->cell('B'.$i, $value['firstname']);
                            $sheet->cell('C'.$i, $value['lastname']);
                            $sheet->cell('D'.$i, $value['nationality']);
                            $sheet->cell('E'.$i, $value['occupation']);
                            $sheet->cell('F'.$i, $value['telephone']);
                            $sheet->cell('G'.$i, $value['gender']);
                            $sheet->cell('H'.$i, $value['dob']);
                            $sheet->cell('I'.$i, $value['marital_status']);
                            $sheet->cell('J'.$i, $value['hometown']);
                            $sheet->cell('K'.$i, $value['firstfruit']);
                            $sheet->cell('L'.$i, $value['convenant_family']);
                            $sheet->cell('M'.$i, $value['groups']);
                            $sheet->cell('N'.$i, $value['membershipstatus']);
                            $sheet->cell('O'.$i, $value['membership_cat']);

                        }
//                    $sheet->fromArray($data);
                    }

                });
            })->download($type);
            
        }elseif($title=='2'){

        }
    }
}
