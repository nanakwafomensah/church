<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use Image;
use DB;
use Datatables;
class oReportController extends Controller
{

    //
    public function index(){
        $membergroups = DB::table('membergroups')->get();
        $servicetype=DB::table('services')->select(['id','service_name'])->get();
        return view('other/oreport', ['membergroups' => $membergroups,'servicetype'=>$servicetype]);
    }
}
