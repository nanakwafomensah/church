<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use PDF;
use DB;
use Datatables;
use DateTime;
class PdfController extends Controller
{
    //

    public function allmembers(Request $request){

         ////////////////CHURCH DETAILS//////////////
        $churchdetails =  DB::table('profiles')->first();
        /////////////////ALL MEMBER DETAILS/////////////////////////////
        $gender=$request->gender_type;
        $marital_status_type=$request->marital_status_type;
        $membership_status_type=$request->membership_status_type;
        $location_type=$request->location_type;
        $member_sec_title=$request->member_sec_title;

        if($request->member_type=='all'){
            $allmembers=DB::table('members')->whereDate('registration_date','>=',$request->date_from)
                ->whereDate('registration_date','<=',$request->date_to)
                ->where('gender',$gender)
                ->where('marital_status',$marital_status_type)
                ->where('membershipstatus',$membership_status_type)
                ->where('hometown',$location_type)
             ->orderBy('registration_date','asc')->get();
        }else{
            $allmembers=DB::table('members')->where('groups','LIKE',$request->member_type.'%')->whereDate('registration_date','>=',$request->date_from)
                ->whereDate('registration_date','<=',$request->date_to)
                ->where('gender',$gender)
                ->where('marital_status',$marital_status_type)
                ->where('membershipstatus',$membership_status_type)
                ->where('hometown',$location_type)
                ->orderBy('registration_date','asc')->get();
        }

        /////////////////////////////////////////////////////
        $html=view('pdfs.memberpdf')->with([
            'churchdetails'=>$churchdetails,
            'allmembers'=>$allmembers,
            'date_from'=>$request->date_from,
            'date_to'=>$request->date_to,
            'title'=>$member_sec_title,

        ])->render();
        return PDF::load($html, 'A4', 'landscape')->show();

    }
    public function quarterly(Request $request){
//      dd($request->radtwo);
        $from_date=$request->from_date;
        $to_date=$request->to_date;
//        dd($from_date);
        ///////////////PREVIOUS QUARTER DATE//////////////
        $previous_quarterdate_from=$this->previousquarterfrom();
        $previous_quarterdate_to=$this->previousquarterto();
        ////////////////DATE PERIOD//////////////
        $_fromdatedata= strtotime($request->from_date);
        $_todatedata= strtotime($request->to_date);
        $no_of_sundays=$this->no_of_sundays($from_date,$to_date);
        ///////////////////AVERAGE MALE CHILDRENSERVICE/////////////////////////

        $children_serviceattendancedetailsquery_male =  DB::table('attendances')->select([DB::raw('sum(male) as c')])->where('servicetype','children service')->where('day','sunday') ->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $children_serviceattendancedetailsquery_visitormale =  DB::table('attendances')->select([DB::raw('sum(visitor_male) as c')])->where('servicetype','children service')->where('day','sunday') ->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $average_Sunday_Church_Attendance_at_Children_Servicemale =  (number_format($children_serviceattendancedetailsquery_male->c)+  number_format($children_serviceattendancedetailsquery_visitormale->c))/ number_format($no_of_sundays) ;

        ///////////////////AVERAGE FEMALE CHILDRENSERVICE/////////////////////////
        $children_serviceattendancedetailsquery_female =  DB::table('attendances')->select([DB::raw('sum(female) as c')])->where('servicetype','children service')->where('day','sunday') ->whereDate('registration_date','>=',$to_date)->whereDate('registration_date','<=',$to_date)->first();
        $children_serviceattendancedetailsquery_visitorfemale =  DB::table('attendances')->select([DB::raw('sum(visitor_female) as c')])->where('servicetype','children service')->where('day','sunday') ->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $average_Sunday_Church_Attendance_at_Children_Servicfemale =  (number_format($children_serviceattendancedetailsquery_female->c)+  number_format($children_serviceattendancedetailsquery_visitorfemale->c))/ number_format($no_of_sundays) ;

        ///////////////////AVERAGE MALE ADULTSERVICE/////////////////////////

        $adult_serviceattendancedetailsquery_male =  DB::table('attendances')->select([DB::raw('sum(male) as c')])->where('servicetype','adult service')->where('day','sunday') ->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $adult_serviceattendancedetailsquery_visitormale =  DB::table('attendances')->select([DB::raw('sum(visitor_male) as c')])->where('servicetype','adult service')->where('day','sunday') ->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $average_Sunday_Church_Attendance_at_Aldult_Servicemale =  (number_format($adult_serviceattendancedetailsquery_male->c)+  number_format($adult_serviceattendancedetailsquery_visitormale->c))/ number_format($no_of_sundays) ;

        ///////////////////AVERAGE FEMALE ADULTSERVICE/////////////////////////

        $adult_serviceattendancedetailsquery_female =  DB::table('attendances')->select([DB::raw('sum(female) as c')])->where('servicetype','adult service')->where('day','sunday') ->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $adult_serviceattendancedetailsquery_visitorfemale =  DB::table('attendances')->select([DB::raw('sum(visitor_female) as c')])->where('servicetype','adult service')->where('day','sunday') ->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $average_Sunday_Church_Attendance_at_Aldult_Servicefemale =  (number_format($adult_serviceattendancedetailsquery_female->c)+  number_format($adult_serviceattendancedetailsquery_visitorfemale->c))/ number_format($no_of_sundays) ;

          ///////////////////AVERAGE MALE YOUTHSERVICE/////////////////////////

        $youth_serviceattendancedetailsquery_male =  DB::table('attendances')->select([DB::raw('sum(male) as c')])->where('servicetype','youth service')->where('day','sunday') ->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $youth_serviceattendancedetailsquery_visitormale =  DB::table('attendances')->select([DB::raw('sum(visitor_male) as c')])->where('servicetype','youth service')->where('day','sunday') ->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $average_Sunday_Church_Attendance_at_youth_Servicemale =  (number_format($youth_serviceattendancedetailsquery_male->c)+  number_format($youth_serviceattendancedetailsquery_visitormale->c))/ number_format($no_of_sundays) ;
       
        ///////////////////AVERAGE FEMALE YOUTHSERVICE/////////////////////////

        $youth_serviceattendancedetailsquery_female =  DB::table('attendances')->select([DB::raw('sum(female) as c')])->where('servicetype','youth service')->where('day','sunday') ->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $youth_serviceattendancedetailsquery_visitorfemale =  DB::table('attendances')->select([DB::raw('sum(visitor_female) as c')])->where('servicetype','youth service')->where('day','sunday') ->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $average_Sunday_Church_Attendance_at_youth_Servicefemale =  (number_format($youth_serviceattendancedetailsquery_female->c)+  number_format($youth_serviceattendancedetailsquery_visitorfemale->c))/ number_format($no_of_sundays) ;

        /////////////////////CURENT ADULT MEMBERSHIP//////////////////////////
        $Current_Adult_membership_male =  DB::table('members')->select([DB::raw('count(*) as c')])->where('membership_cat','adult')->where('gender','male')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $Current_Adult_membership_female =  DB::table('members')->select([DB::raw('count(*) as c')])->where('membership_cat','adult')->where('gender','female')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
         ////////////////////CURENT YOUTH MEMBERSHIP//////////////////////////
        $Current_youth_membership_male =  DB::table('members')->select([DB::raw('count(*) as c')])->where('membership_cat','youth')->where('gender','male')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $Current_youth_membership_female =  DB::table('members')->select([DB::raw('count(*) as c')])->where('membership_cat','youth')->where('gender','female')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        ////////////////////CURENT CHILD MEMBERSHIP//////////////////////////
        $Current_child_membership_male =  DB::table('members')->select([DB::raw('count(*) as c')])->where('membership_cat','child')->where('gender','male')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $Current_child_membership_female =  DB::table('members')->select([DB::raw('count(*) as c')])->where('membership_cat','child')->where('gender','female')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();

        //////////////////factors that affect the church//////////////////////
        $newmembers_were_added_to_your_assembly_male=DB::table('members')->select([DB::raw('count(*) as c')])->where('gender','male')->where('membershipstatus','member')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $newmembers_were_added_to_your_assembly_female=DB::table('members')->select([DB::raw('count(*) as c')])->where('gender','female')->where('membershipstatus','member')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();

        $newmembers_went_through_membership_classes_male=DB::table('members')->select([DB::raw('count(*) as c')])->where('gender','male')->where('membershipstatus','member')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $newmembers_went_through_membership_classes_female=DB::table('members')->select([DB::raw('count(*) as c')])->where('gender','female')->where('membershipstatus','member')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();

        $members_baptised_by_the_church_male=DB::table('members')->select([DB::raw('count(*) as c')])->where('gender','male')->where('membershipstatus','member')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $members_baptised_by_the_church_female=DB::table('members')->select([DB::raw('count(*) as c')])->where('gender','female')->where('membershipstatus','member')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();

        $members_filled_membership_form_male=DB::table('members')->select([DB::raw('count(*) as c')])->where('gender','male')->where('membershipstatus','member')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $members_filled_membership_form_female=DB::table('members')->select([DB::raw('count(*) as c')])->where('gender','female')->where('membershipstatus','member')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();

        $members_completed_abc_male=DB::table('members')->select([DB::raw('count(*) as c')])->where('gender','male')->where('membershipstatus','member')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $members_completed_abc_female=DB::table('members')->select([DB::raw('count(*) as c')])->where('gender','female')->where('membershipstatus','member')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();


        $members_left_the_church_altogether_male=DB::table('members')->select([DB::raw('count(*) as c')])->where('gender','male')->where('membershipstatus','member')->first();
        $members_left_the_church_altogether_female=DB::table('members')->select([DB::raw('count(*) as c')])->where('gender','female')->where('membershipstatus','member')->first();
        //////////////////////convenant details/////////////////////////////
        $covenant_families_were_there_at_the_beginning_of_the_quarter=DB::table('convenantfamilies')->select([DB::raw('count(*) as c')])->whereDate('establishmentdate','<=',$from_date)->first();
        $new_covenant_families_were_established_in_your_church=DB::table('convenantfamilies')->select([DB::raw('count(*) as c')])->whereDate('establishmentdate','>=',$from_date)->whereDate('establishmentdate','<=',$to_date)->first();
        $number_of_covenant_families_that_meet_weekly_in_your_assembly=DB::table('convenantfamilies')->select([DB::raw('count(*) as c')])->where('meetingtimes','weekly')->first();
        $covenant_families_that_are_not_active_hardly_meet=DB::table('convenantfamilies')->select([DB::raw('count(*) as c')])->where('status','inactive')->first();
       ////////////////////////small groups////////////////////////////////
        $small_groups_does_the_assembly=DB::table('membergroups')->select([DB::raw('count(*) as c')])->first();
        $small_groups_are_being_use_for_pastoral_care=DB::table('membergroups')->select([DB::raw('count(*) as c')])->where('purpose','pastorial')->first();
       //////////////////Family Enrichment //////////////////////////////////////
         $marriages_at_the_beginning_of_the_quarter =DB::table('events')->select([DB::raw('count(*) as c')])->where('eventtype','marriage')->whereDate('recordered_date','>=',$from_date)->whereDate('recordered_date','<=',$to_date)->first();
          $new_marriages_were_blessed_in_the_church =DB::table('events')->select([DB::raw('count(*) as c')])->where('eventtype','marriage')->first();
        $couples_joined_the_assembly_already_married=DB::table('members')->select([DB::raw('count(*) as c')])->where('marital_status','Married')->whereNotNull('couplename')->groupBy('couplename')->first();
        $couples_joined_the_assembly_already_married_number=null;

        if($couples_joined_the_assembly_already_married == ""){
            $couples_joined_the_assembly_already_married_number=0;

           }else{
            $couples_joined_the_assembly_already_married_number=$couples_joined_the_assembly_already_married->c;
        }
        $Married_Couples_families_in_the_Church=DB::table('members')->select([DB::raw('count(*) as c')])->groupBy('couplename')->where('marital_status','Married')->whereNotNull('couplename')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $Married_Couples_families_in_the_Church_number=null;
        if($Married_Couples_families_in_the_Church == ""){
            $Married_Couples_families_in_the_Church_number=0;

        }else{
            $Married_Couples_families_in_the_Church_number=$Married_Couples_families_in_the_Church->c;
        }


        $Number_of_Widows_widowers_in_the_church =DB::table('members')->select([DB::raw('count(*) as c')])->where('marital_status','Widows')->first();
        $divorce_cases_happened_in_the_church=DB::table('members')->select([DB::raw('count(*) as c')])->where('marital_status','Divorced')->first();
        $training_to_do_councelling=DB::table('members')->select([DB::raw('count(*) as c')])->where('trC','Yes')->first();
        $births_were_recorded_in_the_Assembly=DB::table('events')->select([DB::raw('count(*) as c')])->where('eventtype','birth')->first();
        $baby_naming_ceremonies_were_organized=DB::table('events')->select([DB::raw('count(*) as c')])->where('eventtype','namingceremony')->first();
        $babydedication=DB::table('events')->select([DB::raw('count(*) as c')])->where('eventtype','babydedication')->whereDate('recordered_date','>=',$from_date)->whereDate('recordered_date','<=',$to_date)->first();
        $members_passed_on_to_glory_in_the_Assembly=DB::table('members')->select([DB::raw('count(*) as c')])->where('passed_to_glory','Yes')->first();
         $burial_services_did_you_organize=DB::table('events')->select([DB::raw('count(*) as c')])->where('eventtype','burial')->first();
       ///////////////students/////////////////////
        $student_ece_male=DB::table('members')->select([DB::raw('count(*) as c')])->where('level_edu','ece')->where('gender','male')->first();
        $student_pe_male=DB::table('members')->select([DB::raw('count(*) as c')])->where('level_edu','pe')->where('gender','male')->first();
        $student_shs_male=DB::table('members')->select([DB::raw('count(*) as c')])->where('level_edu','shs')->where('gender','male')->first();
        $student_jhs_male=DB::table('members')->select([DB::raw('count(*) as c')])->where('level_edu','jhs')->where('gender','male')->first();

        $student_ece_female=DB::table('members')->select([DB::raw('count(*) as c')])->where('level_edu','ece')->where('gender','female')->first();
        $student_pe_female=DB::table('members')->select([DB::raw('count(*) as c')])->where('level_edu','pe')->where('gender','female')->first();
        $student_shs_female=DB::table('members')->select([DB::raw('count(*) as c')])->where('level_edu','shs')->where('gender','female')->first();
        $student_jhs_female=DB::table('members')->select([DB::raw('count(*) as c')])->where('level_edu','jhs')->where('gender','female')->first();
        ////teacher//////////////
        $teacher_trained_male=DB::table('members')->select([DB::raw('count(*) as c')])->where('teacher_qualification','trained')->where('gender','male')->first();
        $teacher_trained_female=DB::table('members')->select([DB::raw('count(*) as c')])->where('teacher_qualification','trained')->where('gender','female')->first();
        $teacher_nontrained_male=DB::table('members')->select([DB::raw('count(*) as c')])->where('teacher_qualification','nontrained')->where('gender','male')->first();
        $teacher_nontrained_female=DB::table('members')->select([DB::raw('count(*) as c')])->where('teacher_qualification','nontrained')->where('gender','female')->first();
        ////////ABC INFORMATION/////////////
        $totalenrollment_beginners= DB::table('classes')->where('stage','ABC')->whereDate('registrationdate','>=',$from_date)->whereDate('registrationdate','<=',$to_date)->count();
        $totalenrollment_Membership= DB::table('classes')->where('stage','Membership')->whereDate('registrationdate','>=',$from_date)->whereDate('registrationdate','<=',$to_date)->count();
        $totalenrollment_Maturity= DB::table('classes')->where('stage','Maturity')->whereDate('registrationdate','>=',$from_date)->whereDate('registrationdate','<=',$to_date)->count();
        $totalenrollment_Ministry= DB::table('classes')->where('stage','Ministry')->whereDate('registrationdate','>=',$from_date)->whereDate('registrationdate','<=',$to_date)->count();

         $completed_beginners=DB::table('classes')->where('stage','ABC')->where('status','complete')->whereDate('updated_at','>=',$from_date)->whereDate('updated_at','<=',$to_date)->count();
         $completed_membership=DB::table('classes')->where('stage','Membership')->where('status','complete')->whereDate('updated_at','>=',$from_date)->whereDate('updated_at','<=',$to_date)->count();
         $completed_Maturitry=DB::table('classes')->where('stage','Maturity')->where('status','complete')->whereDate('updated_at','>=',$from_date)->whereDate('updated_at','<=',$to_date)->count();
         $completed_Ministry=DB::table('classes')->where('stage','Ministry')->where('status','complete')->whereDate('updated_at','>=',$from_date)->whereDate('updated_at','<=',$to_date)->count();

        $gra_beginners=DB::table('classes')->where('stage','ABC')->where('graduation_status','Graduated')->whereDate('updated_at','>=',$from_date)->whereDate('updated_at','<=',$to_date)->count();
        $gra_membership=DB::table('classes')->where('stage','Membership')->where('graduation_status','Graduated')->whereDate('updated_at','>=',$from_date)->whereDate('updated_at','<=',$to_date)->count();
        $gra_Maturity=DB::table('classes')->where('stage','Maturity')->where('graduation_status','Graduated')->whereDate('updated_at','>=',$from_date)->whereDate('updated_at','<=',$to_date)->count();
        $gra_Ministry=DB::table('classes')->where('stage','Ministry')->where('graduation_status','Graduated')->whereDate('updated_at','>=',$from_date)->whereDate('updated_at','<=',$to_date)->count();











        ////////////////////////////finances//////////////////////////
        $firstfruit_amount=  DB::table('offerings')->select([DB::raw('sum(amount) as c')])->where('offeringtype','titheoffering')->whereDate('date_submitted','>=',$from_date)->whereDate('date_submitted','<=',$to_date)->first();
        $firstfruit_amountpreviousquarter=  DB::table('offerings')->select([DB::raw('sum(amount) as c')])->where('offeringtype','titheoffering')->whereDate('date_submitted','>=',$previous_quarterdate_from)->whereDate('date_submitted','<=',$previous_quarterdate_to)->first();

        $firstfruit_percentage= $firstfruit_amountpreviousquarter->c == 0 ? 0 :(($firstfruit_amount->c - $firstfruit_amountpreviousquarter->c)/$firstfruit_amountpreviousquarter->c)*100;



        $allotheroffering_amount=  DB::table('offerings')->select([DB::raw('sum(amount) as c')])->where('offeringtype','<>','titheoffering')->whereDate('date_submitted','>=',$from_date)->whereDate('date_submitted','<=',$to_date)->first();
        $allotheroffering_amountpreviousquarter=  DB::table('offerings')->select([DB::raw('sum(amount) as c')])->where('offeringtype','<>','titheoffering')->whereDate('date_submitted','>=',$previous_quarterdate_from)->whereDate('date_submitted','<=',$previous_quarterdate_to)->first();
        $allotheroffering_amount_percentage=$allotheroffering_amountpreviousquarter->c == 0 ? 0 :(($allotheroffering_amount->c - $allotheroffering_amountpreviousquarter->c)/$allotheroffering_amountpreviousquarter->c)*100;

        $Total_Annual_Income =  DB::table('offerings')->select([DB::raw('sum(amount) as c')])->whereYear('date_submitted', date('Y'))->first();
        $Total_Annual_Income_previous =  DB::table('offerings')->select([DB::raw('sum(amount) as c')])->whereYear('date_submitted', date("Y",strtotime("-1 year")))->first();
        $Total_Annual_Income_percentage =  $Total_Annual_Income_previous->c == 0 ? 0 :(($Total_Annual_Income->c - $Total_Annual_Income_previous->c)/$Total_Annual_Income_previous->c)*100;
         
         
         
          /////////////////////////////expenses/////////////////////////////
        $MissionsChurchPlantingExpenses= DB::table('expenses')->select([DB::raw('sum(amount) as c')])->where('category','Church Planting Expenses')->whereDate('rec_date','>=',$from_date)->whereDate('rec_date','<=',$to_date)->first();
        $MissionsChurchPlantingExpenses_previousquarter= DB::table('expenses')->select([DB::raw('sum(amount) as c')])->where('category','Church Planting Expenses')->whereDate('rec_date','>=',$previous_quarterdate_from)->whereDate('rec_date','<=',$previous_quarterdate_to)->first();
        $MissionsChurchPlantingExpenses_percentage= $MissionsChurchPlantingExpenses_previousquarter->c == 0 ? 0 :(($MissionsChurchPlantingExpenses->c - $MissionsChurchPlantingExpenses_previousquarter->c)/$MissionsChurchPlantingExpenses_previousquarter->c)*100;

        $Administrative_Expenses= DB::table('expenses')->select([DB::raw('sum(amount) as c')])->where('category','Administrative Expenses')->whereDate('rec_date','>=',$from_date)->whereDate('rec_date','<=',$to_date)->first();
        $Administrative_Expenses_previousquarter= DB::table('expenses')->select([DB::raw('sum(amount) as c')])->where('category','Administrative Expenses')->whereDate('rec_date','>=',$previous_quarterdate_from)->whereDate('rec_date','<=',$previous_quarterdate_to)->first();
        $Administrative_Expenses_percentage=$Administrative_Expenses_previousquarter->c == 0 ? 0 :(($Administrative_Expenses->c - $Administrative_Expenses_previousquarter->c)/$Administrative_Expenses_previousquarter->c)*100;

        $MinistryExpenses= DB::table('expenses')->select([DB::raw('sum(amount) as c')])->where('category','Ministry Expenses')->whereDate('rec_date','>=',$from_date)->whereDate('rec_date','<=',$to_date)->first();
        $MinistryExpenses_previousquarter= DB::table('expenses')->select([DB::raw('sum(amount) as c')])->where('category','Ministry Expenses')->whereDate('rec_date','>=',$previous_quarterdate_from)->whereDate('rec_date','<=',$previous_quarterdate_to)->first();
        $MinistryExpenses_percentage=$MinistryExpenses_previousquarter->c == 0 ? 0 :(($MinistryExpenses->c - $MinistryExpenses_previousquarter->c)/$MinistryExpenses_previousquarter->c)*100;

        $SocialActionProjects= DB::table('expenses')->select([DB::raw('sum(amount) as c')])->where('category','Social Action Projects')->whereDate('rec_date','>=',$from_date)->whereDate('rec_date','<=',$to_date)->first();
        $SocialActionProjects_previousquarter= DB::table('expenses')->select([DB::raw('sum(amount) as c')])->where('category','Social Action Projects')->whereDate('rec_date','>=',$previous_quarterdate_from)->whereDate('rec_date','<=',$previous_quarterdate_to)->first();
        $SocialActionProjects_percentage=$SocialActionProjects_previousquarter->c == 0 ? 0 :(($SocialActionProjects->c - $SocialActionProjects_previousquarter->c)/$SocialActionProjects_previousquarter->c)*100;
            
        $CapitalExpenditure = DB::table('expenses')->select([DB::raw('sum(amount) as c')])->where('category','Capital Expenditure')->whereDate('rec_date','>=',$from_date)->whereDate('rec_date','<=',$to_date)->first();
        $CapitalExpenditure_previousquarter = DB::table('expenses')->select([DB::raw('sum(amount) as c')])->where('category','Capital Expenditure')->whereDate('rec_date','>=',$previous_quarterdate_from)->whereDate('rec_date','<=',$previous_quarterdate_to)->first();
        $CapitalExpenditure_percentage=$CapitalExpenditure_previousquarter->c == 0 ? 0 :(($CapitalExpenditure->c - $CapitalExpenditure_previousquarter->c)/$CapitalExpenditure_previousquarter->c)*100;
             
        $TotalExpenditure = DB::table('expenses')->select([DB::raw('sum(amount) as c')])->whereDate('rec_date','>=',$from_date)->whereDate('rec_date','<=',$to_date)->first();
        $TotalExpenditure_previousquarter = DB::table('expenses')->select([DB::raw('sum(amount) as c')])->whereDate('rec_date','>=',$previous_quarterdate_from)->whereDate('rec_date','<=',$previous_quarterdate_to)->first();
        $TotalExpenditure_percentage = $TotalExpenditure_previousquarter->c == 0 ? 0 :(($TotalExpenditure->c - $TotalExpenditure_previousquarter->c)/$TotalExpenditure_previousquarter->c)*100;
        ////////////////CHURCH DETAILS//////////////
        $churchdetails =  DB::table('profiles')->first();
        
        
        $html=view('pdfs.quaterlypdf')->with([
            'churchdetails'=>$churchdetails,
            'from_date'=>date('M-Y', $_fromdatedata),
            'to_date'=>date('M-Y', $_todatedata),
            'rad1'=>$request->rad1,
            'rad2'=>$request->rad2,
            'rad3'=>$request->rad3,
            'rad4'=>$request->rad4,
            'rad5'=>$request->rad5,
            'rad6'=>$request->rad6,
            'rad7'=>$request->rad7,
            'rad8'=>$request->rad8,
            'rad9'=>$request->rad9,
            'rad10'=>$request->rad10,
            'rad11'=>$request->rad11,
            'rad12'=>$request->rad12,
            'rad13'=>$request->rad13,
            'rad14'=>$request->rad14,
            'rad15'=>$request->rad15,
            'rad16'=>$request->rad16,
            'rad17'=>$request->rad17,
            'rad18'=>$request->rad18,
            'rad19'=>$request->rad19,
            'rad20'=>$request->rad20,
            'rad21'=>$request->rad21,
            'rad22'=>$request->rad22,
            'rad23'=>$request->rad23,
            'rad24'=>$request->rad24,
            'rad25'=>$request->rad25,
            'rad26'=>$request->rad26,
            'rad27'=>$request->rad27,
            'rad28'=>$request->rad28,
            'rad29'=>$request->rad29,
            'rad30'=>$request->rad30,
            
            'do_you_have'=>$request->do_you_have,
            'members_do_you_have_serving_on_this_Body'=>$request->members_do_you_have_serving_on_this_Body,
            'times_did_they_meet_in_the_quarter'=>$request->times_did_they_meet_in_the_quarter,
            'decision'=>$request->decision,
            'average_Sunday_Church_Attendance_at_Children_Servicemale'=>round($average_Sunday_Church_Attendance_at_Children_Servicemale),
            'average_Sunday_Church_Attendance_at_Children_Servicfemale'=>round($average_Sunday_Church_Attendance_at_Children_Servicfemale),
            'average_Sunday_Church_Attendance_at_Aldult_Servicemale'=>round($average_Sunday_Church_Attendance_at_Aldult_Servicemale),
            'average_Sunday_Church_Attendance_at_Aldult_Servicefemale'=>round($average_Sunday_Church_Attendance_at_Aldult_Servicefemale),
            'average_Sunday_Church_Attendance_at_youth_Servicemale'=>round($average_Sunday_Church_Attendance_at_youth_Servicemale),
            'average_Sunday_Church_Attendance_at_youth_Servicefemale'=>round($average_Sunday_Church_Attendance_at_youth_Servicefemale),
            'Current_Adult_membership_male'=>round($Current_Adult_membership_male->c),
            'Current_Adult_membership_female'=>round($Current_Adult_membership_female->c),
            'Current_youth_membership_male'=>round($Current_youth_membership_male->c),
            'Current_youth_membership_female'=>round($Current_youth_membership_female->c),
            'Current_child_membership_male'=>round($Current_child_membership_male->c),
            'Current_child_membership_male'=>round($Current_child_membership_male->c),
            'Current_child_membership_female'=>round($Current_child_membership_female->c),
            'newmembers_were_added_to_your_assembly_male'=>round($newmembers_were_added_to_your_assembly_male->c),
            'newmembers_were_added_to_your_assembly_female'=>round($newmembers_were_added_to_your_assembly_female->c),
            'members_left_the_church_altogether_male'=>round($members_left_the_church_altogether_male->c),
            'members_left_the_church_altogether_female'=>round($members_left_the_church_altogether_female->c),
            'covenant_families_were_there_at_the_beginning_of_the_quarter'=>round($covenant_families_were_there_at_the_beginning_of_the_quarter->c),
            'new_covenant_families_were_established_in_your_church'=>round($new_covenant_families_were_established_in_your_church->c),
            'number_of_covenant_families_that_meet_weekly_in_your_assembly'=>round($number_of_covenant_families_that_meet_weekly_in_your_assembly->c),
            'covenant_families_that_are_not_active_hardly_meet'=>round($covenant_families_that_are_not_active_hardly_meet->c),
            'small_groups_does_the_assembly'=>round($small_groups_does_the_assembly->c),
            'small_groups_are_being_use_for_pastoral_care'=>round($small_groups_are_being_use_for_pastoral_care->c),
            'marriages_at_the_beginning_of_the_quarter'=>round($marriages_at_the_beginning_of_the_quarter->c),
            'new_marriages_were_blessed_in_the_church'=>round($new_marriages_were_blessed_in_the_church->c),
            'couples_joined_the_assembly_already_married'=>$couples_joined_the_assembly_already_married_number,
            'Married_Couples_families_in_the_Church'=>$Married_Couples_families_in_the_Church_number,
            'Number_of_Widows_widowers_in_the_church'=>round($Number_of_Widows_widowers_in_the_church->c),
            'divorce_cases_happened_in_the_church'=>round($divorce_cases_happened_in_the_church->c),
            'training_to_do_councelling'=>round($training_to_do_councelling->c),
            'births_were_recorded_in_the_Assembly'=>round($births_were_recorded_in_the_Assembly->c),
            'baby_naming_ceremonies_were_organized'=>round($baby_naming_ceremonies_were_organized->c),
            'babydedication'=>round($babydedication->c),
            'members_passed_on_to_glory_in_the_Assembly'=>round($members_passed_on_to_glory_in_the_Assembly->c),
            'burial_services_did_you_organize'=>round($burial_services_did_you_organize->c),
            'no_of_layleaders'=>$request->no_of_layleaders,
            'no_of_programs'=>$request->no_of_programs,
            'leaders_in_training'=>$request->leaders_in_training,
            'children_facilitators'=>$request->children_facilitators,
            'pastor_meet_children'=>$request->pastor_meet_children,
            'pastor_interact_children'=>$request->pastor_interact_children,
            'youth_children_facilatators'=>$request->youth_children_facilatators,
            'youth_pastor_meet_facilatators'=>$request->youth_pastor_meet_facilatators,
            'pastor_interact_youth'=>$request->pastor_interact_youth,
            'community_project'=>$request->community_project,
            'others'=>$request->others,
            'student_ece_male'=>round($student_ece_male->c),
            'student_pe_male'=>round($student_pe_male->c),
            'student_shs_male'=>round($student_shs_male->c),
            'student_jhs_male'=>round($student_jhs_male->c),
            'student_ece_female'=>round($student_ece_female->c),
            'student_pe_female'=>round($student_pe_female->c),
            'student_shs_female'=>round($student_shs_female->c),
            'student_jhs_female'=>round($student_jhs_female->c),
            
            'teacher_trained_male'=>round($teacher_trained_male->c),
            'teacher_trained_female'=>round($teacher_trained_female->c),
            'teacher_nontrained_male'=>round($teacher_nontrained_male->c),
            'teacher_nontrained_female'=>round($teacher_nontrained_female->c),
            
            'firstfruit_amount'=>round($firstfruit_amount->c),
            'allotheroffering_amount'=>round($allotheroffering_amount->c),
            'Total_Annual_Income'=>round($Total_Annual_Income->c),
            
            
            'MissionsChurchPlantingExpenses'=>round($MissionsChurchPlantingExpenses->c),
            'Administrative_Expenses'=>round($Administrative_Expenses->c),
            'MinistryExpenses'=>round($MinistryExpenses->c),
            'SocialActionProjects'=>round($SocialActionProjects->c),
            'CapitalExpenditure'=>round($CapitalExpenditure->c),
            'TotalExpenditure'=>round($TotalExpenditure->c),
            
            'firstfruit_percentage'=>round($firstfruit_percentage),
            'allotheroffering_amount_percentage'=>round($allotheroffering_amount_percentage),
            'Total_Annual_Income_percentage'=>round($Total_Annual_Income_percentage),
            
            'MissionsChurchPlantingExpenses_percentage'=>round($MissionsChurchPlantingExpenses_percentage),
            'Administrative_Expenses_percentage'=>round($Administrative_Expenses_percentage),
            'MinistryExpenses_percentage'=>round($MinistryExpenses_percentage),
            'SocialActionProjects_percentage'=>round($SocialActionProjects_percentage),
            'CapitalExpenditure_percentage'=>round($CapitalExpenditure_percentage),
            'TotalExpenditure_percentage'=>round($TotalExpenditure_percentage),
            
            
            'Total_Common_Fund_Payments'=>$request->Total_Common_Fund_Payments,
            'Central_Aid_Day_Contribution'=>$request->Central_Aid_Day_Contribution,
            
            
            'totalenrollment_beginners'=>$totalenrollment_beginners,
            'totalenrollment_Membership'=>$totalenrollment_Membership,
            'totalenrollment_Maturity'=>$totalenrollment_Maturity,
            'totalenrollment_Ministry'=>$totalenrollment_Ministry,
            
            
            'completed_beginners'=>$completed_beginners,
            'completed_Membership'=>$completed_membership,
            'completed_Maturity'=>$completed_Maturitry,
            'completed_Ministry'=>$completed_Ministry,
            
            'gra_beginners'=>$gra_beginners,
            'gra_Membership'=>$gra_membership,
            'gra_Maturity'=>$gra_Maturity,
            'gra_ministry'=>$gra_Ministry,
            
            
            
        
        ])->render();
        return PDF::load($html)->show();
    }
    public function weekly(Request $request){
        
        $from_date=$request->from_date;
        $to_date=$request->to_date;
//dd($from_date);
        /////////ATTENDANCE/////////////////
        $Adultsservice_attendancedetailsquery_male =  DB::table('attendances')->select([DB::raw('sum(male) as c')])->where('servicetype','adult service')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $Adultsservice_attendancedetailsquery_male_value=null;
        if($Adultsservice_attendancedetailsquery_male->c==''){$Adultsservice_attendancedetailsquery_male_value=0;}else{$Adultsservice_attendancedetailsquery_male_value=$Adultsservice_attendancedetailsquery_male->c;}
        
        $Adultsservice_attendancedetailsquery_female =  DB::table('attendances')->select([DB::raw('sum(female) as c')])->where('servicetype','adult service')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $Adultsservice_attendancedetailsquery_female_value=null;
        if($Adultsservice_attendancedetailsquery_female->c == ''){$Adultsservice_attendancedetailsquery_female_value=0;}else{$Adultsservice_attendancedetailsquery_female_value=$Adultsservice_attendancedetailsquery_female->c;}
        
        $YouthService_attendancedetailsquery_male =  DB::table('attendances')->select([DB::raw('sum(male) as c')])->where('servicetype','youth service')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $YouthService_attendancedetailsquery_male_value=null;
        if($YouthService_attendancedetailsquery_male->c == ''){$YouthService_attendancedetailsquery_male_value=0;}else{$YouthService_attendancedetailsquery_male_value=$YouthService_attendancedetailsquery_male->c;}
       
        $YouthService_attendancedetailsquery_female =  DB::table('attendances')->select([DB::raw('sum(female) as c')])->where('servicetype','youth service')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $YouthService_attendancedetailsquery_female_value=null;
       if($YouthService_attendancedetailsquery_female->c == ''){$YouthService_attendancedetailsquery_female_value=0;}else{$YouthService_attendancedetailsquery_female_value=$YouthService_attendancedetailsquery_female->c;}
       
        $ChildrenService_attendancedetailsquery_male =  DB::table('attendances')->select([DB::raw('sum(male) as c')])->where('servicetype','children service')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $ChildrenService_attendancedetailsquery_male_value=null;
        if($ChildrenService_attendancedetailsquery_male->c == ''){$ChildrenService_attendancedetailsquery_male_value=0;}else{$ChildrenService_attendancedetailsquery_male_value=$ChildrenService_attendancedetailsquery_male->c;}
       
        $ChildrenService_attendancedetailsquery_female =  DB::table('attendances')->select([DB::raw('sum(female) as c')])->where('servicetype','children service')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $ChildrenService_attendancedetailsquery_female_value=null;
        if($ChildrenService_attendancedetailsquery_female->c == ''){$ChildrenService_attendancedetailsquery_female_value=0;}else{$ChildrenService_attendancedetailsquery_female_value=$ChildrenService_attendancedetailsquery_female->c;}
        ////////////PASTORIAL CARE///////////////
        ////convenant family///////
        $baselinevalue=$request->baseline;
       
        $convenantfamilyestablished= DB::table('convenantfamilies')->select([DB::raw('count(*) as c')])->whereDate('establishmentdate','>=',$from_date)->whereDate('establishmentdate','<=',$to_date)->first();
        $convenantfamily_that_meet=$request->convenant_did_met;
        $convenantfamily_that_did_not_meet=$request->convenant_did_not_met;
        ////family enrichment///////
        $Marriages_blessed_by_this_assembly=DB::table('events')->select([DB::raw('count(*) as c')])->where('eventtype','marriage')->whereDate('recordered_date','>=',$from_date)->whereDate('recordered_date','<=',$to_date)->first();
        $couples_joined_the_assembly_already_married=DB::table('members')->select([DB::raw('count(*) as c')])->where('marital_status','Married')->whereNotNull('couplename')->groupBy('couplename')->first();
        $couples_joined_the_assembly_already_married_number=null;

        if($couples_joined_the_assembly_already_married == ""){
            $couples_joined_the_assembly_already_married_number=0;

        }else{
            $couples_joined_the_assembly_already_married_number=$couples_joined_the_assembly_already_married->c;
        }





        $widows_weekly=DB::table('members')->select([DB::raw('count(*) as c')])->where('marital_status','Widows')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        $divorce_weekly=DB::table('members')->select([DB::raw('count(*) as c')])->where('marital_status','Divorced')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
        ////Ministry to Families - Children’s Data//////
        $Births   =DB::table('events')->select([DB::raw('count(*) as c')])->where('eventtype','birth')->whereDate('recordered_date','>=',$from_date)->whereDate('recordered_date','<=',$to_date)->first();
        $Baby_Naming_ceremonies=DB::table('events')->select([DB::raw('count(*) as c')])->where('eventtype','namingceremony')->whereDate('recordered_date','>=',$from_date)->whereDate('recordered_date','<=',$to_date)->first();
        $Baby_dedications=DB::table('events')->select([DB::raw('count(*) as c')])->where('eventtype','babydedication')->whereDate('recordered_date','>=',$from_date)->whereDate('recordered_date','<=',$to_date)->first();
         ///Ministry to Bereaved Families////
        $Deaths=DB::table('members')->select([DB::raw('count(*) as c')])->where('passed_to_glory','yes')->whereDate('updated_at','>=',$from_date)->whereDate('updated_at','<=',$to_date)->first();
        $Burial_services=DB::table('events')->select([DB::raw('count(*) as c')])->where('eventtype','burial')->whereDate('recordered_date','>=',$from_date)->whereDate('recordered_date','<=',$to_date)->first();

        ////Abc Attendance////////////New Beginners
         $abc_male_membership= DB::table('attendances')->select([DB::raw('sum(male) as c')])->where('servicetype','abc classes')->where('stage_classes','Membership')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
         $abc_female_membership= DB::table('attendances')->select([DB::raw('sum(female) as c')])->where('servicetype','abc classes')->where('stage_classes','Membership')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();

        $abc_male__New_Beginners= DB::table('attendances')->select([DB::raw('sum(male) as c')])->where('servicetype','abc classes')->where('stage_classes','ABC')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
         $abc_female_New_Beginners= DB::table('attendances')->select([DB::raw('sum(female) as c')])->where('servicetype','abc classes')->where('stage_classes','ABC')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();

         $abc_male_Maturity= DB::table('attendances')->select([DB::raw('sum(male) as c')])->where('servicetype','abc classes')->where('stage_classes','Maturity')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
         $abc_female_Maturity= DB::table('attendances')->select([DB::raw('sum(female) as c')])->where('servicetype','abc classes')->where('stage_classes','Maturity')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();

        $abc_male_Ministry= DB::table('attendances')->select([DB::raw('sum(male) as c')])->where('servicetype','abc classes')->where('stage_classes','Ministry')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();
         $abc_female_Ministry= DB::table('attendances')->select([DB::raw('sum(female) as c')])->where('servicetype','abc classes')->where('stage_classes','Ministry')->whereDate('registration_date','>=',$from_date)->whereDate('registration_date','<=',$to_date)->first();

        ////////FINANCIAL DATE/OBLIGATIONS/////////////////
        $firstfruit_amount=  DB::table('offerings')->select([DB::raw('sum(amount) as c')])->where('offeringtype','titheoffering')->whereDate('date_submitted','>=',$from_date)->whereDate('date_submitted','<=',$to_date)->first();
        $mainoffering_amount=  DB::table('offerings')->select([DB::raw('sum(amount) as c')])->where('offeringtype','mainoffering')->whereDate('date_submitted','>=',$from_date)->whereDate('date_submitted','<=',$to_date)->first();
        $projectoffering_amount=  DB::table('offerings')->select([DB::raw('sum(amount) as c')])->where('offeringtype','projectoffering')->whereDate('date_submitted','>=',$from_date)->whereDate('date_submitted','<=',$to_date)->first();
        $tuesdayoffering_amount=  DB::table('offerings')->select([DB::raw('sum(amount) as c')])->where('servicetype','tuesday service')->whereDate('date_submitted','>=',$from_date)->whereDate('date_submitted','<=',$to_date)->first();
        $totalincome= $firstfruit_amount->c + $mainoffering_amount->c +$projectoffering_amount->c + $tuesdayoffering_amount->c ;
        ////////////////CHURCH DETAILS//////////////
            $churchdetails =  DB::table('profiles')->first();



            $html=view('pdfs.weeklypdf')->with([
                'churchdetails'=>$churchdetails,
                'from_date'=>$from_date,
                'to_date'=>$to_date,
                
                
                'Adultsservice_attendancedetailsquery_male_value' =>$Adultsservice_attendancedetailsquery_male_value,
                'Adultsservice_attendancedetailsquery_female_value' =>$Adultsservice_attendancedetailsquery_female_value,
                'YouthService_attendancedetailsquery_male_value' =>$YouthService_attendancedetailsquery_male_value,
                'YouthService_attendancedetailsquery_female_value' =>$YouthService_attendancedetailsquery_female_value,
                'ChildrenService_attendancedetailsquery_male_value' =>$ChildrenService_attendancedetailsquery_male_value,
                'ChildrenService_attendancedetailsquery_female_value' =>$ChildrenService_attendancedetailsquery_female_value,

                'baselinevalue' =>$baselinevalue,
                'convenantfamilyestablished' =>$convenantfamilyestablished->c,
                'convenantfamily_that_meet' =>$convenantfamily_that_meet,
                'convenantfamily_that_did_not_meet' =>$convenantfamily_that_did_not_meet,
                
                'Marriages_blessed_by_this_assembly' =>$Marriages_blessed_by_this_assembly->c,
                'couples_joined_the_assembly_already_married_number' =>$couples_joined_the_assembly_already_married_number,
                'widows_weekly' =>$widows_weekly->c,
                'divorce_weekly' =>$divorce_weekly->c,
                'Births' =>$Births->c,
                'Baby_Naming_ceremonies' =>$Baby_Naming_ceremonies->c,
                'Baby_dedications' =>$Baby_dedications->c,

                'Deaths' =>$Deaths->c,
                'Burial_services' =>$Burial_services->c,
                
                'abc_male_membership' =>$abc_male_membership->c,
                'abc_female_membership' =>$abc_female_membership->c,
                
                'abc_male__New_Beginners' =>$abc_male__New_Beginners->c,
                'abc_female_New_Beginners' =>$abc_female_New_Beginners->c,
                'abc_male_Maturity' =>$abc_male_Maturity->c,
                'abc_female_Maturity' =>$abc_female_Maturity->c,
                'abc_male_Ministry' =>$abc_male_Ministry->c,
                'abc_female_Ministry' =>$abc_female_Ministry->c,
                
                'firstfruit_amount' => $firstfruit_amount->c == 0 ? 0 :  $firstfruit_amount->c,
                'mainoffering_amount' => $mainoffering_amount->c == 0 ? 0 :  $mainoffering_amount->c,
                'projectoffering_amount' => $projectoffering_amount->c == 0 ? 0 :  $projectoffering_amount->c,
                'tuesdayoffering_amount' => $tuesdayoffering_amount->c == 0 ? 0 :  $tuesdayoffering_amount->c,
                
                'Provident_fund_for_employees' =>$request->Provident_fund_for_employees,
                'Social_Security_for_employed_staff' =>$request->Social_Security_for_employed_staff,
                'PAYE_for_employed_staff' =>$request->PAYE_for_employed_staff,
                
                'child_dpt_nature' =>$request->child_dpt_nature,
                'child_dpt_atten' =>$request->child_dpt_atten,
                
                'youth_dpt_nature' =>$request->youth_dpt_nature,
                'youth_dpt_atten' =>$request->youth_dpt_atten,
                
                'convenant_fami_nature' =>$request->convenant_fami_nature,
                'convenant_fami_atten' =>$request->convenant_fami_atten,
                
                'Counselors_nature' =>$request->Counselors_nature,
                'Counselors_atten' =>$request->Counselors_atten,
                
                'Church_Council_nature' =>$request->Church_Council_nature,
                'Church_Council_atten' =>$request->Church_Council_atten,
                
                'ministry_department1' =>$request->ministry_department1,
                'Purpose_of_Meeting1' =>$request->Purpose_of_Meeting1,
                
                'ministry_department2' =>$request->ministry_department2,
                'Purpose_of_Meeting2' =>$request->Purpose_of_Meeting2,

                
                

                


            ])->render();
            return PDF::load($html)->show();
        }
    public function finance(Request $request){
        ////////////////CHURCH DETAILS//////////////
        $service_type=$request->service_type;
        $member_sec_title=$request->member_sec_title;
        $churchdetails =  DB::table('profiles')->first();

             if($request->offering_type=='all'){
                 $offeringsdetails =  DB::table('offerings')
                     ->whereDate('date_submitted','>=',$request->date_from)
                     ->whereDate('date_submitted','<=',$request->date_to)
                     ->where('servicetype',$service_type)
                     ->orderBy('date_submitted','asc')->get();

             }
             elseif ($request->offering_type == 'daybornoffering'){
                 $offeringsdetails =  DB::table('dayborns')
                     ->whereDate('date','>=',$request->date_from)
                     ->whereDate('date','<=',$request->date_to)

                     ->orderBy('date','asc')->get();

                     $html=view('pdfs.dayborns')->with([
                         'churchdetails'=>$churchdetails,

                         'offeringsdetails'=>$offeringsdetails,
                         'date_from'=>$request->date_from,
                         'date_to'=>$request->date_to,
                         'member_sec_title'=>$member_sec_title,

                     ])->render();
                     return PDF::load($html)->show();
             }
             else{
                 $offeringsdetails =  DB::table('offerings')
                     ->where('offeringtype',$request->offering_type)
                     ->whereDate('date_submitted','>=',$request->date_from)
                     ->whereDate('date_submitted','<=',$request->date_to)
                     ->where('servicetype',$service_type)
                     ->orderBy('date_submitted','asc')->get();

             }

        $html=view('pdfs.financepdf')->with([
            'churchdetails'=>$churchdetails,

            'offeringsdetails'=>$offeringsdetails,
            'date_from'=>$request->date_from,
            'date_to'=>$request->date_to,
            'member_sec_title'=>$member_sec_title,

        ])->render();
        return PDF::load($html)->show();
    }
    public function attendance(Request $request){
        ////////////////CHURCH DETAILS//////////////
        $churchdetails =  DB::table('profiles')->first();
        $member_sec_title=$request->member_sec_title;
        if($request->service_type=='all'){
            $attendancedetails =  DB::table('attendances')
                ->whereDate('registration_date','>=',$request->date_from)
                ->whereDate('registration_date','<=',$request->date_to) 
                ->orderBy('registration_date','asc')->get();

        }else{

            $attendancedetails =  DB::table('attendances')->where('servicetype',$request->service_type)
                ->whereDate('registration_date','>=',$request->date_from)
                ->whereDate('registration_date','<=',$request->date_to) 
                ->orderBy('registration_date','asc')
                ->get();

        }
       
        $html=view('pdfs.attendancepdf')->with([
            'churchdetails'=>$churchdetails,
            'attendancedetails'=>$attendancedetails,
             'date_from'=>$request->date_from,
            'date_to'=>$request->date_to,
            'member_sec_title'=>$member_sec_title,

        ])->render();
        return PDF::load($html)->show();
    }
    public function classes(Request $request ){
        ////////////////CHURCH DETAILS//////////////
        $churchdetails =  DB::table('profiles')->first();
        $member_sec_title=$request->member_sec_title;

        if($request->level_type=='all'){
            $classesdetails =  DB::table('classes')
                ->where('status',$request->completion_status)
                ->where('graduation_status',$request->graduation_status)
                ->whereDate('registrationdate','>=',$request->date_from)
                ->whereDate('registrationdate','<=',$request->date_to)
                ->orderBy('registrationdate','asc')
                ->get();

        }else{

            $classesdetails =  DB::table('classes')
                ->where('stage',$request->level_type)
                ->where('status',$request->completion_status)
                ->where('graduation_status',$request->graduation_status)
                ->whereDate('registrationdate','>=',$request->date_from)
                ->whereDate('registrationdate','<=',$request->date_to)
                ->orderBy('registrationdate','asc')
                ->get();

        }
        $html=view('pdfs.classespdf')->with([
            'churchdetails'=>$churchdetails,
            'classesdetails'=>$classesdetails,
            'date_from'=>$request->date_from,
            'date_to'=>$request->date_to,
            'member_sec_title'=>$member_sec_title,

        ])->render();
        return PDF::load($html)->show();
    }
    public function individual($memberid){
        ////////////////CHURCH DETAILS//////////////
        $churchdetails =  DB::table('profiles')->first();

        $members=DB::table('members')->where('member_id',$memberid)->first();
 //dd($members);
        $html=view('pdfs.individualpdf')->with([
            'churchdetails'=>$churchdetails,
            'members'=>$members

        ])->render();
        return PDF::load($html)->show();
    }

    public function special(Request $request){
        $churchdetails =  DB::table('profiles')->first();
        $title=null;
        if($request->specialreporttype=='1'){
            $title="Employed and Unemployed members";
            $details =  DB::table('members')
                ->where('membershipstatus','member')
                ->whereDate('registration_date','>=',$request->date_from)
                ->whereDate('registration_date','<=',$request->date_to)
                ->orderBy('registration_date','asc')
                ->get();
        }
        $html=view('pdfs.specialpdf')->with([
            'churchdetails'=>$churchdetails,

            'date_from'=>$request->date_from,
            'date_to'=>$request->date_to,
            'title'=>$title,
            'details'=>$details,
            'type'=>$request->specialreporttype,

        ])->render();
        return PDF::load($html, 'A4', 'landscape')->show();

    }

     public function no_of_sundays($fromdate,$todate){
        $start = new DateTime($fromdate);
        $end = new DateTime($todate);
        $days = $start->diff($end, true)->days;

        $sundays = intval($days / 7) + ($start->format('N') + $days % 7 >= 7);

        return $sundays;
    }


    public function previousquarterfrom(){
        $start_date = strtotime('3 months ago');
        $start_quarter = ceil(date('m', $start_date) / 3);
        $start_month = ($start_quarter * 3) - 2;
        $start_year = date('Y', $start_date);
        $start_timestamp = mktime(0, 0, 0, $start_month, 1, $start_year);

       return $previous_lastquater_from_date=date('d-m-Y', $start_timestamp);

    }
    public function previousquarterto(){
        $start_date = strtotime('3 months ago');
        $start_quarter = ceil(date('m', $start_date) / 3);
        $start_month = ($start_quarter * 3) - 2;
        $start_year = date('Y', $start_date);
        $start_timestamp = mktime(0, 0, 0, $start_month, 1, $start_year);

        $previous_lastquater_from_date=date('d-m-Y', $start_timestamp);
        $previous_lastquater_to_date = date('d-m-Y', strtotime("+2 months", strtotime($previous_lastquater_from_date)));
        return $previous_lastquater_from_date ."---".$previous_lastquater_to_date;
    }
}
