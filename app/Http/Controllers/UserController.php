<?php

namespace App\Http\Controllers;

use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Http\Request;
use Sentinel;
use Image;
use DB;
use Datatables;
class UserController extends Controller
{
    //
    public function get_all_users()
    {
        $userdetails = DB::table('users')
            ->join('role_users', 'users.id', '=', 'role_users.user_id')
            ->join('roles', 'roles.id', '=', 'role_users.role_id')
            ->select('users.id','users.first_name','users.last_name','users.location','users.email','roles.name as priviledge','users.last_login','users.username')
            ->get();

        return Datatables::of($userdetails)
            ->addColumn('action', function ($userdetails) {
                $details='';
                $details='<a href="" class="edit-modal btn btn-sm btn-warning" data-toggle="modal" data-target="#edit-user" data-id="'.$userdetails->id.'"  data-firstname="'.$userdetails->first_name.'" data-lastname="'.$userdetails->last_name.'" data-location="'.$userdetails->location.'" data-priviledge="'.$userdetails->priviledge.'" data-username="'.$userdetails->username.'" data-email="'.$userdetails->email.'" >Edit</a>&nbsp
                    <a href="" data-toggle="modal" data-target="#delete-user"  data-id="'.$userdetails->id.'"data-firstname="'.$userdetails->first_name.'" data-lastname="'.$userdetails->last_name.'" class="deletebtn btn btn-danger" > Delete user</a>&nbsp
                   ';

                return $details;
            })
            ->make(true);
    }
    public  function index(){
        return view('admin/user');
    }
    public function adduser(Request $request){
       // dd($request->all());
        $credentials = [
            'email'    => $request->email,
            'password' => $request->password,
            'first_name' => $request->firstname,
            'last_name' => $request->lastname,
            'username' => $request->username,
            'location' => $request->location,
        ];

        $user = Sentinel::registerAndActivate($credentials);
        $role=Sentinel::findRoleBySlug($request->role);
        $role->users()->attach($user);
        $notification=array(
            'message'=>"$request->firstname has been Succesfully added",
            'alert-type'=>'success'
        );
        return redirect('user')->with($notification);
    }
    public function edituser(Request $request){
       // dd($request->id_edit);
        $previousrole = Sentinel::findById($request->id_edit)->roles()->first();//get previous role of user

        $user = Sentinel::findById($request->id_edit);               ////////////////////
        $role = Sentinel::findRoleByName($previousrole->name);       /////Remove user role////////////////
        $role->users()->detach($user);                               ////////////////


        $user1 = Sentinel::findById($request->id_edit);
        Sentinel::update($user1, array(
            'first_name' => $request->firstname_edit,
            'last_name' => $request->lastname_edit,
            'password' => $request->password_edit,
            'username' => $request->username_edit,
            'location' => $request->location_edit,
            'email' => $request->email_edit,
        ));

        $role_edit=Sentinel::findRoleBySlug($request->role_edit);
        $role_edit->users()->attach($user);

        $notification=array(
                'message'=>"User details has been Succesfully updated",
                'alert-type'=>'success'
            );
            return redirect('user')->with($notification);



    }

    public function deleteuser(Request $request){
        
        $user = Sentinel::findById($request->id_delete);

        if($user->delete()){
            $notification=array(
                'message'=>"user has been deleted",
                'alert-type'=>'success'
            );
            return redirect('user')->with($notification);
        }

    }
}
