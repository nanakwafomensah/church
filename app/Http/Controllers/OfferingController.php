<?php

namespace App\Http\Controllers;

use App\Dayborn;
use Illuminate\Http\Request;
use App\Offering;
use Image;
use DB;
use Datatables;
use Excel;
class OfferingController extends Controller
{
    //
    public function index()
    {
         $servicetype=DB::table('services')->select(['id','service_name'])->get();
        $allmembers=DB::table('members')->select(['id','member_id','firstname','lastname'])->get();
        $current_year=date('Y');
        $current_month=date('Y-m');
        $current_start_week_date=$this->current_start_week_date(date('Y-m-d'));
        $current_end_week_date=$this->current_end_week_date(date('Y-m-d'));
//dd($current_month);
        ///main///////
        $main_year=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->whereYear('date_submitted', $current_year)->first();
        $main_month=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->whereDate('date_submitted','LIKE', $current_month.'%')->first();
        $main_week=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->whereDate('date_submitted','>=',$current_start_week_date)->whereDate('date_submitted','<=',$current_end_week_date)->first();
            
        ///Thanksgiving///////
        $thank_year=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','thanksgivingoffering')->whereYear('date_submitted', $current_year)->first();
        $thank_month=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','thanksgivingoffering')->whereDate('date_submitted','LIKE', $current_month.'%')->first();
        $thank_week=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','thanksgivingoffering')->whereDate('date_submitted','>=',$current_start_week_date)->whereDate('date_submitted','<=',$current_end_week_date)->first();

        ///Project///////
        $project_year=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','projectoffering')->whereYear('date_submitted', $current_year)->first();
        $project_month=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','projectoffering')->whereDate('date_submitted','LIKE', $current_month.'%')->first();
        $project_week=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','projectoffering')->whereDate('date_submitted','>=',$current_start_week_date)->whereDate('date_submitted','<=',$current_end_week_date)->first();

        ///Tithe///////
        $tithe_year=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->whereYear('date_submitted', $current_year)->first();
        $tithe_month=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->whereDate('date_submitted','LIKE', $current_month.'%')->first();
        $tithe_week=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->whereDate('date_submitted','>=',$current_start_week_date)->whereDate('date_submitted','<=',$current_end_week_date)->first();
        ///Pledge///////
        $pledge_year=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','pledgeoffering')->whereYear('date_submitted', $current_year)->first();
        $pledge_month=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','pledgeoffering')->whereDate('date_submitted','LIKE', $current_month.'%')->first();
        $pledge_week=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','pledgeoffering')->whereDate('date_submitted','>=',$current_start_week_date)->whereDate('date_submitted','<=',$current_end_week_date)->first();
        ///Welfare///////
        $welfare_year=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','welfareoffering')->whereYear('date_submitted', $current_year)->first();
        $welfare_month=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','welfareoffering')->whereDate('date_submitted','LIKE', $current_month.'%')->first();
        $welfare_week=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','welfareoffering')->whereDate('date_submitted','>=',$current_start_week_date)->whereDate('date_submitted','<=',$current_end_week_date)->first();
        ///Day Born///////
        $dayborn_year=DB::table('dayborns')->select([DB::raw('sum(amount) as total')])->whereYear('date', $current_year)->first();
        $dayborn_month=DB::table('dayborns')->select([DB::raw('sum(amount) as total')])->whereDate('date','LIKE', $current_month.'%')->first();
        $dayborn_week=DB::table('dayborns')->select([DB::raw('sum(amount) as total')])->whereDate('date','>=',$current_start_week_date)->whereDate('date','<=',$current_end_week_date)->first();
//dd($dayborn_year);
        ///Other///////
        $other_year=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','othersoffering')->whereYear('date_submitted', $current_year)->first();
        $other_month=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','othersoffering')->whereDate('date_submitted','LIKE', $current_month.'%')->first();
        $other_week=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','othersoffering')->whereDate('date_submitted','>=',$current_start_week_date)->whereDate('date_submitted','<=',$current_end_week_date)->first();

        return view('admin.offering')->with([
            'servicetype'=>$servicetype,
            'allmembers'=>$allmembers,

            'main_year'=>$main_year,
            'main_month'=>$main_month,
            'main_week'=>$main_week,

            'thank_year'=>$thank_year,
            'thank_month'=>$thank_month,
            'thank_week'=>$thank_week,

            'project_year'=>$project_year,
            'project_month'=>$project_month,
            'project_week'=>$project_week,

             'tithe_year'=>$tithe_year,
            'tithe_month'=>$tithe_month,
            'tithe_week'=>$tithe_week,

            'pledge_year'=>$pledge_year,
            'pledge_month'=>$pledge_month,
            'pledge_week'=>$pledge_week,

            'welfare_year'=>$welfare_year,
            'welfare_month'=>$welfare_month,
            'welfare_week'=>$welfare_week,

            'other_year'=>$other_year,
            'other_month'=>$other_month,
            'other_week'=>$other_week,

            'dayborn_year'=>$dayborn_year,
            'dayborn_month'=>$dayborn_month,
            'dayborn_week'=>$dayborn_week,

        ]);
    }
    
    
    
    
    
    

    
    public function savedaybornoffering(Request $request){
        $m = new Dayborn();
        $m->date=$request->date_received;
        $m->dayborn=$request->dayborn;
        $m->amount=$request->amount_received;
        if($m->save()){
            $notification=array(
                'message'=>"Day born Offering  has been Succesfully Added ",
                'alert-type'=>'success'
            );

            return redirect('offering')->with($notification);
        }
     
        
//      
    }


    public function get_alloffering(){
        $members = DB::table('offerings')->select(['number','offeringtype', 'servicetype', 'memberid', 'amount','date_submitted','description'])->orderBy('date_submitted','asc');

        return Datatables::of($members)
            ->addColumn('action', function ($members) {

                return'<a href="" data-toggle="modal" data-target="#edit-Modal"  data-number="'.$members->number.'"  data-offeringtype="'.$members->offeringtype.'" data-servicetype="'.$members->servicetype.'" data-amount="'.$members->amount.'" data-date_submitted="'.$members->date_submitted.'" data-description="'.$members->description.'" data-name="'.$members->memberid.'" class="editbtn btn btn-warning" > <i class="fa fa-pencil-square-o" aria-hidden="true">&nbsp;</i></a>
                                          <a href="" data-toggle="modal" data-target="#delete-Modal"   data-number="'.$members->number.'"  data-offeringtype="'.$members->offeringtype.'" data-servicetype="'.$members->servicetype.'" data-amount="'.$members->amount.'" data-date_submitted="'.$members->date_submitted.'" data-description="'.$members->description.'"   class="deletebtn btn btn-danger" ><i class="fa fa-trash" aria-hidden="true">&nbsp;</i></a>
                                             ';
            })


            ->make(true);

    }

    public function get_alldayborn(){
        $members = DB::table('dayborns')->select(['id','date','dayborn', 'amount', 'created_at', 'updated_at'])->orderBy('date','asc');

        return Datatables::of($members)
            ->addColumn('action', function ($members) {

                return'<a href="" data-toggle="modal" data-target="#edit-Modal"  data-id="'.$members->id.'"  data-date="'.$members->date.'" data-dayborn="'.$members->dayborn.'" data-amount="'.$members->amount.'" data-created_at="'.$members->created_at.'" data-updated_at="'.$members->updated_at.'" class="editbtn btn btn-warning" > <i class="fa fa-pencil-square-o" aria-hidden="true">&nbsp;</i></a>
                                          <a href="" data-toggle="modal" data-target="#delete-Modal"  data-id="'.$members->id.'"  data-date="'.$members->date.'" data-dayborn="'.$members->dayborn.'" data-amount="'.$members->amount.'" data-created_at="'.$members->created_at.'" data-updated_at="'.$members->updated_at.'"   class="deletebtn btn btn-danger" ><i class="fa fa-trash" aria-hidden="true">&nbsp;</i></a>
                                             ';
            })


            ->make(true);

    }

    public function indexall()
    {
         $servicetype=DB::table('services')->select(['id','service_name'])->get();
         $allmembers=DB::table('members')->select(['id','member_id','firstname','lastname'])->get();
         return view('admin/alloffering')->with(['servicetype'=>$servicetype,'allmembers'=>$allmembers]);
    }
    public function daybornall(){
        $servicetype=DB::table('services')->select(['id','service_name'])->get();
        $allmembers=DB::table('members')->select(['id','member_id','firstname','lastname'])->get();
        return view('admin/alldaybornoffering')->with(['servicetype'=>$servicetype,'allmembers'=>$allmembers]);

    }
    public function save(Request $request){
       
        try{
            $churchdetails =  DB::table('profiles')->first();
            $mem_mobile_number=null;
            $name=trim($request->member_id);
            if($request->offeringtype!='mainoffering'){
                $memberdetails =  DB::table('members')->where('fullname',$name)->first();
                if(isset($memberdetails->telephone)){
                    $mem_mobile_number=$memberdetails->telephone;
                }

            }
          
            $m = new Offering();

            $m->date_submitted=$request->date_submitted;
            $m->number=$request->number;
            $m->offeringtype=$request->offeringtype;
            $m->servicetype=$request->servicetype;
            $m->memberid=$request->member_id;
            $m->amount=$request->amount;
            $m->description=$request->description;

             if($request->offeringtype=='thanksgivingoffering'){
                 $parentname=$churchdetails->parentchurchname;
                 $city=$churchdetails->city;
                 $name=$churchdetails->churchname;
                 $sender=$churchdetails->smssenderid;
                 $msisdn=$mem_mobile_number;
                 $message="Dear $request->member_id  , $parentname $name ,$city have received a ThanksGiving Offering of GHC $request->amount From You at the $request->servicetype.\n offeringID:$request->number.Thank You.";
                 $msg = urlencode($message);
                 $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
                 @file_get_contents($api);
             }
            if($request->offeringtype=='projectoffering'){
                $parentname=$churchdetails->parentchurchname;
                $city=$churchdetails->city;
                $name=$churchdetails->churchname;
                $sender=$churchdetails->smssenderid;
                $msisdn=$mem_mobile_number;
                $message="Dear $request->member_id  , $parentname $name ,$city have received a Project Offering of GHC $request->amount From You at the $request->servicetype.\n offeringID:$request->number.Thank You.";
                $msg = urlencode($message);
                $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
                if(isset($msisdn)){
                    @file_get_contents($api);
                }

            } if($request->offeringtype=='titheoffering'){
                $parentname=$churchdetails->parentchurchname;
                $city=$churchdetails->city;
                $name=$churchdetails->churchname;
                $sender=$churchdetails->smssenderid;
                $msisdn=$mem_mobile_number;
                $message="Dear $request->member_id  , $parentname $name ,$city have received a Tithe Offering of GHC $request->amount From You at the $request->servicetype.\n offeringID:$request->number.Thank You.";
                $msg = urlencode($message);
                $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
                if(isset($msisdn)){
                    @file_get_contents($api);
                }

            }
            if($request->offeringtype=='welfareoffering'){
                $parentname=$churchdetails->parentchurchname;
                $city=$churchdetails->city;
                $name=$churchdetails->churchname;
                $sender=$churchdetails->smssenderid;
                $msisdn=$mem_mobile_number;
                $message="Dear $request->member_id  , $parentname $name ,$city have received a Welfare Offering of GHC $request->amount From You at the $request->servicetype.\n offeringID:$request->number.Thank You.";
                $msg = urlencode($message);
                $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
                if(isset($msisdn)){
                    @file_get_contents($api);
                }

            }
            if($request->offeringtype=='othersoffering'){
                $parentname=$churchdetails->parentchurchname;
                $city=$churchdetails->city;
                $name=$churchdetails->churchname;
                $sender=$churchdetails->smssenderid;
                $msisdn=$mem_mobile_number;
                $message="Dear Pastor,The Church have received  an Offering from other sources of GHC $request->amount at the $request->servicetype.Thank You.";
                $msg = urlencode($message);
                $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
                if(isset($msisdn)){
                    @file_get_contents($api);
                }

            }
            if($request->offeringtype=='pledgeoffering'){
                $parentname=$churchdetails->parentchurchname;
                $city=$churchdetails->city;
                $name=$churchdetails->churchname;
                $sender=$churchdetails->smssenderid;
                $msisdn=$mem_mobile_number;
                $message="Dear Pastor,The Church have received  a Plegde Offering of GHC $request->amount at the $request->servicetype.Thank You.";
                $msg = urlencode($message);
                $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
                if(isset($msisdn)){
                    @file_get_contents($api);
                }

            }
            if ($request->offeringtype=='mainoffering'){
                 $parentname=$churchdetails->parentchurchname;
                 $city=$churchdetails->city;
                 $name=$churchdetails->churchname;
                 $sender=$churchdetails->smssenderid;
                 $msisdn=preg_replace('/^0/','233',$churchdetails->phone);
//                 dd($msisdn);
                 $message="Dear Pastor,The Church have received  a Main Offering of GHC $request->amount at the $request->servicetype.Thank You.";
                 $msg = urlencode($message);
                 $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
                 @file_get_contents($api);
             }

            if($m->save()){
                $notification=array(
                    'message'=>"Offering  has been Succesfully Added ",
                    'alert-type'=>'success'
                );

                return redirect('alloffering')->with($notification);
            }
        }catch(Exception $e){
            $notification=array(
                'message'=>'A Error Occured',
                'alert-type'=>'error'
            );
            return redirect('offering')->with($notification);
        }
    }


    public function update(Request $request){
          //dd($request->offering_number_edit_id);::where('postal', $postal)->firstOrFail();
        try{
            //$m = Offering::findorfail($request->offering_number_edit_id);
            $m = Offering::where('number', $request->offering_number_edit_id)->firstOrFail();
            $m->servicetype=$request->servicetype_edit;
            $m->memberid=$request->member_edit;
            $m->amount=$request->amount_edit;
            $m->date_submitted=$request->date_submitted_edit;
            $m->description=$request->description_edit;



            if($m->update()){

                $notification=array(
                    'message'=>"Offering Type details has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('alloffering')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"Offering  details  failed to be updated",
                'alert-type'=>'success'
            );
            return redirect('alloffering')->with($notification);
        }
    }
    public function delete(request $request){
        try{
            $m = Offering::where('number', $request->offering_number_delete)->firstOrFail();
            if($m->delete()){
                $notification=array(
                    'message'=>"Offering  has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('alloffering')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error occured",
                'alert-type'=>'error'
            );
            return redirect('alloffering')->with($notification);
        }
    }

    public function updatedayborn(Request $request){

        try{
            //$m = Offering::findorfail($request->offering_number_edit_id);
            $m = Dayborn::where('id', $request->daybornid)->firstOrFail();
            $m->date=$request->date_submitted_edit;
            $m->dayborn=$request->dayborn_edit;
            $m->amount=$request->amount_edit;

            if($m->update()){

                $notification=array(
                    'message'=>"Offering Type details has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('alldaybornoffering')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"Offering  details  failed to be updated",
                'alert-type'=>'success'
            );
            return redirect('alldaybornoffering')->with($notification);
        }
    }
    public function deletedayborn(request $request){
        
        try{
            $m = Dayborn::where('id', $request->offering_number_delete)->firstOrFail();
            if($m->delete()){
                $notification=array(
                    'message'=>"Offering  has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('alldaybornoffering')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error occured",
                'alert-type'=>'error'
            );
            return redirect('alloffering')->with($notification);
        }
    }
    public function current_start_week_date($datestr){
        date_default_timezone_set(date_default_timezone_get());
        $dt = strtotime($datestr);
        return $res['start'] = date('N', $dt)==1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt));

    }
    public function current_end_week_date($datestr){
        date_default_timezone_set(date_default_timezone_get());
        $dt = strtotime($datestr);
        return $res['end'] = date('N', $dt)==7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next sunday', $dt));
     }
    public function offeringchart(Request $request) {
        $current_year=date('Y');
        ///////january
        $current_month1=$current_year.'-01';
        $main_jan=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->whereDate('date_submitted','LIKE', $current_month1.'%')->first();
        $thank_jan=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','thanksgivingoffering')->whereDate('date_submitted','LIKE', $current_month1.'%')->first();
        $project_jan=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','projectoffering')->whereDate('date_submitted','LIKE', $current_month1.'%')->first();
        $tithe_jan=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->whereDate('date_submitted','LIKE', $current_month1.'%')->first();
        $pledge_jan=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','pledgeoffering')->whereDate('date_submitted','LIKE', $current_month1.'%')->first();
        $welfare_jan=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','welfareoffering')->whereDate('date_submitted','LIKE', $current_month1.'%')->first();
        $other_jan=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','othersoffering')->whereDate('date_submitted','LIKE', $current_month1.'%')->first();

        ///////february
        $current_month2=$current_year.'-02';
        $main_feb=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->whereDate('date_submitted','LIKE', $current_month2.'%')->first();
        $thank_feb=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','thanksgivingoffering')->whereDate('date_submitted','LIKE', $current_month2.'%')->first();
        $project_feb=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','projectoffering')->whereDate('date_submitted','LIKE', $current_month2.'%')->first();
        $tithe_feb=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->whereDate('date_submitted','LIKE', $current_month2.'%')->first();
        $pledge_feb=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','pledgeoffering')->whereDate('date_submitted','LIKE', $current_month2.'%')->first();
        $welfare_feb=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','welfareoffering')->whereDate('date_submitted','LIKE', $current_month2.'%')->first();
        $other_feb=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','othersoffering')->whereDate('date_submitted','LIKE', $current_month2.'%')->first();

        ///////mar
        $current_month3=$current_year.'-03';
        $main_mar=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->whereDate('date_submitted','LIKE', $current_month3.'%')->first();
        $thank_mar=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','thanksgivingoffering')->whereDate('date_submitted','LIKE', $current_month3.'%')->first();
        $project_mar=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','projectoffering')->whereDate('date_submitted','LIKE', $current_month3.'%')->first();
        $tithe_mar=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->whereDate('date_submitted','LIKE', $current_month3.'%')->first();
        $pledge_mar=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','pledgeoffering')->whereDate('date_submitted','LIKE', $current_month3.'%')->first();
        $welfare_mar=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','welfareoffering')->whereDate('date_submitted','LIKE', $current_month3.'%')->first();
        $other_mar=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','othersoffering')->whereDate('date_submitted','LIKE', $current_month3.'%')->first();


        ///////apr
        $current_month4=$current_year.'-04';
        $main_apr=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->whereDate('date_submitted','LIKE', $current_month4.'%')->first();
        $thank_apr=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','thanksgivingoffering')->whereDate('date_submitted','LIKE', $current_month4.'%')->first();
        $project_apr=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','projectoffering')->whereDate('date_submitted','LIKE', $current_month4.'%')->first();
        $tithe_apr=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->whereDate('date_submitted','LIKE', $current_month4.'%')->first();
        $pledge_apr=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','pledgeoffering')->whereDate('date_submitted','LIKE', $current_month4.'%')->first();
        $welfare_apr=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','welfareoffering')->whereDate('date_submitted','LIKE', $current_month4.'%')->first();
        $other_apr=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','othersoffering')->whereDate('date_submitted','LIKE', $current_month4.'%')->first();


        //////may
        $current_month5=$current_year.'-05';
        $main_may=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->whereDate('date_submitted','LIKE', $current_month5.'%')->first();
        $thank_may=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','thanksgivingoffering')->whereDate('date_submitted','LIKE', $current_month5.'%')->first();
        $project_may=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','projectoffering')->whereDate('date_submitted','LIKE', $current_month5.'%')->first();
        $tithe_may=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->whereDate('date_submitted','LIKE', $current_month5.'%')->first();
        $pledge_may=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','pledgeoffering')->whereDate('date_submitted','LIKE', $current_month5.'%')->first();
        $welfare_may=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','welfareoffering')->whereDate('date_submitted','LIKE', $current_month5.'%')->first();
        $other_may=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','othersoffering')->whereDate('date_submitted','LIKE', $current_month5.'%')->first();

        ///////jun
        $current_month6=$current_year.'-06';
        $main_jun=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->whereDate('date_submitted','LIKE', $current_month6.'%')->first();
        $thank_jun=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','thanksgivingoffering')->whereDate('date_submitted','LIKE', $current_month6.'%')->first();
        $project_jun=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','projectoffering')->whereDate('date_submitted','LIKE', $current_month6.'%')->first();
        $tithe_jun=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->whereDate('date_submitted','LIKE', $current_month6.'%')->first();
        $pledge_jun=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','pledgeoffering')->whereDate('date_submitted','LIKE', $current_month6.'%')->first();
        $welfare_jun=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','welfareoffering')->whereDate('date_submitted','LIKE', $current_month6.'%')->first();
        $other_jun=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','othersoffering')->whereDate('date_submitted','LIKE', $current_month6.'%')->first();

        ///////jul
        $current_month7=$current_year.'-07';
        $main_jul=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->whereDate('date_submitted','LIKE', $current_month7.'%')->first();
        $thank_jul=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','thanksgivingoffering')->whereDate('date_submitted','LIKE', $current_month7.'%')->first();
        $project_jul=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','projectoffering')->whereDate('date_submitted','LIKE', $current_month7.'%')->first();
        $tithe_jul=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->whereDate('date_submitted','LIKE', $current_month7.'%')->first();
        $pledge_jul=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','pledgeoffering')->whereDate('date_submitted','LIKE', $current_month7.'%')->first();
        $welfare_jul=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','welfareoffering')->whereDate('date_submitted','LIKE', $current_month7.'%')->first();
        $other_jul=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','othersoffering')->whereDate('date_submitted','LIKE', $current_month7.'%')->first();

        ///////aug
        $current_month8=$current_year.'-08';
        $main_aug=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->whereDate('date_submitted','LIKE', $current_month8.'%')->first();
        $thank_aug=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','thanksgivingoffering')->whereDate('date_submitted','LIKE', $current_month8.'%')->first();
        $project_aug=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','projectoffering')->whereDate('date_submitted','LIKE', $current_month8.'%')->first();
        $tithe_aug=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->whereDate('date_submitted','LIKE', $current_month8.'%')->first();
        $pledge_aug=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','pledgeoffering')->whereDate('date_submitted','LIKE', $current_month8.'%')->first();
        $welfare_aug=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','welfareoffering')->whereDate('date_submitted','LIKE', $current_month8.'%')->first();
        $other_aug=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','othersoffering')->whereDate('date_submitted','LIKE', $current_month8.'%')->first();

        ///////sep
        $current_month9=$current_year.'-09';
        $main_sep=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->whereDate('date_submitted','LIKE', $current_month9.'%')->first();
        $thank_sep=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','thanksgivingoffering')->whereDate('date_submitted','LIKE', $current_month9.'%')->first();
        $project_sep=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','projectoffering')->whereDate('date_submitted','LIKE', $current_month9.'%')->first();
        $tithe_sep=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->whereDate('date_submitted','LIKE', $current_month9.'%')->first();
        $pledge_sep=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','pledgeoffering')->whereDate('date_submitted','LIKE', $current_month9.'%')->first();
        $welfare_sep=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','welfareoffering')->whereDate('date_submitted','LIKE', $current_month9.'%')->first();
        $other_sep=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','othersoffering')->whereDate('date_submitted','LIKE', $current_month9.'%')->first();


        ///////oct
        $current_month10=$current_year.'-10';
        $main_oct=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->whereDate('date_submitted','LIKE', $current_month10.'%')->first();
        $thank_oct=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','thanksgivingoffering')->whereDate('date_submitted','LIKE', $current_month10.'%')->first();
        $project_oct=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','projectoffering')->whereDate('date_submitted','LIKE', $current_month10.'%')->first();
        $tithe_oct=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->whereDate('date_submitted','LIKE', $current_month10.'%')->first();
        $pledge_oct=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','pledgeoffering')->whereDate('date_submitted','LIKE', $current_month10.'%')->first();
        $welfare_oct=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','welfareoffering')->whereDate('date_submitted','LIKE', $current_month10.'%')->first();
        $other_oct=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','othersoffering')->whereDate('date_submitted','LIKE', $current_month10.'%')->first();


        ///////nov
        $current_month11=$current_year.'-11';
        $main_nov=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->whereDate('date_submitted','LIKE', $current_month11.'%')->first();
        $thank_nov=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','thanksgivingoffering')->whereDate('date_submitted','LIKE', $current_month11.'%')->first();
        $project_nov=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','projectoffering')->whereDate('date_submitted','LIKE', $current_month11.'%')->first();
        $tithe_nov=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->whereDate('date_submitted','LIKE', $current_month11.'%')->first();
        $pledge_nov=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','pledgeoffering')->whereDate('date_submitted','LIKE', $current_month11.'%')->first();
        $welfare_nov=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','welfareoffering')->whereDate('date_submitted','LIKE', $current_month11.'%')->first();
        $other_nov=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','othersoffering')->whereDate('date_submitted','LIKE', $current_month11.'%')->first();


        ///////dec
        $current_month12=$current_year.'-12';
        $main_dec=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->whereDate('date_submitted','LIKE', $current_month12.'%')->first();
        $thank_dec=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','thanksgivingoffering')->whereDate('date_submitted','LIKE', $current_month12.'%')->first();
        $project_dec=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','projectoffering')->whereDate('date_submitted','LIKE', $current_month12.'%')->first();
        $tithe_dec=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->whereDate('date_submitted','LIKE', $current_month12.'%')->first();
        $pledge_dec=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','pledgeoffering')->whereDate('date_submitted','LIKE', $current_month12.'%')->first();
        $welfare_dec=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','welfareoffering')->whereDate('date_submitted','LIKE', $current_month12.'%')->first();
        $other_dec=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','othersoffering')->whereDate('date_submitted','LIKE', $current_month12.'%')->first();



        ///results
        echo     $main_jan->total ."|".$thank_jan->total ."|". $project_jan->total ."|". $tithe_jan->total ."|". $pledge_jan->total ."|". $welfare_jan->total ."|". $other_jan->total
            ."|".$main_feb->total ."|".$thank_feb->total ."|". $project_feb->total ."|". $tithe_feb->total ."|". $pledge_feb->total ."|". $welfare_feb->total ."|". $other_feb->total
            ."|".$main_mar->total ."|".$thank_mar->total ."|". $project_mar->total ."|". $tithe_mar->total ."|". $pledge_mar->total ."|". $welfare_mar->total ."|". $other_mar->total
            ."|".$main_apr->total ."|".$thank_apr->total ."|". $project_apr->total ."|". $tithe_apr->total ."|". $pledge_apr->total ."|". $welfare_apr->total ."|". $other_apr->total
            ."|".$main_may->total ."|".$thank_may->total ."|". $project_may->total ."|". $tithe_may->total ."|". $pledge_may->total ."|". $welfare_may->total ."|". $other_may->total
            ."|".$main_jun->total ."|".$thank_jun->total ."|". $project_jun->total ."|". $tithe_jun->total ."|". $pledge_jun->total ."|". $welfare_jun->total ."|". $other_jun->total
            ."|".$main_jul->total ."|".$thank_jul->total ."|". $project_jul->total ."|". $tithe_jul->total ."|". $pledge_jul->total ."|". $welfare_jul->total ."|". $other_jul->total
            ."|".$main_aug->total ."|".$thank_aug->total ."|". $project_aug->total ."|". $tithe_aug->total ."|". $pledge_aug->total ."|". $welfare_aug->total ."|". $other_aug->total
            ."|".$main_sep->total ."|".$thank_sep->total ."|". $project_sep->total ."|". $tithe_sep->total ."|". $pledge_sep->total ."|". $welfare_sep->total ."|". $other_sep->total
            ."|".$main_oct->total ."|".$thank_oct->total ."|". $project_oct->total ."|". $tithe_oct->total ."|". $pledge_oct->total ."|". $welfare_oct->total ."|". $other_oct->total
            ."|".$main_nov->total ."|".$thank_nov->total ."|". $project_nov->total ."|". $tithe_nov->total ."|". $pledge_nov->total ."|". $welfare_nov->total ."|". $other_nov->total
            ."|".$main_dec->total ."|".$thank_dec->total ."|". $project_dec->total ."|". $tithe_dec->total ."|". $pledge_dec->total ."|". $welfare_dec->total ."|". $other_dec->total
        ;
    }




    //////Excel


    function excelImportDayBornOffering(Request $request){
        if ($request->hasFile('excel')) {
            // $name=$request->file('excel');
            Excel::load($request->file('excel')->getRealPath(), function ($reader) {
                foreach ($reader->toArray() as $key => $row) {

                    $data['date'] = $row['date'];
                    $data['dayborn'] = $row['dayborn'];
                    $data['amount'] = $row['amount'];
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');


                    if(!empty($data)) {
                        DB::table('dayborns')->insert($data);

                        $notification=array(
                            'message'=>"Day born Offering  has been Succesfully Being Uploaded ",
                            'alert-type'=>'success'
                        );

                        return redirect('offering')->with($notification);
                    }
                }
            });
        }else{
            $notification=array(
                'message'=>"No Excel file was selected ",
                'alert-type'=>'error'
            );

            return redirect('offering')->with($notification);
        }

    }
    public function downloadExceldayborn($type)
    {
        // dd("hello");
        $data = Dayborn::get()->toArray();
        return Excel::create('excel', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }



    function excelImportotherOffering(Request $request){

        if ($request->hasFile('excel')) {
            // $name=$request->file('excel');
            Excel::load($request->file('excel')->getRealPath(), function ($reader) {
                foreach ($reader->toArray() as $key => $row) {

                    $data['number'] = mt_rand(100000, 999999).'O';
                    $data['offeringtype'] = 'othersoffering';

                    $data['servicetype'] = $row['servicetype'];
                    $data['memberid'] = $row['memberid'];
                    $data['amount'] = $row['amount'];
                    $data['description'] = $row['description'];
                    $data['date_submitted'] = $row['datesubmitted'];
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');


                    if(!empty($data)) {
                        DB::table('offerings')->insert($data);

                        $notification=array(
                            'message'=>"Other Offering  has been Succesfully Being Uploaded ",
                            'alert-type'=>'success'
                        );

                        return redirect('offering')->with($notification);
                    }
                }
            });
        }else{
            $notification=array(
                'message'=>"No Excel file was selected ",
                'alert-type'=>'error'
            );

            return redirect('offering')->with($notification);
        }

    }
    public function downloadExcelother($type)
    {
        // dd("hello");
        $data = Offering::get()->where('offeringtype','othersoffering')->toArray();
        return Excel::create('excel', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }



    function excelImportwelfareOffering(Request $request){

        if ($request->hasFile('excel')) {
            // $name=$request->file('excel');
            Excel::load($request->file('excel')->getRealPath(), function ($reader) {
                foreach ($reader->toArray() as $key => $row) {

                    $data['number'] = mt_rand(100000, 999999).'W';
                    $data['offeringtype'] = 'welfareoffering';

                    $data['servicetype'] = $row['servicetype'];
                    $data['memberid'] = $row['memberid'];
                    $data['amount'] = $row['amount'];
                    $data['description'] = $row['description'];
                    $data['date_submitted'] = $row['datesubmitted'];
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');


                    if(!empty($data)) {
                        DB::table('offerings')->insert($data);

                        $notification=array(
                            'message'=>"Welfare Offering  has been Succesfully Being Uploaded ",
                            'alert-type'=>'success'
                        );

                        return redirect('offering')->with($notification);
                    }
                }
            });
        }else{
            $notification=array(
                'message'=>"No Excel file was selected ",
                'alert-type'=>'error'
            );

            return redirect('offering')->with($notification);
        }

    }
    public function downloadExcelwelfare($type)
    {
        // dd("hello");
        $data = Dayborn::get()->toArray();
        return Excel::create('excel', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }


    function excelImportpledgeOffering(Request $request){
       // dd("Hello");
        if ($request->hasFile('excel')) {
            // $name=$request->file('excel');
            Excel::load($request->file('excel')->getRealPath(), function ($reader) {
                foreach ($reader->toArray() as $key => $row) {

                    $data['number'] = mt_rand(100000, 999999).'PD';
                    $data['offeringtype'] = 'pledgeoffering';

                    $data['servicetype'] = $row['servicetype'];
                    $data['memberid'] = $row['memberid'];
                    $data['amount'] = $row['amount'];
                    $data['description'] = $row['description'];
                    $data['date_submitted'] = $row['datesubmitted'];
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');

                    if(!empty($data)) {
                        DB::table('offerings')->insert($data);

                        $notification=array(
                            'message'=>"Pledge Offering  has been Succesfully Being Uploaded ",
                            'alert-type'=>'success'
                        );

                        return redirect('offering')->with($notification);
                    }
                }
            });
        }else{
            $notification=array(
                'message'=>"No Excel file was selected ",
                'alert-type'=>'error'
            );

            return redirect('offering')->with($notification);
        }

    }
    public function downloadExcelpledge($type)
    {
        // dd("hello");
        $data = Dayborn::get()->toArray();
        return Excel::create('excel', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }



    function excelImporttitheOffering(Request $request){

        if ($request->hasFile('excel')) {
            // $name=$request->file('excel');
            Excel::load($request->file('excel')->getRealPath(), function ($reader) {
                foreach ($reader->toArray() as $key => $row) {

                    $data['number'] = mt_rand(100000, 999999).'T';
                    $data['offeringtype'] = 'titheoffering';

                    $data['servicetype'] = $row['servicetype'];
                    $data['memberid'] = $row['memberid'];
                    $data['amount'] = $row['amount'];
                    $data['description'] = $row['description'];
                    $data['date_submitted'] = $row['datesubmitted'];
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');


                    if(!empty($data)) {
                        DB::table('offerings')->insert($data);

                        $notification=array(
                            'message'=>"Tithe Offering  has been Succesfully Being Uploaded ",
                            'alert-type'=>'success'
                        );

                        return redirect('offering')->with($notification);
                    }
                }
            });
        }else{
            $notification=array(
                'message'=>"No Excel file was selected ",
                'alert-type'=>'error'
            );

            return redirect('offering')->with($notification);
        }

    }
    public function downloadExceltithe($type)
    {
        // dd("hello");
        $data = Dayborn::get()->toArray();
        return Excel::create('excel', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }




    function excelImportprojectOffering(Request $request){
        //dd("Hello");
        if ($request->hasFile('excel')) {
            // $name=$request->file('excel');
            Excel::load($request->file('excel')->getRealPath(), function ($reader) {
                foreach ($reader->toArray() as $key => $row) {

                    $data['number'] = mt_rand(100000, 999999).'P';
                    $data['offeringtype'] = 'projectoffering';

                    $data['servicetype'] = $row['servicetype'];
                    $data['memberid'] = $row['memberid'];
                    $data['amount'] = $row['amount'];
                    $data['description'] = $row['description'];
                    $data['date_submitted'] = $row['datesubmitted'];
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');

                    if(!empty($data)) {
                        DB::table('offerings')->insert($data);

                        $notification=array(
                            'message'=>"Project Offering  has been Succesfully Being Uploaded ",
                            'alert-type'=>'success'
                        );

                        return redirect('offering')->with($notification);
                    }
                }
            });
        }else{
            $notification=array(
                'message'=>"No Excel file was selected ",
                'alert-type'=>'error'
            );

            return redirect('offering')->with($notification);
        }

    }
    public function downloadExcelproject($type)
    {
        // dd("hello");
        $data = Dayborn::get()->toArray();
        return Excel::create('excel', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }




    function excelImportthanksOffering(Request $request){

        if ($request->hasFile('excel')) {
            // $name=$request->file('excel');
            Excel::load($request->file('excel')->getRealPath(), function ($reader) {
                foreach ($reader->toArray() as $key => $row) {

                    $data['number'] = mt_rand(100000, 999999).'G';
                    $data['offeringtype'] = 'thanksgivingoffering';

                    $data['servicetype'] = $row['servicetype'];
                    $data['memberid'] = $row['memberid'];
                    $data['amount'] = $row['amount'];
                    $data['description'] = $row['description'];
                    $data['date_submitted'] = $row['datesubmitted'];
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');

                    if(!empty($data)) {
                        DB::table('offeringss')->insert($data);

                        $notification=array(
                            'message'=>"Thanks Giving Offering  has been Succesfully Being Uploaded ",
                            'alert-type'=>'success'
                        );

                        return redirect('offering')->with($notification);
                    }
                }
            });
        }else{
            $notification=array(
                'message'=>"No Excel file was selected ",
                'alert-type'=>'error'
            );

            return redirect('offering')->with($notification);
        }

    }
    public function downloadExcelthanks($type)
    {
        // dd("hello");
        $data = Dayborn::get()->toArray();
        return Excel::create('excel', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }



    function excelImportmainOffering(Request $request){


        if ($request->hasFile('excel')) {
            // $name=$request->file('excel');
            Excel::load($request->file('excel')->getRealPath(), function ($reader) {
                foreach ($reader->toArray() as $key => $row) {

                    $data['number'] = mt_rand(100000, 999999).'M';
                    $data['offeringtype'] = 'mainoffering';

                    $data['servicetype'] = $row['servicetype'];
                    $data['memberid'] = $row['memberid'];
                    $data['amount'] = $row['amount'];
                    $data['description'] = $row['description'];
                    $data['date_submitted'] = $row['datesubmitted'];
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');


                    if(!empty($data)) {
                        DB::table('offerings')->insert($data);

                        $notification=array(
                            'message'=>"Main Offering  has been Succesfully Being Uploaded ",
                            'alert-type'=>'success'
                        );

                        return redirect('offering')->with($notification);
                    }
                }
            });
        }else{
            $notification=array(
                'message'=>"No Excel file was selected ",
                'alert-type'=>'error'
            );

            return redirect('offering')->with($notification);
        }

    }
    public function downloadExcelmain($type)
    {
        // dd("hello");
        $data = Dayborn::get()->toArray();
        return Excel::create('excel', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }









}
