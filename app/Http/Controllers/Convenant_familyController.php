<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Convenantfamily;
use Image;
use DB;
use Datatables;
class Convenant_familyController extends Controller
{
    //
    public function index(){
        
        return view('admin.convenant_family');
    }
    public function get_all_convenantfamily(){
        $members = DB::table('convenantfamilies')->select(['id','convenantname',DB::raw('date(establishmentdate) as establishmentdate'),'meetingtimes','status', 'created_at', 'updated_at'] )->orderBy('establishmentdate','asc');

        return Datatables::of($members)
            ->addColumn('action', function ($members) {

                return'<a href="" data-toggle="modal" data-target="#edit-modal"  data-id="'.$members->id.'" data-convenantname="'.$members->convenantname.'" data-establishmentdate="'.$members->establishmentdate.'"  data-meetingtimes="'.$members->meetingtimes.'"  data-status="'.$members->status.'" class="editbtn btn btn-warning" > <i class="fa fa-pencil-square-o" aria-hidden="true">&nbsp;</i></a>
                         <a href="" data-toggle="modal" data-target="#delete-modal" data-id="'.$members->id.'" data-convenantname="'.$members->convenantname.'" data-establishmentdate="'.$members->establishmentdate.'"  data-meetingtimes="'.$members->meetingtimes.'"  data-status="'.$members->status.'"  class="deletebtn btn btn-danger" ><i class="fa fa-trash" aria-hidden="true">&nbsp;</i></a>';

            })
            ->make(true);
    }
    public function save(Request $request){
        try{
            $m = new Convenantfamily();

            $m->convenantname=$request->convenantname;
            $m->establishmentdate=$request->establishmentdate;
            $m->meetingtimes=$request->meetingtimes;
            $m->status=$request->status;
            

            if($m->save()){
                $notification=array(
                    'message'=>"New Convenant family group has been Succesfully Added",
                    'alert-type'=>'success'
                );

                return redirect('convenantfamily')->with($notification);
            }
        }catch(Exception $e){
            $notification=array(
                'message'=>'A Error Occured',
                'alert-type'=>'error'
            );
            return redirect('convenantfamily')->with($notification);
        }
    }
    public function update(Request $request){
//        dd($request->id_edit);
        try{$m = Convenantfamily::findorfail($request->id_edit);
            
            $m->convenantname=$request->convenantname_edit;
            $m->establishmentdate=$request->establishmentdate_edit;
            $m->meetingtimes=$request->meetingtimes_edit;
            $m->status=$request->status_edit;
            if($m->update()){

                $notification=array(
                    'message'=>"Convenant family details has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('convenantfamily')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"convenant family  details  failed to updated",
                'alert-type'=>'success'
            );
            return redirect('convenantfamily')->with($notification);
        }
    }
    public function delete(request $request){
        try{
            $m = Convenantfamily::findorfail($request->convenantid);
            if($m->delete()){
                $notification=array(
                    'message'=>"convenant family has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('convenantfamily')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error occured",
                'alert-type'=>'error'
            );
            return redirect('convenantfamily')->with($notification);
        }
    }
}
