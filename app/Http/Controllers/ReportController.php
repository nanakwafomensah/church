<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use Image;
use DB;
use Datatables;
class ReportController extends Controller
{
    //
    public function index(){
        $membergroups = DB::table('membergroups')->get();
        $memberlocations = DB::table('members')->select(['hometown'])->distinct()->get();
//        dd($memberlocations);
        $servicetype=DB::table('services')->select(['id','service_name'])->get();
        return view('admin.report', ['membergroups' => $membergroups,'servicetype'=>$servicetype,'memberlocations'=>$memberlocations]);
    }
    
    public  function quarterlyrpt(){
        return view('admin.quaterlyrpt');
    }
    public function weeklyrpt(){
        return view('admin.weeklyrpt');
    }
    
    
}
