<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use DB;
use Datatables;
class DashboardController extends Controller
{
    //
    public function index(){
        $totalmembers=DB::table('members')->where('membershipstatus','member')->count();
        $totalmembers_male=DB::table('members')->where('gender','male') ->where('membershipstatus','member') ->count();
        $totalmembers_female=DB::table('members')->where('gender','female') ->where('membershipstatus','member') ->count();

        $totalgroups=DB::table('membergroups')->get()->count();
        $totalgroups_active=DB::table('membergroups')->where('status','active')->get()->count();
        $totalgroups_inactive=DB::table('membergroups')->where('status','inactive')->get()->count();

        $querytotaltithe_total=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->first();
        $querytotaltithe_main=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->first();
        $querytotaltithe_tithe=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->first();
        $totaltithe_total= number_format($querytotaltithe_total->total) ;
        $totaltithe_main= number_format($querytotaltithe_main->total) ;
        $totaltithe_tithe= number_format($querytotaltithe_tithe->total) ;


        ////////////NEWLY ADDED MEMBERS///////
        $newlymembers=DB::table('members') ->where('membershipstatus','member')->orderBy('created_at','desc') ->take(5)->paginate(3);
        $newlymemberscount=DB::table('members') ->where('membershipstatus','member')->orderBy('created_at','desc') ->take(5)->get()->count();
      
        ////////////Monthlybirthdays//////////////////
        $birthdaymembers=DB::table('members') ->where('membershipstatus','member')->where(DB::raw('MONTH(dob)'),date('m'))->orderBy('created_at','desc')->paginate(3);
        $birthdaymemberscount=DB::table('members') ->where('membershipstatus','member')->where(DB::raw('MONTH(dob)'),date('m'))->orderBy('created_at','desc')->get()->count();

        ///////////////service  attendance/////
        $lastweekdate=$this->lastweekdate();
        $lastweekdate_start=explode("|",$lastweekdate);
//         dd($lastweekdate_start);

        $previousattend_male=DB::table('attendances')->select([DB::raw('sum(male)  as c')])->whereNull('stage_classes')->whereDate('registration_date','>=',$lastweekdate_start[0])->whereDate('registration_date','<=',$lastweekdate_start[1]) ->first();
        $previousattend_female=DB::table('attendances')->select([DB::raw('sum(female) as c')])->whereNull('stage_classes')->whereDate('registration_date','>=',$lastweekdate_start[0])->whereDate('registration_date','<=',$lastweekdate_start[1])  ->first();
        $previousattendance=number_format($previousattend_male->c == 0 ? 0 :  $previousattend_male->c) + number_format($previousattend_female->c == 0 ? 0 :  $previousattend_female->c);

//             dd($previousattend_male->c == 0 ? 0 :  $previousattend_male->c);

        
        /////////////////////VISITORS//////////////////////////////////////////
        $totalmembers_visitors=DB::table('members') ->where('membershipstatus','visitor')->count();
        $totalmembers_male_visitors=DB::table('members')->where('gender','male') ->where('membershipstatus','visitor')->count();
        $totalmembers_female_visitors=DB::table('members')->where('gender','female') ->where('membershipstatus','visitor') ->count();
//////////////////////////////////////////////////////////////////////////////////
        $previousmonday=date('Y-m-d', strtotime("last Monday"));
        $nextmonday=date('Y-m-d', strtotime("next Monday"));
        $adult_serviceattendancedetailsquery_male =  DB::table('attendances')->select([DB::raw('sum(male) as c')]) ->whereDate('created_at','>=',$previousmonday)->whereDate('created_at','<=',$nextmonday)->first();
        $adult_serviceattendancedetailsquery_female =  DB::table('attendances')->select([DB::raw('sum(female) as c')]) ->whereDate('created_at','>=',$previousmonday)->whereDate('created_at','<=',$nextmonday)->first();
$weekattendancemale=$adult_serviceattendancedetailsquery_male->c;
$weekattendancefemale=$adult_serviceattendancedetailsquery_female->c;
      $totalweekattendance=$weekattendancemale+$weekattendancefemale;
        
        
        /////////////////////////////////Graph/////////////////////////
        
        
        
        

        return view('admin/dashboard')->with(
            [
                'totalmembers'=>$totalmembers,
                'totalmembers_male'=>$totalmembers_male,
                'totalmembers_female'=>$totalmembers_female,
                'totalgroups'=>$totalgroups,
                'totalgroups_active'=> $totalgroups_active,
                'totalgroups_inactive'=> $totalgroups_inactive,
                'totaltithe_total'=> $totaltithe_total,
                'totaltithe_main'=> $totaltithe_main,
                'totaltithe_tithe'=> $totaltithe_tithe,
                'newlymembers'=> $newlymembers,
                'birthdaymembers'=> $birthdaymembers,
                'previousattendance'=>$previousattendance,
                'previousattend_male'=>$previousattend_male->c == 0 ? 0 :  $previousattend_male->c,
                'previousattend_female'=>$previousattend_female->c == 0 ? 0 :  $previousattend_female->c,
                'totalmembers_visitors'=>$totalmembers_visitors,
                'totalmembers_male_visitors'=>$totalmembers_male_visitors,
                'totalmembers_female_visitors'=>$totalmembers_female_visitors,
                'weekattendancemale'=>$weekattendancemale,
                'weekattendancefemale'=>$weekattendancefemale,
                'totalweekattendance'=>$totalweekattendance,
                'newlymemberscount'=>$newlymemberscount,
                'birthdaymemberscount'=>$birthdaymemberscount,

            ]
        );
    }

    public function chart(Request $request){
        $current_year=$request->current_year;
        ///////january
        $current_year1=$current_year.'-01';
        $totalmembers_01=DB::table('members')->where('membershipstatus','member')->whereDate('registration_date','LIKE','%'.$current_year1.'%')->count();
        $totalvisitors_01=DB::table('members')->where('membershipstatus','visitor')->whereDate('registration_date','LIKE','%'.$current_year1.'%')->count();
        $totalregular_visitor_01=DB::table('members')->where('membershipstatus','regular_visitor')->whereDate('registration_date','LIKE','%'.$current_year1.'%')->count();
        ///////feb
        $current_year2=$current_year.'-02';
        $totalmembers_02=DB::table('members')->where('membershipstatus','member')->whereDate('registration_date','LIKE','%'.$current_year2.'%')->count();
        $totalvisitors_02=DB::table('members')->where('membershipstatus','visitor')->whereDate('registration_date','LIKE','%'.$current_year2.'%')->count();
        $totalregular_visitor_02=DB::table('members')->where('membershipstatus','regular_visitor')->whereDate('registration_date','LIKE','%'.$current_year2.'%')->count();

        ///////mar
        $current_year3=$current_year.'-03';
        $totalmembers_03=DB::table('members')->where('membershipstatus','member')->whereDate('registration_date','LIKE','%'.$current_year3.'%')->count();
        $totalvisitors_03=DB::table('members')->where('membershipstatus','visitor')->whereDate('registration_date','LIKE','%'.$current_year3.'%')->count();
        $totalregular_visitor_03=DB::table('members')->where('membershipstatus','regular_visitor')->whereDate('registration_date','LIKE','%'.$current_year3.'%')->count();

        ///////apr
        $current_year4=$current_year.'-04';
        $totalmembers_04=DB::table('members')->where('membershipstatus','member')->whereDate('registration_date','LIKE','%'.$current_year4.'%')->count();
        $totalvisitors_04=DB::table('members')->where('membershipstatus','visitor')->whereDate('registration_date','LIKE','%'.$current_year4.'%')->count();
        $totalregular_visitor_04=DB::table('members')->where('membershipstatus','regular_visitor')->whereDate('registration_date','LIKE','%'.$current_year4.'%')->count();

        //////may
        $current_year5=$current_year.'-05';
        $totalmembers_05=DB::table('members')->where('membershipstatus','member')->whereDate('registration_date','LIKE','%'.$current_year5.'%')->count();
        $totalvisitors_05=DB::table('members')->where('membershipstatus','visitor')->whereDate('registration_date','LIKE','%'.$current_year5.'%')->count();
        $totalregular_visitor_05=DB::table('members')->where('membershipstatus','regular_visitor')->whereDate('registration_date','LIKE','%'.$current_year5.'%')->count();

        ///////jun
        $current_year6=$current_year.'-06';
        $totalmembers_06=DB::table('members')->where('membershipstatus','member')->whereDate('registration_date','LIKE','%'.$current_year6.'%')->count();
        $totalvisitors_06=DB::table('members')->where('membershipstatus','visitor')->whereDate('registration_date','LIKE','%'.$current_year6.'%')->count();
        $totalregular_visitor_06=DB::table('members')->where('membershipstatus','regular_visitor')->whereDate('registration_date','LIKE','%'.$current_year6.'%')->count();

        ///////jul
        $current_year7=$current_year.'-07';
        $totalmembers_07=DB::table('members')->where('membershipstatus','member')->whereDate('registration_date','LIKE','%'.$current_year7.'%')->count();
        $totalvisitors_07=DB::table('members')->where('membershipstatus','visitor')->whereDate('registration_date','LIKE','%'.$current_year7.'%')->count();
        $totalregular_visitor_07=DB::table('members')->where('membershipstatus','regular_visitor')->whereDate('registration_date','LIKE','%'.$current_year7.'%')->count();

        ///////aug
        $current_year8=$current_year.'-08';
        $totalmembers_08=DB::table('members')->where('membershipstatus','member')->whereDate('registration_date','LIKE','%'.$current_year8.'%')->count();
        $totalvisitors_08=DB::table('members')->where('membershipstatus','visitor')->whereDate('registration_date','LIKE','%'.$current_year8.'%')->count();
        $totalregular_visitor_08=DB::table('members')->where('membershipstatus','regular_visitor')->whereDate('registration_date','LIKE','%'.$current_year8.'%')->count();

        ///////sep
        $current_year9=$current_year.'-09';
        $totalmembers_09=DB::table('members')->where('membershipstatus','member')->whereDate('registration_date','LIKE','%'.$current_year9.'%')->count();
        $totalvisitors_09=DB::table('members')->where('membershipstatus','visitor')->whereDate('registration_date','LIKE','%'.$current_year9.'%')->count();
        $totalregular_visitor_09=DB::table('members')->where('membershipstatus','regular_visitor')->whereDate('registration_date','LIKE','%'.$current_year9.'%')->count();

        ///////oct
        $current_year10=$current_year.'-10';
        $totalmembers_10=DB::table('members')->where('membershipstatus','member')->whereDate('registration_date','LIKE','%'.$current_year10.'%')->count();
        $totalvisitors_10=DB::table('members')->where('membershipstatus','visitor')->whereDate('registration_date','LIKE','%'.$current_year10.'%')->count();
        $totalregular_visitor_10=DB::table('members')->where('membershipstatus','regular_visitor')->whereDate('registration_date','LIKE','%'.$current_year10.'%')->count();

        ///////nov
        $current_year11=$current_year.'-11';
        $totalmembers_11=DB::table('members')->where('membershipstatus','member')->whereDate('registration_date','LIKE','%'.$current_year11.'%')->count();
        $totalvisitors_11=DB::table('members')->where('membershipstatus','visitor')->whereDate('registration_date','LIKE','%'.$current_year11.'%')->count();
        $totalregular_visitor_11=DB::table('members')->where('membershipstatus','regular_visitor')->whereDate('registration_date','LIKE','%'.$current_year11.'%')->count();

        ///////dec
        $current_year12=$current_year.'-12';
        $totalmembers_12=DB::table('members')->where('membershipstatus','member')->whereDate('registration_date','LIKE','%'.$current_year12.'%')->count();
        $totalvisitors_12=DB::table('members')->where('membershipstatus','visitor')->whereDate('registration_date','LIKE','%'.$current_year12.'%')->count();
        $totalregular_visitor_12=DB::table('members')->where('membershipstatus','regular_visitor')->whereDate('registration_date','LIKE','%'.$current_year12.'%')->count();




        echo  $totalmembers_01."|".$totalvisitors_01
            ."|".$totalmembers_02."|".$totalvisitors_02
            ."|".$totalmembers_03."|".$totalvisitors_03
            ."|".$totalmembers_04."|".$totalvisitors_04
            ."|".$totalmembers_05."|".$totalvisitors_05
            ."|".$totalmembers_06."|".$totalvisitors_06
            ."|".$totalmembers_07."|".$totalvisitors_07
            ."|".$totalmembers_08."|".$totalvisitors_08
            ."|".$totalmembers_09."|".$totalvisitors_09
            ."|".$totalmembers_10."|".$totalvisitors_10
            ."|".$totalmembers_11."|".$totalvisitors_11
            ."|".$totalmembers_12."|".$totalvisitors_12
            ."|".$totalregular_visitor_01."|".$totalregular_visitor_02
            ."|".$totalregular_visitor_03."|".$totalregular_visitor_04
            ."|".$totalregular_visitor_05."|".$totalregular_visitor_06
            ."|".$totalregular_visitor_07."|".$totalregular_visitor_08
            ."|".$totalregular_visitor_09."|".$totalregular_visitor_10
            ."|".$totalregular_visitor_11."|".$totalregular_visitor_12

        ;
    }
   public function piechart(Request $request){
       $current_year=$request->current_year;

       $totalmoney=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('date_submitted','LIKE','%'.$current_year.'%')->first();
       $total_total= number_format($totalmoney->total) ;


/////////////mainoffering
       $total_mainoffering=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','mainoffering')->where('date_submitted','LIKE','%'.$current_year.'%')->first();
       $total_mainoffering= number_format($total_mainoffering->total) ;
       $pecent_total_mainoffering=($total_mainoffering/$total_total)*100;
       /////////////thanksgivingoffering
       $total_thanksoffering=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','thanksgivingoffering')->where('date_submitted','LIKE','%'.$current_year.'%')->first();
       $total_thanksoffering= number_format($total_thanksoffering->total) ;
       $pecent_total_thanksoffering=($total_thanksoffering/$total_total)*100;

      
       /////////////projectoffering
       $total_projectoffering=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','projectoffering')->where('date_submitted','LIKE','%'.$current_year.'%')->first();
       $total_projectoffering= number_format($total_projectoffering->total) ;
       $pecent_total_projectoffering=($total_projectoffering/$total_total)*100;

       /////////////titheoffering
       $total_titheoffering=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','titheoffering')->where('date_submitted','LIKE','%'.$current_year.'%')->first();
       $total_titheoffering= number_format($total_titheoffering->total) ;
       $pecent_total_titheoffering=($total_titheoffering/$total_total)*100;

       //////////////pledgeoffering
       $total_pledgeoffering=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','pledgeoffering')->where('date_submitted','LIKE','%'.$current_year.'%')->first();
       $total_pledgeoffering= number_format($total_pledgeoffering->total) ;
       $pecent_total_pledgeoffering=($total_pledgeoffering/$total_total)*100;

       //////////////welfareoffering
       $total_welfareoffering=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','welfareoffering')->where('date_submitted','LIKE','%'.$current_year.'%')->first();
       $total_welfareoffering= number_format($total_welfareoffering->total) ;
       $pecent_total_welfareoffering=($total_welfareoffering/$total_total)*100;
       
       ////////////othersoffering
       $total_othersoffering=DB::table('offerings')->select([DB::raw('sum(amount) as total')])->where('offeringtype','othersoffering')->where('date_submitted','LIKE','%'.$current_year.'%')->first();
       $total_othersoffering= number_format($total_othersoffering->total) ;
       $pecent_total_othersoffering=($total_othersoffering/$total_total)*100;
       echo $total_total."|".$pecent_total_mainoffering."|".$pecent_total_thanksoffering."|".$pecent_total_projectoffering."|".$pecent_total_titheoffering."|".$pecent_total_pledgeoffering."|".$pecent_total_welfareoffering."|".$pecent_total_othersoffering;
   }


    public function lastweekdate(){
        $previous_week = strtotime("-1 week +1 day");

        $start_week = strtotime("last sunday midnight",$previous_week);
        $end_week = strtotime("next saturday",$start_week);

        $start_week = date("Y-m-d",$start_week);
        $end_week = date("Y-m-d",$end_week);

        return $start_week.'|'.$end_week ;
    }
}



