<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use Image;
use DB;
use Datatables;
class EventController extends Controller
{
    //
    public function index(){
        return view('admin.event');
    }

    public function get_all_events(){
        $members = DB::table('events')->select(['id','eventtype', 'name', 'description',DB::raw('date(recordered_date) as recordered_date')])->orderBy('recordered_date','asc');

        return Datatables::of($members)
            ->addColumn('action', function ($members) {

                return'<a href="" data-toggle="modal" data-target="#edit-modal"  data-id="'.$members->id.'" data-eventtype="'.$members->eventtype.'" data-eventname="'.$members->name.'"  data-description="'.$members->description.'" data-recordered_date="'.$members->recordered_date.'" class="editbtn btn btn-warning" > <i class="fa fa-pencil-square-o" aria-hidden="true">&nbsp;</i></a>
                                          <a href="" data-toggle="modal" data-target="#delete-modal" data-id="'.$members->id.'" data-eventtype="'.$members->eventtype.'" data-eventname="'.$members->name.'"  data-description="'.$members->description.'" data-recordered_date="'.$members->recordered_date.'"  class="deletebtn btn btn-danger" ><i class="fa fa-trash" aria-hidden="true">&nbsp;</i></a>';

            })
            ->make(true);
    }
    public function save(Request $request){

        try{
            $m = new Event();

            $m->eventtype=$request->eventtype;
            $m->name=$request->eventname;
            $m->description=$request->description;
            $m->recordered_date=$request->recordered_date;

            if($m->save()){
                $notification=array(
                    'message'=>"New Event has been Succesfully Added",
                    'alert-type'=>'success'
                );

                return redirect('newevent')->with($notification);
            }
        }catch(Exception $e){
            $notification=array(
                'message'=>'A Error Occured',
                'alert-type'=>'error'
            );
            return redirect('newevent')->with($notification);
        }   
    }
    public function update(Request $request){
        try{

            $m = Event::where('id', $request->id_edit)->firstOrFail();
            $m->eventtype=$request->eventtype_edit;
            $m->name=$request->eventname_edit;
            $m->description=$request->description_edit;
            $m->recordered_date=$request->recordered_date_edit;




            if($m->update()){

                $notification=array(
                    'message'=>" details has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('newevent')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"details  failed to be updated",
                'alert-type'=>'success'
            );
            return redirect('newevent')->with($notification);
        }
    }
    public function delete(Request $request){
        try{
            $m = Event::where('id', $request->id_delete)->firstOrFail();
            if($m->delete()){
                $notification=array(
                    'message'=>"Record  has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('newevent')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error Occured",
                'alert-type'=>'error'
            );
            return redirect('newevent')->with($notification);
        }
    }
}
