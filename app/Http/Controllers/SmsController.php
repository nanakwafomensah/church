<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Jobs\SendSMS;
use Illuminate\Http\Request;
use App\Member;
use Image;
use DB;
use Datatables;
use Illuminate\Database\Eloquent\Collection;

class SmsController extends Controller
{
    //
    public function membercontact(){
        $members = DB::table('members')->select(['members.member_id', DB::raw('CONCAT_WS(" ",members.firstname,members.lastname) as fullname'), 'members.occupation', 'members.telephone', 'members.marital_status','members.lastname','members.firstname','members.photo']);

        return Datatables::of($members)
            ->addColumn('fullname', function ($members) {

                return' <a href="" id="member_click" data-telephone="'.$members->telephone.'" data-fullname="'.$members->fullname.'">'.$members->fullname.'</a>
                                ';
            })

            ->addColumn('telephone', function ($members) {

               return'<a href=""  id="telephone_click"  class="btn btn-success" data-telephone="'.$members->telephone.'" data-fullname="'.$members->fullname.'" >'.$members->telephone.'</a>
                                 ';

            })
            ->make(true);
    }
    public function allmembers(){
        $members = DB::table('members')->select(['members.member_id', DB::raw('CONCAT_WS(" ",members.firstname,members.lastname) as fullname'), 'members.occupation', 'members.telephone', 'members.marital_status','members.lastname','members.firstname','members.photo']);

        return Datatables::of($members)
            ->addColumn('fullname', function ($members) {

                return' <a href="" id="member_click" data-telephone="'.$members->telephone.'" data-fullname="'.$members->fullname.'">'.$members->fullname.'</a>
                                ';
            })

            ->addColumn('telephone', function ($members) {

                return'<a href=""  id="telephone_click"  class="btn btn-success" data-telephone="'.$members->telephone.'" data-fullname="'.$members->fullname.'" >'.$members->telephone.'</a>
                                 ';

            })
            ->make(true);
    }
    public function index(){
        $members = DB::table('members')->select(['members.member_id', DB::raw('CONCAT_WS(" ",members.firstname,members.lastname) as fullname'), 'members.occupation', 'members.telephone', 'members.marital_status','members.lastname','members.firstname','members.photo']);

        //return view('singlesms', ['members' => $members]);
        return view('admin/singlesms');
    }
    public function singlesms(Request $request)
    {
        $msisdn = $request->number;
        $sender = $request->sender;
            $job = (new SendSMS($msisdn,$sender))->delay(Carbon::now()->addMinutes(5));
            dispatch($job);
//        $msisdn = $request->number;
//        $sender = $request->sender;
//        $message = $request->messagecontent;
//        $receipient = $request->name_of_receipient;
//        $msg = urlencode($message);
//        $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
//        file_get_contents($api);


    }
    public function bulkindex(){
        $membergroups = DB::table('membergroups')->get();
        return view('admin/bulksms', ['membergroups' => $membergroups]);
       
    }
    public function sendbulksms(Request $request){
        $name=$request->name;
        $msisdn=$request->telephone;
        $sender=$request->sender;
        $message=$request->messagecontent;
        $msg = urlencode($message);

        
        $api= 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination='.$msisdn.'&source='.$sender.'&message=' . $msg;
        file_get_contents($api);


    }
    public function loadbulksms(Request $request){

        $value=$request->value;
        $output="";

            $members = DB::table('members')->select(['members.member_id', DB::raw('CONCAT_WS(" ",members.firstname,members.lastname) as fullname'), 'members.occupation', 'members.telephone', 'members.marital_status','members.lastname','members.firstname','members.photo'])  ->where('groups', 'like', $value.'%')->get();



            $member = new Collection($members);
            if($member){
                foreach($member as $key=>$allmembers) {
                    $output .= '<tr>' .
                        '<td class="td_4"><input id="check-box"  type="checkbox"  data-name="'.$allmembers->fullname .'" data-telephone="'.$allmembers->telephone.'"/></td>' .
                        '<td class="guestname">' . $allmembers->fullname . '</td>' .
                        '<td class="number">' . $allmembers->telephone . '</td>' .
                        '</tr>';
                }
                return Response($output);
            }


    
      
    }
    
    
}
