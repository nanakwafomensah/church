<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offering;
use Image;
use DB;
use Datatables;
class FiofferingController extends Controller
{
    //
    //
    public function index()
    {
        $servicetype=DB::table('services')->select(['id','service_name'])->get();
        $allmembers=DB::table('members')->select(['id','member_id','firstname','lastname'])->get();
        $mainofferingsummary=DB::table('offerings')->select([DB::raw('sum(amount) as total'),'date_submitted',DB::raw('MONTH (date_submitted) as month')])
            ->groupBy(DB::raw('YEAR (date_submitted),MONTH (date_submitted),date_submitted'))

            ->where('offeringtype','mainoffering')->get();


        return view('finance/fioffering')->with(['servicetype'=>$servicetype,'allmembers'=>$allmembers,'mainofferingsummary'=>$mainofferingsummary]);
    }
    public function get_alloffering(){
        $members = DB::table('offerings')->select(['number','offeringtype', 'servicetype', 'memberid', 'amount','date_submitted','description']);

        return Datatables::of($members)
            ->addColumn('action', function ($members) {

                return'<a href="" data-toggle="modal" data-target="#edit-Modal"  data-number="'.$members->number.'"  data-offeringtype="'.$members->offeringtype.'" data-servicetype="'.$members->servicetype.'" data-amount="'.$members->amount.'" data-date_submitted="'.$members->date_submitted.'" data-description="'.$members->description.'" data-name="'.$members->memberid.'" class="editbtn btn btn-warning" > <i class="fa fa-pencil-square-o" aria-hidden="true">&nbsp;</i></a>
                                          <a href="" data-toggle="modal" data-target="#delete-Modal"   data-number="'.$members->number.'"  data-offeringtype="'.$members->offeringtype.'" data-servicetype="'.$members->servicetype.'" data-amount="'.$members->amount.'" data-date_submitted="'.$members->date_submitted.'" data-description="'.$members->description.'"   class="deletebtn btn btn-danger" ><i class="fa fa-trash" aria-hidden="true">&nbsp;</i></a>
                                             ';
            })


            ->make(true);

    }
    public function indexall()
    {
        $servicetype=DB::table('services')->select(['id','service_name'])->get();
        $allmembers=DB::table('members')->select(['id','member_id','firstname','lastname'])->get();
        return view('finance/fialloffering')->with(['servicetype'=>$servicetype,'allmembers'=>$allmembers]);
    }
    public function save(Request $request){

        try{
            $churchdetails =  DB::table('profiles')->first();
            $mem_mobile_number=null;
            $name=trim($request->member_id);
            if($request->offeringtype!='mainoffering'){
                $memberdetails =  DB::table('members')->where('fullname',$name)->first();
                if(isset($memberdetails->telephone)){
                    $mem_mobile_number=$memberdetails->telephone;
                }

            }

            $m = new Offering();

            $m->date_submitted=$request->date_submitted;
            $m->number=$request->number;
            $m->offeringtype=$request->offeringtype;
            $m->servicetype=$request->servicetype;
            $m->memberid=$request->member_id;
            $m->amount=$request->amount;
            $m->description=$request->description;

            if($request->offeringtype=='thanksgivingoffering'){
                $parentname=$churchdetails->parentchurchname;
                $city=$churchdetails->city;
                $name=$churchdetails->churchname;
                $sender=$churchdetails->smssenderid;
                $msisdn=$mem_mobile_number;
                $message="Dear $request->member_id  , $parentname $name ,$city have received a ThanksGiving Offering of GHC $request->amount From You at the $request->servicetype.\n offeringID:$request->number.Thank You.";
                $msg = urlencode($message);
                $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
                @file_get_contents($api);
            }
            if($request->offeringtype=='projectoffering'){
                $parentname=$churchdetails->parentchurchname;
                $city=$churchdetails->city;
                $name=$churchdetails->churchname;
                $sender=$churchdetails->smssenderid;
                $msisdn=$mem_mobile_number;
                $message="Dear $request->member_id  , $parentname $name ,$city have received a Project Offering of GHC $request->amount From You at the $request->servicetype.\n offeringID:$request->number.Thank You.";
                $msg = urlencode($message);
                $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
                if(isset($msisdn)){
                    @file_get_contents($api);
                }

            } if($request->offeringtype=='titheoffering'){
                $parentname=$churchdetails->parentchurchname;
                $city=$churchdetails->city;
                $name=$churchdetails->churchname;
                $sender=$churchdetails->smssenderid;
                $msisdn=$mem_mobile_number;
                $message="Dear $request->member_id  , $parentname $name ,$city have received a Tithe Offering of GHC $request->amount From You at the $request->servicetype.\n offeringID:$request->number.Thank You.";
                $msg = urlencode($message);
                $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
                if(isset($msisdn)){
                    @file_get_contents($api);
                }

            }
            if($request->offeringtype=='welfareoffering'){
                $parentname=$churchdetails->parentchurchname;
                $city=$churchdetails->city;
                $name=$churchdetails->churchname;
                $sender=$churchdetails->smssenderid;
                $msisdn=$mem_mobile_number;
                $message="Dear $request->member_id  , $parentname $name ,$city have received a Welfare Offering of GHC $request->amount From You at the $request->servicetype.\n offeringID:$request->number.Thank You.";
                $msg = urlencode($message);
                $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
                if(isset($msisdn)){
                    @file_get_contents($api);
                }

            }
            if($request->offeringtype=='othersoffering'){
                $parentname=$churchdetails->parentchurchname;
                $city=$churchdetails->city;
                $name=$churchdetails->churchname;
                $sender=$churchdetails->smssenderid;
                $msisdn=$mem_mobile_number;
                $message="Dear Pastor,The Church have received  an Offering from other sources of GHC $request->amount at the $request->servicetype.Thank You.";
                $msg = urlencode($message);
                $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
                if(isset($msisdn)){
                    @file_get_contents($api);
                }

            }
            if($request->offeringtype=='pledgeoffering'){
                $parentname=$churchdetails->parentchurchname;
                $city=$churchdetails->city;
                $name=$churchdetails->churchname;
                $sender=$churchdetails->smssenderid;
                $msisdn=$mem_mobile_number;
                $message="Dear Pastor,The Church have received  a Plegde Offering of GHC $request->amount at the $request->servicetype.Thank You.";
                $msg = urlencode($message);
                $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
                if(isset($msisdn)){
                    @file_get_contents($api);
                }

            }
            if ($request->offeringtype=='mainoffering'){
                $parentname=$churchdetails->parentchurchname;
                $city=$churchdetails->city;
                $name=$churchdetails->churchname;
                $sender=$churchdetails->smssenderid;
                $msisdn=preg_replace('/^0/','233',$churchdetails->phone);
//                 dd($msisdn);
                $message="Dear Pastor,The Church have received  a Main Offering of GHC $request->amount at the $request->servicetype.Thank You.";
                $msg = urlencode($message);
                $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
                @file_get_contents($api);
            }

            if($m->save()){
                $notification=array(
                    'message'=>"Offering  has been Succesfully Added ",
                    'alert-type'=>'success'
                );

                return redirect('fialloffering')->with($notification);
            }
        }catch(Exception $e){
            $notification=array(
                'message'=>'A Error Occured',
                'alert-type'=>'error'
            );
            return redirect('fioffering')->with($notification);
        }
    }


    public function update(Request $request){
        //dd($request->offering_number_edit_id);::where('postal', $postal)->firstOrFail();
        try{
            //$m = Offering::findorfail($request->offering_number_edit_id);
            $m = Offering::where('number', $request->offering_number_edit_id)->firstOrFail();
            $m->servicetype=$request->servicetype_edit;
            $m->memberid=$request->member_edit;
            $m->amount=$request->amount_edit;
            $m->date_submitted=$request->date_submitted_edit;
            $m->description=$request->description_edit;



            if($m->update()){

                $notification=array(
                    'message'=>"Offering Type details has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('fialloffering')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"Offering  details  failed to be updated",
                'alert-type'=>'success'
            );
            return redirect('fialloffering')->with($notification);
        }
    }
    public function delete(request $request){
        try{
            $m = Offering::where('number', $request->offering_number_delete)->firstOrFail();
            if($m->delete()){
                $notification=array(
                    'message'=>"Offering  has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('fialloffering')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error occured",
                'alert-type'=>'error'
            );
            return redirect('fialloffering')->with($notification);
        }
    }

    public function viewallmembers(){
        $allmembers=DB::table('members')->where('membershipstatus','member')->paginate(3);
        return view('finance/viewallmembers')->with(['allmembers'=>$allmembers]);
    }
}
