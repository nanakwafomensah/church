<?php

namespace App\Http\Controllers;

use App\Membergroup;
use Illuminate\Http\Request;
use Image;
use DB;
use Datatables;
class MembergroupController extends Controller
{
    //
    public function index(){
        return view('admin/member_group');
        
    }
    public function get_all_group(){
        $membersg = DB::table('membergroups')->select(['membergroups.id','membergroups.name', 'membergroups.created_at', 'membergroups.updated_at', 'membergroups.purpose','status'])->orderBy('created_at','asc');

        return Datatables::of($membersg)
            ->addColumn('action', function ($membersg) {

                return'<a href="" data-toggle="modal" data-target="#edit-modal"  data-id="'.$membersg->id.'"  data-name="'.$membersg->name.'" data-purpose="'.$membersg->purpose.'" data-status="'.$membersg->status.'"  class="editbtn btn btn-warning" > <i class="fa fa-pencil-square-o" aria-hidden="true">&nbsp;</i></a>
                                          <a href=""  data-toggle="modal" data-target="#delete-modal"  data-id="'.$membersg->id.'"  data-name="'.$membersg->name.'"  data-purpose="'.$membersg->purpose.'" class="deletebtn btn btn-danger" ><i class="fa fa-trash" aria-hidden="true">&nbsp;</i></a>';

            })
            ->make(true);
    }

    public function save(Request $request){
        try{
            $m = new Membergroup();

            $m->name=$request->name;
            $m->purpose=$request->purpose;
            $m->status='active';

            if($m->save()){
                $notification=array(
                    'message'=>"New Membergroup has been Succesfully Added",
                    'alert-type'=>'success'
                );

                return redirect('member_group')->with($notification);
            }
        }catch(Exception $e){
            $notification=array(
                'message'=>'A Error Occured',
                'alert-type'=>'error'
            );
            return redirect('member_group')->with($notification);
        }
    }
    public function update(Request $request){
        try{$m = Membergroup::findorfail($request->id_edit);
            $m->name=$request->name_edit;
            $m->purpose=$request->purpose_edit;
            $m->status=$request->status_edit;


            if($m->update()){

                $notification=array(
                    'message'=>"Member Group details has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('member_group')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"Member Group details  failed to updated",
                'alert-type'=>'success'
            );
            return redirect('member_group')->with($notification);
        }
    }
    public function delete(request $request){
            try{
                $m = Membergroup::findorfail($request->id_delete);
                if($m->delete()){
                    $notification=array(
                        'message'=>"Member Group has been deleted",
                        'alert-type'=>'success'
                    );
                    return redirect('member_group')->with($notification);
                }


            }catch(Exception $e){
                $notification=array(
                    'message'=>"An error occured",
                    'alert-type'=>'error'
                );
                return redirect('member_group')->with($notification);
            }
        }
}
