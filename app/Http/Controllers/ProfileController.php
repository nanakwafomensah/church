<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use DB;
use Image;
class ProfileController extends Controller
{
    //
    public function saveprofile()
    {
        $churchdetails =  DB::table('profiles')->first();
        //  dd($hoteldetails);
        return view('admin/profile')->with(['churchdetails'=>$churchdetails]);
    }

    public function updateprofile(Request $request){

        if($request->hasFile('avatar')){
            $avatar=$request->file('avatar');
            $filename=time().'.'.$avatar->getClientOriginalExtension();


            $profile = Profile::findorfail($request->profileid);
            $profile->churchname=$request->hotelname;
            $profile->parentchurchname=$request->parentchurchname;
            $profile->smssenderid=$request->smssenderid;
            $profile->website=$request->website;
            $profile->address=$request->address;
            $profile->telephone=$request->telephone;
            $profile->phone=$request->phone;
            $profile->avatar=$filename;
            $profile->country=$request->country;
            $profile->city=$request->city;
            $profile->email=$request->email;
            $profile->pastorname=$request->pastorname;
            $profile->no_of_pastors=$request->no_of_pastors;
            $profile->district=$request->district;
            if($profile->update()){
                Image::make($avatar)->resize(300,300)->save( public_path('/uploads/avatars/'.$filename));
                $notification=array(
                    'message'=>"Church details has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('profile')->with($notification);
            }


        }else{
            $profile = Profile::findorfail($request->profileid);
            $profile->churchname=$request->hotelname;
            $profile->parentchurchname=$request->parentchurchname;
            $profile->smssenderid=$request->smssenderid;
            $profile->website=$request->website;
            $profile->address=$request->address;
            $profile->telephone=$request->telephone;
            $profile->phone=$request->phone;

            $profile->country=$request->country;
            $profile->city=$request->city;
            $profile->email=$request->email;
            if($profile->update()){

                $notification=array(
                    'message'=>"Church details has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('profile')->with($notification);
            }

        }

    }
}
