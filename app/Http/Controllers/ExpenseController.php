<?php

namespace App\Http\Controllers;

use App\Expense;
use Illuminate\Http\Request;
use App\Expenses;
use Image;
use DB;
use Datatables;
class ExpenseController extends Controller
{
    //
    public  function index(){
        return view('admin.expenses');
    }
    public function save(Request $request){

        
        $m = new Expense();

        $m->category= $request->category;
        $m->description=$request->description;
        $m->rec_date=$request->rec_date;
        $m->amount=$request->amount;
        if($m->save()){
            $notification=array(
                'message'=>"New record has been Succesfully Added ",
                'alert-type'=>'success'
            );

            return redirect('expenses')->with($notification);
        }
    }
    public function get_all_expenses(){
        $members = DB::table('expenses')->select(['id','category','description', 'amount',DB::raw('date(rec_date) as rec_date')]) ->orderBy('rec_date','asc');

        return Datatables::of($members)
            ->addColumn('action', function ($members) {

                return'<a href="" data-toggle="modal" data-target="#edit-Modal"  data-id="'.$members->id.'" data-category="'.$members->category.'"  data-description="'.$members->description.'"  data-amount="'.$members->amount.'"  data-rec_date="'.$members->rec_date.'" class="editbtn btn btn-warning" > <i class="fa fa-pencil-square-o" aria-hidden="true">&nbsp;</i></a>
                       <a href="" data-toggle="modal" data-target="#delete-Modal" data-id="'.$members->id.'" data-category="'.$members->category.'"  data-description="'.$members->description.'"  data-amount="'.$members->amount.'"   class="deletebtn btn btn-danger" ><i class="fa fa-trash" aria-hidden="true">&nbsp;</i></a>
                                             ';
            })

            ->make(true);
    }
   
    public function update(Request $request){
        try{

            $m = Expense::where('id', $request->id_edit)->firstOrFail();

            $m->category=$request->category_edit;
            $m->description=$request->description_edit;
            $m->amount=$request->amount_edit;
            $m->rec_date=$request->rec_date_edit;




            if($m->update()){

                $notification=array(
                    'message'=>" details has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('expenses')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"details  failed to be updated",
                'alert-type'=>'success'
            );
            return redirect('expenses')->with($notification);
        }
    }
    public function delete(Request $request){
//        dd($request->all());
        try{
            $m = Expense::where('id', $request->id_delete)->firstOrFail();
            if($m->delete()){
                $notification=array(
                    'message'=>"Record  has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('expenses')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error Occured",
                'alert-type'=>'error'
            );
            return redirect('expenses')->with($notification);
        }
    }
}
