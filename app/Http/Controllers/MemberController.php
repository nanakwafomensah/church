<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Member;
use Image;
use DB;
use Datatables;

class MemberController extends Controller
{
    //
    public function index(){
        $membergroups = DB::table('membergroups')->get();
        $cf = DB::table('convenantfamilies')->get();
        return view('admin.newmember', ['membergroups' => $membergroups,'cf' => $cf ]);
    }
    public function memberlist(){
        $membergroups = DB::table('membergroups')->get();
        $cf = DB::table('convenantfamilies')->get();
        return view('admin/allmember', ['membergroups' => $membergroups,'cf' => $cf]);
    }
    public function get_all_members(){
        $members = DB::table('members')->select(['members.member_id', DB::raw('CONCAT_WS(" ",members.firstname,members.lastname) as fullname'), 'members.occupation', 'members.telephone', 'members.marital_status','members.lastname','members.firstname','members.photo','members.nationality','members.dob','members.gender','members.convenant_family','members.firstfruit','members.hometown','groups','membershipstatus','firstfruit','couplename','membership_cat','level_edu','trC','passed_to_glory','teacher_qualification',DB::raw('date(registration_date) as registration_date')])->orderBy('registration_date','asc');

        return Datatables::of($members)
            ->addColumn('action', function ($members) {
                  $route="individualRpt/$members->member_id";
                $routechangestatus="changestatus/".$members->member_id."/".$members->membershipstatus;
                 return'<a href="" data-toggle="modal" data-target="#edit-Modal"  data-memberid="'.$members->member_id.'"  data-firstname="'.$members->firstname.'" data-lastname="'.$members->lastname.'" data-occupation="'.$members->occupation.'" data-telephone="'.$members->telephone.'" data-maritalstatus="'.$members->marital_status.'" data-nationality="'.$members->nationality.'"  data-dob="'.$members->dob.'"  data-photo="'.$members->photo.'"  data-gender="'.$members->gender.'" data-convenant="'.$members->convenant_family.'" data-firstfruit="'.$members->firstfruit.'" data-hometown="'.$members->hometown.'" data-couplename="'.$members->couplename.'" data-membershipstatus="'.$members->membershipstatus.'" data-membershipcat="'.$members->membership_cat.'"   data-passed_to_glory="'.$members->passed_to_glory.'" data-trC="'.$members->trC.'"  data-leveledu="'.$members->level_edu.'" data-teacher_qualification="'.$members->teacher_qualification.'" data-registration_date="'.$members->registration_date.'" class="editbtn btn btn-warning" > <i class="fa fa-pencil-square-o" aria-hidden="true">&nbsp;</i></a>
                                          <a href="" data-toggle="modal" data-target="#delete-Modal" data-memberid="'.$members->member_id.'"  data-firstname="'.$members->firstname.'"  data-lastname="'.$members->lastname.'" class="deletebtn btn btn-danger" ><i class="fa fa-trash" aria-hidden="true">&nbsp;</i></a>
                                          <a href="'.$route.'"  data-memberid="'.$members->member_id.'"  data-firstname="'.$members->firstname.'" class="btn btn-success" ><i class="fa fa-print" aria-hidden="true">&nbsp;</i></a>
                                          
                                ';
//                <a href="'.$routechangestatus.'"  data-memberid="'.$members->member_id.'"  data-firstname="'.$members->firstname.'" class="btn btn-info" ><i class="fa fa-undo" aria-hidden="true">&nbsp;</i></a>
            })

            ->addColumn('photo', function ($members) {
                  $image=$members->photo;
                return'<img src="uploads/avatars/'.$image.'" width="50px" height="50px" class="img-responsive text-center" >';
            })
            ->make(true);
    }
    public function addmember(Request $request){
        $churchdetails =  DB::table('profiles')->first();

        if($request->hasFile('avatar')){
            $avatar=$request->file('avatar');
            $filename=time().'.'.$avatar->getClientOriginalExtension();


            $member = new Member();
            $member->member_id=$request->member_id;
            $member->firstname=$request->firstname;
            $member->lastname=$request->lastname;
            $member->nationality=$request->nationality;
            $member->occupation=$request->occupation;
            $member->telephone=preg_replace('/^0/','233',$request->telephone);
            $member->gender=$request->gender;
            $member->dob=$request->dob;
            $member->couplename=$request->couplename;
            $member->trC=$request->trC;
            $member->passed_to_glory=$request->passed_to_glory;
            $member->marital_status=$request->marital_status;
            $member->hometown=$request->hometown;
            $member->firstfruit=$request->firstfruit;
            $member->membership_cat=$request->membership_cat;
            $member->registration_date=$request->registration_date;
            $member->convenant_family=$request->convenant_family;
            $member->level_edu=$request->level_edu;
            $member->teacher_qualification=$request->teacher_qualification;
            $member->groups=implode(",",$request->my_multi_select1);

            $member->photo=$filename;
            $member->membershipstatus=$request->membership_status;
            $member->fullname=$request->firstname." ".$request->lastname;

            if($member->save()){
                Image::make($avatar)->resize(300,300)->save( public_path('/uploads/avatars/'.$filename));

                $notification=array(
                    'message'=>"New member has been Succesfully Added",
                    'alert-type'=>'success'
                );
                $parentname=$churchdetails->parentchurchname;
                $city=$churchdetails->city;
                $name=$churchdetails->churchname;
                $sender=$churchdetails->smssenderid;
                $msisdn=preg_replace('/^0/','233',$request->telephone);
                $message="Dear $request->firstname $request->lastname  you have been successfully added as member of $parentname $name ,$city.Thank You.";
                $msg = urlencode($message);
                $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
                      if($request->membership_status=='member'){
                          file_get_contents($api);
                      }


                return redirect('allmember')->with($notification);
            }
        }else{
//            $avatar=$request->file('avatar');
//            $filename=time().'.'.$avatar->getClientOriginalExtension();


            $member = new Member();
            $member->member_id=$request->member_id;
            $member->firstname=$request->firstname;
            $member->lastname=$request->lastname;
            $member->nationality=$request->nationality;
            $member->occupation=$request->occupation;
            $member->telephone=preg_replace('/^0/','233',$request->telephone);
            $member->gender=$request->gender;
            $member->dob=$request->dob;
            $member->couplename=$request->couplename;
            $member->level_edu=$request->level_edu;
            $member->trC=$request->trC;
            $member->passed_to_glory=$request->passed_to_glory;
            $member->marital_status=$request->marital_status;
            $member->hometown=$request->hometown;
            $member->firstfruit=$request->firstfruit;
            $member->teacher_qualification=$request->teacher_qualification;
            $member->membership_cat=$request->membership_cat;
            $member->registration_date=$request->registration_date;
            $member->convenant_family=$request->convenant_family;
            $member->groups=implode(",",$request->my_multi_select1);

           // $member->photo=$filename;
            $member->membershipstatus=$request->membership_status;
            $member->fullname=$request->firstname." ".$request->lastname;

            if($member->save()){
              //  Image::make($avatar)->resize(300,300)->save( public_path('/uploads/avatars/'.$filename));

                $notification=array(
                    'message'=>"New member has been Succesfully Added",
                    'alert-type'=>'success'
                );

                $parentname=$churchdetails->parentchurchname;
                $city=$churchdetails->city;
                $name=$churchdetails->churchname;
                $sender=$churchdetails->smssenderid;
                $msisdn=preg_replace('/^0/','233',$request->telephone);
                $message="Dear $request->firstname $request->lastname  you have been successfully added as member of $parentname $name ,$city.Thank You.";
                $msg = urlencode($message);
                $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
                if($request->membership_status=='member'){
                    file_get_contents($api);
                }

                return redirect('allmember')->with($notification);
            }
        }

    }
    public  function deletemember(Request $request){
        try{
            $m = Member::where('member_id', $request->member_id_delete)->firstOrFail();
            if($m->delete()){
                $notification=array(
                    'message'=>"Member  has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('allmember')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error occured",
                'alert-type'=>'error'
            );
            return redirect('allmember')->with($notification);
        }
    }
    public function changestatus($memberid,$membershipstatus){
        $member =  Member::where('member_id', '=' ,$memberid)->firstOrFail();
        $churchdetails =  DB::table('profiles')->first();
        if($membershipstatus=='visitor'){
            $member->membershipstatus='member';

            /////////////////////
            $parentname=$churchdetails->parentchurchname;
            $city=$churchdetails->city;
            $name=$churchdetails->churchname;
            $sender=$churchdetails->smssenderid;
            $msisdn=$member->telephone;
            $message="Dear $member->firstname $member->lastname  you have been successfully added as member of $parentname $name ,$city.Thank You.";
            $msg = urlencode($message);
            $api = 'http://api.nalosolutions.com/bulksms/?username=naname&password=christ123!@&type=0&dlr=1&destination=' . $msisdn . '&source=' . $sender . '&message=' . $msg;
            @file_get_contents($api);

            $member->update();
            ///////////////////////
        }elseif ($membershipstatus=='member'){
            $member->membershipstatus='visitor';
            $member->update();
        }

        $notification=array(
            'message'=>"Member Status Changed",
            'alert-type'=>'success'
        );
        return redirect('allmember')->with($notification);
    }
    public function editmember(Request $request){
//        dd($request->all());

        try{
            if($request->hasFile('avatar')){
                $avatar=$request->file('avatar');
                $filename=time().'.'.$avatar->getClientOriginalExtension();
                $member =  Member::where('member_id', '=' ,$request->member_edit_id)->firstOrFail();
                $member->firstname=$request->firstname_edit;
            $member->lastname=$request->lastname_edit;
            $member->nationality=$request->nationality_edit;
            $member->occupation=$request->occupation_edit;
            $member->telephone=preg_replace('/^0/','233',$request->telephone_edit);
            $member->gender=$request->gender_edit;
            $member->dob=$request->dob_edit;
            $member->couplename=$request->couplename_edit;
            $member->trC=$request->trC_edit;
            $member->passed_to_glory=$request->passed_to_glory_edit;
            $member->teacher_qualification=$request->teacher_qualification_edit;
            $member->registration_date=$request->registration_date_edit;
            $member->level_edu=$request->level_edu_edit;

            $member->marital_status=$request->maritalstatus_edit;
            $member->hometown=$request->hometown_edit;
            $member->firstfruit=$request->firstfruit_edit;
            $member->membership_cat=$request->membership_cat_edit;
            $member->registration_date=$request->registration_date_edit;
            $member->convenant_family=$request->convenant_edit;
            $member->fullname=$request->firstname_edit." ".$request->lastname_edit;
            $member->photo=$filename;
            $member->membershipstatus=$request->membershipstatus_edit;

            $member->groups=implode(",",$request->my_multi_select1);
                if($member->update()){

                    $notification=array(
                        'message'=>"Member details has been Succesfully updated",
                        'alert-type'=>'success'
                    );
                    return redirect('allmember')->with($notification);

                }
            }else{
                $member =  Member::where('member_id', '=' ,$request->member_edit_id)->firstOrFail();
                     $member->firstname=$request->firstname_edit;
            $member->lastname=$request->lastname_edit;
            $member->nationality=$request->nationality_edit;
            $member->occupation=$request->occupation_edit;
            $member->telephone=preg_replace('/^0/','233',$request->telephone_edit);
            $member->gender=$request->gender_edit;
            $member->dob=$request->dob_edit;
            $member->couplename=$request->couplename_edit;
            $member->trC=$request->trC_edit;
            $member->passed_to_glory=$request->passed_to_glory_edit;
                $member->teacher_qualification=$request->teacher_qualification_edit;
                $member->registration_date=$request->registration_date_edit;
                $member->level_edu=$request->level_edu_edit;
            $member->marital_status=$request->maritalstatus_edit;
            $member->hometown=$request->hometown_edit;
            $member->firstfruit=$request->firstfruit_edit;
            $member->membership_cat=$request->membership_cat_edit;
            $member->registration_date=$request->registration_date_edit;
            $member->convenant_family=$request->convenant_edit;
            $member->fullname=$request->firstname_edit." ".$request->lastname_edit;
                $member->membershipstatus=$request->membershipstatus_edit;
                $member->groups=implode(",",$request->my_multi_select1);
                if($member->update()){

                    $notification=array(
                        'message'=>"Member details has been Succesfully updated",
                        'alert-type'=>'success'
                    );
                    return redirect('allmember')->with($notification);

                }
            }

        }catch(Exception $e){
            $notification=array(
                'message'=>"Member details  failed to updated",
                'alert-type'=>'success'
            );
            return redirect('allmember')->with($notification);
        }
    }
}
