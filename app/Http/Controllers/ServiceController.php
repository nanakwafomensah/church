<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use App\Member;
use Image;
use DB;
use Datatables;
class ServiceController extends Controller
{
    //
    public function index(){
        return view('admin/service');
    }
    public function get_all_services(){
        $members = DB::table('services')->select(['services.id','services.service_name', 'services.created_at', 'services.updated_at'])->orderBy('created_at','asc');

        return Datatables::of($members)
            ->addColumn('action', function ($members) {

                return'<a href="" data-toggle="modal" data-target="#edit-modal"  data-id="'.$members->id.'" data-name="'.$members->service_name.'"  class="editbtn btn btn-warning" > <i class="fa fa-pencil-square-o" aria-hidden="true">&nbsp;</i></a>
                                          <a href="" data-toggle="modal" data-target="#delete-modal" data-id="'.$members->id.'" data-name="'.$members->service_name.'"  class="deletebtn btn btn-danger" ><i class="fa fa-trash" aria-hidden="true">&nbsp;</i></a>';

            })
            ->make(true);
    }

    public function save(Request $request){
        try{
            $m = new Service();

            $m->service_name=$request->name;

            if($m->save()){
                $notification=array(
                    'message'=>"New Service group has been Succesfully Added",
                    'alert-type'=>'success'
                );

                return redirect('service')->with($notification);
            }
        }catch(Exception $e){
            $notification=array(
                'message'=>'A Error Occured',
                'alert-type'=>'error'
            );
            return redirect('service')->with($notification);
        }
    }
    public function update(Request $request){
        try{$m = Service::findorfail($request->id_edit);
            $m->service_name=$request->name_edit;


            if($m->update()){

                $notification=array(
                    'message'=>"Service Type details has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('service')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"Service  details  failed to updated",
                'alert-type'=>'success'
            );
            return redirect('service')->with($notification);
        }
    }
    public function delete(request $request){
        try{
            $m = Service::findorfail($request->id_delete);
            if($m->delete()){
                $notification=array(
                    'message'=>"Service has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('service')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error occured",
                'alert-type'=>'error'
            );
            return redirect('service')->with($notification);
        }
    }
}
